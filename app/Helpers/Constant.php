<?php
define('PAGINATE', 20);
define('IMAGENEWS', '/upload/news/');
define('IMAGEUSER', '/upload/user/');
define('IMAGE_PARTNER', '/upload/partner/');
define('IMAGE_COMPANY', '/upload/company/');
define('JOB_ATTACHMENT', '/upload/job/');
define('ONE', 1);
define('ZERO', 0);
define('FILE_ASSET', '/upload/asset/');
define('FILE_CONTRACT', '/upload/contract/');
define('APPROVE', 1);
define('STATUS_ONE', 1);
define('STATUS_ZERO', 0);
define('PRIORITIZE_ONE', 1);
define('PRIORITIZE_ZERO', 0);

//active user
define('USER_ACTIVE', 1);

define('NO_AVATAR', '/no-avatar.ico');

define('ADMIN_ID', '1');
define('UNIT_MEASUREMENT', ['m', 'mét', 'met', 'm2']);