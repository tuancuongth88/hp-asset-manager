<?php
function safestrtotime($s) {
    $basetime = 0;
    if (preg_match("/19(\d\d)/", $s, $m) && ($m[1] < 70)) {
        $s        = preg_replace("/19\d\d/", 1900 + $m[1] + 68, $s);
        $basetime = 0x80000000 + 1570448;
    }
    return $basetime + strtotime($s);
}

function stringToDate($var) {
    if (!empty($var)) {
        return implode("-", array_reverse(explode("/", $var)));
    } else {
        return $var;
    }

}

function dateToString($date = '') {
    if ($date != '' AND $date != '0000-00-00') {
        return date("d/m/Y", safestrtotime($date));
    } else {
        return $date;
    }
}

function dateToStringY($date = '') {
    if ($date != '' AND $date != '0000-00-00') {
        return date("d/m/y", safestrtotime($date));
    } else {
        return $date;
    }
}

function dateToTime($var) {
    if (!empty($var)) {
        return safestrtotime(implode("-", array_reverse(explode("/", $var))));
    } else {
        return $var;
    }
}

function dateTimeToTime($var) {
    if (!empty($var)) {
        $dateTime = explode('-', str_replace(' ', '', $var));
        return safestrtotime(implode("-", array_reverse(explode("/", $dateTime[0]))) . ' ' . $dateTime[1]);
    } else {
        return $var;
    }
}

function timeToDate($date = '') {
    if ($date != '') {
        return date("d/m/Y", $date);
    } else {
        return $date;
    }
}

function timeToDateTime($date = '') {
    if ($date != '') {
        return date("d/m/Y - H:i", $date);
    } else {
        return $date;
    }
}

function validateDate($date, $format = 'Y-m-d H:i:s') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function exportWord($text, $font, $size, $bold) {
    $phpWord = new \PhpOffice\PhpWord\PhpWord();
    $section = $phpWord->addSection();
//
    //    $section->addText($text);
    //
    //    $section->addText('Hello world! I am formatted.',
    //        array('name'=>'Tahoma', 'size'=>16, 'bold'=>true));

//    $phpWord->addFontStyle('myOwnStyle',
    //        array('name'=>'Verdana', 'size'=>14, 'color'=>'1B2232'));
    //    $section->addText('Hello world! I am formatted by a user defined style',
    //        'myOwnStyle');

    $fontStyle = new \PhpOffice\PhpWord\Style\Font();
    $fontStyle->setBold($bold);
    $fontStyle->setName($font);
    $fontStyle->setSize($size);
    $myTextElement = $section->addText($text);
    $myTextElement->setFontStyle($fontStyle);

    $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save('helloWorld.docx');
}

function validatePhone($phone) {
    if(!empty($phone)) {
        $pattern1 = '#^01[0-9]{2}[0-9]{7}$#';
        $pattern2 = '#^09[0-9]{1}[0-9]{7}$#';
        $pattern3 = '#^0[2-8][0-9]{8}$#';
        if(!preg_match($pattern1, $phone, $match) && !preg_match($pattern2, $phone, $match) && !preg_match($pattern3, $phone, $match)){
            return false;
        }
        return true;
    }
    return false;
}

function VndText($amount) {
    if ($amount <= 0) {
        return $textnumber = "Tiền phải là số nguyên dương lớn hơn số 0";
    }
    $Text        = array("không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín");
    $TextLuythua = array("", "nghìn", "triệu", "tỷ", "ngàn tỷ", "triệu tỷ", "tỷ tỷ");
    $textnumber  = "";
    $length      = strlen($amount);

    for ($i = 0; $i < $length; $i++) {
        $unread[$i] = 0;
    }

    for ($i = 0; $i < $length; $i++) {
        $so = substr($amount, $length - $i - 1, 1);

        if (($so == 0) && ($i % 3 == 0) && ($unread[$i] == 0)) {
            for ($j = $i + 1; $j < $length; $j++) {
                $so1 = substr($amount, $length - $j - 1, 1);
                if ($so1 != 0) {
                    break;
                }

            }

            if (intval(($j - $i) / 3) > 0) {
                for ($k = $i; $k < intval(($j - $i) / 3) * 3 + $i; $k++) {
                    $unread[$k] = 1;
                }

            }
        }
    }

    for ($i = 0; $i < $length; $i++) {
        $so = substr($amount, $length - $i - 1, 1);
        if ($unread[$i] == 1) {
            continue;
        }

        if (($i % 3 == 0) && ($i > 0)) {
            $textnumber = $TextLuythua[$i / 3] . " " . $textnumber;
        }

        if ($i % 3 == 2) {
            $textnumber = 'trăm ' . $textnumber;
        }

        if ($i % 3 == 1) {
            $textnumber = 'mươi ' . $textnumber;
        }

        $textnumber = $Text[$so] . " " . $textnumber;
    }

    //Phai de cac ham replace theo dung thu tu nhu the nay
    $textnumber = str_replace("không mươi", "lẻ", $textnumber);
    $textnumber = str_replace("lẻ không", "", $textnumber);
    $textnumber = str_replace("mươi không", "mươi", $textnumber);
    $textnumber = str_replace("một mươi", "mười", $textnumber);
    $textnumber = str_replace("mươi năm", "mươi lăm", $textnumber);
    $textnumber = str_replace("mươi một", "mươi mốt", $textnumber);
    $textnumber = str_replace("mười năm", "mười lăm", $textnumber);

    return ucfirst($textnumber . "đồng chẵn");
}

function uploadFile($file, $part) {
    $filename        = time() . '_' . str_replace(' ', '_', $file->getClientOriginalName());
    $destinationPath = public_path() . $part;
    $uploadSuccess   = $file->move($destinationPath, $filename);
    if ($uploadSuccess) {
        return $part . $filename;
    }
    return false;
}

/**
 * Remove the specified resource from storage.
 *
 * @param  class $model
 * @param  array $arrRelations
 * @param  int $id
 *
 * @return bool
 */
function deleteRelation($model, $arrRelations, $id) {
    $obj = $model::find($id);
    if ($arrRelations) {
        DB::beginTransaction();
        foreach ($arrRelations as $relation) {
            if (!$obj->$relation()->delete()) {
                DB::rollBack();
                return false;
            }
        }
    }
    if (!$obj->delete()) {
        return false;
    }
    DB::commit();
    return true;
}

function formatSizeUnits($bytes) {
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}

function getAllPermissionFacebook() {
    return [
        "user_birthday",
        "user_hometown",
        "user_location",
        "user_likes",
        "user_events",
        "user_photos",
        "user_videos",
        "user_friends",
        "user_status",
        "user_tagged_places",
        "user_posts",
        "user_gender",
        "user_link",
        "user_age_range",
        "email",
        "user_managed_groups",
        "manage_pages",
        "pages_manage_cta",
        "pages_manage_instant_articles",
        "pages_show_list",
        "publish_pages",
        "read_page_mailboxes",
        "ads_management",
        "ads_read",
        "business_management",
        "pages_messaging",
        "pages_messaging_phone_number",
        "pages_messaging_subscriptions",
        "publish_to_groups",
        "groups_access_member_info",
        "public_profile",
    ];
}

function filterAttachmentFree($value) {
    if (!isset($value->project_id)) {
        return $value;
    }
    return null;
}
function hiddenText($value, $status = null) {
    $length = strlen($value);
    if ($length > 6) {
        return mb_substr($value, 0, 3) . "*****" . mb_substr($value, -3);
    }
    return $value;
}

function rateByPoint(int $point) {
    if ($point > 5 && $point < 8) {
        return 'Khá';
    }
    if ($point < 2) {
        return 'Kém';
    }
    if ($point > 8) {
        return 'Tốt';
    }
    return 'Trung bình';
}

function getClassByRatePoint(int $point) {
    if ($point > 5 && $point < 8) {
        return 'danger';
    }
    if ($point < 2) {
        return 'metal';
    }
    if ($point > 8) {
        return 'success';
    }
    return 'warning';
}

function getOptionByModel($modelName, $id, $name)
{
    return $modelName::pluck($name, $id)->toArray();
}
function getObject($ob, $method, $default = null){
    if( !$ob ){
        return $default;
    }
    if( !$ob->$method ){
        return $default;
    }
    return $ob->$method;
}

function convertDate($format, $date)
{
    $date = strtotime($date);
    return date($format, $date);
}

function trim_all($str, $what = NULL, $with = ' ')
{
    if ($what === NULL) {
        //  Character      Decimal      Use
        //  "\0"            0           Null Character
        //  "\t"            9           Tab
        //  "\n"           10           New line
        //  "\x0B"         11           Vertical Tab
        //  "\r"           13           New Line in Mac
        //  " "            32           Space

        $what = PHP_EOL;    //all white-spaces and control chars
    }

    $string = trim(preg_replace("/[" . $what . "]+/", $with, $str), $what);
    return trim(preg_replace('/\s\s+/', ' ', $string));
}