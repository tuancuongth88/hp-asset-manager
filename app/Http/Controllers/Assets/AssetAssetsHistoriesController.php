<?php

namespace App\Http\Controllers\Assets;


use App\Http\Controllers\Controller;
use App\Http\Requests\Assets\AssetAssetsHistoryCreateRequest;
use App\Http\Requests\Assets\AssetAssetsHistoryUpdateRequest;
use App\Models\Assets\Asset;
use App\Models\Assets\AssetAssetsHistory;
use App\Models\Contracts\Contract;
use App\Models\Partners\Group;
use App\Models\Partners\Partner;
use App\Models\Systems\Company;
use App\Models\Users\User;
use App\Repositories\Assets\AssetAssetsHistoryRepository;
use App\Validators\Assets\AssetAssetsHistoryValidator;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Http\Request;

/**
 * Class AssetAssetsHistoriesController.
 *
 * @package namespace App\Http\Controllers;
 */
class AssetAssetsHistoriesController extends Controller
{
    /**
     * @var AssetAssetsHistoryRepository
     */
    protected $repository;

    /**
     * @var AssetAssetsHistoryValidator
     */
    protected $validator;

    private $partView;

    /**
     * AssetAssetsHistoriesController constructor.
     *
     * @param AssetAssetsHistoryRepository $repository
     * @param AssetAssetsHistoryValidator $validator
     */
    public function __construct(
        AssetAssetsHistoryRepository $repository,
        AssetAssetsHistoryValidator $validator
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->partView = 'administrator.assets.repair';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $listPartner = Partner::all()->pluck('name', 'id')->toArray();
        $listAssetGroup = Group::all()->pluck('name', 'id')->toArray();
        $data = $this->repository->paginate();

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->partView . '.index', compact('data', 'listPartner', 'listAssetGroup'));
    }


    public function create()
    {

        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $listCompany = Company::all()->pluck('name', 'id')->toArray();
        $listAssetGroup = Group::all()->pluck('name', 'id')->toArray();

        return view($this->partView . '.create', compact('listCompany', 'listAssetGroup'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  AssetAssetsHistoryCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(AssetAssetsHistoryCreateRequest $request)
    {
        try {
//            $input = $request->all();
//            $modelUser = new User();
            $repair_detail = $request->get('repair_content', []);
            //validate
            $listRule = [
                'company_owner' => 'required|numeric',
                'structure_owner' => 'required|numeric',
//                'type' => 'required|numeric',
//                'group_id' => 'required|numeric',
//                'number_repair' => 'required|numeric',
                'people_notice' => 'required',
            ];
            $fieldVN = [
                'company_owner' => 'công ty',
                'structure_owner' => 'đơn vị',
                'people_notice' => 'người thông báo',
//                'group_id' => 'nhóm tài sản',
//                'number_repair' => 'số lượng',
//                'history_type' => 'loại sửa chữa',
            ];

            $checkValidation = $this->validateInput($request->all(), $listRule, $fieldVN);
            if ($checkValidation) {
                return redirect()->back()->withErrors($checkValidation->getMessageBag())->withInput();
            }
            $company_owner = $request->get('company_owner', 0);
            $structure_owner = $request->get('structure_owner', 0);
            $people_notice = $request->get('people_notice', '');
            if ($repair_detail) {
                foreach ($repair_detail as $key_d => $value) {
                    $mAssetCount = new Asset();
                    $asset_id = isset($value['asset_id']) ? $value['asset_id'] : null;
                    $name = isset($value['name']) ? $value['name'] : '';
                    $group_id = isset($value['group_id']) ? $value['group_id'] : 0;
                    $type = isset($value['type']) ? $value['type'] : 0;
                    $status = isset($value['status']) ? $value['status'] : 0;
                    $price = isset($value['price']) ? $value['price'] : null;
                    $quantity = isset($value['quantity']) ? $value['quantity'] : 0;
                    $note = isset($value['note']) ? $value['note'] : '';
                    $unit_id = isset($value['unit_id']) ? $value['unit_id'] : 0;
                    $start_time = isset($value['start_time']) ? $value['start_time'] : null;
                    $end_time = isset($value['end_time']) ? $value['end_time'] : null;
                    $price_repair = isset($value['price_repair']) ? $value['price_repair'] : 0;
                    $history_type = isset($value['history_type']) ? $value['history_type'] : 0;
                    //kiểm tra tài sản trong kho
                    if (!$asset_id) {
                        if (trim($name) !== '') {
                            $mAssetCount = $mAssetCount->where(Asset::NAME, 'like', '%' . $name . '%');
                        }
                        if ($group_id) {
                            $mAssetCount = $mAssetCount->where(Asset::GROUP_ID, $group_id);
                        }
                        if ($type) {
                            $mAssetCount = $mAssetCount->where(Asset::TYPE, $type);
                        }
                        if ($status) {
                            $mAssetCount = $mAssetCount->where(Asset::STATUS, $status);
                        }
                        if ($structure_owner) {
                            $mAssetCount = $mAssetCount->where(Asset::STRUCTURE_OWNER, $structure_owner);
                        }
                        if (!$price) {
                            $mAssetCount = $mAssetCount->where(function ($query) use ($price) {
                                $query->where(Asset::PRICE, $price);
                                $query->orWhereNull(Asset::PRICE);
                            });
                        } else {
                            $mAssetCount = $mAssetCount->where(Asset::PRICE, $price);
                        }
                    } else {
                        $mAssetCount = $mAssetCount->where('id', $asset_id);
                    }

                    $mAssetCount = $mAsset = $mAssetCount;
                    $countAsset = $mAssetCount->count();
                    $listFail = [];
                    if (!$quantity || (int)$quantity < 1) {
                        $listFail[] = 'Số lượng không được trống và lớn hơn 0';
                    }
                    if ($countAsset < $quantity) {
                        $listFail[] = 'Số lượng sửa chữa không được lớn hơn tổng số tài sản đã chọn';
                    }
                    if (!$countAsset) {
                        $listFail[] = 'Không có tài sản nào thỏa mãn điều kiện lọc';
                    }

                    if (count($listFail) > 0) {
                        return redirect()->back()->with('error', trans('Lỗi thêm mới sửa chữa'))
                            ->with('list-fail', $listFail);
                    }

                    $listAsset = $mAsset->take($quantity)->get();
                    $history_status = 1;
                    if ($history_type == 2) {
                        $history_status = 2;
                    }

                    foreach ($listAsset as $key_asset => $val_asset) {
                        $data_history = [
                            AssetAssetsHistory::COMPANY_OWNER => $company_owner,
                            AssetAssetsHistory::STRUCTURE_OWNER => $structure_owner,
                            AssetAssetsHistory::STATUS => $history_status,
                            AssetAssetsHistory::TYPE => $history_type,
                            AssetAssetsHistory::START_TIME => Carbon::createFromFormat('Y-m-d', $start_time),
                            AssetAssetsHistory::END_TIME => Carbon::createFromFormat('Y-m-d', $end_time),
                            AssetAssetsHistory::CREATED_BY => Auth::user()->id,
                            AssetAssetsHistory::ASSET_ID => $val_asset->id,
                            AssetAssetsHistory::PEOPLE_NOTICE => $people_notice,
                            AssetAssetsHistory::NOTE => $note,
                            AssetAssetsHistory::PRICE_REPAIR => $price_repair,
                        ];

                        $this->repository->create($data_history);

                        //Chú ý, chưa check trường hợp phân quyền
                        if ($history_status == 0) {
                            Asset::where('id', $val_asset->id)->where('status', Asset::STATUS_USE)
                                ->update(array(Asset::STATUS => Asset::STATUS_WARRANTY));
                        } else {
                            Asset::where('id', $val_asset->id)->where('status', Asset::STATUS_USE)
                                ->update(array(Asset::STATUS => Asset::STATUS_REPAIR));
                        }
                    }
                }
            }
            return redirect()->route('repair.index')->with('message', trans('Thêm mới tài sản sửa chữa bảo hành thành công'));

        } catch (ValidatorException $e) {
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assetAssetsHistory = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $assetAssetsHistory,
            ]);
        }

        return view('assetAssetsHistories.show', compact('assetAssetsHistory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assetAssetsHistory = $this->repository->find($id);

        return view('assetAssetsHistories.edit', compact('assetAssetsHistory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AssetAssetsHistoryUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AssetAssetsHistoryUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $assetAssetsHistory = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data' => $assetAssetsHistory->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function countAsset(Request $request)
    {
        $input = $request->all();

        $type = $input['type'];
        $group_id = $input['group_id'];
        $name = $input['name'];
        $owner = $input['owner'];

        if (!$owner) {
            return response()->json([
                'data' => 0,
            ]);
        }

        $modelAsset = new Asset();
        //kiểm tra tài sản trong kho
        $modelAsset = $modelAsset->where('owner', $owner)->where('status', Asset::STATUS_USE);
        $modelAsset = $modelAsset->where('type', $type)->where('group_id', $group_id);
        if (trim($name) != '') {
            $modelAsset = $modelAsset->where('name', 'like', '%' . $input['name'] . '%');
        }
        $countAsset = $modelAsset->count();

        return response()->json([
            'data' => $countAsset,
        ]);
    }

    public function validateInput($input, $listRule, $customAtr)
    {
        $checkValidate = Validator::make($input, $listRule, [], $customAtr);
        if ($checkValidate->fails()) {
            $errors = $checkValidate->errors();
            return $errors;
        } else {
            return false;
        }
    }

    public function updateStatus(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];
        $status = $input['status'];

        $objHistory = AssetAssetsHistory::where('id', $id)->first();

        AssetAssetsHistory::where('id', $id)->update(array(AssetAssetsHistory::STATUS => $status));

        if ($status == AssetAssetsHistory::STATUS_FINISH) {
            Asset::where('id', $objHistory->asset_id)
                ->update(array(Asset::STATUS => Asset::STATUS_USE));
        }

        if ($status == AssetAssetsHistory::STATUS_NOT_FIXING) {
            Asset::where('id', $objHistory->asset_id)
                ->update(array(Asset::STATUS => Asset::STATUS_BREAKDOWN));
        }

        return response()->json([
            'data' => true,
        ]);
    }


}
