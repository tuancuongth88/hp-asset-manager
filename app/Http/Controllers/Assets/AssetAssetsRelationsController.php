<?php

namespace App\Http\Controllers\Assets;

use App\Http\Controllers\Controller;
use App\Http\Requests\Assets\AssetAssetsRelationCreateRequest;
use App\Http\Requests\Assets\AssetAssetsRelationUpdateRequest;
use App\Repositories\Assets\AssetAssetsRelationRepository;
use App\Validators\Assets\AssetAssetsRelationValidator;

use App\Http\Requests;
use Illuminate\Http\Response;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
/**
 * Class AssetAssetsRelationsController.
 *
 * @package namespace App\Http\Controllers;
 */
class AssetAssetsRelationsController extends Controller
{
    /**
     * @var AssetAssetsRelationRepository
     */
    protected $repository;

    /**
     * @var AssetAssetsRelationValidator
     */
    protected $validator;

    /**
     * AssetAssetsRelationsController constructor.
     *
     * @param AssetAssetsRelationRepository $repository
     * @param AssetAssetsRelationValidator $validator
     */
    public function __construct(AssetAssetsRelationRepository $repository, AssetAssetsRelationValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $assetAssetsRelations = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $assetAssetsRelations,
            ]);
        }

        return view('assetAssetsRelations.index', compact('assetAssetsRelations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AssetAssetsRelationCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(AssetAssetsRelationCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $assetAssetsRelation = $this->repository->create($request->all());

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $assetAssetsRelation->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assetAssetsRelation = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $assetAssetsRelation,
            ]);
        }

        return view('assetAssetsRelations.show', compact('assetAssetsRelation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assetAssetsRelation = $this->repository->find($id);

        return view('assetAssetsRelations.edit', compact('assetAssetsRelation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AssetAssetsRelationUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AssetAssetsRelationUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $assetAssetsRelation = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $assetAssetsRelation->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }
}
