<?php

namespace App\Http\Controllers\Assets;

use App\Http\Controllers\Controller;
use App\Models\Assets\Asset;
use App\Models\Assets\Unit;
use App\Models\Partners\Group;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\Assets\AssetContractShowCreateRequest;
use App\Http\Requests\Assets\AssetContractShowUpdateRequest;
use App\Repositories\Assets\AssetContractShowRepository;
use App\Validators\Assets\AssetContractShowValidator;

/**
 * Class AssetContractShowsController.
 *
 * @package namespace App\Http\Controllers;
 */
class AssetContractShowsController extends Controller
{
    /**
     * @var AssetContractShowRepository
     */
    protected $repository;

    /**
     * @var AssetContractShowValidator
     */
    protected $validator;

    protected $partView;

    protected $asset;

    /**
     * AssetContractShowsController constructor.
     *
     * @param AssetContractShowRepository $repository
     * @param AssetContractShowValidator $validator
     */
    public function __construct(AssetContractShowRepository $repository, AssetContractShowValidator $validator,  Asset $asset)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->asset = $asset;
        $this->partView = 'administrator.assets.shows';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $assetContractShows = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $assetContractShows,
            ]);
        }

        return view('assetContractShows.index', compact('assetContractShows'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AssetContractShowCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(AssetContractShowCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $request['created_by'] = Auth::user()->id;

            $assetShowsParent = $this->repository->create($request->all());

            if (isset($request->director)) {
                foreach ($request->director as $value) {
                    $listRule = [
                        'name_asset' => 'required',
                        'group_id' => 'required|numeric',
                        'status' => 'required',
                        'type' => 'required',
                    ];
                    $fieldVN = [
                        'name_asset' => 'tên tài sản',
                        'group_id' => 'nhóm tài sản',
                        'status' => 'tình trạng sử dụng',
                        'type' => 'phân loại tài sản',
                    ];

                    $checkValidation = $this->validateInput($value, $listRule, $fieldVN);
                    if ($checkValidation) {
                        return redirect()->back()->withErrors($checkValidation->getMessageBag())->withInput();
                    }
                    $assets = new Asset();
                    $listImport = [];
                    if (isset($request['structure_id'])) {
                        $listImport['structure_id'] = $request['structure_id'];
                        $listImport['structure_owner'] = $request['structure_id'];
                    }
                    if (isset($request['company_id'])) {
                        $listImport['company_owner'] = $request['company_id'];
                    }
                    if (isset($value['receiver_handover'])) {
                        $listImport['receiver_handover'] = strip_tags($value['receiver_handover']);
                    }
                    if (isset($value['name_asset'])) {
                        $listImport['name'] = strip_tags($value['name_asset']);
                    }
                    if (isset($value['group_id'])) {
                        $listImport['group_id'] = $value['group_id'];
                    }
                    if (isset($value['material'])) {
                        $listImport['material'] = strip_tags($value['material']);
                    }
                    if (isset($value['color'])) {
                        $listImport['color'] = strip_tags($value['color']);
                    }
                    if (isset($value['warranty'])) {
                        $listImport['warranty'] = strip_tags($value['warranty']);
                    }
                    if (isset($value['quantity'])) {
                        $listImport['quantity'] = strip_tags($value['quantity']);
                    }
                    if (isset($value['unit_id'])) {
                        $listImport['unit_id'] = $value['unit_id'];
                    }
                    if (isset($value['producer'])) {
                        $listImport['producer'] = strip_tags($value['producer']);
                    }
                    if (isset($value['year_use'])) {
                        $listImport['year_use'] = strip_tags($value['year_use']);
                    }
                    if (isset($value['date_import'])) {
                        $listImport['date_import'] = strip_tags($value['date_import']);
                    }
                    if (isset($value['status'])) {
                        $listImport['status'] = $value['status'];
                    }
                    if (isset($value['type'])) {
                        $listImport['type'] = $value['type'];
                    }
                    if (isset($value['description'])) {
                        $listImport['description'] = $value['description'];
                    }
                    if (isset($value['vat']) && filter_var($value['vat'], FILTER_SANITIZE_NUMBER_INT)) {
                        $vat = $value['vat'];
                    } else {
                        $vat = NULL;
                    }
                    if (isset($value['price']) && filter_var($value['price'], FILTER_SANITIZE_NUMBER_INT)) {
                        $price = strip_tags($value['price']);
                    } else {
                        $price = NULL;
                    }
                    $listImport['vat'] = $vat;
                    $listImport['price'] = $price;
                    $listImport['created_by'] = Auth::user()->id;

                    $assetShowsParent->update($listImport);
                    $number_asset = $value['quantity'];
                    $unit_special = ['m2', 'mét'];
                    $unit_lower = '';
                    $unit_name = Unit::where('id', $value['unit_id'])->first();
                    if ($unit_name) {
                        $unit_lower = mb_strtolower(trim_all($unit_name->name), 'UTF-8');
                    }
                    if (in_array($unit_lower, $unit_special)) {
                        $assets->create($listImport);
                    } else {
                        for ($i = 1; $i <= $number_asset; $i++) {
                            $listImport['quantity'] = 1;
                            $objAssset = $assets->create($listImport);

                            // Create asset_code
                            $groupCode = Group::where('id', $value['group_id'])->first();
                            if ($groupCode) {
                                $asset_code = $groupCode->group_code . '-' . $objAssset->id;
                            }
                            $assets->where('id', $objAssset->id)->update(['asset_code' => $asset_code]);
                        }
                    }
                }
            }

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $assetShowsParent->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assetContractShow = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $assetContractShow,
            ]);
        }

        return view('assetContractShows.show', compact('assetContractShow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assetContractShow = $this->repository->find($id);

        return view('assetContractShows.edit', compact('assetContractShow'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AssetContractShowUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AssetContractShowUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $assetContractShow = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'AssetContractShow updated.',
                'data'    => $assetContractShow->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'AssetContractShow deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'AssetContractShow deleted.');
    }
}
