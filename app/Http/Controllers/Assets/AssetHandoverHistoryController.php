<?php

namespace App\Http\Controllers\Assets;

use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Models\Assets\AssetHandover;
use App\Models\Assets\AssetLog;
use App\Models\Assets\Unit;
use App\Models\Systems\StructureCompany;


/**
 * Class AssetLogsController.
 *
 * @package namespace App\Http\Controllers;
 */
class AssetHandoverHistoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $mAssetLog = new AssetLog();
        $data = [];
        $data = $mAssetLog->where('asset_id', $id)->first();
//        $data = $mAssetLog->all();
//        foreach ($data as $key => $value) {
//
//            if (sizeof($value->params) ==1){
//                continue;
//            }
//            $obj_params = new \stdClass();
//            $obj_asset_log = new \stdClass();
//
//            foreach ($value->params as $key_p => $param_p) {
//                $assetHanover = AssetHandover::where('id', data_get($param_p, 'asset_handover_id'))->first();
//                $string1 = $key_p;
//                $params_asset_log = new \stdClass();
//
//                $params_asset_log->person_name = (string)data_get($param_p, 'person_name');
//                $params_asset_log->asset_handover_id = (string)data_get($param_p, 'asset_handover_id');
//                $params_asset_log->person_structure_id = (string)data_get($param_p, 'person_structure_id');
//                $params_asset_log->receiver_name = (string)data_get($assetHanover, 'receiver_name');
//                $params_asset_log->receiver_structure_id = (string)data_get($param_p, 'receiver_structure_id');
//                $params_asset_log->person_start_time = (string)data_get($param_p, 'person_start_time');
//                $params_asset_log->person_end_time = (string)data_get($param_p, 'person_end_time');
//                $params_asset_log->unit_id = (string)data_get($param_p, 'unit_id');
//                $params_asset_log->quantity = (string)data_get($param_p, 'quantity');
//
//
//                //update param
//                $obj_asset_log->$string1 = $params_asset_log;
//                $obj_params = $obj_asset_log;
//            }
//
//            AssetLog::where('id', $value->id)
//                ->update(
//                    array(
//                        AssetLog::PARAMS => json_encode($obj_params),
//                    ));
//        }


        $arrStructureCompany = StructureCompany::all()->pluck('name', 'id')->toArray();
        $arrUnit = Unit::all()->pluck('name', 'id')->toArray();

        if (!$data) {
            return redirect()->back()->with('error', trans('Tài sản không có lịch sử bàn giao nào'));
        }
        return view('administrator.assets.history.index', compact('data', 'arrStructureCompany', 'arrUnit'));
    }

}
