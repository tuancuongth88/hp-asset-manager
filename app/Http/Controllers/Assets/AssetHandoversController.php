<?php

namespace App\Http\Controllers\Assets;

use App\Http\Controllers\Controller;
use App\Http\Requests\Assets\AssetHandoverCreateRequest;
use App\Http\Requests\Assets\AssetHandoverUpdateRequest;
use App\Models\Assets\Asset;
use App\Models\Assets\AssetHandover;
use App\Models\Assets\AssetHandoverDetail;
use App\Models\Assets\AssetLog;
use App\Models\Assets\Unit;
use App\Models\Partners\Group;
use App\Models\Systems\Company;
use App\Models\Systems\StructureCompany;
use App\Models\Users\User;
use App\Repositories\Assets\AssetHandoverRepository;
use App\Validators\Assets\AssetHandoverValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class AssetHandoversController.
 *
 * @package namespace App\Http\Controllers;
 */
class AssetHandoversController extends Controller
{
    /**
     * @var AssetHandoverRepository
     */
    protected $repository;

    /**
     * @var AssetHandoverValidator
     */
    protected $validator;

    private $partView;

    /**
     * AssetHandoversController constructor.
     *
     * @param AssetHandoverRepository $repository
     * @param AssetHandoverValidator $validator
     */
    public function __construct(
        AssetHandoverRepository $repository,
        AssetHandoverValidator $validator
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->partView = 'administrator.assets.handover';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!Auth::user()->can('handover.view')) {
            abort(506, trans('messages.you_do_not_have_permission'));
        }

        if (!Auth::user()->isRoleAdmin() && !Auth::user()->isRoleAdminHcns() && !Auth::user()->isRoleQuanLyBanGiao()) {
            abort(403, trans('messages.you_do_not_have_permission'));
        }

        $input = $request->all();

        $structure_owner = isset($input['structure_owner']) ? $input['structure_owner'] : null;
        $status = isset($input['q_status']) ? $input['q_status'] : null;
        $q_handover_id = isset($input['q_handover_id']) ? $input['q_handover_id'] : null;

        $filter = $filter_handover = 1;
        if (isset($input['filter_handover'])) {
            $filter = $filter_handover = $input['filter_handover'];
        }

        $mAssetHandover = new AssetHandover();

        if ($q_handover_id) {
            $mAssetHandover = $mAssetHandover->where('id', $q_handover_id);
        }

        switch ($filter) {
            case 1:
                if (isset($structure_owner)) {
                    $mAssetHandover = $mAssetHandover->where('person_structure_id', $structure_owner);
                }
                break;
            case 2:
                if (isset($structure_owner)) {
                    $mAssetHandover = $mAssetHandover->where('receiver_structure_id', $structure_owner);
                }
                break;
            default:
                if (isset($structure_owner)) {
                    $mAssetHandover = $mAssetHandover->where(function ($query) use ($structure_owner) {
                        $query->where('person_structure_id', $structure_owner);
                        $query->orWhere('receiver_structure_id', $structure_owner);
                    });
                }
                break;
        }

        if ($status) {
            $mAssetHandover = $mAssetHandover->where('status', $status);
        }

        if (Auth::user()->isRoleQuanLyBanGiao()) {
            $mAssetHandover = $mAssetHandover->where('created_by', Auth::user()->id);
        }

        $mAssetHandover = $mAssetHandover->orderBy('created_at', 'desc');
        $data = $mAssetHandover->paginate();

        $handoverStatus = AssetHandover::$assetHandoverStatus;
        $assetHandoverType = AssetHandover::$assetHandoverType;

        $listCompany = Company::first();

        $list_structure = [];
        $structure = StructureCompany::getHierarchy($listCompany->id);
        if ($structure) {
            $list_structure = $structure;
        }

        return view($this->partView . '.index', compact('data', 'filter_handover', 'handoverStatus', 'list_structure', 'assetHandoverType'));
    }


    public function create(Request $request)
    {
//        if (Gate::denies('handover.create')) {
//            abort(505, trans('messages.you_do_not_have_permission'));
//        }

        if (!Auth::user()->isRoleAdmin() && !Auth::user()->isRoleAdminHcns() && !Auth::user()->isRoleQuanLyBanGiao()) {
            abort(403, trans('messages.you_do_not_have_permission'));
        }

        $input = $request->all();
        $type_handover = isset($input['type_handover']) ? $input['type_handover'] : 2;
        if (!in_array($type_handover, [1, 2, 3])) {
            $type_handover = 2;
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $listCompany = Company::all()->pluck('name', 'id')->toArray();
        $listAssetGroup = Group::all()->pluck('group_code', 'id')->toArray();

        return view($this->partView . '.create', compact('listCompany', 'listAssetGroup', 'type_handover'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AssetHandoverCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(AssetHandoverCreateRequest $request)
    {
        ini_set('max_execution_time', '1000');
        ini_set('memory_limit', '-1');

        if (!Auth::user()->isRoleAdmin() && !Auth::user()->isRoleAdminHcns() && !Auth::user()->isRoleQuanLyBanGiao()) {
            abort(403, trans('messages.you_do_not_have_permission'));
        }

        try {
            //$input = $request->all();
            $modelUser = new User();
            $is_delegate_handover = $request->get('is_delegate_handover', null);
            $handover_detail = $request->get('handover_content', []);
            //validate
            $listRule = [
                'person_company_id' => 'required|numeric',
                'person_structure_id' => 'required|numeric',
                'person_name' => 'required',
                'person_positions' => 'required',
                'receiver_company_id' => 'required|numeric',
                'receiver_structure_id' => 'required|numeric',
                'receiver_name' => 'required',
                'receiver_positions' => 'required',
                'person_description' => 'required',
                'delegate_company_id' => 'required|numeric',
                'delegate_structure_id' => 'required|numeric',
                'delegate_name' => 'required',
                'delegate_positions' => 'required',
            ];

            if (!$is_delegate_handover) {
                unset($listRule['delegate_company_id']);
                unset($listRule['delegate_structure_id']);
                unset($listRule['delegate_name']);
                unset($listRule['delegate_positions']);
            }


            $fieldVN = [
                'person' => 'người bàn giao',
                'receiver' => 'người nhận bàn giao',
                'person_description' => 'nội dung',
            ];


            $listRuleDetail = [
//                'name' => 'required',
//                'group_id' => 'required|numeric',
//                'type' => 'required|numeric',
//                'check_count_handover' => 'required|numeric',
//                'status' => 'required|numeric',
//                'price' => 'required',
                'quantity' => 'required|numeric',
            ];

            $fieldVNDetail = [
                'name' => 'tên tài sản',
                'group_id' => 'nhóm tài sản',
                'type' => 'loại tài sản',
                'check_count_handover' => 'hện trạng tài sản',
                'status' => 'trạng thái',
                'price' => 'đơn giá',
                'quantity' => 'số lượng',
            ];


            $checkValidation = $this->validateInput($request->all(), $listRule, $fieldVN);
            if ($checkValidation) {
                return redirect()->back()->withErrors($checkValidation->getMessageBag())->withInput();
            }
            if ($handover_detail) {
                foreach ($handover_detail as $key => $value) {
                    $checkValidDetail = $this->validateInput($value, $listRuleDetail, $fieldVNDetail);
                    if ($checkValidDetail) {
                        return redirect()->back()->withErrors($checkValidDetail->getMessageBag())->withInput();
                    }
                }
            }
            $person_company_id = $request->get('person_company_id', 0);
            $person_structure_id = $request->get('person_structure_id', 0);
            $person_name = $request->get('person_name', '');
            $person_positions = $request->get('person_positions', '');
            $person_description = $request->get('person_description', '');
            $receiver_company_id = $request->get('receiver_company_id', 0);
            $receiver_structure_id = $request->get('receiver_structure_id', 0);
            $receiver_name = $request->get('receiver_name', '');
            $receiver_positions = $request->get('receiver_positions', '');
            $delegate_company_id = $request->get('delegate_company_id', 0);
            $delegate_structure_id = $request->get('delegate_structure_id', 0);
            $delegate_name = $request->get('delegate_name', '');
            $delegate_positions = $request->get('delegate_positions', '');
            $handovers_datetime = $request->get('handovers_datetime', null);
            $type_handover = $request->get('type_handover', 0);


            $data_handover = [
                AssetHandover::PERSON_NAME => $person_name,
                AssetHandover::PERSON_COMPANY_ID => $person_company_id,
                AssetHandover::PERSON_STRUCTURE_ID => $person_structure_id,
                AssetHandover::PERSON_POSITIONS => $person_positions,
                AssetHandover::PERSON_DESCRIPTION => $person_description,
                AssetHandover::RECEIVER_NAME => $receiver_name,
                AssetHandover::RECEIVER_COMPANY_ID => $receiver_company_id,
                AssetHandover::RECEIVER_STRUCTURE_ID => $receiver_structure_id,
                AssetHandover::RECEIVER_POSITIONS => $receiver_positions,
                AssetHandover::CREATED_BY => Auth::user()->id,
                AssetHandover::STATUS => AssetHandover::STATUS_CREATE_NEW,
                AssetHandover::TYPE => $type_handover,
            ];

            if ($handovers_datetime && $handovers_datetime !== '') {
                $data_handover[AssetHandover::HANDOVERS_DATETIME]
                    = Carbon::createFromFormat('Y-m-d H:i:s', $handovers_datetime . '00:00:00');
            }

            if ($is_delegate_handover && $is_delegate_handover == 'on') {
                $data_handover[AssetHandover::DELEGATE_NAME] = $delegate_name;
                $data_handover[AssetHandover::DELEGATE_COMPANY_ID] = $delegate_company_id;
                $data_handover[AssetHandover::DELEGATE_STRUCTURE_ID] = $delegate_structure_id;
                $data_handover[AssetHandover::DELEGATE_POSITIONS] = $delegate_positions;
            }


            $objCreateAssetHandover = $this->repository->create($data_handover);

            $number_handover = 0;
            $obj_params_handover = new \stdClass();
            $obj_asset_handover = new \stdClass();
            foreach ($handover_detail as $key_d => $value) {
                //kiểm tra tài sản trong kho
                $mAssetCount = new Asset();
                $asset_id = isset($value['asset_id']) ? $value['asset_id'] : null;
                $name = isset($value['name']) ? $value['name'] : '';
                $group_id = isset($value['group_id']) ? $value['group_id'] : 0;
                $type = isset($value['type']) ? $value['type'] : 0;
                $check_count_handover = isset($value['check_count_handover']) ? $value['check_count_handover'] : 0;
                $status = isset($value['status']) ? $value['status'] : 0;
                $price = isset($value['price']) ? $value['price'] : null;
                $quantity = isset($value['quantity']) ? $value['quantity'] : 0;
                $note = isset($value['note']) ? $value['note'] : '';
                $unit_id = isset($value['unit_id']) ? $value['unit_id'] : 0;
                $unit_name = isset($value['unit_name']) ? $value['unit_name'] : '';

                if (!$asset_id) {
                    if (trim($name) !== '') {
                        $mAssetCount = $mAssetCount->where(Asset::SLUG, str_slug($name));
                    }
                    if ($group_id) {
                        $mAssetCount = $mAssetCount->where(Asset::GROUP_ID, $group_id);
                    }
                    if ($type) {
                        $mAssetCount = $mAssetCount->where(Asset::TYPE, $type);
                    }
                    if ($status) {
                        $mAssetCount = $mAssetCount->where(Asset::STATUS, $status);
                    }
                    if ($check_count_handover == 1) {
                        $mAssetCount = $mAssetCount->whereNull(Asset::COUNT_HANDOVER);
                    }
                    if ($check_count_handover == 2) {
                        $mAssetCount = $mAssetCount->whereNotNull(Asset::COUNT_HANDOVER)->where(Asset::COUNT_HANDOVER, '>', 0);
                    }
                    if ($person_structure_id) {
                        $mAssetCount = $mAssetCount->where(Asset::STRUCTURE_OWNER, $person_structure_id);
                    }
                    if (!$price) {
                        $mAssetCount = $mAssetCount->where(function ($query) use ($price) {
                            $query->where(Asset::PRICE, $price);
                            $query->orWhereNull(Asset::PRICE);
                        });
                    } else {
                        $mAssetCount = $mAssetCount->where(Asset::PRICE, $price);
                    }
                    if ($unit_id) {
                        $mAssetCount = $mAssetCount->where(Asset::UNIT_ID, $unit_id);
                    }
                } else {
                    $mAssetCount = $mAssetCount->where('id', $asset_id);
                }

                $assetCount = clone $mAssetCount;
                $assetAssets = clone $mAssetCount;
                $listFail = [];
                if (in_array(str_slug($unit_name), UNIT_MEASUREMENT)) {


                    $asset = $assetCount->first();
                    $asset_quantity = 0;
                    if ($asset) {
                        $asset_quantity = $asset->quantity;
                        $asset_id = $asset->id;
                    }
                    Log::info('371 ' . $name . ':  ' . $asset_quantity . 'vs' . $quantity);
                    if (!$quantity || $quantity < 1) {
                        $listFail[] = 'Số lượng không được trống và lớn hơn 0';
                    }
                    if ($asset_quantity < $quantity) {
                        Log::info('376 ' . $name . ':  ' . $asset_quantity . 'vs' . $quantity);
                        $listFail[] = 'Số lượng tài sản bàn giao không được lớn hơn tổng số tài sản đã lọc';
                    }
                    if (!$asset) {
                        $listFail[] = 'Không có tài sản nào thỏa mãn điều kiện lọc';
                    }
                } else {
                    $countAsset = $assetCount->count();
                    Log::info($name . '384 :  ' . $countAsset . 'vs' . $quantity);
                    if (!$quantity || $quantity < 1) {
                        $listFail[] = 'Số lượng không được trống và lớn hơn 0';
                    }
                    if ($countAsset < $quantity) {
                        Log::info($name . '389 :  ' . $countAsset . 'vs' . $quantity);
                        $listFail[] = 'Số lượng tài sản bàn giao không được lớn hơn tổng số tài sản đã lọc';
                    }
                    if (!$countAsset) {
                        $listFail[] = 'Không có tài sản nào thỏa mãn điều kiện lọc';
                    }
                }
                if (count($listFail) > 0) {
                    return redirect()->back()->with('error', trans('Lỗi bàn giao tài sản'))
                        ->with('list-fail', $listFail);
                }


                $data_handover_detail = [
                    AssetHandoverDetail::ASSET_ID => $asset_id,
                    AssetHandoverDetail::NAME => $name,
                    AssetHandoverDetail::HANDOVER_ID => $objCreateAssetHandover->id,
                    AssetHandoverDetail::GROUP_ID => $group_id,
                    AssetHandoverDetail::TYPE => $type_handover,
                    AssetHandoverDetail::CHECK_COUNT_HANDOVER => $check_count_handover,
                    AssetHandoverDetail::STATUS => $status,
                    AssetHandoverDetail::PRICE => $price,
                    AssetHandoverDetail::QUANTITY => $quantity,
                    AssetHandoverDetail::NOTE => $note,
                    AssetHandoverDetail::UNIT_ID => $unit_id,
                ];

                $objCreateHandoverDetail = AssetHandoverDetail::create($data_handover_detail);

                if (in_array(str_slug($unit_name), UNIT_MEASUREMENT)) {
                    $listAsset = $assetAssets->where('id', $asset_id)->get();
                } else {
                    $listAsset = $assetAssets->take($quantity)->get();
                }
                $obj_params_handover_detail = new \stdClass();
                $obj_handover_detail = new \stdClass();
                foreach ($listAsset as $key_asset => $val_asset) {
                    //Chú ý, chưa check trường hợp phân quyền
                    Asset::where('id', $val_asset->id)
                        ->update(array(
                            Asset::STATUS => Asset::STATUS_WAIT,
                        ));

                    $obj_handover_params_detail = new \stdClass();
                    $obj_handover_params_detail->asset_id = (int)$val_asset->id;
                    $obj_handover_params_detail->partner_id = (int)$val_asset->partner_id;
                    $obj_handover_params_detail->contract_id = (int)$val_asset->contract_id;
                    $key_handover_params_detail = $key_asset + 1;
                    $obj_handover_detail->$key_handover_params_detail = $obj_handover_params_detail;
                }
                $obj_params_handover_detail = $obj_handover_detail;
                $obj_asset_handover_params = new \stdClass();
                $obj_asset_handover_params->detail = $obj_params_handover_detail;
                $key_handover_params = $key_d + 1;
                $obj_asset_handover->$key_handover_params = $obj_asset_handover_params;

                AssetHandoverDetail::where('id', $objCreateHandoverDetail->id)
                    ->update(array(AssetHandoverDetail::PARAMS => json_encode($obj_params_handover_detail)));
                if (in_array(str_slug($unit_name), UNIT_MEASUREMENT)) {
                    $number_handover = $number_handover + 1;
                } else {
                    $number_handover = $number_handover + $quantity;
                }
            }
            $obj_params_handover = $obj_asset_handover;
            $this->repository->find($objCreateAssetHandover->id)
                ->update(array(AssetHandover::NUMBER_HANDOVER => $number_handover, AssetHandover::PARAMS => $obj_params_handover));
            return redirect()->route('handover.index')->with('message', trans('Tạo bàn giao tài sản thành công'));

        } catch (ValidatorException $e) {
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mAssetHandover = new AssetHandover();
        $mAssetHandoverDetail = new AssetHandoverDetail();
        $data = $mAssetHandover->where('id', $id)->first();
        $dataDetail = $mAssetHandoverDetail->where('handover_id', $id)->get();

        return view($this->partView . '.show', compact('data', 'dataDetail'));
    }

    public function detail($id)
    {
        $mAssetHandover = new AssetHandover();
        $mAssetHandoverDetail = new AssetHandoverDetail();
        $data = $mAssetHandover->where('id', $id)->first();
        $dataDetail = $mAssetHandoverDetail->where('handover_id', $id)->get();

        return view($this->partView . '.detail', compact('data', 'dataDetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('handover.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }

        $assetHandover = $this->repository->find($id);

        return view('assetHandovers.edit', compact('assetHandover'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AssetHandoverUpdateRequest $request
     * @param string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AssetHandoverUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $assetHandover = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data' => $assetHandover->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    //Xóa bàn giao tài sản
    public function destroy($id)
    {
//        if (Gate::denies('handover.delete')) {
//            abort(505, trans('messages.you_do_not_have_permission'));
//        }
        $mAssetHandover = new AssetHandover();
        $data = $mAssetHandover->where('id', $id)->where('status', AssetHandover::STATUS_CREATE_NEW)->first();
        if ($data) {
            $mAssetHandoverDetail = new AssetHandoverDetail();
            $dataDetail = $mAssetHandoverDetail->where('handover_id', $id)->get();
            foreach ($dataDetail as $key => $value) {
                $paramDetail = $value->params;
                if ($paramDetail) {
                    foreach ($paramDetail as $key_p => $val_p) {
                        Asset::where('id', $val_p['asset_id'])->where(Asset::STATUS, Asset::STATUS_WAIT)
                            ->update([Asset::STATUS => $value->status]);
                    }
                    $mAssetHandoverDetail->delete($value->id);
                }
            }
            $deleted = $this->repository->delete($id);
        } else {
            return redirect()->back()->withErrors('Không thể xóa bàn giao này');
        }

        if (request()->wantsJson()) {
            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }
        return redirect()->route('handover.index')->with('message', trans('messages.delete_success'));
    }


    //Hủy và từ chối nhận bàn giao tài sản
    public function cancel($id)
    {
        $mAssetHandover = new AssetHandover();
        $data = $mAssetHandover->where('id', $id)
            ->where(function ($query) {
                $query->where('created_by', Auth::user()->id);
                $query->orWhere(AssetHandover::RECEIVER, Auth::user()->id);
            })
            ->where('status', AssetHandover::STATUS_CREATE_NEW)->first();
        if ($data) {
            AssetHandover::where('id', $id)->update([AssetHandover::STATUS => AssetHandover::STATUS_CANCEL]);
            $handoverParams = $data->params;
            foreach ($handoverParams as $key => $val) {
                $asset = Asset::where('id', $val['asset_id'])->where('status', Asset::STATUS_WAIT)->first();
                if ($asset) {
                    Asset::where('id', $val['asset_id'])->update(['status', Asset::STATUS_USE]);
                }
            }
        } else {
            return redirect()->back()->withErrors('Lỗi hủy bàn giao tài sản');
        }

        return redirect()->back()->with('message', 'Hủy bàn giao tài sản thành công');
    }

    public function getUser(Request $request)
    {
        $input = $request->all();
        $listDepartment = User::where('branch_id', $input['branch_id'])->where('company_id', $input['company_id'])
            ->where('company_id', $input['company_id'])
            ->orderBy('fullname')->get();
        $html = '';
        if ($listDepartment) {
            $html = '<option value="0">-- Chọn user --</option>';
            foreach ($listDepartment as $value) {
                $html .= '<option value="' . $value->id . '">' . $value->email . ' | ' . $value->fullname . ' | ' . $value->position->name . '</option>';
            }
        }

        return response()->json([
            'data' => $html,
        ]);
    }

    public function validateInput($input, $listRule, $customAtr)
    {
        $checkValidate = Validator::make($input, $listRule, [], $customAtr);
        if ($checkValidate->fails()) {
            $errors = $checkValidate->errors();
            return $errors;
        } else {
            return false;
        }
    }

    // Xác nhận bàn giao tài sản
    public function active(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];
        $handovers_datetime = $request->get('handovers_datetime');
        $mAssetHandover = new AssetHandover();
        $update_detail = $request->get('detail');

        $mAssetHandoverDetail = new AssetHandoverDetail();
        $dataHandover = $mAssetHandover->where('id', $id)->first();
        $dataDetail = $mAssetHandoverDetail->where('handover_id', $id)->get();

        if ($dataDetail) {
            $list_asset_id = [];
            foreach ($dataDetail as $key => $value) {
                if ($value->asset_id) {
                    $list_asset_id[] = $value->asset_id;
                    $asset = Asset::where('id', $value->asset_id)->where(Asset::STATUS, Asset::STATUS_WAIT)->first();
                    if (!$asset) {
                        continue;
                    }
                    $this->activeProcess($asset, $dataHandover, $value);
                } else {
                    $paramDetail = $value->params;
                    if ($paramDetail) {
                        foreach ($paramDetail as $key_p => $val_p) {
                            $list_asset_id[] = $val_p['asset_id'];
                            $asset = Asset::where('id', $val_p['asset_id'])->where(Asset::STATUS, Asset::STATUS_WAIT)->first();
                            if (!$asset) {
                                continue;
                            }
                            $this->activeProcess($asset, $dataHandover, $value);
                        }
                    }
                }
            }
            $data_handover = [
                AssetHandover::UPDATED_BY => Auth::user()->id,
                AssetHandover::STATUS => AssetHandover::STATUS_CONFIRM,
                AssetHandover::LIST_ASSET_ID => json_encode($list_asset_id),
            ];
            if ($handovers_datetime && $handovers_datetime !== '') {
                $data_handover[AssetHandover::HANDOVERS_DATETIME] = Carbon::createFromFormat('Y-m-d H:i:s', $handovers_datetime . '00:00:00');
            }
            AssetHandover::where('id', $dataHandover->id)->update($data_handover);
        }

        //update handover detail
        if ($update_detail && !empty($update_detail)) {
            foreach ($update_detail as $key_dt => $val_dt) {
                if (isset($val_dt['note'])) {
                    $data_update_detail = [
                        AssetHandoverDetail::NOTE => $val_dt['note']
                    ];
                    AssetHandoverDetail::where('id', $val_dt['id'])->update($data_update_detail);
                }
            }
        }

        return redirect()->back()->with('message', 'Xác nhận nhận bàn giao thành công');
    }

    public function activeProcess($asset, $dataHandover, $detail)
    {
        $unit_name = '';
        $unit = Unit::where('id', $detail->unit_id)->first();
        if ($unit) {
            $unit_name = $unit->name;
        }
        //check asset log
        $asset_logs = AssetLog::where('asset_id', $asset->id)->first();

        $obj_params = new \stdClass();
        $string1 = "1";
        $obj_asset_log = new \stdClass();

        $params_asset_log = new \stdClass();
        $params_asset_log->person_name = (string)$dataHandover->person_name;
        $params_asset_log->asset_handover_id = (string)$dataHandover->id;
        $params_asset_log->person_structure_id = (string)$dataHandover->person_structure_id;
        $params_asset_log->receiver_name = (string)$dataHandover->receiver_name;
        $params_asset_log->receiver_structure_id = (string)$dataHandover->receiver_structure_id;
        $params_asset_log->person_start_time = isset($asset->date_use) ? $asset->date_use : date("d-m-Y H:i:s");
        $params_asset_log->person_end_time = date("d-m-Y H:i:s");
        if (in_array(str_slug($unit_name), UNIT_MEASUREMENT)) {
            $params_asset_log->unit_id = (string)$detail->unit_id;
            $params_asset_log->quantity = (string)$detail->quantity;
        } else {
            $params_asset_log->unit_id = (string)$detail->unit_id;
            $params_asset_log->quantity = "1";
        }

        if ($asset_logs) {
            //update param
            if ($asset_logs->params) {
                $count_item_params = count($asset_logs->params) + 1;
            } else {
                $count_item_params = 1;
            }
            $obj_asset_log->$count_item_params = $params_asset_log;
            $obj_params = $obj_asset_log;
            AssetLog::where('id', $asset_logs->id)
                ->update(
                    array(
                        AssetLog::PARAMS => DB::raw("JSON_MERGE(params, '" . json_encode($obj_params, JSON_UNESCAPED_UNICODE) . "')"),
                    ));

        } else {
            //insert new
            $obj_asset_log->$string1 = $params_asset_log;
            $obj_params = $obj_asset_log;
            $data_asset_log = [
                AssetLog::ASSET_ID => $asset->id,
                AssetLog::CONTRACT_ID => $asset->contract_id,
                AssetLog::PARTNER_ID => $asset->partner_id,
                AssetLog::PARAMS => $obj_params,
            ];
            AssetLog::create($data_asset_log);
        }


        $date_use = isset($dataHandover->handovers_datetime)
            ? $dataHandover->handovers_datetime : Carbon::now()->toDateTimeString();

        //check type hand over
        $statusAsset = Asset::STATUS_USE;
        if ($dataHandover->type == AssetHandover::TYPE_HANDOVER_IMPORT) {
            $statusAsset = Asset::STATUS_STORE;
        }

        $asset_handovers_params = $asset->handovers_params;
        if ($asset_handovers_params) {
            if (in_array(str_slug($unit_name), UNIT_MEASUREMENT) && $detail->quantity !== $asset->quantity) {
                $newAssets = $asset->replicate();
                $newAssets->save();
                Asset::where('id', $newAssets->id)
                    ->update(
                        array(
                            Asset::RECEIVER_HANDOVER => $dataHandover->receiver_name,
                            Asset::COMPANY_OWNER => $dataHandover->receiver_company_id,
                            Asset::STRUCTURE_OWNER => $dataHandover->receiver_structure_id,
                            Asset::HANDOVERS_PARAMS => '{"list_asset_handover":["' . (int)$dataHandover->id . '"]}',
                            Asset::QUANTITY => $detail->quantity,
                            Asset::DATE_USE => $date_use,
                        ));
                Asset::where('id', $asset->id)
                    ->update(
                        array(
                            Asset::HANDOVERS_PARAMS => '{"list_asset_handover":["' . (int)$dataHandover->id . '"]}',
                            Asset::QUANTITY => ($asset->quantity - $detail->quantity),
                            Asset::STATUS => $statusAsset,
                        ));
            } else {
                Asset::where('id', $asset->id)
                    ->update(
                        array(
                            Asset::RECEIVER_HANDOVER => $dataHandover->receiver_name,
                            Asset::COMPANY_OWNER => $dataHandover->receiver_company_id,
                            Asset::STRUCTURE_OWNER => $dataHandover->receiver_structure_id,
                            Asset::HANDOVERS_PARAMS => '{"list_asset_handover":["' . (int)$dataHandover->id . '"]}',
                            Asset::STATUS => $statusAsset,
                            Asset::DATE_USE => $date_use,
                        ));
            }
        } else {
            if (in_array(str_slug($unit_name), UNIT_MEASUREMENT) && $detail->quantity !== $asset->quantity) {
                $newAssets = $asset->replicate();
                $newAssets->save();
                Asset::where('id', $newAssets->id)
                    ->update(
                        array(
                            Asset::RECEIVER_HANDOVER => $dataHandover->receiver_name,
                            Asset::COMPANY_OWNER => $dataHandover->receiver_company_id,
                            Asset::STRUCTURE_OWNER => $dataHandover->receiver_structure_id,
                            Asset::HANDOVERS_PARAMS => '{"list_asset_handover":["' . (int)$dataHandover->id . '"]}',
                            Asset::QUANTITY => $detail->quantity,
                            Asset::DATE_USE => $date_use,
                        ));
                Asset::where('id', $asset->id)
                    ->update(
                        array(
                            Asset::HANDOVERS_PARAMS => DB::raw("JSON_ARRAY_APPEND(handovers_params, '$.list_asset_handover', " . (int)$asset->id . ")"),
                            Asset::QUANTITY => ($asset->quantity - $detail->quantity),
                            Asset::STATUS => $statusAsset,
                        ));
            } else {
                Asset::where('id', $asset->id)
                    ->update(
                        array(
                            Asset::RECEIVER_HANDOVER => $dataHandover->receiver_name,
                            Asset::COMPANY_OWNER => $dataHandover->receiver_company_id,
                            Asset::STRUCTURE_OWNER => $dataHandover->receiver_structure_id,
                            Asset::HANDOVERS_PARAMS => DB::raw("JSON_ARRAY_APPEND(handovers_params, '$.list_asset_handover', " . (int)$asset->id . ")"),
                            Asset::STATUS => $statusAsset,
                            Asset::DATE_USE => $date_use,
                        ));
            }
        }
    }

    // Xác nhận bàn giao tài sản vaf luu kho
    public function activeStore(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];
        $filter_handover = $input['filter_handover'];
        //filter_handover = 0 ban giao cua ban tao
        $mAssetHandover = new AssetHandover();
        if ($filter_handover == 1) {
            $mAssetHandover = $mAssetHandover->where('receiver', Auth::user()->id)->where('id', $id)
                ->where('status', AssetHandover::STATUS_CREATE_NEW)
                ->where('type', AssetHandover::TYPE_HANDOVER_RETURN);
        }

        $data = $mAssetHandover->first();

        if ($data) {
            $params = $data->params;
            foreach ($params as $key => $value) {
                $asset = Asset::where('id', $value['asset_id'])->first();
                if (!$asset) {
                    continue;
                }
                //check asset log
                $asset_logs = AssetLog::where('asset_id', $asset->id)->first();

                $obj_params = new \stdClass();
                $string1 = "1";
                $obj_asset_log = new \stdClass();

                $params_asset_log = new \stdClass();
                $params_asset_log->person = (int)$data->person;
                $params_asset_log->asset_handover_id = (int)$data->id;
                $params_asset_log->receiver = (int)$data->receiver;
                $params_asset_log->person_start_time = isset($asset->date_use) ? $asset->date_use : date("d-m-Y H:i:s");
                $params_asset_log->person_end_time = date("d-m-Y H:i:s");

                if ($asset_logs) {
                    //update param
                    $count_item_params = count($asset_logs->params) + 1;
                    $obj_asset_log->$count_item_params = $params_asset_log;
                    $obj_params = $obj_asset_log;
                    AssetLog::where('id', $asset_logs->id)
                        ->update(
                            array(
                                AssetLog::PARAMS => DB::raw("JSON_MERGE(params, '" . json_encode($obj_params) . "')"),
                            ));

                } else {
                    //insert new
                    $obj_asset_log->$string1 = $params_asset_log;
                    $obj_params = $obj_asset_log;
                    $data_asset_log = [
                        AssetLog::ASSET_ID => $asset->id,
                        AssetLog::CONTRACT_ID => $asset->contract_id,
                        AssetLog::PARTNER_ID => $asset->partner_id,
                        AssetLog::PARAMS => $obj_params,
                    ];
                    AssetLog::create($data_asset_log);
                }


                $handovers_params = $asset->handovers_params;
                if ($handovers_params) {
                    Asset::where('id', $asset->id)
                        ->update(
                            array(
                                Asset::OWNER => NULL,
                                Asset::HANDOVERS_PARAMS => '{"list_asset_handover":["' . (int)$data->id . '"]}',
                                Asset::STATUS => Asset::STATUS_BREAKDOWN,
                            ));
                } else {
                    Asset::where('id', $asset->id)
                        ->update(
                            array(
                                Asset::OWNER => NUll,
                                Asset::HANDOVERS_PARAMS => DB::raw("JSON_ARRAY_APPEND(handovers_params, '$.list_asset_handover', " . (int)$asset->id . ")"),
                                Asset::STATUS => Asset::STATUS_BREAKDOWN,
                            ));
                }
            }
            $data_handover = [
                AssetHandover::UPDATED_BY => Auth::user()->id,
                AssetHandover::STATUS => AssetHandover::STATUS_CONFIRM_STORE,
            ];

            AssetHandover::where('id', $data->id)
                ->update($data_handover);
        }
    }

    public function getAssetHtml(Request $request)
    {
        $input = $request->all();
        $name = isset($input['name']) ? $input['name'] : '';
        $group_id = isset($input['group_id']) ? $input['group_id'] : 0;
        $type = isset($input['type']) ? $input['type'] : '';
        $status = isset($input['status']) ? $input['status'] : '';
        $structure_id = isset($input['structure_id']) ? $input['structure_id'] : null;


        $mAsset = new Asset();
        $mAsset = $mAsset->select(DB::raw('COUNT(`id`) AS asset_count, asset_assets.*'));
        if (trim($name) !== '') {
            $mAsset = $mAsset->where('name', 'like', '%' . $name . '%');
        }
        if ($group_id) {
            $mAsset = $mAsset->where('group_id', $group_id);
        }
        if ($type !== '') {
            $mAsset = $mAsset->where('type', $type);
        }
        if ($status !== '') {
            $mAsset = $mAsset->where('status', $status)->whereIn(Asset::STATUS, array_keys(Asset::$assetStatusHandover));
        } else {
            $mAsset = $mAsset->whereIn(Asset::STATUS, array_keys(Asset::$assetStatusHandover));
        }

        if ($structure_id) {
            $mAsset = $mAsset->where('structure_owner', $structure_id);
        }

        $mAsset = $mAsset->groupBy(Asset::YEAR_USE, Asset::NAME, Asset::PRICE, Asset::STATUS, Asset::TYPE, Asset::GROUP_ID, Asset::UNIT_ID);
        $data = $mAsset;

        $typeAsset = Asset::$assetClassification;

        if (isset($input['page'])) {
            unset($input['page']);
        }

        $html = View::make($this->partView . '.list-asset')
            ->with(compact('data'))
            ->with(compact('typeAsset'))
            ->with(compact('input'));

        return response()->json([
            'data' => $html->render(),
        ]);
    }

    public function getHistoryStructure(Request $request, $structure_id)
    {

        $name = $request->get('name');
        $group_id = $request->get('group_id');

        $mAssetHandover = new AssetHandover();


        $mAssetHandover = $mAssetHandover->where(function ($query) use ($structure_id) {
            $query->where('person_structure_id', $structure_id);
            $query->orWhere('receiver_structure_id', $structure_id);
        });
        $mAssetHandover = $mAssetHandover->get();
        $arr_list_asset_id = [];
        foreach ($mAssetHandover as $k_hand => $v_hand) {
            if (!is_array($v_hand->list_asset_id) || empty($v_hand->list_asset_id)) {
                continue;
            }
            $arr_list_asset_id = array_merge($arr_list_asset_id, $v_hand->list_asset_id);
        }

        $mAsset = new Asset();
        if (trim($name) !== '') {
            $mAsset = $mAsset->where('name', 'like', '%' . $name . '%');
        }
        if ($group_id) {
            $mGroup = new Group();
            $listGroup = $mGroup->getAllChildGroup($group_id);
            array_unshift($listGroup, $group_id);
            $mAsset = $mAsset->whereIn('group_id', $listGroup);
        }

        $arr_list_asset = $mAsset->whereIn('id', $arr_list_asset_id)->pluck('id')->toArray();

        $mAssetLog = new AssetLog();
        $data = [];
        $data = $mAssetLog->whereIn('asset_id', $arr_list_asset)->paginate();
        $arrStructureCompany = StructureCompany::all()->pluck('name', 'id')->toArray();
        $arrUnit = Unit::all()->pluck('name', 'id')->toArray();

        if (!$data) {
            return redirect()->back()->with('error', trans('Không có lịch sử tài sản nào'));
        }
        return view('administrator.assets.handover.history-structure', compact('data', 'arrStructureCompany', 'arrUnit', 'structure_id'));
    }


    public function updateAllAssetLog()
    {
        ini_set('max_execution_time', '1000');
        ini_set('memory_limit', '-1');

        $mAssetHandover = new AssetHandover();
        $mAssetHandover = $mAssetHandover->orderBy('id', 'asc')->get();

        foreach ($mAssetHandover as $keyhad => $valuehad) {
            $mAssetHandoverDetail = new AssetHandoverDetail();
            $dataDetail = $mAssetHandoverDetail->where('handover_id', $valuehad->id)->get();
            if ($dataDetail) {
                foreach ($dataDetail as $key => $value) {
                    if ($value->asset_id) {
                        $asset = Asset::where('id', $value->asset_id)->first();
                        if ($asset) {
                            $this->activeProcessUpdate($asset, $valuehad, $value);
                        }
                    } else {
                        $paramDetail = $value->params;
                        if ($paramDetail) {
                            foreach ($paramDetail as $key_p => $val_p) {
                                $asset = Asset::where('id', $val_p['asset_id'])->first();
                                if ($asset) {
                                    $this->activeProcessUpdate($asset, $valuehad, $value);
                                }
                            }
                        }
                    }
                }
                sleep(1);
            }
            sleep(1);
        }

        dd('Chạy xong');


        $mAssetHandoverDetail = new AssetHandoverDetail();
        $dataHandover = $mAssetHandover->where('status', 2)->get();


        foreach ($dataHandover as $key_had => $val_hand) {
            $dataDetail = $mAssetHandoverDetail->where('handover_id', $val_hand->id)->get();
            if ($dataDetail) {
                $list_asset_id = [];
                foreach ($dataDetail as $key => $value) {
                    if ($value->asset_id) {
                        $list_asset_id[] = $value->asset_id;
                    } else {
                        $paramDetail = $value->params;
                        if ($paramDetail) {
                            foreach ($paramDetail as $key_p => $val_p) {
                                $list_asset_id[] = $val_p['asset_id'];
                            }
                        }
                    }
                }
                $data_handover = [
                    AssetHandover::LIST_ASSET_ID => json_encode($list_asset_id),
                ];
                AssetHandover::where('id', $val_hand->id)->update($data_handover);
            }
        }

    }


    public function activeProcessUpdate($asset, $dataHandover, $detail)
    {
        ini_set('max_execution_time', '1000');
        ini_set('memory_limit', '-1');

        $unit_name = '';
        $unit = Unit::where('id', $detail->unit_id)->first();
        if ($unit) {
            $unit_name = $unit->name;
        }
        //check asset log
        $asset_logs = AssetLog::where('asset_id', $asset->id)->first();

        $obj_params = new \stdClass();
        $string1 = "1";
        $obj_asset_log = new \stdClass();

        $params_asset_log = new \stdClass();
        $params_asset_log->person_name = (string)$dataHandover->person_name;
        $params_asset_log->asset_handover_id = (string)$dataHandover->id;
        $params_asset_log->person_structure_id = (string)$dataHandover->person_structure_id;
        $params_asset_log->receiver_name = (string)$dataHandover->receiver_name;
        $params_asset_log->receiver_structure_id = (string)$dataHandover->receiver_structure_id;
        $params_asset_log->person_start_time = '';
        $params_asset_log->person_end_time = $dataHandover->created_at->format('d-m-Y H:i:s');
        if (in_array(str_slug($unit_name), UNIT_MEASUREMENT)) {
            $params_asset_log->unit_id = (string)$detail->unit_id;
            $params_asset_log->quantity = (string)$detail->quantity;
        } else {
            $params_asset_log->unit_id = (string)$detail->unit_id;
            $params_asset_log->quantity = "1";
        }

        if ($asset_logs) {
            //update param
            if ($asset_logs->params) {
                $count_item_params = count($asset_logs->params) + 1;
            } else {
                $count_item_params = 1;
            }
            $obj_asset_log->$count_item_params = $params_asset_log;
            $obj_params = $obj_asset_log;
            AssetLog::where('id', $asset_logs->id)
                ->update(
                    array(
                        AssetLog::PARAMS => DB::raw("JSON_MERGE(params, '" . json_encode($obj_params, JSON_UNESCAPED_UNICODE) . "')"),
                    ));

        }
    }
}
