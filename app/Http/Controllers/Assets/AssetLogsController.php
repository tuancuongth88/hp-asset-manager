<?php

namespace App\Http\Controllers\Assets;

use App\Http\Controllers\Controller;
use App\Http\Requests\Assets\AssetLogCreateRequest;
use App\Http\Requests\Assets\AssetLogUpdateRequest;
use App\Repositories\Assets\AssetLogRepository;
use App\Validators\Assets\AssetLogValidator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class AssetLogsController.
 *
 * @package namespace App\Http\Controllers;
 */
class AssetLogsController extends Controller
{
    /**
     * @var AssetLogRepository
     */
    protected $repository;

    /**
     * @var AssetLogValidator
     */
    protected $validator;

    /**
     * AssetLogsController constructor.
     *
     * @param AssetLogRepository $repository
     * @param AssetLogValidator $validator
     */
    public function __construct(AssetLogRepository $repository, AssetLogValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $assetLogs = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $assetLogs,
            ]);
        }

        return view('assetLogs.index', compact('assetLogs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AssetLogCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(AssetLogCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $request['created_by'] = Auth::user()->id;
            $assetLog = $this->repository->create($request->all());

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $assetLog->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assetLog = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $assetLog,
            ]);
        }

        return view('assetLogs.show', compact('assetLog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assetLog = $this->repository->find($id);

        return view('assetLogs.edit', compact('assetLog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AssetLogUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AssetLogUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $request['updated_by'] = Auth::user()->id;
            $assetLog = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $assetLog->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }
}
