<?php

namespace App\Http\Controllers\Assets;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

    public function index() {
        return view('administrator.dashboard');
    }

}
