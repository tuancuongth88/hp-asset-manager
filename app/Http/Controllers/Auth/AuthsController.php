<?php

namespace App\Http\Controllers\Auth;

use app\Mail\ForgotPassword;
use Illuminate\Http\Request;
use App\Models\Users\User;
use App\Services\AuthService;
use App\Models\Systems\Company;
use App\Services\ResponseService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthsController extends Controller
{
    private $request;

    private $response;

    private $user;

    public function __construct(Request $request, User $user, AuthService $auth, ResponseService $response) {
        $this->request  = $request;
        $this->user     = $user;
        $this->auth     = $auth;
        $this->response = $response;
        $this->middleware('auth', ['except' => ['postLogin', 'getLogin', 'getActiveUser', 'postRegister', 'postForgot', 'getResetPassword', 'postResetPassword']]);
    }

    public function postLogin() {
        return $this->auth->login();
    }

    public function getLogout() {
        return $this->auth->logout();
}

    public function getLogin() {
        if (Auth::check()) {
            return redirect()->action('Assets\DashboardController@index');
        }
        return view('administrator.auth.login');
    }

    public function getActiveUser($token) {
        return $this->auth->activateUser($token);
}

    public function postRegister() {
        return $this->auth->registerUser();
    }

    public function postForgot() {
        $email = $this->request->input('email');
        $user  = $this->user->where(User::EMAIL, $email)->first();
        if (!$user) {
            return $this->response->json(false, '', trans('messages.account_does_not_exists'));
        }
        $currentTime = time();
        $expire      = $currentTime + 1 * 86400;
        $token       = hash_hmac('sha256', str_random(40), config('app.key'));
        // $token       = md5(uniqid($email, true)) . $currentTime;
        \DB::table('password_resets')->insert([
            'token'      => $token,
            'expire'     => $expire,
            'email'      => $email,
            'created_at' => \Carbon\Carbon::now(),
        ]);
        $url      = url("/reset-password/{$token}");
        $mailable = new ForgotPassword($url);
        config([
            'mail' => [
                'driver'     => 'smtp',
                'port'       => '587',
                'host'       => 'smtp.gmail.com',
                'username'   => 'speedandsound7890@gmail.com',
                'password'   => 'rweivdnkyrdfojuz',
                'encryption' => 'tls',
                'from'       => array(
                    'address' => 'speedandsound7890@gmail.com',
                    'name'    => 'HaiPhat-Tech',
                ),
                'stream'     => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer'       => false,
                        'verify_peer_name'  => false,
                    ],
                ],
            ],
        ]);
        Mail::to($email)->send($mailable);
        return $this->response->json(true, '', trans('messages.you_need_to_authenticate_your_account') . trans('messages.we_sent_authenticate_code_to_your_email') . trans('messages.please_check_and_do_it'));
    }

    public function getResetPassword($token) {
        return view('administrator.auth.reset-password', compact('fb'));
    }

    public function postResetPassword() {
        $input     = $this->request->all();
        $validator = Validator::make($input, [
            USER::EMAIL    => 'required|email',
            USER::PASSWORD => 'min:6|required|confirmed',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($input);
        }
        $order = \DB::table('password_resets')
            ->where('email', $input['email'])
            ->where('token', $input['code'])
            ->orderBy('created_at', 'desc')
            ->first();
        if (!$order) {
            return redirect()
                ->back()
                ->with('error', true)
                ->with('message', trans('messages.you_have_not_sent_request_reset_password'));
        }
        if ($order->expire <= time()) {
            return redirect()
                ->back()
                ->with('error', true)
                ->with('message', trans('messages.your_request_have_been_expired'));
        }
        $user = $this->user->where(USER::EMAIL, $input[USER::EMAIL])->first();
        if (!$user) {
            return $this->response->json(false, '', trans('messages.account_does_not_exists'));
        }
        $user->password = \Hash::make($input['password']);
        $user->save();
        return redirect()->route('login')->with('status', true)->with('message', trans('messages.change_password_success'));
    }

}
