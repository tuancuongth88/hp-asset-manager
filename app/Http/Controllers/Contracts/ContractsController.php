<?php

namespace App\Http\Controllers\Contracts;

use App\Http\Controllers\Controller;
use App\Models\Assets\Asset;
use App\Models\Assets\AssetContractShow;
use App\Models\Assets\AssetImport;
use App\Models\Assets\Unit;
use App\Models\Contracts\Contract;
use App\Models\Documents\Documents;
use App\Models\Partners\Category;
use App\Models\Partners\Group;
use App\Models\Partners\Partner;
use App\Models\Systems\Company;
use App\Models\Systems\Position;

use App\Http\Requests;
use Couchbase\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\Contracts\ContractCreateRequest;
use App\Http\Requests\Contracts\ContractUpdateRequest;
use App\Repositories\Contracts\ContractRepository;
use App\Validators\Contracts\ContractValidator;

/**
 * Class ContractsController.
 *
 * @package namespace App\Http\Controllers\Contracts;
 */
class ContractsController extends Controller
{
    /**
     * @var ContractRepository
     */
    protected $repository;

    /**
     * @var ContractValidator
     */
    protected $validator;

    protected $partView;
    /**
     * ContractsController constructor.
     *
     * @param ContractRepository $repository
     * @param ContractValidator $validator
     */
    public function __construct(ContractRepository $repository, ContractValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->partView   = 'administrator.contracts';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->can('contract.view')){
            abort(506, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $contract_code = isset($request['contract_code']) ? $request['contract_code'] : '';
        $city_id = isset($request['city_id']) ? $request['city_id'] : 0;
        $district_id = isset($request['district_id']) ? $request['district_id'] : 0;

        $mContract = new Contract();
        $mPartner = new Partner();
        if ($contract_code && $contract_code !== '') {
            $mContract = $mContract->where('contract_code', 'like', '%' . $contract_code . '%');
        }
        if ($city_id) {
            $mPartner = $mPartner->where('city_id', $city_id);
        }
        if ($district_id) {
            $mPartner = $mPartner->where('district_id', $district_id);
        }
        $dataPartner = $mPartner->pluck('id')->toArray();
        if($dataPartner && !empty($dataPartner)) {
            $mContract = $mContract->whereIn(Contract::PARTNER_ID, $dataPartner);
        }
        $data = $mContract->paginate();

        $listDelegate = $data->pluck('delegate', 'id')->toArray();
        $listCategory = Category::all()->pluck('name', 'id')->toArray();
        $listPartner  = $data->pluck('name', 'id')->toArray();
        $listCity     = DB::table('provinces')->pluck('name', 'city_id')->toArray();
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->partView.'.index',
            compact('data', 'listDelegate', 'listCategory', 'listPartner', 'listCity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ContractCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ContractCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $request['created_by'] = Auth::user()->id;
            $input = $request->all();
            $objContractParent = $this->repository->create($input);
            $file = $request->id_file;
            $objContractParent['file_id'] = $file;
            $objContractParent->save();

            $listFile = explode(',', $request->id_file);
            if ($listFile) {
                $document = Documents::where('model_name', 'Contract')->whereIn('id', $listFile);
                if ($document->count() > 0) {
                    $a['model_id'] = $objContractParent->id;
                    $document->update($a);
                }
            }
            if (isset($request->director)) {
                $assetShowsParent = new AssetContractShow();
                foreach ($request->director as $value) {
                    $listImport = [];
                    if (isset($request['structure_id'])) {
                        $listImport['structure_id'] = $request['structure_id'];
                        $listImport['structure_owner'] = $request['structure_id'];
                    }
                    if (isset($objContractParent->id)) {
                        $listImport['contract_id'] = $objContractParent->id;
                    }
                    if (isset($request['company_id'])) {
                        $listImport['company_owner'] = $request['company_id'];
                    }
                    if (isset($value['receiver_handover'])) {
                        $listImport['receiver_handover'] = strip_tags($value['receiver_handover']);
                    }
                    if (isset($value['name_asset'])) {
                        $listImport['name'] = strip_tags($value['name_asset']);
                    }
                    if (isset($value['group_id'])) {
                        $listImport['group_id'] = $value['group_id'];
                    }
                    if (isset($value['material'])) {
                        $listImport['material'] = strip_tags($value['material']);
                    }
                    if (isset($value['color'])) {
                        $listImport['color'] = strip_tags($value['color']);
                    }
                    if (isset($value['warranty'])) {
                        $listImport['warranty'] = strip_tags($value['warranty']);
                    }
                    if (isset($value['quantity'])) {
                        $listImport['quantity'] = strip_tags($value['quantity']);
                    }
                    if (isset($value['unit_id'])) {
                        $listImport['unit_id'] = $value['unit_id'];
                    }
                    if (isset($value['producer'])) {
                        $listImport['producer'] = strip_tags($value['producer']);
                    }
                    if (isset($value['year_use'])) {
                        $listImport['year_use'] = strip_tags($value['year_use']);
                    }
                    if (isset($value['date_import'])) {
                        $listImport['date_import'] = strip_tags($value['date_import']);
                    }
                    if (isset($value['status'])) {
                        $listImport['status'] = $value['status'];
                    }
                    if (isset($value['type'])) {
                        $listImport['type'] = $value['type'];
                    }
                    if (isset($value['description'])) {
                        $listImport['description'] = $value['description'];
                    }
                    if (isset($value['vat']) && filter_var($value['vat'], FILTER_SANITIZE_NUMBER_INT)) {
                        $vat = $value['vat'];
                    } else {
                        $vat = NULL;
                    }
                    if (isset($value['price']) && filter_var($value['price'], FILTER_SANITIZE_NUMBER_INT)) {
                        $price = strip_tags($value['price']);
                    } else {
                        $price = NULL;
                    }
                    $listImport['vat'] = $vat;
                    $listImport['price'] = $price;
                    $listImport['created_by'] = Auth::user()->id;

                    $assetShowsParent->create($listImport);
                }
            }

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $objContractParent->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->route('contract.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Contract::find($id);
        $mContractAssetShow = new AssetContractShow();
        $dataContractShow = $mContractAssetShow->where('contract_id', $id)->get();

        if (request()->wantsJson()) {

            return response()->json([
                'data'         => $data,
                'dataContractShow' => $dataContractShow,
            ]);
        }

        return view($this->partView .'.show', compact('data', 'dataContractShow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('contract.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data = $this->repository->find($id);
        $listAsset    = Asset::all();
        $listPosition = Position::all()->pluck('name', 'id')->toArray();
        $listCity     = DB::table('provinces')->pluck('name', 'city_id')->toArray();
        $listPartner  = Partner::all()->pluck('name', 'id')->toArray();
        $listGroup    = Group::all()->pluck('name', 'id')->toArray();
        $listUnit     = Unit::all()->pluck('name', 'id')->toArray();
        $listCompany  = Company::all();
        return view($this->partView.'.edit', compact('data','listAsset', 'listPosition',
            'listPartner', 'listGroup', 'listCity', 'listUnit', 'listCompany'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ContractUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ContractUpdateRequest $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $request['updated_by'] = Auth::user()->id;
            $objContractParent = $this->repository->update(
                array(
                    Contract::NAME => $request->name,
                    Contract::CONTRACT_CODE => $request->contract_code,
                    Contract::SIGN_DATE => $request->sign_date,
                    Contract::PARTNER_ID => $request->partner_id,
                    Contract::FILE_ID => $request->id_file
                ), $id);
            $file = $request->id_file;
            $objContractParent['file_id'] = $file;
            $objContractParent->update();
            $listFile = explode(',', $request->id_file);
            if ($listFile) {
                $document = Documents::where('model_name', 'Contract')->whereIn('id', $listFile);
                if ($document->count() > 0) {
                    $a['model_id'] = $objContractParent->id;
                    $document->update($a);
                }
            }
            if (isset($request->director)) {
                $listContract = [];
                $assetsContract = [];
                $assets = new Asset();
                foreach ($request->director as $value) {
                    $number_asset = $value['quantity'];
                    for ($i = 1; $i <= $number_asset; $i++) {
                        $assetsContract['created_by'] = Auth::user()->id;
                        $assetsContract['vat'] = $value['vat'];
                        $assetsContract['name'] = $value['name_asset'];
                        $assetsContract['price'] = $value['price'];
                        $assetsContract['group_id'] = $value['group_id'];
                        $assetsContract['total_money'] = $value['total_money'];
                        $assetsContract['warranty'] = $value['warranty'];
                        $assetsContract['type'] = $value['type'];
                        $assetsContract['partner_id'] = $objContractParent->partner_id;
                        $assetsContract['contract_id'] = $objContractParent->id;
                        $assetsContract['sign_date'] = Carbon::createFromFormat('Y-m-d', $objContractParent->sign_date)->format('Y-m-d');
                        $assets->create($assetsContract);
                    }
                }
            }
            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $objContractParent->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('contract.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('contract.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('contract.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data         = Contract::all();
        $listAsset    = Asset::all();
        $listPosition = Position::all()->pluck('name', 'id')->toArray();
        $listCity     = DB::table('provinces')->pluck('name', 'city_id')->toArray();
        $listPartner = Partner::all()->pluck('name', 'id')->toArray();
        $listGroup   = Group::all()->pluck('name', 'id')->toArray();
        $listUnit    = Unit::all()->pluck('name', 'id')->toArray();
        $listCompany = Company::all();
        return view($this->partView.'.create', compact('data','listAsset', 'listPosition',
            'listPartner', 'listGroup', 'listCity', 'listUnit', 'listCompany'));
    }

    public function postUploadAttachment(Request $request)
    {
        $file = $request->file;
        $attachment[Documents::URL] = uploadFile($file, FILE_CONTRACT);
        $attachment[Documents::MODEL_NAME] = 'Contract';
        $attachment[Documents::NAME] = $file->getClientOriginalName();

        $obj = Documents::create($attachment);
        $this->listImage[] = $obj->id;

        return response()->json([
            'message' => trans('messages.upload_success'),
            'data' => $obj,
        ]);
    }

    public function postDeleteAttachment(Request $request)
    {
        $file = $request->all();
        $data = Documents::find($file['id']);
        if ($data) {
            $data->delete();
            unlink(public_path() . $file['url']);
        };
        return response()->json([
            'message' => trans('messages.delete_success'),
            'status' => true,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDistrictOnCity($id) {
        $listCity = DB::table('districts')->where('city_id', $id)->pluck('name', 'id');
        return response()->json([
            'data' => $listCity,
        ]);
    }
    public function getPartnerContract($id) {
        $data = Partner::find($id);
        $listAsset    = Asset::all();
        $listPosition = Position::all()->pluck('name', 'id')->toArray();
        $listCity     = DB::table('provinces')->pluck('name', 'city_id')->toArray();
        $listPartner = Partner::all()->pluck('name', 'id')->toArray();
        $listGroup   = Group::all()->pluck('name', 'id')->toArray();
        $listUnit    = Unit::all()->pluck('name', 'id')->toArray();
        $listCompany = Company::all();
        return view($this->partView.'.partner-contract', compact('data','listAsset', 'listPosition', 'listPartner', 'listGroup', 'listCity', 'listUnit', 'listCompany'));
    }

    public function validateInput($input, $listRule, $customAtr)
    {
        $checkValidate = Validator::make($input, $listRule, [], $customAtr);
        if ($checkValidate->fails()) {
            $errors = $checkValidate->errors();
            return $errors;
        } else {
            return false;
        }
    }

    public function downloadFile($id)
    {
        $doc = Documents::find($id);
        if ($doc->count() > 0 && file_exists(public_path() . $doc->file_url)) {

            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: application/octet-stream");
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:" . filesize(public_path() . $doc->file_url));
            header("Content-Disposition: attachment; filename=" . $doc->name);
            readfile(public_path() . $doc->file_url);
        } else {
        }
    }

}
