<?php

namespace App\Http\Controllers\Invest;

use App\Http\Controllers\Controller;
use App\Http\Requests\Invest\AssetPackageCreateRequest;
use App\Http\Requests\Invest\AssetPackageUpdateRequest;
use App\Models\Contracts\Contract;
use App\Models\Invest\AssetPackage;
use App\Models\Partners\Partner;
use App\Repositories\Invest\AssetPackageRepository;
use App\Validators\Invest\AssetPackageValidator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class AssetPackagesController.
 *
 * @package namespace App\Http\Controllers;
 */
class AssetPackagesController extends Controller
{
    /**
     * @var AssetPackageRepository
     */
    protected $repository;

    /**
     * @var AssetPackageValidator
     */
    protected $validator;

    protected $partView;

    /**
     * AssetPackagesController constructor.
     *
     * @param AssetPackageRepository $repository
     * @param AssetPackageValidator $validator
     */
    public function __construct(AssetPackageRepository $repository, AssetPackageValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->partView = 'administrator.invest.package';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->partView . '.index', compact('data'));
    }


    public function create()
    {

        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        return view($this->partView . '.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AssetPackageCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $input = $request->all();
        try {
            $this->validator->with($input)->passesOrFail(ValidatorInterface::RULE_CREATE);
            $input[AssetPackage::CREATED_BY] = Auth::user()->id;
            $assetPackage = $this->repository->create($input);

            $response = [
                'message' => 'Thêm mới gói thầu thành công',
                'data' => $assetPackage->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }
            return redirect()->route('package.index')->with('message', $response['message']);

        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->with('message', $response['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assetPackage = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $assetPackage,
            ]);
        }

        return view('assetPackages.show', compact('assetPackage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->repository->find($id);

        return view($this->partView . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AssetPackageUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AssetPackageUpdateRequest $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $input = $request->all();
            $input[AssetPackage::UPDATED_BY] = Auth::user()->id;
            $assetPackage = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'AssetPackage updated.',
                'data' => $assetPackage->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('package.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'AssetPackage deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Xóa gói thầu thành công');
    }

    public function getContractByPackage($id)
    {
        $data_packages = AssetPackage::where('id', $id)->first();
        $package_params = isset($data_packages->params['list_contract']) ? $data_packages->params['list_contract'] : [];
        $data = Contract::whereIn('id', $package_params)->paginate();
        return view($this->partView . '.list_map_contract', compact('data', 'id', 'data_packages'));
    }

    public function addContractByPackage($id)
    {
        $data = Contract::paginate();
        $data_packages = AssetPackage::where('id', $id)->first();

        $package_params = isset($data_packages->params['list_contract']) ? $data_packages->params['list_contract'] : [];
        return view($this->partView . '.add_contract_to_package', compact('data', 'id', 'package_params'));
    }

    public function addContract(Request $request)
    {
        $input = $request->all();
        $id = $input['package_id'];
        $contract_id = (int)$input['contract_id'];

        $check_package = AssetPackage::where('id', $id)->first();

        $list_contract = isset($check_package->params['list_contract']) ? $check_package->params['list_contract'] : [];
        if (empty($list_contract)) {
            AssetPackage::where('id', $id)
                ->update(
                    array(
                        AssetPackage::PARAMS => '{"list_contract":[' . $contract_id . ']}',
                        AssetPackage::UPDATED_BY => Auth::user()->id,
                    ));
        } else {
            if (!in_array($contract_id, $list_contract)) {
                AssetPackage::where('id', $id)
                    ->update(
                        array(
                            AssetPackage::PARAMS => DB::raw("JSON_ARRAY_APPEND(params, '$.list_contract', " . $contract_id . ")"),
                            AssetPackage::UPDATED_BY => Auth::user()->id,
                        ));
            }
        }
        return redirect()->back()->with('message', 'Thêm mới thành công');
    }

    public function deleteContract(Request $request)
    {
        $input = $request->all();
        $id = $input['package_id'];
        $contract_id = (int)$input['contract_id'];

        $check_package = AssetPackage::where('id', $id)->first();
        $list_contract = isset($check_package->params['list_contract']) ? $check_package->params['list_contract'] : [];

        if (($key = array_search($contract_id, $list_contract)) !== false) {
            unset($list_contract[$key]);
            if (empty($list_contract)) {
                AssetPackage::where('id', $id)
                    ->update(
                        array(
                            AssetPackage::PARAMS => NULL,
                            AssetPackage::UPDATED_BY => Auth::user()->id,
                        ));
            } else {
                AssetPackage::where('id', $id)
                    ->update(
                        array(
                            AssetPackage::PARAMS => '{"list_contract":' . json_encode($list_contract) . '}',
                            AssetPackage::UPDATED_BY => Auth::user()->id,
                        ));
            }
        }

        return redirect()->back()->with('message', 'Xóa nhà thầu thành công');
    }

}
