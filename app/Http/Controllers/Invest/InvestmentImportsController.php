<?php

namespace App\Http\Controllers\Invest;

use App\Http\Controllers\Controller;
use App\Models\Assets\Asset;
use App\Models\Assets\AssetImport;
use App\Models\Assets\Unit;
use App\Models\Contracts\Contract;
use App\Models\Partners\Group;
use App\Models\Partners\Partner;
use App\Models\Systems\Company;
use App\Models\Systems\StructureCompany;
use App\Validators\Assets\AssetImportValidator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\Assets\AssetImportCreateRequest;
use App\Http\Requests\Assets\AssetImportUpdateRequest;
use App\Repositories\Assets\AssetImportRepository;
use PHPExcel_Settings;

/**
 * Class AssetImportsController.
 *
 * @package namespace App\Http\Controllers;
 */
class InvestmentImportsController extends Controller
{
    /**
     * @var AssetImportRepository
     */
    protected $repository;

    /**
     * @var AssetImportValidator
     */
    protected $validator;

    protected $partView;

    protected $asset;

    /**
     * AssetImportsController constructor.
     *
     * @param AssetImportRepository $repository
     * @param AssetImportValidator $validator
     */
    public function __construct(AssetImportRepository $repository, AssetImportValidator $validator, Asset $asset)
    {
        $this->repository = $repository;

        $this->validator = $validator;
        $this->asset = $asset;
        $this->partView = 'administrator.invest.imports';

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $assetImports = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $assetImports,
            ]);
        }

        return view($this->partView.'.index', compact('assetImports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AssetImportCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(AssetImportCreateRequest $request)
    {
        try {
            // Chỉ sử dụng store không update, delete
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $request['created_by'] = Auth::user()->id;
            $assetImportParent = $this->repository->create($request->all());

            if (isset($request->director)) {
                foreach ($request->director as $value) {
                    $listRule = [
                        'name_asset' => 'required',
                        'group_id' => 'required|numeric',
                        'status' => 'required',
                        'type' => 'required',
                        'original' => 'required',
                    ];
                    $fieldVN = [
                        'name_asset' => 'tên tài sản',
                        'group_id' => 'nhóm tài sản',
                        'status' => 'tình trạng sử dụng',
                        'type' => 'phân loại tài sản',
                        'original' => 'hiện trạng tài sản',
                    ];

                    $checkValidation = $this->validateInput($value, $listRule, $fieldVN);
                    if ($checkValidation) {
                        return redirect()->back()->withErrors($checkValidation->getMessageBag())->withInput();
                    }
                    $assets = new Asset();
                    $listImport = [];
                    if (isset($request['structure_id'])) {
                        $listImport['structure_id'] = $request['structure_id'];
                        $listImport['structure_owner'] = $request['structure_id'];
                    }
                    if (isset($assetImportParent->id)) {
                        $listImport['contract_id'] = $assetImportParent->id;
                    }
                    if (isset($request['company_id'])) {
                        $listImport['company_owner'] = $request['company_id'];
                    }
                    if (isset($value['receiver_handover'])) {
                        $listImport['receiver_handover'] = strip_tags($value['receiver_handover']);
                    }
                    if (isset($value['name_asset'])) {
                        $listImport['name'] = strip_tags($value['name_asset']);
                    }
                    if (isset($value['group_id'])) {
                        $listImport['group_id'] = $value['group_id'];
                    }
                    if (isset($value['material'])) {
                        $listImport['material'] = strip_tags($value['material']);
                    }
                    if (isset($value['color'])) {
                        $listImport['color'] = strip_tags($value['color']);
                    }
                    if (isset($value['warranty'])) {
                        $listImport['warranty'] = strip_tags($value['warranty']);
                    }
                    if (isset($value['quantity'])) {
                        $listImport['quantity'] = strip_tags($value['quantity']);
                    }
                    if (isset($value['unit_id'])) {
                        $listImport['unit_id'] = $value['unit_id'];
                    }
                    if (isset($value['producer'])) {
                        $listImport['producer'] = strip_tags($value['producer']);
                    }
                    if (isset($value['year_use'])) {
                        $listImport['year_use'] = strip_tags($value['year_use']);
                    }
                    if (isset($value['date_import'])) {
                        $listImport['date_import'] = strip_tags($value['date_import']);
                    }
                    if (isset($value['status'])) {
                        $listImport['status'] = $value['status'];
                    }
                    if (isset($value['original'])) {
                        $listImport['original'] = $value['original'];
                    }
                    if (isset($value['type'])) {
                        $listImport['type'] = $value['type'];
                    }
                    if (isset($value['description'])) {
                        $listImport['description'] = $value['description'];
                    }
                    if (isset($value['vat']) && filter_var($value['vat'], FILTER_SANITIZE_NUMBER_INT)) {
                        $vat = $value['vat'];
                    } else {
                        $vat = NULL;
                    }
                    if (isset($value['price']) && filter_var($value['price'], FILTER_SANITIZE_NUMBER_INT)) {
                        $price = strip_tags($value['price']);
                    } else {
                        $price = NULL;
                    }
                    $listImport['vat'] = $vat;
                    $listImport['price'] = $price;
                    $listImport['created_by'] = Auth::user()->id;

                    $assetImportParent->update($listImport);
                    $number_asset = $value['quantity'];
                    $unit_lower = '';
                    $unit_name = Unit::where('id', $value['unit_id'])->first();
                    if ($unit_name) {
                        $unit_lower = mb_strtolower(trim_all($unit_name->name), 'UTF-8');
                    }
                    if (in_array($unit_lower, UNIT_MEASUREMENT)) {
                        $assets->create($listImport);
                    } else {
                        for ($i = 1; $i <= $number_asset; $i++) {
                            $listImport['quantity'] = 1;
                            $objAssset = $assets->create($listImport);

                            // Create asset_code
                            $groupCode = Group::where('id', $value['group_id'])->first();
                            if ($groupCode) {
                                $asset_code = $groupCode->group_code . '-' . $objAssset->id;
                            }
                            $assets->where('id', $objAssset->id)->update(['asset_code' => $asset_code]);
                        }
                    }
                }
            }
            $response = [
                'message' => trans('messages.create_success'),
                'data' => $assetImportParent->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }
            return redirect()->route('investment.index')->with('message', $response['message']);

        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assetImport = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $assetImport,
            ]);
        }

        return view('assetImports.show', compact('assetImport'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->repository->find($id);
        $listPartner = Partner::all()->pluck('name', 'id')->toArray();
        $listGroup = Group::all()->pluck('name', 'id')->toArray();
        $listUnit = Unit::all()->pluck('name', 'id')->toArray();
        $listContract = Contract::all()->pluck('contract_code', 'id')->toArray();
        $listCompany = Company::all();
        $listStructer = [];
        if (isset($listCompany[0])) {
            $listStructer = StructureCompany::getHierarchy($listCompany[0]->id);
            if (!$listStructer) {
                $listStructer = [];
            }
        }
        return view($this->partView . '.edit', compact('data', 'listPartner', 'listGroup',
            'listUnit', 'listContract', 'listCompany', 'listStructer'));
    }

    /**listContract
     * Update the specified resource in storage.
     *
     * @param  AssetImportUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AssetImportUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $assetImport = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'AssetImport updated.',
                'data' => $assetImport->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'AssetImport deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'AssetImport deleted.');
    }

    public function create()
    {
        $listPartner = Partner::all()->pluck('name', 'id')->toArray();
        $listGroup = Group::all()->pluck('name', 'id')->toArray();
        $listUnit = Unit::all()->pluck('name', 'id')->toArray();
        $listContract = Contract::all()->pluck('contract_code', 'id')->toArray();
        $listCompany = Company::all();
        $listStructer = [];
        if (isset($listCompany[0])) {
            $listStructer = StructureCompany::getHierarchy($listCompany[0]->id);
            if (!$listStructer) {
                $listStructer = [];
            }
        }
        return view($this->partView . '.create', compact('listPartner', 'listGroup',
            'listContract', 'listUnit', 'listCompany', 'listStructer'));
    }

    public function validateInput($input, $listRule, $customAtr)
    {
        $checkValidate = Validator::make($input, $listRule, [], $customAtr);
        if ($checkValidate->fails()) {
            $errors = $checkValidate->errors();
            return $errors;
        } else {
            return false;
        }
    }

    public function getImportExcelForm()
    {
        $listPartner = Partner::all()->pluck('name', 'id')->toArray();
        $listGroup = Group::all()->pluck('name', 'id')->toArray();
        $listUnit = Unit::all()->pluck('name', 'id')->toArray();
        $listContract = Contract::all()->pluck('name', 'id')->toArray();
        $listCompany = Company::all();
        $listStructer = [];
        if (isset($listCompany[0])) {
            $listStructer = StructureCompany::getHierarchy($listCompany[0]->id);
        }
        return view($this->partView . '.import', compact('listPartner', 'listGroup',
            'listContract', 'listUnit', 'listCompany', 'listStructer'));
    }

    public function postImportExcel(Request $request)
    {
        PHPExcel_Settings::setZipClass(PHPExcel_Settings::ZIPARCHIVE);
        $listFail = [];
        if ($request->hasFile('import_file')) {
            $validator = Validator::make($request->all(), [
                //or this
                'import_file' => 'required|max:50000|mimetypes:application/csv,application/excel,' .
                    'application/vnd.ms-excel, application/vnd.msexcel,' .
                    'text/csv, text/anytext, text/plain, text/x-c,' .
                    'text/comma-separated-values,' .
                    'inode/x-empty,' .
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ]);
            if ($validator->fails()) {
                $listFail[] = 'File import phải là file Excel';
                return redirect()->back()->with('error', trans('Lỗi import dữ liệu'))
                    ->with('list-fail', $listFail);
            }

            $check_success = false;
            $path_temp = $request->file('import_file')->getRealPath();
            if (!$path_temp) {
                $path_temp = $request->file('import_file');
            }
            Excel::load($path_temp, function ($reader) use (&$listFail, &$check_success, $request) {
                $data = [];
                $company_id = $request->get('company_id', null);
                $structure_id = $request->get('structure_id', null);
                $area_company = $request->get('area_company', null);
                if (!isset($reader->toArray()[0])) {
                    $listFail[] = 'File import không đúng mẫu';
                    return false;
                }
                $row_check = $reader->toArray()[0];
                if (!array_key_exists('ten_tai_san', $row_check) ||
                    !array_key_exists('so_luong', $row_check)
                ) {
                    $listFail[] = 'File import không đúng mẫu';
                    return false;
                }
                foreach ($reader->toArray() as $item) {
                    if (!isset($item['ten_tai_san']) ||
                        !isset($item['so_luong'])
                    ) {
                        $listFail[] = 'Thiếu dữ liệu đầu vào';
                        continue;
                    }

                    $quantity_asset = $item['so_luong'];

                    $data_import = [
                        AssetImport::NAME => trim_all($item['ten_tai_san']),
                        AssetImport::QUANTITY => (int)$quantity_asset,
                    ];

                    $group = null;
                    if (isset($item['ma_nhom_tai_san']) && trim_all($item['ma_nhom_tai_san']) !== '') {
                        $group = Group::whereRaw('LOWER(`group_code`) = ? ',
                            [
                                mb_strtolower(trim_all($item['ma_nhom_tai_san']), 'UTF-8'),
                            ])->first();
                        if ($group) {
                            $data_import[AssetImport::GROUP_ID] = $group->id;
                        }
                    }

                    if (isset($item['so_hieu_hd']) && trim_all($item['so_hieu_hd']) !== '') {
                        $contract = Contract::whereRaw('LOWER(`contract_code`) = ? ',
                            [
                                mb_strtolower(trim_all($item['so_hieu_hd']), 'UTF-8'),
                            ])
                            ->first();
                        $data_import[AssetImport::CONTRACT_CODE] = trim_all($item['so_hieu_hd']);
                        if ($contract) {
                            $data_import[AssetImport::CONTRACT_ID] = $contract->id;
                        }
                    }
                    if (isset($item['loai_tai_san']) && trim_all($item['loai_tai_san']) !== '') {
                        $type = array_search(trim_all($item['loai_tai_san']), Asset::$assetClassification);
                        if ($type) {
                            $data_import[AssetImport::TYPE] = $type;
                        }
                    }
                    if (isset($item['tinh_trang_su_dung']) && trim_all($item['tinh_trang_su_dung']) !== '') {
                        $status = array_search(trim_all($item['tinh_trang_su_dung']), Asset::$assetListStatus);
                        if ($status) {
                            $data_import[AssetImport::STATUS] = $status;
                        }
                    }

                    if (isset($item['don_vi']) && trim_all($item['don_vi']) !== '') {
                        $unit = Unit::whereRaw('LOWER(`name`) = ? ',
                            [
                                mb_strtolower(trim_all($item['don_vi']), 'UTF-8'),
                            ])->first();
                        if ($unit) {
                            $data_import[AssetImport::UNIT_ID] = $unit->id;
                        }
                    }
                    if (isset($item['chat_lieu'])) {
                        $data_import[AssetImport::MATERIAL] = trim_all($item['chat_lieu']);
                    }
                    if (isset($item['mau_sac'])) {
                        $data_import[AssetImport::COLOR] = trim_all($item['mau_sac']);
                    }
                    if (isset($item['don_vi_cung_cap'])) {
                        $data_import[AssetImport::PRODUCER] = trim_all($item['don_vi_cung_cap']);
                    }
                    if (isset($item['nam_dua_vao_sd'])) {
                        $data_import[AssetImport::YEAR_USE] = trim_all($item['nam_dua_vao_sd']);
                    }
                    if (isset($item['ngay_nhap_ts'])) {
                        $data_import[AssetImport::DATE_IMPORT] = $item['ngay_nhap_ts'];
                    }
                    if (isset($item['don_gia'])) {
                        $data_import[AssetImport::PRICE] = $item['don_gia'];
                    }
                    if (isset($item['vat'])) {
                        $data_import[AssetImport::VAT] = $item['vat'];
                    }
                    if (isset($item['ghi_chu'])) {
                        $data_import[AssetImport::DESCRIPTION] = $item['ghi_chu'];
                    }
                    if (isset($item['bao_hanh'])) {
                        $data_import[AssetImport::WARRANTY] = $item['bao_hanh'];
                    }
                    if (isset($item['nguoi_nhan_ban_giao'])) {
                        $data_import[AssetImport::RECEIVER_HANDOVER] = trim_all($item['nguoi_nhan_ban_giao']);
                    }
                    if ($company_id) {
                        $data_import[AssetImport::COMPANY_ID] = $company_id;
                        $data_import[Asset::COMPANY_OWNER] = $company_id;
                    }
                    if ($structure_id) {
                        $data_import[AssetImport::STRUCTURE_ID] = $structure_id;
                        $data_import[Asset::STRUCTURE_OWNER] = $structure_id;
                    }

                    $data_import[AssetImport::CREATED_BY] = Auth::user()->id;

                    $objImport = AssetImport::create($data_import);
                    unset($data_import['so_luong']);
                    $unit_lower = mb_strtolower(trim_all($item['don_vi']), 'UTF-8');
                    if (in_array($unit_lower, UNIT_MEASUREMENT)) {
                        $obj_asset = Asset::create($data_import);
                    } else {
                        for ($i = 1; $i <= $quantity_asset; $i++) {
                            $data_import[AssetImport::QUANTITY] = 1;
                            $obj_asset = Asset::create($data_import);
                            if ($group) {
                                $asset_code = $group->group_code . '-' . $obj_asset->id;
                                Asset::where('id', $obj_asset->id)->update(['asset_code' => $asset_code]);
                            }
                        }
                    }
                }
                $check_success = true;
                return redirect()->route('assets.index')->with('message', trans('messages.import_success'))
                    ->with('list-fail', $listFail);
            });
            if (count($listFail) && !$check_success) {
                return redirect()->back()->with('error', trans('Lỗi import'))->with('list-fail', $listFail);
            } else {
                return redirect()->route('assets.index')->with('message', trans('messages.import_success'));
            }
        }
        return redirect()->back()->with('error', trans('Không tồn tại file import'));
    }
}