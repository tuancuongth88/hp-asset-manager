<?php

namespace App\Http\Controllers\Invest;

use App\Http\Controllers\Controller;
use App\Http\Requests\Assets\AssetCreateRequest;
use App\Http\Requests\Assets\AssetUpdateRequest;
use App\Models\Assets\Asset;
use App\Models\Assets\Unit;
use App\Models\Contracts\Contract;
use App\Models\Documents\Documents;
use App\Models\Partners\Group;
use App\Models\Partners\Partner;
use App\Models\Systems\Company;
use App\Models\Systems\StructureCompany;
use App\Repositories\Assets\AssetRepository;
use App\Validators\Assets\AssetValidator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class InvestmentsController.
 *
 * @package namespace App\Http\Controllers;
 */
class InvestmentsController extends Controller
{
    /**
     * @var AssetRepository
     */
    protected $repository;

    /**
     * @var AssetValidator
     */
    protected $validator;

    protected $partView;

    protected $asset;

    /**
     * AssetsController constructor.
     *
     * @param AssetRepository $repository
     * @param AssetValidator $validator
     * @param Asset $asset
     */
    public function __construct(AssetRepository $repository, AssetValidator $validator, Asset $asset)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->asset = $asset;
        $this->partView = 'administrator.invest.investment';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!Auth::user()->can('assets.view')) {
            abort(506, trans('messages.you_do_not_have_permission'));
        }

        $input = $request->all();
        $name = isset($input['name']) ? $input['name'] : '';
        $group_id = isset($input['group_id']) ? $input['group_id'] : 0;
        $type = isset($input['type']) ? $input['type'] : '';
        $status = isset($input['status']) ? $input['status'] : '';
        $name_partner = isset($input['name_partner']) ? $input['name_partner'] : '';
        $company_owner = isset($input['company_owner']) ? $input['company_owner'] : null;
        $owner = isset($input['owner']) ? $input['owner'] : null;
        $structure_owner = isset($input['structure_owner']) ? $input['structure_owner'] : null;

        // Search trong trang index asset
        $mAsset = new Asset();
        if (trim($name) !== '') {
            $mAsset = $mAsset->where('name', 'like', '%' . $name . '%');
        }
        if ($group_id) {
            $mAsset = $mAsset->where('group_id', $group_id);
        }
        $mAsset = $mAsset->where('type', Asset::TYPE_3);
        if ($status !== '') {
            $mAsset = $mAsset->where('status', $status);
        }
        if (trim($name_partner) !== '') {
            $partner = Partner::where('name', 'like', '%' . $name_partner . '%')->pluck('id');
            $mAsset = $mAsset->whereIn(Asset::PARTNER_ID, $partner);
        }
        if ($company_owner) {
            $mAsset = $mAsset->where('company_owner', $company_owner);
        }
        if ($owner) {
            $mAsset = $mAsset->where('owner', $owner);
        }
        if ($structure_owner) {
            $mAsset = $mAsset->where('structure_owner', $structure_owner);
        }
        $data = $mAsset->paginate();

        $listPartner = Partner::all()->pluck('name', 'id')->toArray();
        $listGroup = Group::all();
        $listContract = Contract::all()->pluck('name', 'id')->toArray();
        $listCompany = Company::all()->pluck('name', 'id')->toArray();
        $typeAsset = Asset::$assetClassification;
        $list_structure = [];
        if ($company_owner) {
            $structure = StructureCompany::getHierarchy($company_owner);
            if ($structure) {
                $list_structure = $structure;
            }
        }

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->partView . '.index',
            compact('data', 'listGroup', 'listContract',
                'listCompany', 'list_structure', 'listPartner', 'typeAsset'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AssetCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(AssetCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $request['created_by'] = Auth::user()->id;
            $asset = $this->repository->create($request->all());

            if (isset($request->director)) {
                foreach ($request->director as $value) {
                    $listRule = [
                        'name_asset' => 'required',
                        'group_id' => 'required|numeric',
                        'status' => 'required',
                    ];
                    $fieldVN = [
                        'name_asset' => 'tên tài sản',
                        'group_id' => 'nhóm tài sản',
                        'status' => 'tình trạng sử dụng',
                    ];
                    $checkValidation = $this->validateInput($value, $listRule, $fieldVN);
                    if ($checkValidation) {
                        return redirect()->back()->withErrors($checkValidation->getMessageBag())->withInput();
                    }
                    $listAsset = [];
                    if (isset($request['structure_id'])) {
                        $listAsset['structure_id'] = $request['structure_id'];
                    }
                    if (isset($request['company_id'])) {
                        $listAsset['company_owner'] = $request['company_id'];
                    }
                    if (isset($request->receiver_handover)) {
                        $listAsset['receiver_handover'] = strip_tags($value['receiver_handover']);
                    }
                    if (isset($value['name_asset'])) {
                        $listAsset['name'] = strip_tags($value['name_asset']);
                    }
                    if (isset($value['group_id'])) {
                        $listAsset['group_id'] = $value['group_id'];
                    }
                    if (isset($value['material'])) {
                        $listAsset['material'] = strip_tags($value['material']);
                    }
                    if (isset($value['color'])) {
                        $listAsset['color'] = strip_tags($value['color']);
                    }
                    if (isset($value['warranty'])) {
                        $listAsset['warranty'] = strip_tags($value['warranty']);
                    }
                    if (isset($value['quantity'])) {
                        $listAsset['quantity'] = strip_tags($value['quantity']);
                    }
                    if (isset($value['unit_id'])) {
                        $listAsset['unit_id'] = $value['unit_id'];
                    }
                    if (isset($value['producer'])) {
                        $listAsset['producer'] = strip_tags($value['producer']);
                    }
                    if (isset($value['year_use'])) {
                        $listAsset['year_use'] = strip_tags($value['year_use']);
                    }
                    if (isset($value['date_import'])) {
                        $listAsset['date_import'] = strip_tags($value['date_import']);
                    }
                    if (isset($value['contract_id'])) {
                        $listAsset['contract_id'] = $value['contract_id'];
                    }
                    if (isset($value['status'])) {
                        $listAsset['status'] = $value['status'];
                    }
                    if (isset($value['type'])) {
                        $listAsset['type'] = $value['type'];
                    }
                    if (isset($value['description'])) {
                        $listAsset['description'] = $value['description'];
                    }
                    if (isset($value['vat']) && filter_var($value['vat'], FILTER_SANITIZE_NUMBER_INT)) {
                        $vat = $value['vat'];
                    } else {
                        $vat = NULL;
                    }
                    if (isset($value['price']) && filter_var($value['price'], FILTER_SANITIZE_NUMBER_INT)) {
                        $price = strip_tags($value['price']);
                    } else {
                        $price = NULL;
                    }

                    $listImport['vat'] = $vat;
                    $listImport['price'] = $price;
                    $asset->update($listAsset);
                }
            }

            $response = [
                'message' => trans('messages.create_success'),
                'data' => $asset->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }
            return redirect()->route($this->partView.'.index')->with('message', $response['message']);

        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asset = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $asset,
            ]);
        }

        return view('assets.show', compact('asset'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('assets.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }

        $data = $this->repository->find($id);

        $listPartner = Partner::all()->pluck('name', 'id')->toArray();
        $listGroup = Group::all()->pluck('name', 'id')->toArray();
        $listUnit = Unit::all()->pluck('name', 'id')->toArray();
        $listContract = Contract::all()->pluck('contract_code', 'id')->toArray();
        $listCompany = Company::all()->pluck('name', 'id')->toArray();

        return view($this->partView . '.edit', compact('data', 'listPartner', 'listGroup',
            'listContract', 'listUnit', 'listCompany'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AssetUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AssetUpdateRequest $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $request['updated_by'] = Auth::user()->id;
            $asset = $this->repository->update($request->all(), $id);
            if (isset($request->director)) {
                $listAsset = [];

                // Update request directior
                foreach ($request->director as $value) {
                    $listRule = [
                        'name_asset' => 'required',
                        'group_id' => 'required|numeric',
                        'status' => 'required',
                        'type' => 'required',
                        'original' => 'required',
                    ];
                    $fieldVN = [
                        'name_asset' => 'tên tài sản',
                        'group_id' => 'nhóm tài sản',
                        'status' => 'tình trạng sử dụng',
                        'type' => 'phân loại tài sản',
                        'original' => 'hiện trạng tài sản',
                    ];
                    $listAsset = [];
                    if (isset($value['receiver_handover'])) {
                        $listAsset['receiver_handover'] = strip_tags($value['receiver_handover']);
                    }
                    if (isset($value['name_asset'])) {
                        $listAsset['name'] = strip_tags($value['name_asset']);
                    }
                    if (isset($value['group_id'])) {
                        $listAsset['group_id'] = $value['group_id'];
                    }
                    if (isset($value['material'])) {
                        $listAsset['material'] = strip_tags($value['material']);
                    }
                    if (isset($value['color'])) {
                        $listAsset['color'] = strip_tags($value['color']);
                    }
                    if (isset($value['warranty'])) {
                        $listAsset['warranty'] = strip_tags($value['warranty']);
                    }
                    if (isset($value['quantity'])) {
                        $listAsset['quantity'] = strip_tags($value['quantity']);
                    }
                    if (isset($value['unit_id'])) {
                        $listAsset['unit_id'] = $value['unit_id'];
                    }
                    if (isset($value['producer'])) {
                        $listAsset['producer'] = strip_tags($value['producer']);
                    }
                    if (isset($value['year_use'])) {
                        $listAsset['year_use'] = strip_tags($value['year_use']);
                    }
                    if (isset($value['date_import'])) {
                        $listAsset['date_import'] = strip_tags($value['date_import']);
                    }
                    if (isset($value['contract_id'])) {
                        $listAsset['contract_id'] = $value['contract_id'];
                    }
                    if (isset($value['status'])) {
                        $listAsset['status'] = $value['status'];
                    }
                    if (isset($value['original'])) {
                        $listAsset['original'] = $value['original'];
                    }
                    if (isset($value['type'])) {
                        $listAsset['type'] = $value['type'];
                    }
                    if (isset($value['description'])) {
                        $listAsset['description'] = $value['description'];
                    }
                    if (isset($value['vat']) && filter_var($value['vat'], FILTER_SANITIZE_NUMBER_INT)) {
                        $vat = $value['vat'];
                    } else {
                        $vat = NULL;
                    }
                    if (isset($value['price']) && filter_var($value['price'], FILTER_SANITIZE_NUMBER_INT)) {
                        $price = strip_tags($value['price']);
                    } else {
                        $price = NULL;
                    }

                    $listImport['vat'] = $vat;
                    $listImport['price'] = $price;
                    $asset->update($listAsset);

//                    $checkValidation = $this->validateInput($value, $listRule, $fieldVN);
//                    if ($checkValidation) {
//                        return redirect()->back()->withErrors($checkValidation->getMessageBag())->withInput();
//                    }

                    $groupCode = Group::where('id', $value['group_id'])->first();
                    if ($groupCode) {
                        $asset_code = $groupCode->group_code . '-' . $id;
                    }
                    $request['asset_code'] = $asset_code;
                }
            }
            $response = [
                'message' => trans('messages.update_success'),
                'data' => $asset->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }
            return redirect()->route('investment.index')->with('message', $response['message']);

        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('assets.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create()
    {
        if (Gate::denies('assets.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }

        $listPartner = Partner::all()->pluck('name', 'id')->toArray();
        $listGroup = Group::all()->pluck('name', 'id')->toArray();
        $listContract = Contract::all()->pluck('contract_code', 'id')->toArray();
        $listUnit = Unit::all()->pluck('name', 'id')->toArray();
        $listCompany = Company::all();
        $listStructer = [];
        if (isset($listCompany[0])) {
            $listStructer = StructureCompany::getHierarchy($listCompany[0]->id);
        }

        return view($this->partView . '.create', compact('listPartner',
            'listGroup', 'listContract', 'listUnit', 'listCompany', 'listStructer'));
    }

    public function postUploadAttachment(Request $request)
    {
        $file = $request->file;
        $attachment[Documents::URL] = uploadFile($file, FILE_ASSET);
        $attachment[Documents::MODEL_NAME] = 'Asset';
        $attachment[Documents::NAME] = $file->getClientOriginalName();

        $obj = Documents::create($attachment);
        $this->listImage[] = $obj->id;

        return response()->json([
            'message' => trans('messages.upload_success'),
            'data' => $obj,
        ]);
    }

    public function postDeleteAttachment(Request $request)
    {
        $file = $request->all();
        $data = Documents::find($file['id']);

        if ($data) {
            $data->delete();
            unlink(public_path() . $file['url']);
        };

        return response()->json([
            'message' => trans('messages.delete_success'),
            'status' => true,
        ]);
    }

//    public function validateInput($input, $listRule, $customAtr)
//    {
//        $checkValidate = Validator::make($input, $listRule, [], $customAtr);
//        if ($checkValidate->fails()) {
//            $errors = $checkValidate->errors();
//            return $errors;
//        } else {
//            return false;
//        }
//    }
}
