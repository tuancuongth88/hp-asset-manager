<?php

namespace App\Http\Controllers\Partners;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\Partners\CatPartnerCreateRequest;
use App\Http\Requests\Partners\CatPartnerUpdateRequest;
use App\Repositories\Partners\CatPartnerRepository;
use App\Validators\Partners\CatPartnerValidator;

/**
 * Class CatPartnersController.
 *
 * @package namespace App\Http\Controllers\Partners;
 */
class CatPartnersController extends Controller
{
    /**
     * @var CatPartnerRepository
     */
    protected $repository;

    /**
     * @var CatPartnerValidator
     */
    protected $validator;

    /**
     * CatPartnersController constructor.
     *
     * @param CatPartnerRepository $repository
     * @param CatPartnerValidator $validator
     */
    public function __construct(CatPartnerRepository $repository, CatPartnerValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $catPartners = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $catPartners,
            ]);
        }

        return view('catPartners.index', compact('catPartners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CatPartnerCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CatPartnerCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $request['created_by'] = Auth::user()->id;
            $catPartner = $this->repository->create($request->all());

            $response = [
                'message' => 'CatPartner created.',
                'data'    => $catPartner->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $catPartner = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $catPartner,
            ]);
        }

        return view('catPartners.show', compact('catPartner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catPartner = $this->repository->find($id);

        return view('catPartners.edit', compact('catPartner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CatPartnerUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CatPartnerUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $request['updated_by'] = Auth::user()->id;
            $catPartner = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'CatPartner updated.',
                'data'    => $catPartner->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'CatPartner deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'CatPartner deleted.');
    }
}
