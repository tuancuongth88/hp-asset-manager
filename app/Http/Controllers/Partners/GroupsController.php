<?php

namespace App\Http\Controllers\Partners;

use App\Http\Controllers\Controller;
use App\Models\Partners\Group;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Settings;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\Partners\GroupCreateRequest;
use App\Http\Requests\Partners\GroupUpdateRequest;
use App\Repositories\Partners\GroupRepository;
use App\Validators\Partners\GroupValidator;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class GroupsController.
 *
 * @package namespace App\Http\Controllers\Partners;
 */
class GroupsController extends Controller
{
    /**
     * @var GroupRepository
     */
    protected $repository;

    /**
     * @var GroupValidator
     */
    protected $validator;

    private $partView;

    /**
     * GroupsController constructor.
     *
     * @param GroupRepository $repository
     * @param GroupValidator $validator
     */
    public function __construct(GroupRepository $repository, GroupValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->partView = 'administrator.group';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        if (!Auth::user()->can('group.view')) {
            abort(506, trans('messages.you_do_not_have_permission'));
        }
        $list_all_group = Group::getHierarchy('name');

        if ($request->get('name') && !empty($list_all_group) && $list_all_group) {
            foreach ($list_all_group as $key => $value) {
                $arrCheck = preg_grep('~' . $request->get('name') . '~', $value);
                if (empty($arrCheck)) {
                    unset($list_all_group[$key]);
                }
            }
        }
        // Get current page form url e.x. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        // Create a new Laravel collection from the array data
        $itemCollection = collect($list_all_group);
        // Define how many items we want to be visible in each page
        $perPage = 50;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $data = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        // set url path for generted links
        $data->setPath($request->url());

        return view($this->partView . '.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  GroupCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(GroupCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $request['created_by'] = Auth::user()->id;
            $request[Group::PARENT_ID] = $request->get('parent_id', 0);
            $group = $this->repository->create($request->all());

            $response = [
                'message' => trans('messages.create_success'),
                'data' => $group->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('group.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $group,
            ]);
        }

        return view('groups.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('group.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data = $this->repository->find($id);

        return view($this->partView . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  GroupUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(GroupUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $request['updated_by'] = Auth::user()->id;
            $request[Group::PARENT_ID] = $request->get('parent_id', 0);
            $group = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data' => $group->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('group.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('group.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create()
    {
        if (Gate::denies('group.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        return view($this->partView . '.create');
    }

    public function getImportForm()
    {
        return view($this->partView . '.import');
    }

    public function postImport(Request $request)
    {
        PHPExcel_Settings::setZipClass(PHPExcel_Settings::ZIPARCHIVE);
        $listFail = [];
        if ($request->hasFile('import_file')) {
            $validator = Validator::make($request->all(), [
                //or this
                'import_file' => 'required|max:50000|mimetypes:application/csv,application/excel,' .
                    'application/vnd.ms-excel, application/vnd.msexcel,' .
                    'text/csv, text/anytext, text/plain, text/x-c,' .
                    'text/comma-separated-values,' .
                    'inode/x-empty,' .
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ]);
            if ($validator->fails()) {
                $listFail[] = 'File import phải là file Excel';
                return redirect()->back()->with('error', trans('Lỗi import dữ liệu'))
                    ->with('list-fail', $listFail);
            }

            $check_success = false;
            $path_temp = $request->file('import_file')->getRealPath();
            if (!$path_temp) {
                $path_temp = $request->file('import_file');
            }
            Excel::load($path_temp, function ($reader) use (&$listFail, &$check_success, $request) {
                $data = [];
                if (!isset($reader->toArray()[0])) {
                    $listFail[] = 'File import không đúng mẫu';
                    return false;
                }
                $row_check = $reader->toArray()[0];
                if (!array_key_exists('ten_nhom_tai_san', $row_check) ||
                    !array_key_exists('ma_nhom_tai_san', $row_check)
                ) {
                    $listFail[] = 'File import không đúng mẫu';
                    return false;
                }
                foreach ($reader->toArray() as $item) {
                    if (!isset($item['ten_nhom_tai_san']) ||
                        !isset($item['ma_nhom_tai_san'])
                    ) {
                        $listFail[] = 'Thiếu dữ liệu đầu vào';
                        continue;
                    }
                    $name = trim_all($item['ten_nhom_tai_san']);
                    $group_code = trim_all($item['ma_nhom_tai_san']);
                    $data_import = [
                        Group::NAME => $name,
                        Group::GROUP_CODE => $group_code,
                    ];
                    $group_check_parent_exit = null;
                    if (isset($item['ma_nhom_cha']) && trim_all($item['ma_nhom_cha']) !== '') {
                        $group_code_parent = trim_all($item['ma_nhom_cha']);
                        $group_check_parent_exit = Group::whereRaw('LOWER(`group_code`) = ? ',
                            [
                                mb_strtolower(trim_all($group_code_parent), 'UTF-8'),
                            ])->first();
                    }

                    $group_check_exit = Group::whereRaw('LOWER(`group_code`) = ? ',
                        [
                            mb_strtolower(trim_all($group_code), 'UTF-8'),
                        ])->first();

                    if ($group_check_exit) {
                        if ($group_check_parent_exit) {
                            Group::where('id', $group_check_exit->id)
                                ->update([Group::PARENT_ID => $group_check_parent_exit->id]);
                        } else {
                            if ($group_check_exit->parent_id == null || $group_check_exit->parent_id == '') {
                                Group::where('id', $group_check_exit->id)->update([Group::PARENT_ID => 0]);
                            }
                        }
                    } else {
                        if ($group_check_parent_exit) {
                            $data_import[Group::PARENT_ID] = $group_check_parent_exit->id;
                        } else {
                            $data_import[Group::PARENT_ID] = 0;
                        }
                        Group::create($data_import);
                    }
                }
                $check_success = true;
                return redirect()->route('group.index')->with('message', trans('group.import_success'))
                    ->with('list-fail', $listFail);
            });
            if (count($listFail) && !$check_success) {
                return redirect()->back()->with('error', trans('Lỗi import'))->with('list-fail', $listFail);
            } else {
                return redirect()->route('group.index')->with('message', trans('messages.import_success'));
            }
        }
        return redirect()->back()->with('error', trans('Không tồn tại file import'));
    }
}