<?php

namespace App\Http\Controllers\Partners;

use App\Http\Controllers\Controller as Controller;
use App\Models\Contracts\Contract;
use App\Models\Documents\Documents;
use App\Models\Partners\Category;
use App\Models\Partners\CatPartner;
use App\Models\Partners\Partner;
use App\Models\Systems\Position;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\Partners\PartnerCreateRequest;
use App\Http\Requests\Partners\PartnerUpdateRequest;
use App\Repositories\Partners\PartnerRepository;
use App\Validators\Partners\PartnerValidator;

/**
 * Class PartnersController.
 *
 * @package namespace App\Http\Controllers\Partners;
 */
class PartnersController extends Controller
{
    /**
     * @var PartnerRepository
     */
    protected $repository;

    /**
     * @var PartnerValidator
     */
    protected $validator;

    private $partView;
    /**
     * PartnersController constructor.
     *
     * @param PartnerRepository $repository
     * @param PartnerValidator $validator
     */
    public function __construct(PartnerRepository $repository, PartnerValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->partView   = 'administrator.partner';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!Auth::user()->can('partner.view')){
            abort(506, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $input = $request->all();
        $name = isset($input['name']) ? $input['name'] : '';
        $category = isset($input['category']) ? $input['category'] : '';
        $city_id = isset($input['city_id']) ? $input['city_id'] : 0;
        $district_id = isset($input['district_id']) ? $input['district_id'] : 0;

        $mPartner = new Partner();

        if ($name && $name !== '') {
            $mPartner = $mPartner->where('name', 'like', '%' . $name . '%');
        }
        if ($category && $category !== '') {
            $mPartner = $mPartner->where('category_id', $category);
        }
        if ($city_id && $city_id !== 0) {
            $mPartner = $mPartner->where('city_id', $city_id);
        }
        if ($district_id && $district_id !== 0) {
            $mPartner = $mPartner->where('district_id', $district_id);
        }

        $data = $mPartner->paginate();
        $partner_contract_id = $data->pluck('id')->first();

        $listDelegate       = $data->pluck('delegate', 'id')->toArray();
        $listCategory       = Category::all()->pluck('name', 'id')->toArray();
        $listCity           = DB::table('provinces')->pluck('name', 'city_id')->toArray();
        $listPartner        = $data->pluck('name', 'id')->toArray();
        $listContract       = Contract::where(Contract::PARTNER_ID, $partner_contract_id)->get();

        return view($this->partView.'.index', compact('data','listCategory', 'listDelegate', 'listPartner', 'listCity', 'listContract'));
    }

    public function provideCategory($id)
    {
        $categories = Category::where('parent', 0)->get();
        $partner = Partner::where('id', $id)->first();
        $provide_category = $partner->provide_category;
        $list_category = isset($provide_category['list_category']) ? $provide_category['list_category'] : [];

        return view($this->partView . '.provide_category', compact('categories', 'partner','list_category'));
    }

    public function updateProvideCategory($id, Request $request)
    {
        $input = $request->all();
        $provide_category = isset($input['provide_category']) ? $input['provide_category'] : null;
        if ($provide_category){
            $provide_category = array_map('intval', $provide_category);
            Partner::where('id', $id)
                ->update(
                    array(
                        Partner::PROVIDE_CATEGORY => '{"list_category":' . json_encode($provide_category) . '}',
                    ));
        }

        return  Redirect::route('partner.provide-category',$id);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  PartnerCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(PartnerCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $request['created_by'] = Auth::user()->id;

            $log = $request->contact_direct;

            $contact_obj = new \stdClass();
            foreach ($log as $key => $value) {
                $listRule = [
                    'clue_contact'     => 'required|max:191',
                    'position_contact' => 'required|max:191',
                    'email_contact'    => 'required|email|max:191',
                    'phone_contact'    => 'min:8|max:15|regex:/^[0-9]+$/',
                ];
                $fieldVN = [
                    'clue_contact'     => 'đầu mối liên hệ',
                    'position_contact' => 'chức vụ',
                    'email_contact'    => 'email',
                    'phone_contact'    => 'điện thoại liên hệ',
                ];
                $checkValidation = $this->validateInput($value, $listRule, $fieldVN);
                if ($checkValidation) {
                    return redirect()->back()->withErrors($checkValidation->getMessageBag())->withInput();
                }
                $string_k = (string)($key + 1);
                $contact_item = new \stdClass();
                $contact_item->clue_contact = $value['clue_contact'];
                $contact_item->email_contact = $value['email_contact'];
                $contact_item->phone_contact = $value['phone_contact'];
                $contact_item->position_contact = $value['position_contact'];
                $contact_obj->$string_k = $contact_item;
            }

            $partner = $this->repository->create(
                array(
                    Partner::NAME => $request->name,
                    Partner::EMAIL => $request->email,
                    Partner::PHONE => $request->phone,
                    Partner::ADDRESS => $request->address,
                    Partner::HOT_LINE => $request->hot_line,
                    Partner::POSITION => $request->position,
                    Partner::TAX_CODE => $request->tax_code,
                    Partner::DELEGATE => $request->delegate,
                    Partner::WEBSITE => $request->website,
                    Partner::CITY_ID => $request->city_id,
                    Partner::DISTRICT_ID => $request->district_id,
                    Partner::CONTACT_INFO => $contact_obj,
                )
            );
            $logo = $request->logo_image;
            $partner['logo'] = $logo;
            $partner->save();

            $catPartner = new CatPartner();

            if($request->category_id) {
                $category_id = $request->category_id;
                $partner_id  = $partner->id;
                $created_at  = Auth::user()->id;
                $catPartner->category_id = $category_id;
                $catPartner->partner_id  = $partner_id;
                $catPartner->created_by  = $created_at;
                $catPartner->save();
            }

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $partner->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->route('partner.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category        = Category::find($id);
        $listCategory    = Category::where(Category::PARENT, $id)->get();
        if (request()->wantsJson()) {
            $viewCategory = view("administrator.partner.listCategory", compact('listCategory'))->render();
            return response()->json([
                'data'         => $category,
                'viewCategory' => $viewCategory,
            ]);
        }

        return view($this->partView.'.index', compact('category', 'listCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('partner.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data = $this->repository->find($id);
        $listContract = Contract::where(Contract::PARTNER_ID, $id)->get();
        $listCategory = Category::pluck('name', 'id')->toArray();
        $listPosition = Position::all()->pluck('name', 'id')->toArray();
        $listCity     = DB::table('provinces')->pluck('name', 'city_id')->toArray();
        $listDistrict = DB::table('districts')->where('city_id', $data->city_id)->pluck('name', 'id')->toArray();
        return view($this->partView.'.edit', compact('data', 'listCategory', 'listContract', 'listPosition', 'listCity', 'listDistrict'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PartnerUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(PartnerUpdateRequest $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $request['updated_by'] = Auth::user()->id;

            $log = $request->contact_direct;
            $contact_obj = new \stdClass();
            foreach ($log as $key =>$value){
                $string_k = (string)($key + 1);
                $contact_item = new \stdClass();
                $contact_item->clue_contact     = $value['clue_contact'];
                $contact_item->email_contact    = $value['email_contact'];
                $contact_item->phone_contact    = $value['phone_contact'];
                $contact_item->position_contact = $value['position_contact'];
                $contact_obj->$string_k = $contact_item;
            }

            $partner = $this->repository->update(
                array(
                    Partner::NAME         => $request->name,
                    Partner::EMAIL        => $request->email,
                    Partner::PHONE        => $request->phone,
                    Partner::ADDRESS      => $request->address,
                    Partner::HOT_LINE     => $request->hot_line,
                    Partner::POSITION     => $request->position,
                    Partner::TAX_CODE     => $request->tax_code,
                    Partner::DELEGATE     => $request->delegate,
                    Partner::WEBSITE      => $request->website,
                    Partner::CITY_ID      => $request->city_id,
                    Partner::DISTRICT_ID  => $request->district_id,
                    Partner::CONTACT_INFO => $contact_obj,
                ), $id);

            $logo = $request->logo_image;
            $partner['logo'] = $logo;
            $partner->update();

            $catPartner = new CatPartner();

            if($request->category_id) {
                $category_id = $request->category_id;
                $partner_id  = $partner->id;
                $updated_by  = Auth::user()->id;

                $catPartner->category_id = $category_id;
                $catPartner->partner_id  = $partner_id;
                $catPartner->updated_by  = $updated_by;
                $catPartner->save();
            }
            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $partner->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('partner.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('partner.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('partner.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $listContract = Contract::all();
        $listCategory = Category::pluck('name', 'id')->toArray();
        $listPosition = Position::all()->pluck('name', 'id')->toArray();
        $listCity     = DB::table('provinces')->pluck('name', 'city_id')->toArray();
        return view($this->partView.'.create', compact('listContract', 'listPosition', 'listCategory', 'listCity'));
    }

    public function getDistrictOnCity($id) {
        $listCity = DB::table('districts')->where('city_id', $id)->pluck('name', 'id');
        return response()->json([
            'data' => $listCity,
        ]);
    }

    public function postUploadAttachment(Request $request)
    {
        $file = $request->file;
        $attachment[Documents::URL] = uploadFile($file, IMAGE_PARTNER);
        $attachment[Documents::MODEL_NAME] = 'Partner';
        $attachment[Documents::NAME] = $file->getClientOriginalName();

        $obj = Documents::create($attachment);
        $this->listImage[] = $obj->id;

        return response()->json([
            'message' => trans('messages.upload_success'),
            'data' => $obj,
        ]);
    }

    public function postDeleteAttachment(Request $request)
    {
        $file = $request->all();
        $data = Documents::find($file['id']);
        if ($data) {
            $data->delete();
            unlink(public_path() . $file['url']);
        };
        return response()->json([
            'message' => trans('messages.delete_success'),
            'status' => true,
        ]);
    }

    public function validateInput($input, $listRule, $customAtr)
    {
        $checkValidate = Validator::make($input, $listRule, [], $customAtr);
        if ($checkValidate->fails()) {
            $errors = $checkValidate->errors();
            return $errors;
        } else {
            return false;
        }
    }

    public function downloadFile($id)
    {
        $doc = Documents::find($id);
        if ($doc->count() > 0 && file_exists(public_path() . $doc->file_url)) {

            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: application/octet-stream");
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:" . filesize(public_path() . $doc->file_url));
            header("Content-Disposition: attachment; filename=" . $doc->name);
            readfile(public_path() . $doc->file_url);
        } else {
        }
    }
}
