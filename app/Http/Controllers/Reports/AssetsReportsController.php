<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Models\Assets\Asset;
use App\Models\Contracts\Contract;
use App\Models\Partners\Group;
use App\Models\Partners\Partner;
use App\Models\Systems\Company;
use App\Models\Systems\StructureCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Assets\AssetRepository;
use App\Validators\Assets\AssetValidator;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class AssetsController.
 *
 * @package namespace App\Http\Controllers\Assets;
 */
class AssetsReportsController extends Controller
{
    /**
     * @var AssetRepository
     */
    protected $repository;

    /**
     * @var AssetValidator
     */
    protected $validator;

    protected $partView;

    /**
     * AssetsController constructor.
     *
     * @param AssetRepository $repository
     * @param AssetValidator $validator
     */
    public function __construct(AssetRepository $repository)
    {
        $this->repository = $repository;
        $this->partView = 'administrator.reports';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        if (!Auth::user()->can('assets.view')) {
//            abort(506, trans('messages.you_do_not_have_permission'));
//        }
        $input = $request->all();
        $name = isset($input['name']) ? $input['name'] : '';
        $group_id = isset($input['group_id']) ? $input['group_id'] : 0;
        $type = isset($input['type']) ? $input['type'] : '';
        $status = isset($input['status']) ? $input['status'] : [];
        $name_partner = isset($input['name_partner']) ? $input['name_partner'] : '';
        $company_owner = isset($input['company_owner']) ? $input['company_owner'] : null;
        $owner = isset($input['owner']) ? $input['owner'] : null;
        $structure_owner = isset($input['structure_owner']) ? $input['structure_owner'] : null;

        $mAsset = new Asset();
        $mAsset = $mAsset->select(DB::raw(
            'COUNT(`id`) AS asset_count, SUM(asset_assets.quantity) AS sub_quantity, asset_assets.*'));
        if (trim($name) !== '') {
            $mAsset = $mAsset->where('name', 'like', '%' . $name . '%');
        }
        if ($group_id) {
            $mGroup = new Group();
            $listGroup = $mGroup->getAllChildGroup($group_id);
            array_unshift($listGroup, $group_id);
            $mAsset = $mAsset->whereIn('group_id', $listGroup);
        }
        if ($type !== '') {
            $mAsset = $mAsset->where('type', $type);
        }
        if (!empty($status) && is_array($status)) {
            $mAsset = $mAsset->whereIn('status', $status);
        }
        if (trim($name_partner) !== '') {
            $partner = Partner::where('name', 'like', '%' . $name_partner . '%')->pluck('id');
            $mAsset = $mAsset->whereIn(Asset::PARTNER_ID, $partner);
        }

        if ($company_owner) {
            $mAsset = $mAsset->where('company_owner', $company_owner);
        }
        if ($owner) {
            $mAsset = $mAsset->where('owner', $owner);
        }
        if ($structure_owner) {
            $StructureCompany = new StructureCompany();
            $ListStructureCompany = $StructureCompany->getAllChildStructure($structure_owner);
            array_push($ListStructureCompany, $structure_owner);
            $mAsset = $mAsset->whereIn('structure_owner', $ListStructureCompany);
        }

        $groupbyArr = [];
        if ($request->get('groupby_structure_owner', '') == 'on') {
            $groupbyArr[] = 'structure_owner';
        }

        if ($request->get('groupby_year_use', '') == 'on') {
            $groupbyArr[] = 'year_use';
        }

        if ($request->get('groupby_name', '') == 'on') {
            $groupbyArr[] = 'name';
        }

        if ($request->get('groupby_price', '') == 'on') {
            $groupbyArr[] = 'price';
        }

        if ($request->get('groupby_status', '') == 'on') {
            $groupbyArr[] = 'status';
        }

        if ($request->get('groupby_group', '') == 'on') {
            $groupbyArr[] = 'group_id';
        }

        if (!$groupbyArr) {
            $mAsset = $mAsset->groupBy('id');
        } else {
            $mAsset = $mAsset->groupBy($groupbyArr);
        }
        $data = $mAsset->orderBy('name', 'asc');

        $listPartner = Partner::all()->pluck('name', 'id')->toArray();
        $listGroup = Group::all()->pluck('name', 'id')->toArray();
        $listContract = Contract::all()->pluck('name', 'id')->toArray();
        $listCompany = Company::all()->pluck('name', 'id')->toArray();
        $typeAsset = Asset::$assetClassification;

        $list_structure = [];
        if ($company_owner) {
            $structure = StructureCompany::getHierarchy($company_owner);
            if ($structure) {
                $list_structure = $structure;
            }
        }

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $data,
            ]);
        }
        if (isset($input['page'])) {
            unset($input['page']);
        }
        return view(
            $this->partView . '.index',
            compact('data', 'listPartner', 'listGroup', 'listContract', 'listCompany', 'typeAsset', 'input', 'list_structure')
        );
    }

    public function export(Request $request)
    {
        $input = $request->all();
        $name = isset($input['name']) ? $input['name'] : '';
        $group_id = isset($input['group_id']) ? $input['group_id'] : 0;
        $type = isset($input['type']) ? $input['type'] : '';
        $status = isset($input['status']) ? $input['status'] : [];
        $name_partner = isset($input['name_partner']) ? $input['name_partner'] : '';
        $company_owner = isset($input['company_owner']) ? $input['company_owner'] : null;
        $owner = isset($input['owner']) ? $input['owner'] : null;
        $structure_owner = isset($input['structure_owner']) ? $input['structure_owner'] : null;

        $mAsset = new Asset();
        $mAsset = $mAsset->select(DB::raw(
            'COUNT(`id`) AS asset_count, SUM(asset_assets.quantity) AS sub_quantity, asset_assets.*'));
        if (trim($name) !== '') {
            $mAsset = $mAsset->where('name', 'like', '%' . $name . '%');
        }
        if ($group_id) {
            $mGroup = new Group();
            $listGroup = $mGroup->getAllChildGroup($group_id);
            array_unshift($listGroup, $group_id);
            $mAsset = $mAsset->whereIn('group_id', $listGroup);
        }
        if ($type !== '') {
            $mAsset = $mAsset->where('type', $type);
        }
        if (!empty($status) && is_array($status)) {
            $mAsset = $mAsset->whereIn('status', $status);
        }
        if (trim($name_partner) !== '') {
            $partner = Partner::where('name', 'like', '%' . $name_partner . '%')->pluck('id');
            $mAsset = $mAsset->whereIn(Asset::PARTNER_ID, $partner);
        }
        if ($company_owner) {
            $mAsset = $mAsset->where('company_owner', $company_owner);
        }
        if ($owner) {
            $mAsset = $mAsset->where('owner', $owner);
        }
        if ($structure_owner) {
            $StructureCompany = new StructureCompany();
            $ListStructureCompany = $StructureCompany->getAllChildStructure($structure_owner);
            array_unshift($ListStructureCompany, $structure_owner);
//            $mAsset = $mAsset->whereIn('structure_owner', $ListStructureCompany);
        }

        $groupbyArr = [];
        if ($request->get('groupby_structure_owner', '') == 'on') {
            $groupbyArr[] = 'structure_owner';
        }

        if ($request->get('groupby_year_use', '') == 'on') {
            $groupbyArr[] = 'year_use';
        }

        if ($request->get('groupby_name', '') == 'on') {
            $groupbyArr[] = 'name';
        }

        if ($request->get('groupby_price', '') == 'on') {
            $groupbyArr[] = 'price';
        }

        if ($request->get('groupby_status', '') == 'on') {
            $groupbyArr[] = 'status';
        }

        if ($request->get('groupby_group', '') == 'on') {
            $groupbyArr[] = 'group_id';
        }

        if (!$groupbyArr) {
            $mAsset = $mAsset->groupBy('id');
        } else {
            $mAsset = $mAsset->groupBy($groupbyArr);
        }
        $data = $mAsset->orderBy('name', 'asc');

        $typeAsset = Asset::$assetClassification;
        $statusAsset = Asset::$assetListStatus;
        if (isset($structure_owner)) {
            self::exportByStructure($data, $typeAsset, $statusAsset, $ListStructureCompany, $structure_owner, $status);
        } else {
            self::exportAll($data, $typeAsset, $statusAsset, $company_owner, $status);
        }
    }

    public function exportAll($data, $typeAsset, $statusAsset, $companyOwner, $status)
    {
        ini_set('max_execution_time', '1000');
        ini_set('memory_limit', '-1');
        $arrStructure = [];
        $company = $companyOwner;
        if (!$companyOwner) {
            $company_first = Company::first();
            if (!isset($company_first->id)) {
                return;
            }
            $company = $company_first->id;
        }
        $structure = StructureCompany::getHierarchy($company);
        if ($structure && !empty($structure)) {
            $arrStructure = $structure;
        } else {
            return;
        }
        $listNameStructure = [];
        foreach ($arrStructure as $key => $value) {
            $listNameStructure[$value['id']] = StructureCompany::where('id', $value['id'])->first()->name;
        }

        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        //ob_end_clean();
        ob_start();

        Excel::create('Báo cáo tài sản', function ($excel) use ($data, $typeAsset, $statusAsset, $listNameStructure, $status) {
            $excel->sheet('Báo cáo', function ($sheet) use ($data, $typeAsset, $statusAsset, $listNameStructure, $status) {

                $sheet->setFontFamily('Times New Roman');
                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Times New Roman'
                    )
                ));

                $sheet->mergeCells('A1:H1');
                $sheet->mergeCells('A5:H5');
                $sheet->mergeCells('A6:H6');
                $sheet->mergeCells('A7:H7');
                $sheet->mergeCells('A8:H8');
                $sheet->mergeCells('A9:H9');
                $sheet->mergeCells('A10:H10');
                $sheet->mergeCells('A11:H11');
                $sheet->mergeCells('A12:H12');

                $arrColumn = [
                    'B' => [30, 30],
                    'C' => [30, 30],
                    'D' => [20, 30],
                    'E' => [30, 30],
                    'F' => [20, 30],
                    'G' => [20, 30],
                    'H' => [20, 30],
                    'I' => [20, 30],
                    'J' => [20, 30],
                    'K' => [20, 30],
                    'L' => [20, 30],
                ];
                foreach ($arrColumn as $key => $value) {
                    $sheet->setSize($key . 14, $value[0], $value[1]);
                    $sheet->cell($key . '14', function ($cell) {

                        // Set font
                        $cell->setFont(array(
                            'name' => 'Times New Roman',
                            'size' => '14',
                            'bold' => true,
                        ));
                        $cell->setAlignment('center');
                        $cell->setValignment('center');

                    });
                }
                $sheet->loadView($this->partView . '.export')
                    ->with([
                        'data' => $data,
                        'typeAsset' => $typeAsset,
                        'statusAsset' => $statusAsset,
                        'listNameStructure' => $listNameStructure,
                        'status' => $status
                    ]);
            })->download('xlsx');
        });
        ob_flush();
    }

    public function exportByStructure($data, $typeAsset, $statusAsset, $ListStructureCompany, $structure_owner, $status)
    {
        ini_set('max_execution_time', '1000');
        ini_set('memory_limit', '-1');
        $listNameStructure = [];
        foreach ($ListStructureCompany as $key => $value) {
            $listNameStructure[$value] = StructureCompany::where('id', $value)->first()->name;
        }
        if (ob_get_level() > 0) {
            ob_end_clean();
        }
        //ob_end_clean();
        ob_start();
        Excel::create('Báo cáo tài sản theo cấu trúc công ty', function ($excel)
        use ($data, $typeAsset, $statusAsset, $listNameStructure, $structure_owner, $status) {
            $excel->sheet('Báo cáo', function ($sheet)
            use ($data, $typeAsset, $statusAsset, $listNameStructure, $structure_owner, $status) {
                $sheet->setFontFamily('Times New Roman');
                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Times New Roman'
                    )
                ));

                $sheet->mergeCells('A1:H1');
                $sheet->mergeCells('A2:H2');
                $sheet->mergeCells('A3:H3');
                $sheet->mergeCells('A4:H4');
                $sheet->mergeCells('A5:H5');
                $sheet->mergeCells('A6:H6');
                $sheet->mergeCells('A7:H7');
                $sheet->mergeCells('A8:H8');

                $arrColumn = [
                    'B' => [30, 30],
                    'C' => [30, 30],
                    'D' => [20, 30],
                    'E' => [30, 30],
                    'F' => [20, 30],
                    'G' => [20, 30],
                    'H' => [20, 30],
                    'I' => [20, 30],
                    'J' => [20, 30],
                    'K' => [20, 30],
                    'L' => [20, 30],
                ];
                foreach ($arrColumn as $key => $value) {
                    $sheet->setSize($key . 9, $value[0], $value[1]);
                    $sheet->cell($key . '9', function ($cell) {

                        // Set font
                        $cell->setFont(array(
                            'name' => 'Times New Roman',
                            'size' => '14',
                            'bold' => true,
                        ));
                        $cell->setAlignment('center');
                        $cell->setValignment('center');

                    });
                }
                $sheet->loadView($this->partView . '.export_2')
                    ->with([
                        'data' => $data,
                        'typeAsset' => $typeAsset,
                        'statusAsset' => $statusAsset,
                        'listNameStructure' => $listNameStructure,
                        'structure_owner' => $structure_owner,
                        'status' => $status
                    ]);
            })->download('xlsx');
        });
        ob_flush();
    }
}
