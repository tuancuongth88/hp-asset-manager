<?php

namespace App\Http\Controllers\Systems;

use App\Criteria\DeleteRelationCriteria;
use App\Http\Controllers\Controller;
use App\Models\Documents\Documents;
use App\Models\Systems\Company;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\Systems\CompanyCreateRequest;
use App\Http\Requests\Systems\CompanyUpdateRequest;
use App\Repositories\Systems\CompanyRepository;
use App\Validators\Systems\CompanyValidator;

/**
 * Class CompaniesController.
 *
 * @package namespace App\Http\Controllers\Systems;
 */
class CompaniesController extends Controller
{
    /**
     * @var CompanyRepository
     */
    protected $repository;

    /**
     * @var CompanyValidator
     */
    protected $validator;

    private $partView;

    /**
     * CompaniesController constructor.
     *
     * @param CompanyRepository $repository
     * @param CompanyValidator $validator
     */
    public function __construct(CompanyRepository $repository, CompanyValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->partView = 'administrator.systems.company';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $companies = $this->repository->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $companies,
            ]);
        }

        return view($this->partView.'.index', ['data' => $companies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CompanyCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CompanyCreateRequest $request)
    {
        try {
            $model   = $this->repository->model();
            $input   = $request->all();
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $input[$model::CREATED_BY] = Auth::user()->id;

            $company = $this->repository->create($input);

            $logo = $request->file_logo;
            $partner['logo'] = $logo;
            $company->save();

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $company->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }
            return redirect()->route('company.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $company,
            ]);
        }

        return view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = $this->repository->find($id);

        return view($this->partView.'.edit', ['data' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CompanyUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CompanyUpdateRequest $request, $id)
    {
        try {
            $input = $request->all();
            $this->validator->with($input)->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $request['updated_by'] = Auth::user()->id;

            $company = $this->repository->update($input, $id);

            $logo = $request->file_logo;
            $partner['logo'] = $logo;
            $company->save();

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $company->toArray(),
            ];
            if ($request->wantsJson()) {

                return response()->json($response);
            }
            return redirect()->route('company.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {
            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }
        return redirect()->route('company.index')->with('message', trans('messages.delete_success'));
    }

    public function create(){
        return view($this->partView.'.create');
    }

    public function postUploadAttachment(Request $request)
    {
        $file = $request->file;
        $attachment[Documents::URL] = uploadFile($file, IMAGE_COMPANY);
        $attachment[Documents::MODEL_NAME] = 'Company';
        $attachment[Documents::NAME] = $file->getClientOriginalName();

        $obj = Documents::create($attachment);
        $this->listImage[] = $obj->id;

        return response()->json([
            'message' => trans('messages.upload_success'),
            'data' => $obj,
        ]);
    }

    public function postDeleteAttachment(Request $request)
    {
        $file = $request->all();
        $data = Documents::find($file['id']);
        if ($data) {
            $data->delete();
            unlink(public_path() . $file['url']);
        };
        return response()->json([
            'message' => trans('messages.delete_success'),
            'status' => true,
        ]);
    }
}
