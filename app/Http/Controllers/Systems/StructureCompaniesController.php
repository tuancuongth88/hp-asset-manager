<?php

namespace App\Http\Controllers\Systems;

use App\Http\Controllers\Controller;
use App\Http\Requests\Systems\StructureCompanyCreateRequest;
use App\Http\Requests\Systems\StructureCompanyUpdateRequest;
use App\Models\Systems\StructureCompany;
use App\Repositories\Systems\StructureCompanyRepository;
use App\Validators\Systems\StructureCompanyValidator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class StructureCompaniesController.
 *
 * @package namespace App\Http\Controllers;
 */
class StructureCompaniesController extends Controller
{
    /**
     * @var StructureCompanyRepository
     */
    protected $repository;

    /**
     * @var StructureCompanyValidator
     */
    protected $validator;

    /**
     * StructureCompaniesController constructor.
     *
     * @param StructureCompanyRepository $repository
     * @param StructureCompanyValidator $validator
     */
    public function __construct(StructureCompanyRepository $repository, StructureCompanyValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($company)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $mStructure = new StructureCompany();
        $items = $mStructure::where('company_id', $company)->orderBy('order')->get();
        $structure = $this->getHTML($items);

        return view('administrator.structure.index', compact('items', 'structure', 'company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StructureCompanyCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(StructureCompanyCreateRequest $request, $company)
    {
        try {

            $data_input = [
                StructureCompany::NAME => $request->get('name', 'untitled'),
                StructureCompany::PARENT_ID => ZERO,
                StructureCompany::ORDER => StructureCompany::max('order') + 1,
                StructureCompany::COMPANY_ID => $company,
                StructureCompany::TYPE => $request->get('type', StructureCompany::TYPE_BRANCH),
            ];

            if ($request->get('type')) {
                $data_input[StructureCompany::TYPE] = $request->get('type');
            }
            $nestedStructure = $this->repository->create($data_input);

            return redirect()->back()->with('message', 'Thêm mới thành công');
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $structureCompany = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $structureCompany,
            ]);
        }

        return view('structureCompanies.show', compact('structureCompany'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $structureCompany = $this->repository->find($id);

        return view('structureCompanies.edit', compact('structureCompany'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StructureCompanyUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(StructureCompanyUpdateRequest $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $structureCompany = $this->repository->update($request->all(), $id);
            $response = [
                'message' => 'StructureCompany updated.',
                'data' => $structureCompany->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'StructureCompany deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'StructureCompany deleted.');
    }

    public function postIndex(Request $request)
    {
        $source = $request->get('source');
        $destination = $request->get('destination', 0);
        $destination = ($destination == '') ? 0 : $destination;

        $item = StructureCompany::find($source);
        $item->parent_id = $destination;
        $item->save();

        $ordering = json_decode($request->get('order'));
        $rootOrdering = json_decode($request->get('rootOrder'));

        if ($ordering) {
            foreach ($ordering as $order => $item_id) {
                if ($itemToOrder = StructureCompany::find($item_id)) {
                    $itemToOrder->order = $order;
                    $itemToOrder->save();
                }
            }
        } else {
            foreach ($rootOrdering as $order => $item_id) {
                if ($itemToOrder = StructureCompany::find($item_id)) {
                    $itemToOrder->order = $order;
                    $itemToOrder->save();
                }
            }
        }

        return 'ok ';
    }


    public function postDelete(Request $request)
    {
        $id = $request->get('delete_id');
        // Find all items with the parent_id of this one and reset the parent_id to zero
        $items = StructureCompany::where('parent_id', $id)->get()->each(function ($item) {
            $item->parent_id = 0;
            $item->save();
        });

        // Find and delete the item that the user requested to be deleted
        $item = StructureCompany::find($id);
        $item->delete();

        return redirect()->back()->with('message', 'Xóa thành công');
    }

    public function buildStructure($structure, $parentid = 0)
    {
        $result = null;
        foreach ($structure as $item)
            if ($item->parent_id == $parentid) {
                $result .= "<li class='dd-item nested-list-item' data-order='{$item->order}' data-id='{$item->id}'>
	      <div class='dd-handle nested-list-handle'>
	        <i class='fa fa-arrows'></i>
	      </div>
	      <div class='nested-list-content'>{$item->name} | <label style='margin-right: 20px'>" . @StructureCompany::$listType[$item->type] . "</label>
	        <div class='pull-right'>
	          <a onclick='callModalEdit(this)' data-id='{$item->id}'> 
	                <i class='fa fa-pencil'></i>
	          </a> |
	          <a href='#' class='delete_toggle' rel='{$item->id}'><i class='fa fa-trash'></i></a>
	        </div>
	      </div>" . $this->buildStructure($structure, $item->id) . "</li>";
            }
        return $result ? "\n<ol class=\"dd-list\">\n$result</ol>\n" : null;
    }

    // Getter for the HTML structure builder
    public function getHTML($items)
    {
        return $this->buildStructure($items);
    }


    public function getEdit($company, $id)
    {
        $item = StructureCompany::find($id);
        $result = '';
        $result .= '<div class="form-group">
                    <label for="title" class="col-lg-12 control-label">Tên cơ cấu</label>
                    <div class="col-lg-10">
                    <input class="form-control" name="name" value="' . $item->name . '">
                    <input type="hidden" name="id" value="' . $item->id . '">
                    </div>
                </div>
                <div class="form-group">
                    <label for="url" class="col-lg-12 control-label">Loại</label>
                    <div class="col-lg-10">
                        ' . $this->optionTypeHtml($item->type) . '
                    </div>
                </div>';

        return response()->json(['html' => $result]);
    }

    public function postEdit(Request $request)
    {
        $id = $request->get('id');
        $item = StructureCompany::find($id);
        $item->name = e($request->get('name', 'untitled'));
        $item->type = e($request->get('type', StructureCompany::TYPE_BRANCH));
        $item->save();
        return redirect()->back()->with('message', 'Cập nhật thành công');
    }

    public function optionTypeHtml($selected)
    {
        $html = '';
        $html .= '<select name="type" class="form-control">';
        foreach (StructureCompany::$listType as $key => $value) {
            if ($selected == $key) {
                $html .= '<option selected value="' . $key . '">' . $value . '</option>';
            } else {
                $html .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        $html .= ' </select>';
        return $html;
    }

}

