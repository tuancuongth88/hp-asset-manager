<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Users\UserCreateRequest;
use App\Http\Requests\Users\UserUpdateRequest;
use App\Models\Systems\Company;
use App\Models\Systems\Position;
use App\Models\Systems\StructureCompany;
use App\Models\Users\User;
use App\Repositories\Users\UserRepository;
use App\Validators\Users\UserValidator;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Excel\Facades\Excel;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class UsersController.
 *
 * @package namespace App\Http\Controllers\Users;
 */
class UsersController extends Controller {
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var UserValidator
     */
    protected $validator;
    protected $structureCompany;

    protected $position;

    protected $company;
    /**
     * UsersController constructor.
     *
     * @param UserRepository $repository
     * @param UserValidator $validator
     */
    public function __construct(
        UserRepository $repository,
        UserValidator $validator,
        Position $position,
        Company $company,
        StructureCompany $structureCompany
        ) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->position   = $position;
        $this->company    = $company;
        $this->$structureCompany = $structureCompany;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if(!Auth::user()->can('user.view')){
            abort(506, trans('messages.you_do_not_have_permission'));
        }
        $listStructure = StructureCompany::pluck('name', 'id')->toArray();
        $listPosition   = Position::pluck('name', 'id')->toArray();
        return view('administrator.users.index', compact('listDepartment', 'listPosition', 'listBranch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(UserCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $input               = $request->all();
            $input['created_by'] = \Auth::user()->id;
            $input['active']     = USER_ACTIVE;
            $input['avatar']     = '/no-avatar.ico';
            if ($request->hasFile('avatar')) {
                $input['avatar'] = uploadFile($request->avatar, IMAGEUSER);
            }
            $input['password'] = \Hash::make($input['password']);
            $user              = $this->repository->create($input);
            $response          = [
                'message' => trans('messages.create_success'),
                'data'    => $user->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('user.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $user,
            ]);
        }

        return view('administrator.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('user.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $user                 = $this->repository->find($id);
        $data['positions']    = $this->position->all();
        $data['companies']   = $companies  = $this->company->all();
        $listUserByDepartment = $this->repository->all();
        $data['structure']   = [];
        if ($companies){
            $data['structure']   =  StructureCompany::where('company_id',$companies[0]->id)->orderBy('order')->get();
        }

        return view('administrator.users.info', compact('user', 'data', 'listUserByDepartment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UserUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $user     = $this->repository->update($request->all(), $id);
            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $user->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }
            return redirect()->back()->with('message', $response['message']);
//            return redirect()->route('user.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::denies('user.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('user.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data['positions']   = $this->position->all();
        $data['companies']   = $companies  = $this->company->all();
        $data['structure']   = [];
        if (isset($companies[0])) {
            $data['structure'] = StructureCompany::getHierarchy($companies[0]->id);
        }
        return view('administrator.users.create', compact('data','vStructure'));
    }

    public function getUserByDepartment($id) {
        $userId   = Auth::user()->id;
        $listUser = $this->repository->findWhere(['department_id' => $id]);
        return response()->json([
            'data' => $listUser,
        ]);
    }

    public function postImport(Request $request) {

        if ($request->hasFile('import_file')) {
            $allowed = array('xls', 'xlsx');
            $path = $_FILES['import_file']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
                return redirect()->back()->with('error', trans('messages.import_file_extension'));
            } else {
                $path = $request->file('import_file')->getRealPath();
            }
            $data = [];
            $listUserFail = [];
            Excel::load($path, function ($reader) use ($data, &$listUserFail){
                foreach ($reader->toArray() as $row) {
                    if (intval($row['stt']) > 0) {
                        if (!isset($row['email'])) {
                            continue;
                        }
                        // check Email ton tai khong
                        if (isset($data[$row['email']])) {
                            continue;
                        }
                        $check = User::where(User::EMAIL, $row['email'])->first();
                        if ($check) {
                            $listUserFail[] = $row['email'];
                            continue;
                        }
                        $position = Position::where('name', $row['chuc_danh'])
                            ->orderBy('id', 'desc')
                            ->first();
                        if (!$position) {
                            $position = Position::create([
                                Position::NAME       => $row['chuc_danh'],
                                Position::CREATED_BY => \Auth::user()->id,
                            ]);
                        }

                        $branch = Branch::where('name', $row['chi_nhanh'])
                            ->orderBy('id', 'desc')
                            ->first();

                        if (!$branch) {
                            $branch = Branch::create([
                                Branch::NAME         => $row['chi_nhanh'],
                                Position::CREATED_BY => \Auth::user()->id,
                            ]);
                        }
                        $department = Department::where('name', $row['phong_ban'])
                            ->orderBy('id', 'desc')
                            ->first();
                        if (!$department) {
                            $department = Department::create([
                                Department::NAME       => $row['phong_ban'],
                                Department::BRANCH_ID  => $branch->id,
                                Department::COMPANY_ID => 1,
                            ]);
                        }
                        $data[$row['email']][User::COMPANY_ID]    = 1;
                        $data[$row['email']][User::POSITION_ID]   = ($position) ? $position->id : 0;
                        $data[$row['email']][User::DEPARTMENT_ID] = ($department) ? $department->id : 0;
                        $data[$row['email']][User::BRANCH_ID]     = ($branch) ? $branch->id : 0;
                        $data[$row['email']][User::FULLNAME]      = isset($row['ho_va_ten']) ? $row['ho_va_ten'] : '';
                        $data[$row['email']][User::PHONE]         = isset($row['dien_thoai_ca_nhan']) ? $row['dien_thoai_ca_nhan'] : '';
                        $data[$row['email']][User::ACTIVE]        = ONE;
                        $data[$row['email']][User::PASSWORD]      = isset($row['mat_khau']) ? \Hash::make($row['mat_khau']) : '';
                        $data[$row['email']][User::EMAIL]         = isset($row['email']) ? strtolower($row['email']) : '';
                        $data[$row['email']][User::ADDRESS]       = isset($row['dia_chi']) ? $row['dia_chi'] : '';;
                        $data[$row['email']][User::IDENTITY]      = isset($row['chung_minh_thu']) ? $row['chung_minh_thu'] : '';;
                        $data[$row['email']][User::AVATAR]        = '/no-avatar.ico';
                        $data[$row['email']][User::GENDER]        = (strtolower($row['gioi_tinh']) == 'nam') ? USER::MALE : USER::FEMALE;
                        $data[$row['email']][User::BIRTHDAY]      = null;
                        if (validateDate($row['ngay_thang_nam_sinh'], 'd/m/Y')) {
                            $data[$row['email']][User::BIRTHDAY] = \Carbon\Carbon::createFromFormat('d/m/Y', $row['ngay_thang_nam_sinh']);
                        }
                        $data[$row['email']][User::PARENT_ID] = 0;
                        if (isset($row['quan_ly'])) {
                            $user = User::where('email', $row['quan_ly'])->first();
                            if ($user) {
                                $data[$row['email']][User::PARENT_ID] = $user->id;
                            }
                        }
                        $data[$row['email']][User::CREATED_AT] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
                    }
                }
                if (count($data) > 0) {
                    User::insert($data);
                }
            });
            return redirect()->route('user.index')
                ->with('message', trans('messages.import_success'))->with('listUserFail',$listUserFail);
        } else {
            return redirect()->back()->with('error', trans('messages.import_file_empty'));
        }

    }

    public function getList(Request $request) {
        $input = $request->all();
        $model = new User();
        $total = $model::count();
        if (isset($input['query'])) {
            foreach ($input['query'] as $key => $value) {
                if ($value == strval(ZERO) || $value == '') {
                    unset($input['query'][$key]);
                }
            }
            if (isset($input['query']['email'])) {
                $model = $model->where('email', 'LIKE', '%' . $input['query']['email'] . '%')
                    ->orWhere('fullname', 'LIKE', '%' . $input['query']['email'] . '%');
                unset($input['query']['email']);
            }
            $model = $model->where($input['query']);
        } else {
            $model = $model->take($input['pagination']['perpage'])
                ->skip($input['pagination']['perpage'] * ($input['pagination']['page'] - 1));
        }
        $model            = $model->with(['department', 'position', 'branch', 'company']);
        $data             = $model->get()->toArray();
        $response['data'] = $data;
        $meta             = new \stdClass();
        $meta->page       = $input['pagination']['page'];
        $meta->perpage    = $input['pagination']['perpage'];
        $meta->total      = $total;
        $response['meta'] = $meta;
        return response()->json($response);
    }

    public function getSearch(Request $request) {
        $model   = new User();
        $arrUser = $model->getAllChildUser(\Auth::user()->id);
        array_push($arrUser, \Auth::user()->id);
        $user = User::whereIn('id', $arrUser)->search($request->input('keyword'))->get()->toArray();
        // if (!$user) {
        //     return response()->json(false, '', trans('messages.something_went_wrong'));
        // }
        return response()->json($user);
    }

    public function getChangePassword() {
        return view('administrator.users.change-password');
    }

    public function postChangePassword(Request $request) {
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            return redirect()->back()->with("error", 'Mật khẩu hiện tại không đúng');
        }

        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            return redirect()->back()->with("error", "Mật khẩu cũ không được giống mật khẩu mới.");
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password'     => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user           = Auth::user();
        $user->password = Hash::make($request->get('new_password'));
        $user->save();
        return redirect()->back()->with("message", "Thay đổi mật khẩu thành công !");
    }

    public function readNotification($id) {
        $user = Auth::user();
        $user->unreadNotifications->where('id', $id)->markAsRead();
        return redirect()->route('list.index');
    }

    public function getStructureByCompany($company)
    {
        $data = StructureCompany::getHierarchy($company);
        $html = '<option value="">-- Chọn đơn vị trực thuộc --</option>';
        foreach ($data as $key => $value) {
            $html .= '<option value="' . $value['id'] . '">' . $value['name'] . ' | ' . StructureCompany::$listType[$value['type']] . '</option>';
        }

        return response()->json(['html' => $html]);
    }

}