<?php

namespace App\Http\Requests\Assets;

use App\Models\Assets\Asset;
use Illuminate\Foundation\Http\FormRequest;

class AssetCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            Asset::NAME        => 'required|max:191',
//            Asset::CONTRACT_ID => 'required',
//            Asset::PARTNER_ID  => 'required',
//            Asset::GROUP_ID    => 'required',
//            Asset::TYPE        => 'required',
//            Asset::WARRANTY    => 'required',
//            Asset::STATUS      => 'required',
        ];
    }
    public function messages() {
        return [
            'required' => trans('validation.required'),
            'max:191'  => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return Asset::getFieldVietnamese();
    }
}
