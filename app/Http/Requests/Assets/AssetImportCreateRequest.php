<?php

namespace App\Http\Requests\Assets;

use App\Models\Assets\Asset;
use App\Models\Assets\AssetImport;
use Illuminate\Foundation\Http\FormRequest;

class AssetImportCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            AssetImport::COMPANY_ID   => 'required',
            AssetImport::STRUCTURE_ID => 'required',
        ];
    }
    public function messages() {
        return [
            'required' => trans('validation.required'),
            'max:191'  => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return AssetImport::getFieldVietnamese();
    }
}
