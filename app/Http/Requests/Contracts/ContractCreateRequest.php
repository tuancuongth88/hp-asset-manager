<?php

namespace App\Http\Requests\Contracts;

use App\Models\Contracts\Contract;
use Illuminate\Foundation\Http\FormRequest;

class ContractCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Contract::PARTNER_ID          => 'required',
            Contract::CONTRACT_CODE       => 'required|unique:asset_contract',
            Contract::SIGN_DATE           => 'required',
        ];
    }
    public function messages() {
        return [
            'required' => trans('validation.required'),
            'max:191'  => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return Contract::getFieldVietnamese();
    }
}
