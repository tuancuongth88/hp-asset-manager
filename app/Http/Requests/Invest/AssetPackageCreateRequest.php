<?php

namespace App\Http\Requests\Invest;

use App\Models\Invest\AssetPackage;
use Illuminate\Foundation\Http\FormRequest;

class AssetPackageCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            AssetPackage::NAME => 'required|max:191',
            AssetPackage::PRICE => 'required',
            AssetPackage::CAPITAL => 'required',
            AssetPackage::START_TIME => 'required|date',
            AssetPackage::TYPE_CONTRACT => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => trans('validation.required'),
            'max:191' => trans('validation.max.string'),
            'numeric' => trans('validation.numeric'),
        ];
    }

    public function attributes()
    {
        return AssetPackage::getFieldVietnamese();
    }
}
