<?php

namespace App\Http\Requests\Partners;

use App\Models\Partners\Partner;
use Illuminate\Foundation\Http\FormRequest;

class PartnerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Partner::NAME         => 'required|max:191',
//            Partner::EMAIL        => 'required|email|max:191',
//            Partner::PHONE        => 'min:8|max:15|regex:/^[0-9]+$/',
//            Partner::ADDRESS      => 'required',
//            Partner::HOT_LINE     => 'min:8|max:15|regex:/^[0-9]+$/',
//            Partner::POSITION     => 'required',
//            Partner::TAX_CODE     => 'required',
//            Partner::CITY_ID      => 'required',
//            Partner::DISTRICT_ID  => 'required',
//            Partner::DELEGATE     => 'required',
        ];
    }
    public function messages() {
        return [
            'required'      => trans('validation.required'),
            'max:191'       => trans('validation.max.string'),
            'numeric'       => trans('validation.numeric'),
            'regex'         => trans('validation.regex'),
            'email'         => trans('validation.email'),
        ];
    }

    public function attributes() {
        return Partner::getFieldVietnamese();
    }
}
