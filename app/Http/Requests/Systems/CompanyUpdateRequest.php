<?php

namespace App\Http\Requests\Systems;

use App\Models\Systems\Company;
use Illuminate\Foundation\Http\FormRequest;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Company::NAME => 'required|max:250',
//            Company::TAX => 'required',
//            Company::PHONE => 'required|numeric|max:10',
//            Company::ADDRESS => 'required',
//            Company::LOGO => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => trans('validation.required'),
            'max:191' => trans('validation.max.string'),
            'numeric' => trans('validation.numeric'),
        ];
    }

    public function attributes()
    {
        return Company::getFieldVietnamese();
    }
}
