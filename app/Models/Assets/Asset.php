<?php

namespace App\Models\Assets;

use App\Models\Contracts\Contract;
use App\Models\Partners\Group;
use App\Models\Partners\Partner;
use App\Models\Systems\Company;
use App\Models\Systems\Position;
use App\Models\Systems\StructureCompany;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Asset.
 *
 * @package namespace App\Models\Assets;
 */
class Asset extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'asset_assets';
    const NAME = 'name';
    const NAME_FOR_SEARCH = 'name_for_search';
    const SLUG = 'slug';
    const GROUP_ID = 'group_id';
    const PARTNER_ID = 'partner_id';
    const CONTRACT_ID = 'contract_id';
    const CONTRACT_CODE = 'contract_code';
    const MATERIAL = 'material';
    const COLOR = 'color';
    const INSPECTION = 'inspection';
    const UNIT_ID = 'unit_id';
    const WARRANTY = 'warranty';
    const PRICE_VAT = 'price_vat';
    const PRODUCER = 'producer';
    const VAT = 'vat';
    const TYPE = 'type';
    const PRICE = 'price';
    const DESCRIPTION = 'description';
    const STATUS = 'status';
    const HANDOVERS_PARAMS = 'handovers_params';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const OWNER = 'owner';
    const OWNER_NAME = 'owner_name';
    const POSITION_OWNER = 'position_owner';
    const COMPANY_OWNER = 'company_owner';
    const SIGN_DATE = 'sign_date';
    const DATE_IMPORT = 'date_import';
    const YEAR_MANUFACTURE = 'year_manufacture';
    const COUNT_HANDOVER = 'count_handover';
    const DATE_USE = 'date_use';
    const AREA_COMPANY = 'area_company';
    const RECEIVER_HANDOVER = 'receiver_handover';
    const ORIGINAL = 'original';

    const YEAR_USE = 'year_use';
    const ASSET_CODE = 'asset_code';
    const STRUCTURE_OWNER = 'structure_owner';
    const QUANTITY = 'quantity';
    const QUANTITY_CONTRACT = 'quantity_contract';


    const TYPE_1 = 1;
    const TYPE_2 = 2;
    const TYPE_3 = 3;

    //Trạng thái
    const STATUS_USE = 1;
    const STATUS_REPAIR = 2;
    const STATUS_WARRANTY = 3;
    const STATUS_GIFT = 4;
    const STATUS_LIQUIDATION = 5;
    const STATUS_BREAKDOWN = 6;
    const STATUS_WAIT = 7;
    const STATUS_PRIORITY = 8;
    const STATUS_PRIORITIZE = 9;
    const STATUS_STORE = 10;
    const STATUS_NEVER_BEEN_USED = 11;
    const STATUS_NEW = 12;
    const STATUS_USED = 13;
    const STATUS_LOST = 14;

    //Phân loại vat
    const VAT_0 = '0';
    const VAT_5 = '0.05';
    const VAT_10 = '0.1';

    // Trạng thái ban đầu của tài sản
    const ORIGINAL_1 = 1;
    const ORIGINAL_2 = 2;

    protected $fillable = [
        self::NAME,
        self::NAME_FOR_SEARCH,
        self::SLUG,
        self::GROUP_ID,
        self::PARTNER_ID,
        self::CONTRACT_ID,
        self::CONTRACT_CODE,
        self::TYPE,
        self::MATERIAL,
        self::COLOR,
        self::INSPECTION,
        self::UNIT_ID,
        self::PRODUCER,
        self::PRICE,
        self::VAT,
        self::PRICE_VAT,
        self::WARRANTY,
        self::DESCRIPTION,
        self::STATUS,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::OWNER,
        self::OWNER_NAME,
        self::POSITION_OWNER,
        self::COMPANY_OWNER,
        self::DATE_IMPORT,
        self::SIGN_DATE,
        self::YEAR_MANUFACTURE,
        self::COUNT_HANDOVER,
        self::DATE_USE,
        self::AREA_COMPANY,
        self::RECEIVER_HANDOVER,
        self::YEAR_USE,
        self::QUANTITY,
        self::QUANTITY_CONTRACT,

        self::ASSET_CODE,
        self::STRUCTURE_OWNER,
        self::ORIGINAL,
    ];

    protected $casts = [
        'handovers_params' => 'array'
    ];

    public static $assetClassification = [
        self::TYPE_1 => 'Tài sản',
        self::TYPE_2 => 'Công cụ dụng cụ',
        self::TYPE_3 => 'Đầu tư',
    ];
    //Phân loại tài sản

    // Phân loại trạng thái ban đầu tài sản
    public static $assetOriginalClassification = [
        self::ORIGINAL_1 => 'Mua mới',
        self::ORIGINAL_2 => 'Mua thanh lý',
    ];

    //Trạng thái tài sản
    public static $assetListStatus = [
        self::STATUS_NEW => 'Mua mới',
        self::STATUS_USE => 'Đang sử dụng',
        self::STATUS_REPAIR => 'Sửa chữa, bảo trì',
        self::STATUS_WARRANTY => 'Bảo hành',
        self::STATUS_GIFT => 'Đã tặng',
        self::STATUS_LIQUIDATION => 'Đã thanh lý',
        self::STATUS_BREAKDOWN => 'Hỏng hóc',
        self::STATUS_WAIT => 'Chờ bàn giao',
        self::STATUS_PRIORITIZE => 'Ưu tiên',
        self::STATUS_STORE => 'Tồn kho',
        self::STATUS_NEVER_BEEN_USED => 'Chưa sử dụng',
        self::STATUS_USED => 'Đã sử dụng',
        self::STATUS_LOST => 'Bị mất hoặc thất lạc',
    ];
    // trrang thai tai san duoc ban giao
    public static $assetStatusHandover = [
        self::STATUS_USE => 'Đang sử dụng',
        self::STATUS_BREAKDOWN => 'Hỏng hóc',
        self::STATUS_STORE => 'Tồn kho',
        self::STATUS_NEVER_BEEN_USED => 'Chưa sử dụng',
        self::STATUS_NEW => 'Mua mới',
        self::STATUS_USED => 'Đã sử dụng',
    ];

    // danh sách trạng thái xuất báo cáo mặc định
    public static $lstStatusExportDefault = [
        self::STATUS_NEW,
        self::STATUS_USE,
        self::STATUS_REPAIR,
        self::STATUS_WARRANTY,
        self::STATUS_WAIT,
        self::STATUS_PRIORITIZE,
        self::STATUS_STORE,
        self::STATUS_NEVER_BEEN_USED,
        self::STATUS_USED,
    ];

    public static $listVat = [
        self::VAT_0 => '0%',
        self::VAT_5 => '5%',
        self::VAT_10 => '10%'
    ];

    public function partner()
    {
        return $this->hasone(Partner::class, 'id', Asset::PARTNER_ID);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public static function getFieldVietnamese()
    {
        return [
            self::NAME => trans('field.name'),
            self::DESCRIPTION => trans('field.note'),
            self::STRUCTURE_OWNER => trans('field.structure_owner'),
            self::COMPANY_OWNER => trans('field.company_owner'),
        ];
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function ownerUser()
    {
        return $this->belongsTo(User::class, self::OWNER, 'id');
    }

    public function position()
    {
        return $this->belongsTo(Position::class, self::POSITION_OWNER, 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, self::COMPANY_OWNER, 'id');
    }

    public function ownerStructure()
    {
        return $this->belongsTo(StructureCompany::class, self::STRUCTURE_OWNER, 'id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class, self::GROUP_ID, 'id');
    }

    public function assetLog()
    {
        return $this->hasOne(AssetLog::class, 'asset_id', 'id');
    }
}
