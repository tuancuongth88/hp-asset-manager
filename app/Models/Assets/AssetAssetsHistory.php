<?php

namespace App\Models\Assets;

use App\Models\Partners\Group;
use App\Models\Systems\Company;
use App\Models\Systems\Position;
use App\Models\Systems\StructureCompany;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AssetAssetsHistory.
 *
 * @package namespace App\Models;
 */
class AssetAssetsHistory extends Model implements Transformable
{
    use TransformableTrait;

    const ASSET_ID = 'asset_id';
    const OWNER = 'owner';
    const DEPARTMENT_OWNER = 'department_owner';
    const POSITION_OWNER = 'position_owner';
    const COMPANY_OWNER = 'company_owner';
    const STRUCTURE_OWNER = 'structure_owner';
    const BRANCH_OWNER = 'branch_owner';
    const TYPE = 'type';
    const STATUS = 'status';
    const PARAMS = 'params';
    const DESCRIPTION = 'description';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const START_TIME = 'start_time';
    const END_TIME = 'end_time';
    const TOTAL_MONEY = 'total_money';
    const PEOPLE_NOTICE = 'people_notice';
    const NOTE = 'note';
    const PRICE_REPAIR = 'price_repair';


    protected $table = 'asset_assets_histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ASSET_ID,
        self::OWNER,
        self::DEPARTMENT_OWNER,
        self::POSITION_OWNER,
        self::COMPANY_OWNER,
        self::BRANCH_OWNER,
        self::TYPE,
        self::STATUS,
        self::PARAMS,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::START_TIME,
        self::END_TIME,
        self::TOTAL_MONEY,
        self::STRUCTURE_OWNER,
        self::PEOPLE_NOTICE,
        self::NOTE,
        self::PRICE_REPAIR,
    ];


    protected $casts = [
        'params' => 'array'
    ];


    //Loại
    const TYPE_WARRANTY = '1';
    const TYPE_REPAIR = '2';

    public static $assetHistoryType = [
        self::TYPE_WARRANTY => 'Bảo hành, bảo trì',
        self::TYPE_REPAIR => 'Sửa chữa'
    ];

    //Trạng thái
    const STATUS_WARRANTY = '1';
    const STATUS_REPAIRING = '2';
    const STATUS_FINISH = '3';
    const STATUS_NOT_FIXING = '4';

    public static $assetHistoryStatus = [
        self::STATUS_WARRANTY => 'Đang bảo hành',
        self::STATUS_REPAIRING => 'Đang sửa chữa, bảo trì',
        self::STATUS_FINISH => 'Hoàn thành sửa chữa, bảo trì, bảo hành',
        self::STATUS_NOT_FIXING => 'Không sửa chữa được',
    ];

    protected $dates = ['deleted_at'];


    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function historyOwner()
    {
        return $this->belongsTo(User::class, self::OWNER, 'id');
    }

    public function positionOwner()
    {
        return $this->belongsTo(Position::class, self::POSITION_OWNER, 'id');
    }

    public function companyOwner()
    {
        return $this->belongsTo(Company::class, self::COMPANY_OWNER, 'id');
    }

    public function ownerStructure()
    {
        return $this->belongsTo(StructureCompany::class, self::STRUCTURE_OWNER, 'id');
    }

    public function asset()
    {
        return $this->belongsTo(Asset::class, self::ASSET_ID, 'id');
    }

    public function assetGroup()
    {
        return $this->hasManyThrough(Asset::class, Group::class, 'group_id', '');
    }

}
