<?php

namespace App\Models\Assets;

use App\Models\Systems\Company;
use App\Models\Systems\Position;
use App\Models\Systems\StructureCompany;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AssetHandover.
 *
 * @package namespace App\Models;
 */
class AssetHandover extends Model implements Transformable
{
    use TransformableTrait;

    const PERSON_NAME = 'person_name';
    const PERSON_COMPANY_ID = 'person_company_id';
    const PERSON_STRUCTURE_ID = 'person_structure_id';
    const PERSON_STRUCTURE_TYPE = 'person_structure_type';
    const PERSON_POSITIONS = 'person_positions';
    const PERSON_DESCRIPTION = 'person_description';
    const RECEIVER_NAME = 'receiver_name';
    const RECEIVER_COMPANY_ID = 'receiver_company_id';
    const RECEIVER_STRUCTURE_ID = 'receiver_structure_id';
    const RECEIVER_POSITIONS = 'receiver_positions';
    const RECEIVER_DESCRIPTION = 'receiver_description';
    const DELEGATE_NAME = 'delegate_name';
    const DELEGATE_COMPANY_ID = 'delegate_company_id';
    const DELEGATE_STRUCTURE_ID = 'delegate_structure_id';
    const DELEGATE_POSITIONS = 'delegate_positions';
    const STATUS = 'status';
    const PARAMS = 'params';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const HANDOVERS_DATETIME = 'handovers_datetime';
    const NUMBER_HANDOVER = 'number_handover';
    const TYPE = 'type';
    const LIST_ASSET_ID = 'list_asset_id';


    protected $table = 'asset_handovers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::PERSON_NAME,
        self::PERSON_COMPANY_ID,
        self::PERSON_STRUCTURE_ID,
        self::PERSON_POSITIONS,
        self::PERSON_DESCRIPTION,
        self::RECEIVER_NAME,
        self::RECEIVER_COMPANY_ID,
        self::RECEIVER_STRUCTURE_ID,
        self::RECEIVER_POSITIONS,
        self::RECEIVER_DESCRIPTION,
        self::DELEGATE_NAME,
        self::DELEGATE_COMPANY_ID,
        self::DELEGATE_STRUCTURE_ID,
        self::DELEGATE_POSITIONS,
        self::STATUS,
        self::PARAMS,
        self::HANDOVERS_DATETIME,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::TYPE,
        self::NUMBER_HANDOVER,
        self::LIST_ASSET_ID,
    ];


    protected $casts = [
        'params' => 'array',
        'list_asset_id' => 'array'
    ];

    //Trạng thái
    const STATUS_CREATE_NEW = 1;
    const STATUS_CONFIRM = 2;
    const STATUS_CONFIRM_STORE = 3;
    const STATUS_CANCEL = 4;//Từ chối và hủy bàn giao

    public static $assetHandoverStatus = [
        self::STATUS_CREATE_NEW => 'Đang bàn giao',
        self::STATUS_CONFIRM => 'Đã nhận bàn giao'
    ];

    //Loại bàn giao
    const TYPE_HANDOVER_USE = 1;
    const TYPE_HANDOVER_EXPORT = 2;
    const TYPE_HANDOVER_IMPORT = 3;

    public static $assetHandoverType = [
        self::TYPE_HANDOVER_USE => 'Luân chuyển tài sản',
        self::TYPE_HANDOVER_EXPORT => 'Xuất kho',
        self::TYPE_HANDOVER_IMPORT => 'Nhập kho',
    ];


    protected $dates = ['deleted_at'];


    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function personCompany()
    {
        return $this->belongsTo(Company::class, self::PERSON_COMPANY_ID, 'id');
    }

    public function personStructure()
    {
        return $this->belongsTo(StructureCompany::class, self::PERSON_STRUCTURE_ID, 'id');
    }

    public function receiverCompany()
    {
        return $this->belongsTo(Company::class, self::RECEIVER_COMPANY_ID, 'id');
    }

    public function receiverStructure()
    {
        return $this->belongsTo(StructureCompany::class, self::RECEIVER_STRUCTURE_ID, 'id');
    }

    public function delegateCompany()
    {
        return $this->belongsTo(Company::class, self::DELEGATE_COMPANY_ID, 'id');
    }

    public function delegateStructure()
    {
        return $this->belongsTo(StructureCompany::class, self::DELEGATE_STRUCTURE_ID, 'id');
    }

}
