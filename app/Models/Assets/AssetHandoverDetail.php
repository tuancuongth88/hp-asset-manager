<?php

namespace App\Models\Assets;


use App\Models\Partners\Group;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AssetHandover.
 *
 * @package namespace App\Models;
 */
class AssetHandoverDetail extends Model implements Transformable
{
    use TransformableTrait;

    const NAME = 'name';
    const ASSET_ID = 'asset_id';
    const HANDOVER_ID = 'handover_id';
    const GROUP_ID = 'group_id';
    const TYPE = 'type';
    const CONSTRUCT_CODE = 'construct_code';
    const CHECK_COUNT_HANDOVER = 'check_count_handover';
    const STATUS = 'status';
    const PRICE = 'price';
    const PARAMS = 'params';
    const QUANTITY = 'quantity';
    const NOTE = 'note';
    const UNIT_ID = 'unit_id';

    protected $table = 'asset_handover_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::ASSET_ID,
        self::HANDOVER_ID,
        self::GROUP_ID,
        self::TYPE,
        self::CONSTRUCT_CODE,
        self::CHECK_COUNT_HANDOVER,
        self::STATUS,
        self::PRICE,
        self::PARAMS,
        self::QUANTITY,
        self::NOTE,
        self::UNIT_ID,
    ];


    protected $casts = [
        'params' => 'array'
    ];

    protected $dates = ['deleted_at'];

    public function asset()
    {
        return $this->belongsTo(Asset::class, self::ASSET_ID, 'id');
    }

    public function handover()
    {
        return $this->belongsTo(AssetHandover::class, self::HANDOVER_ID, 'id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class, self::GROUP_ID, 'id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, self::UNIT_ID, 'id');
    }

}
