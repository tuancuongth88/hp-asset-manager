<?php

namespace App\Models\Assets;

use App\Models\Contracts\Contract;
use App\Models\Partners\Group;
use App\Models\Partners\Partner;
use App\Models\Assets\Unit;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AssetImport.
 *
 * @package namespace App\Models;
 */
class AssetImport extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'asset_imports';
    const NAME        = 'name';
    const MATERIAL    = 'material';
    const COLOR       = 'color';
    const QUANTITY    = 'quantity';
    const PRODUCER    = 'producer';
    const UNIT_ID     = 'unit_id';
    const STATUS      = 'status';
    const TYPE        = 'type';
    const GROUP_ID    = 'group_id';
    const PRICE       = 'price';
    const VAT         = 'vat';
    const WARRANTY    = 'warranty';
    const CONTRACT_ID = 'contract_id';
    const CONTRACT_CODE = 'contract_code';
    const PARTNER_ID   = 'partner_id';
    const CREATED_BY   = 'created_by';
    const UPDATED_BY   = 'updated_by';
    const COMPANY_ID   = 'company_id';
    const STRUCTURE_ID = 'structure_id';
    const AREA_COMPANY = 'area_company';
    const YEAR_MANUFACTURED = 'year_manufactured';
    const RECEIVER_HANDOVER = 'receiver_handover';
    const YEAR_USE          = 'year_use';
    const DESCRIPTION       = 'description';
    const DATE_IMPORT       = 'date_import';
    const ORIGINAL       = 'original';

    const TYPE_1 = 1;
    const TYPE_2 = 2;

    // Trạng thái ban đầu của tài sản
    const ORIGINAL_1 = 1;
    const ORIGINAL_2 = 2;

    //Trạng thái
    const STATUS_USE = 1;
    const STATUS_REPAIR = 2;
    const STATUS_WARRANTY = 3;
    const STATUS_GIFT = 4;
    const STATUS_LIQUIDATION = 5;
    const STATUS_BREAKDOWN = 6;
    const STATUS_WAIT = 7;
    const STATUS_PRIORITY = 8;
    const STATUS_PRIORITIZE = 9;
    const STATUS_STORE = 10;
    const STATUS_NEVER_BEEN_USED = 11;
    const STATUS_NEW = 12;
    const STATUS_USED = 13;

    //Phân loại vat
    const VAT_0  = '0';
    const VAT_5  = '0.05';
    const VAT_10 = '0.1';

    public static $listStatus = [
        self::STATUS_NEW => 'Mua mới',
        self::STATUS_USE => 'Đang sử dụng',
        self::STATUS_REPAIR => 'Sửa chữa, bảo trì',
        self::STATUS_WARRANTY => 'Bảo hành',
        self::STATUS_GIFT => 'Đã tặng',
        self::STATUS_LIQUIDATION => 'Đã thanh lý',
        self::STATUS_BREAKDOWN => 'Hỏng hóc',
        self::STATUS_WAIT => 'Chờ bàn giao',
        self::STATUS_PRIORITIZE => 'Ưu tiên',
        self::STATUS_STORE => 'Tồn kho',
        self::STATUS_NEVER_BEEN_USED => 'Chưa sử dụng',
        self::STATUS_USED => 'Đã sử dụng',
    ];

    public static $listVat = [
        self::VAT_0  => '0%',
        self::VAT_5  => '5%',
        self::VAT_10 => '10%'
    ];

    //Phân loại tài sản
    public static $assetClassification = [
        self::TYPE_1 => 'Tài sản',
        self::TYPE_2 => 'Công cụ dụng cụ',
    ];

    // Phân loại trạng thái ban đầu tài sản
    public static $assetOriginalClassification = [
        self::ORIGINAL_1 => 'Mua mới',
        self::ORIGINAL_2 => 'Mua thanh lý',
    ];

    protected $fillable = [
        self::NAME,
        self::MATERIAL,
        self::COLOR,
        self::QUANTITY,
        self::PRODUCER,
        self::UNIT_ID,
        self::STATUS,
        self::TYPE,
        self::GROUP_ID,
        self::PRICE,
        self::VAT,
        self::WARRANTY,
        self::YEAR_MANUFACTURED,
        self::CONTRACT_ID,
        self::CONTRACT_CODE,
        self::PARTNER_ID,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::STRUCTURE_ID,
        self::COMPANY_ID,
        self::AREA_COMPANY,
        self::RECEIVER_HANDOVER,
        self::DESCRIPTION,
        self::DATE_IMPORT,
        self::ORIGINAL,
    ];

    public function contract()
    {
        return $this->hasone(Contract::class, 'id', AssetImport::CONTRACT_ID);
    }

    public function partner() {
        return $this->hasone(Partner::class, 'id', AssetImport::PARTNER_ID);
    }

    public function group() {
        return $this->hasone(Group::class, 'id', AssetImport::GROUP_ID);
    }

    public static function getFieldVietnamese()
    {
        return [
            self::NAME => trans('field.name'),
            self::STRUCTURE_ID => trans('field.structure_id'),
            self::COMPANY_ID => trans('field.company_id'),
        ];
    }
}
