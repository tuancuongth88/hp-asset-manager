<?php

namespace App\Models\Assets;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AssetLog.
 *
 * @package namespace App\Models;
 */
class AssetLog extends Model implements Transformable
{
    use TransformableTrait;

    const ASSET_ID    = 'asset_id';
    const CONTRACT_ID = 'contract_id';
    const PARTNER_ID  = 'partner_id';
    const PARAMS      = 'params';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';

    protected $table = 'asset_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ASSET_ID,
        self::CONTRACT_ID,
        self::PARTNER_ID,
        self::PARAMS,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];


    protected $dates = ['deleted_at'];

    protected $casts = [
        'params' => 'array'
    ];

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'asset_id', 'id');
    }
}
