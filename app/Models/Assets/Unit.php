<?php

namespace App\Models\Assets;

use App\Models\Assets\AssetImport;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Unit.
 *
 * @package namespace App\Models;
 */
class Unit extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'units';
    const NAME = 'name';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    protected $fillable = [
        self::NAME,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

//    public function assets_import() {
//        return $this->hasMany(Unit::class, AssetImport::UNIT_ID, 'id');
//    }

    public function asset() {
        return $this->hasMany(Asset::class, Asset::UNIT_ID, 'id');
    }
}
