<?php

namespace App\Models\Contracts;

use App\Models\Assets\Asset;
use App\Models\Assets\AssetImport;
use App\Models\Documents\Documents;
use App\Models\Partners\Group;
use App\Models\Partners\Partner;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Contract.
 *
 * @package namespace App\Models\Contracts;
 */
class Contract extends Model implements Transformable
{
    use TransformableTrait,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table      = 'asset_contract';
    const CONTRACT_CODE   = 'contract_code';
    const NAME            = 'name';
    const PARTNER_ID      = 'partner_id';
    const TOTAL_MONEY     = 'total_money';
    const FILE_ID         = 'file_id';
    const STATUS          = 'status';
    const TYPE            = 'type';
    const SIGN_DATE       = 'sign_date';
    const CREATED_BY      = 'created_by';
    const UPDATED_BY      = 'updated_by';
    const STATUS_HANDOVER = 'status_handover';
    const PRIORITIZE      = 'prioritize';

    const STATUS_PRIORITIZE = 0;
    const STATUS_USE = 1;
    const STATUS_REPAIR = 2;
    const STATUS_WARRANTY = 3;
    const STATUS_GIFT = 4;
    const STATUS_LIQUIDATION = 5;
    const STATUS_BREAKDOWN = 6;
    const STATUS_WAIT = 7;
    const STATUS_PRIORITY = 8;

    protected $fillable = [
        self::CONTRACT_CODE,
        self::NAME,
        self::PARTNER_ID,
        self::TOTAL_MONEY,
        self::FILE_ID,
        self::STATUS,
        self::TYPE,
        self::SIGN_DATE,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::STATUS_HANDOVER,
        self::PRIORITIZE,
    ];

    protected $dates = ['deleted_at'];

    //Trạng thái tài sản
    public static $assetListStatus = [
        self::STATUS_PRIORITIZE => 'Ưu tiên',
        self::STATUS_USE => 'Đang sử dụng',
        self::STATUS_REPAIR => 'Sửa chữa, bảo trì',
        self::STATUS_WARRANTY => 'Bảo hành',
        self::STATUS_GIFT => 'Đã tặng',
        self::STATUS_LIQUIDATION => 'Đã thanh lý',
        self::STATUS_BREAKDOWN => 'Hỏng hóc',
        self::STATUS_WAIT => 'Chờ bàn giao'
    ];

    //Phân loại vat
    const VAT_0  = '0';
    const VAT_5  = '0.05';
    const VAT_10 = '0.1';

    public static $listVat = [
        self::VAT_0  => '0%',
        self::VAT_5  => '5%',
        self::VAT_10 => '10%'
    ];


    public function partner() {
        return $this->hasOne(Partner::class, 'id', Contract::PARTNER_ID);
    }


    public function document() {
        return $this->hasOne(Documents::class, 'id', Contract::FILE_ID);
    }

    public function asset_imports() {
        return $this->hasMany(Contract::class, AssetImport::CONTRACT_ID, 'id');
    }

    public static function getFieldVietnamese() {
        return [
            self::NAME          => trans('field.name'),
            self::CONTRACT_CODE => trans('field.contract_code'),
            self::SIGN_DATE     => trans('field.sign_date'),
        ];
    }

}
