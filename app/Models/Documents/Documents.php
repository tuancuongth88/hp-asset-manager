<?php

namespace App\Models\Documents;

use App\Models\Contracts\Contract;
use App\Models\Partners\Partner;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Documents extends Model implements Transformable {
    use TransformableTrait,
        SoftDeletes;

    const NAME        = 'name';
    const MODEL_NAME  = 'model_name';
    const MODEL_ID    = 'model_id';
    const URL         = 'url';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';

    protected $table = 'asset_document';

    protected $fillable = [
        self::NAME,
        self::MODEL_NAME,
        self::MODEL_ID,
        self::URL,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public function contract() {
        return $this->hasMany(Contract::class, Contract::FILE_ID, 'id');
    }

    public function partner() {
        return $this->hasMany(Partner::class, Partner::LOGO, 'id');
    }
}
