<?php

namespace App\Models\Invest;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AssetPackage.
 *
 * @package namespace App\Models;
 */
class AssetPackage extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const NAME = 'name';
    const PRICE = 'price';
    const CAPITAL = 'capital';
    const START_TIME = 'start_time';
    const TYPE_CONTRACT = 'type_contract';
    const STATUS = 'status';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const PARAMS = 'params';
    const END_TIME = 'end_time';

    protected $table = 'asset_packages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::PRICE,
        self::CAPITAL,
        self::START_TIME,
        self::TYPE_CONTRACT,
        self::STATUS,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::PARAMS,
        self::END_TIME,
    ];


    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese()
    {
        return [
            self::NAME => 'tên gói thầu',
            self::PRICE => 'giá gói thầu',
            self::CAPITAL => 'nguồn vốn',
            self::START_TIME => 'thời gian thầu',
            self::TYPE_CONTRACT => 'loại gói thầu',
        ];
    }

    protected $casts = [
        'params' => 'array'
    ];

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

}
