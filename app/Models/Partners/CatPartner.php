<?php

namespace App\Models\Partners;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class CatPartner.
 *
 * @package namespace App\Models\Partners;
 */
class CatPartner extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table  = 'asset_partner_category';
    const PARTNER_ID  = 'partner_id';
    const CATEGORY_ID = 'category_id';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';

    protected $fillable = [
        self::PARTNER_ID,
        self::CATEGORY_ID,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public function partner() {
        return $this->hasOne(Partner::class, 'id', 'partner_id');
    }

    public function category() {
        return $this->hasOne( Category::class, 'id', 'category_id');
    }
}
