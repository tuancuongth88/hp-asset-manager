<?php

namespace App\Models\Partners;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Category.
 *
 * @package namespace App\Models\Partners;
 */
class Category extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'asset_category';

    const NAME   = 'name';
    const PARENT = 'parent';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';

    protected $fillable = [
        self::NAME,
        self::PARENT,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public function category_partner() {
        return $this->hasMany(Partner::class,  'partner_id', 'category_id');
    }

    public function categoryChild() {
        return $this->hasMany(Category::class,  'parent', 'id');
    }

    public function categoryParent(){
        return $this->hasOne( Category::class, 'id', 'parent' );
    }

    public static function getFieldVietnamese() {
        return [
            self::NAME       => trans('field.name'),
        ];
    }
}