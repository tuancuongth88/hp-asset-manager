<?php

namespace App\Models\Partners;

use App\Models\Assets\Asset;
use App\Models\Assets\AssetImport;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Group.
 *
 * @package namespace App\Models\Partners;
 */
class Group extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $items;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'asset_group';
    const PARENT_ID = 'parent_id';
    const NAME = 'name';
    const ORDER = 'order';
    const STATUS = 'status';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const GROUP_CODE = 'group_code';

    protected $fillable = [
        self::PARENT_ID,
        self::ORDER,
        self::NAME,
        self::STATUS,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::GROUP_CODE,
    ];

//    public function contract() {
//        return $this->hasMany(Group::class, Contract::GROUP_ID, 'id');
//    }

    public function asset_imports()
    {
        return $this->hasMany(Group::class, AssetImport::GROUP_ID, 'id');
    }

    public function assets()
    {
        return $this->hasMany(Group::class, Asset::GROUP_ID, 'id');
    }

    public static function getFieldVietnamese()
    {
        return [
            self::NAME => trans('field.name'),
            self::STATUS => trans('filed.status'),
        ];
    }

    public static function getHierarchy($str_repeat): array
    {
        return (new self())->getGroup($str_repeat);
    }


    private function getGroup($str_repeat): array
    {
        $mainGroup = self::where('parent_id', 0)->orderBy('name', 'asc')->orderBy('order')->get();
        if ($mainGroup->count() > 0 && !empty($mainGroup)) {
            foreach ($mainGroup as $item) {
                $this->items[] = $item->toArray();
                $this->getParentGroup($item, 0, $str_repeat);
            }
            return $this->items;
        } else {
            return [];
        }

    }

    private function getParentGroup($item, $level, $str_repeat)
    {
        $str_repeat = isset($str_repeat) ? $str_repeat : 'name';
        if ($subGroup = $item->hasSubGroup) {
            $level++;
            foreach ($subGroup as $subItem) {
                $subItem->$str_repeat = str_repeat(' - ', $level) . $subItem->$str_repeat;
                $this->items[] = $subItem->toArray();
                $this->getParentGroup($subItem, $level, $str_repeat);
            }
        }

    }

    public function hasSubGroup()
    {
        return $this->hasMany($this, 'parent_id');
    }

    public static function genHtmlGroup($selected)
    {
        $data = Group::getHierarchy('group_code');
        $html = '';
        foreach ($data as $key => $value) {
            if ($value['id'] == (int)$selected) {
                $html .= '<option value="' . $value['id'] . '" selected >';
            } else {
                $html .= '<option value="' . $value['id'] . '">';
            }
            $html .= $value['group_code'] . ' - ' . $value['name'];
            $html .= '</option>';
        }
        return $html;
    }

    public function getAllChildGroup($id, $array = array())
    {
        $return = $this->where('parent_id', $id)->where('id', '<>', $id)->get();
        for ($i = 0; $i < sizeof($return); $i++) {
            $sub_cat = $return[$i]['id'];
            $array[] = $sub_cat;
            $array = $this->getAllChildGroup($sub_cat, $array);
        }
        return $array;
    }
}
