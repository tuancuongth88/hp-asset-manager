<?php

namespace App\Models\Partners;

use App\Models\Assets\Asset;
use App\Models\Assets\AssetImport;
use App\Models\Contracts\Contract;
use App\Models\Documents\Documents;
use App\Models\Systems\Position;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Partner.
 *
 * @package namespace App\Models\Partners;
 */
class Partner extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table   = 'asset_partner';
    const NAME         = 'name';
    const CONTACT_INFO = 'contact_info';
    const EMAIL        = 'email';
    const PHONE        = 'phone';
    const LOGO         = 'logo';
    const TAX_CODE     = 'tax_code';
    const DELEGATE     = 'delegate';
    const POSITION     = 'position';
    const WEBSITE      = 'website';
    const ADDRESS      = 'address';
    const CREATED_BY   = 'created_by';
    const UPDATED_BY   = 'updated_by';
    const HOT_LINE     = 'hot_line';
    const DISTRICT_ID  = 'district_id';
    const CITY_ID      = 'city_id';
    const PROVIDE_CATEGORY      = 'provide_category';

    protected $fillable = [
        self::NAME,
        self::CONTACT_INFO,
        self::EMAIL,
        self::PHONE,
        self::LOGO,
        self::TAX_CODE,
        self::DELEGATE,
        self::POSITION,
        self::WEBSITE,
        self::ADDRESS,
        self::HOT_LINE,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::DISTRICT_ID,
        self::CITY_ID,
        self::PROVIDE_CATEGORY,
    ];

    protected $casts = [
        'contact_info' => 'array',
        'provide_category' => 'array'
    ];
    public static function getFieldVietnamese() {
        return [
            self::NAME         => trans('field.name'),
            self::EMAIL        => trans('field.email'),
            self::PHONE        => trans('field.phone'),
            self::WEBSITE      => trans('field.website'),
            self::ADDRESS      => trans('field.address'),
            self::HOT_LINE     => trans('field.hot_line'),
            self::POSITION     => trans('field.position'),
            self::TAX_CODE     => trans('field.tax'),
            self::CITY_ID      => trans('field.city_id'),
            self::DISTRICT_ID  => trans('field.district_id'),
            self::CONTACT_INFO => trans('field.contact_info'),
            self::DELEGATE     => trans('field.delegate')
        ];
    }

    public function category() {
        return $this->belongsToMany(Category::class,  'asset_partner_category', 'partner_id', 'category_id');
    }

    public function position_partner() {
        return $this->hasOne(Position::class, 'id', Partner::POSITION);
    }

    public function contract() {
        return $this->hasMany(Contract::class, Contract::PARTNER_ID, 'id');
    }

    public function asset() {
        return $this->hasMany(Asset::class, Asset::PARTNER_ID, 'id');
    }

    public function document() {
        return $this->hasOne(Documents::class, 'id', Partner::LOGO);
    }

    public function partner_imports() {
        return $this->hasMany(Partner::class, AssetImport::PARTNER_ID, 'id');
    }
}