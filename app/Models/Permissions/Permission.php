<?php namespace App\Models\Permissions;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission {

    const NAME         = 'name';
    const DISPLAY_NAME = 'display_name';
    const DESCRIPTION  = 'description';

    protected $fillable = [
        self::NAME,
        self::DISPLAY_NAME,
        self::DESCRIPTION,
    ];
}