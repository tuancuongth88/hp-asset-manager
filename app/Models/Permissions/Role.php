<?php
namespace App\Models\Permissions;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {

    const NAME         = 'name';
    const DISPLAY_NAME = 'display_name';
    const DESCRIPTION  = 'description';

    protected $fillable = [
        self::NAME,
        self::DISPLAY_NAME,
        self::DESCRIPTION,
    ];
}