<?php

namespace App\Models\Systems;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class StructureCompany.
 *
 * @package namespace App\Models;
 */
class StructureCompany extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $items;


    const PARENT_ID = 'parent_id';
    const NAME = 'name';
    const SORT = 'sort';
    const ORDER = 'order';
    const TYPE = 'type';
    const COMPANY_ID = 'company_id';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';

    const TYPE_BRANCH = 1;
    const TYPE_BLOCK = 2;
    const TYPE_DEPARTMENT = 3;
    const TYPE_AREA_COMPANY = 4;
    const TYPE_ASSOCIATE_COMPANY = 5;

    protected $table = 'structure_companies';

    protected $fillable = [
        self::PARENT_ID,
        self::NAME,
        self::SORT,
        self::ORDER,
        self::TYPE,
        self::COMPANY_ID,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static $listType = [
        self::TYPE_BRANCH => 'Chi nhánh',
        self::TYPE_BLOCK => 'Khối',
        self::TYPE_DEPARTMENT => 'Phòng ban',
        self::TYPE_AREA_COMPANY => 'Khu vực - VPBH',
        self::TYPE_ASSOCIATE_COMPANY => 'Cty liên kết',
    ];

    public static function getHierarchy($company_id): array
    {
        return (new self())->getStructure($company_id);
    }


    private function getStructure($company_id): array
    {
        $mainStructure = self::where('company_id', $company_id)->where('parent_id', 0)->orderBy('order')->get();
        foreach ($mainStructure as $item) {
            $this->items[] = $item->toArray();
            $this->getParentStructure($item, 0);
        }

        return $this->items;
    }

    private function getParentStructure($item, $level)
    {
        if ($subStructures = $item->hasSubStructures) {
            $level++;
            foreach ($subStructures as $subItem) {
                $subItem->name = str_repeat(' - ', $level) . $subItem->name;
                $this->items[] = $subItem->toArray();
                $this->getParentStructure($subItem, $level);
            }
        }

    }

    public function hasSubStructures()
    {
        return $this->hasMany($this, 'parent_id');
    }

    public static function genHtmlStructure($company, $selected)
    {
        $data = StructureCompany::getHierarchy($company);
        $html = '<option value="">-- Chọn đơn vị trực thuộc --</option>';
        foreach ($data as $key => $value) {
            if ($value['id'] == (int)$selected) {
                $html .= '<option value="' . $value['id'] . '" selected >';
            } else {
                $html .= '<option value="' . $value['id'] . '">';
            }
            $html .= $value['name'] . ' | ' . StructureCompany::$listType[$value['type']];
            $html .= '</option>';
        }
        return $html;
    }

    public function getAllChildStructure($structure_id, $array = array()) {
        $return = $this->where('parent_id', $structure_id)->where('id', '<>', $structure_id)->get();
        for ($i = 0; $i < sizeof($return); $i++) {
            $sub_cat = $return[$i]['id'];
            $array[] = $sub_cat;
            $array   = $this->getAllChildStructure($sub_cat, $array);
        }
        return $array;
    }
}
