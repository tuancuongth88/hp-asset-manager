<?php

namespace App\Models\Users;

use App\Models\Systems\Company;
use App\Models\Systems\Position;
use App\Models\Systems\StructureCompany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User.
 *
 * @package namespace App\Models\Users;
 */
class User extends Authenticatable implements Transformable {
    use TransformableTrait,
        Notifiable;
    use EntrustUserTrait {restore as private restoreA;}
    use SoftDeletes {restore as private restoreB;}

    public function restore() {
        $this->restoreA();
        $this->restoreB();
    }

    const TABLE             = 'users';
    const FULLNAME          = 'fullname';
    const USERNAME          = 'username';
    const EMAIL             = 'email';
    const PHONE             = 'phone';
    const POSITION_ID       = 'position_id';
    const CREATE_BY         = 'create_by';
    const UPDATED_BY        = 'updated_by';
    const AVATAR            = 'avatar';
    const BIRTHDAY          = 'birthday';
    const COMPANY_ID        = 'company_id';
    const GENDER            = 'gender';
    const ADDRESS           = 'address';
    const ACTIVE            = 'active';
    const PASSWORD          = 'password';
    const CURRENT           = 'current';
    const DELETED           = 'deleted';
    const ITEMS             = 'items';
    const TYPE              = 'type';
    const IDENTITY          = 'identity';
    const PROVIDER          = 'provider';
    const PROVIDER_ID       = 'provider_id';
    const PARENT_ID         = 'parent_id';
    const MALE              = 1;
    const FEMALE            = 2;
    const STRUCTURE_ID      = 'structure_id';
    const STRUCTURE_TYPE    = 'structure_type';

    protected $fillable = [
        self::USERNAME,
        self::FULLNAME,
        self::EMAIL,
        self::PASSWORD,
        self::ADDRESS,
        self::POSITION_ID,
        self::BIRTHDAY,
        self::GENDER,
        self::PHONE,
        self::AVATAR,
        self::IDENTITY,
        self::PROVIDER,
        self::PROVIDER_ID,
        self::ACTIVE,
        self::PARENT_ID,
        self::STRUCTURE_ID,
        self::STRUCTURE_TYPE,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::PASSWORD, 'remember_token',
    ];

    public function routeNotificationForMail() {
        return $this->email;
    }

    public static function getFieldVietnamese() {
        return [
            self::EMAIL    => trans('field.email'),
            self::PASSWORD => trans('field.password'),
            self::PHONE    => trans('field.phone'),
            self::BIRTHDAY => trans('field.birthday'),
            self::FULLNAME => trans('field.fullname'),
            self::IDENTITY => trans('field.identity'),
        ];
    }

    public function setEmailAttribute($value) {
        $this->attributes[self::EMAIL] = strtolower($value);
    }

    public function setBirthdayAttribute($value) {
        $this->attributes[self::BIRTHDAY] = \Carbon\Carbon::parse($value);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query) {
        return $query->where(self::ACTIVE, ONE);
    }

    public function getBirthdayAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }

    public static $listGender = [
        self::MALE   => 'Nam',
        self::FEMALE => 'Nữ',
    ];

    public function structure()
    {
        return $this->belongsTo(StructureCompany::class, self::STRUCTURE_ID, 'id');
    }
    public function position() {
        return $this->belongsTo(Position::class, self::POSITION_ID, 'id');
    }

    public function company() {
        return $this->belongsTo(Company::class, self::COMPANY_ID, 'id');
    }

    public function scopeSearch($query, $search = '', $field = '') {
        if (empty($field)) {
            $fields = array(self::FULLNAME, self::EMAIL, self::PHONE);
        } else {
            $fields = explode(',', $field);
        }
        $search = str_replace('%', '\%', $search);
        return $query->where(function ($q) use ($search, $fields) {
            if (!empty($fields)) {
                foreach ($fields as $value) {
                    $array_search = explode('"', $search);
                    foreach ($array_search as $k => $str) {
                        if (!empty($str)) {
                            if ($k % 2 == 1) {
                                $q->orWhere($value, $str);
                            } else {
                                $q->orWhere($value, 'like', '%' . $str . '%');
                            }
                        }

                    }
                }
            }
        });
    }

    public function getListUserByRole($permission) {
        $listUser = $this->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('permission_role', 'role_user.role_id', '=', 'permission_role.role_id')
            ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
            ->where('permissions.name', $permission)->select('users.id', 'users.fullname')->get();
        return $listUser;
    }
    public function getListTeacherByRole($permission, $name_search = '', $email_search = null) {
        $listTeacher = $this->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('permission_role', 'role_user.role_id', '=', 'permission_role.role_id')
            ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
            ->where([['permissions.name', '=', $permission], ['users.fullname', 'like', '%' . $name_search . '%']])
            ->orWhere([['permissions.name', '=', $permission], ['users.email', '=', $email_search]])
            ->select('users.id', 'users.fullname', 'users.email', 'users.phone')->paginate();
        return $listTeacher;
    }

    /* check user in role */

    public static function isRoleAdmin()
    {
        if (\Auth::user()->hasRole('admin')) {
            return true;
        } else {
            return false;
        }
    }

    public static function isRoleAdminHcns()
    {
        if (\Auth::user()->hasRole('adminhcns')) {
            return true;
        } else {
            return false;
        }
    }

    public static function isRoleQuanLyBanGiao()
    {
        if (\Auth::user()->hasRole('quanlybangiao')) {
            return true;
        } else {
            return false;
        }
    }

}
