<?php

namespace App\Policies\Assets;

use App\Models\Assets\Asset;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssetPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the asset.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Asset  $asset
     * @return mixed
     */
    public function view(User $user, Asset $asset = null)
    {
        if (\Auth::user()->can('assets.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create assets.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('assets.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the asset.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Asset  $asset
     * @return mixed
     */
    public function update(User $user, Asset $asset = null)
    {
        if (\Auth::user()->can('assets.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the asset.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Asset  $asset
     * @return mixed
     */
    public function delete(User $user, Asset $asset = null)
    {
        if (\Auth::user()->can('assets.delete')) {
            return true;
        }
        return false;
    }

}
