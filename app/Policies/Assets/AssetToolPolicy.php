<?php

namespace App\Policies\Assets;

use App\Models\Assets\AssetTool;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssetToolPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the asset tool.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\AssetTool  $assetTool
     * @return mixed
     */
    public function view(User $user, AssetTool $assetTool = null)
    {
        if (\Auth::user()->can('tools.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create asset tools.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('tools.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the asset tool.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\AssetTool  $assetTool
     * @return mixed
     */
    public function update(User $user, AssetTool $assetTool = null)
    {
        if (\Auth::user()->can('tools.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the asset tool.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\AssetTool  $assetTool
     * @return mixed
     */
    public function delete(User $user, AssetTool $assetTool = null)
    {
        if (\Auth::user()->can('tools.delete')) {
            return true;
        }
        return false;
    }

}
