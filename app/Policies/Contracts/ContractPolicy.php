<?php

namespace App\Policies\Contracts;

use App\Models\Contracts\Contract;
use App\Models\Users\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class ContractPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the contract.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Contract  $contract
     * @return mixed
     */
    public function view(User $user, Contract $contract = null)
    {
        if (\Auth::user()->can('contract.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create contracts.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('contract.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the contract.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Contract  $contract
     * @return mixed
     */
    public function update(User $user, Contract $contract = null)
    {
        if (\Auth::user()->can('contract.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the contract.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Contract  $contract
     * @return mixed
     */
    public function delete(User $user, Contract $contract = null)
    {
        if (\Auth::user()->can('contract.delete')) {
            return true;
        }
        return false;
    }

}
