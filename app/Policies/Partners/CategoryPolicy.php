<?php

namespace App\Policies\Partners;

use App\Models\Partners\Category;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the category.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Category  $category
     * @return mixed
     */
    public function view(User $user, Category $category = null)
    {
        if (\Auth::user()->can('category.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('category.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Category  $category
     * @return mixed
     */
    public function update(User $user, Category $category = null)
    {
        if (\Auth::user()->can('category.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Category  $category
     * @return mixed
     */
    public function delete(User $user, Category $category = null)
    {
        if (\Auth::user()->can('category.delete')) {
            return true;
        }
        return false;
    }

}
