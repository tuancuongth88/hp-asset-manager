<?php

namespace App\Policies\Partners;

use App\Models\Partners\Group;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the group.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Group  $group
     * @return mixed
     */
    public function view(User $user, Group $group = null)
    {
        if (\Auth::user()->can('group.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create groups.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('group.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the group.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Group  $group
     * @return mixed
     */
    public function update(User $user, Group $group = null)
    {
        if (\Auth::user()->can('group.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the group.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Group  $group
     * @return mixed
     */
    public function delete(User $user, Group $group = null)
    {
        if (\Auth::user()->can('group.delete')) {
            return true;
        }
        return false;
    }
}
