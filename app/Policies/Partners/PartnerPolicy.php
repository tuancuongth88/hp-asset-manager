<?php

namespace App\Policies\Partners;

use App\Models\Partners\Partner;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PartnerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the partner.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Partner  $partner
     * @return mixed
     */
    public function view(User $user, Partner $partner = null)
    {
        if (\Auth::user()->can('partner.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create partners.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('partner.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the partner.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Partner  $partner
     * @return mixed
     */
    public function update(User $user, Partner $partner = null)
    {
        if (\Auth::user()->can('partner.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the partner.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Partner  $partner
     * @return mixed
     */
    public function delete(User $user, Partner $partner = null)
    {
        if (\Auth::user()->can('partner.delete')) {
            return true;
        }
        return false;
    }

}
