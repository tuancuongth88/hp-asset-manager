<?php

namespace App\Policies\Systems;

use App\Models\Systems\Company;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the company.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function view(User $user, Company $company = null)
    {
        if (\Auth::user()->can('company.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create companies.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('company.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the company.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function update(User $user, Company $company = null)
    {
        if (\Auth::user()->can('company.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the company.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Company  $company
     * @return mixed
     */
    public function delete(User $user, Company $company = null)
    {
        if (\Auth::user()->can('company.delete')) {
            return true;
        }
        return false;
    }

}
