<?php

namespace App\Policies\Systems;

use App\Models\Systems\Position;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PositionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the position.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Position  $position
     * @return mixed
     */
    public function view(User $user, Position $position = null)
    {
        if (\Auth::user()->can('position.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create positions.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('position.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the position.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Position  $position
     * @return mixed
     */
    public function update(User $user, Position $position = null)
    {
        if (\Auth::user()->can('position.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the position.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Position  $position
     * @return mixed
     */
    public function delete(User $user, Position $position = null)
    {
        if (\Auth::user()->can('position.delete')) {
            return true;
        }
        return false;
    }
}
