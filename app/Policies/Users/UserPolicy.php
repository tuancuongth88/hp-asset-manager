<?php

namespace App\Policies\Users;

use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model = null)
    {
        if (\Auth::user()->can('user.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('user.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user, User $model = null)
    {
        if (\Auth::user()->can('user.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model = null)
    {
        if (\Auth::user()->can('user.delete')) {
            return true;
        }
        return false;
    }
}
