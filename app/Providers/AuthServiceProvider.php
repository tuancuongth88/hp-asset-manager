<?php

namespace App\Providers;

use App\Models\Assets\Asset;
use App\Models\Contracts\Contract;
use App\Models\Partners\Category;
use App\Models\Partners\Group;
use App\Models\Partners\Partner;
use App\Models\Systems\Company;
use App\Models\Systems\Position;
use App\Models\Users\User;
use App\Policies\Assets\AssetPolicy;
use App\Policies\Assets\AssetToolPolicy;
use App\Policies\Contracts\ContractPolicy;
use App\Policies\Partners\CategoryPolicy;
use App\Policies\Partners\GroupPolicy;
use App\Policies\Partners\PartnerPolicy;
use App\Policies\Systems\CompanyPolicy;
use App\Policies\Systems\PositionPolicy;
use App\Policies\Users\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Asset::class      => AssetPolicy::class,
        Contract::class   => ContractPolicy::class,
        Partner::class    => PartnerPolicy::class,
        Company::class    => CompanyPolicy::class,
        Position::class   => PositionPolicy::class,
        User::class       => UserPolicy::class,
        Group::class      => GroupPolicy::class,
        Category::class   => CategoryPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::resource('partner', PartnerPolicy::class);
        Gate::resource('contract', ContractPolicy::class);
        Gate::resource('assets', AssetPolicy::class);
        Gate::resource('company', CompanyPolicy::class);
        Gate::resource('position', PositionPolicy::class);
        Gate::resource('user', UserPolicy::class);
        Gate::resource('group', GroupPolicy::class);
        Gate::resource('category', CategoryPolicy::class);
    }
}
