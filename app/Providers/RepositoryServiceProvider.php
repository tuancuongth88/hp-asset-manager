<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\Users\UserRepository::class, \App\Repositories\Users\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Systems\CompanyRepository::class, \App\Repositories\Systems\CompanyRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Systems\PositionRepository::class, \App\Repositories\Systems\PositionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Permissions\RoleRepository::class, \App\Repositories\Permissions\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Permissions\PermissionRepository::class, \App\Repositories\Permissions\PermissionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Partners\PartnerRepository::class, \App\Repositories\Partners\PartnerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Partners\CatPartnerRepository::class, \App\Repositories\Partners\CatPartnerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Contracts\ContractRepository::class, \App\Repositories\Contracts\ContractRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Partners\CategoryRepository::class, \App\Repositories\Partners\CategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Partners\GroupRepository::class, \App\Repositories\Partners\GroupRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Assets\AssetRepository::class, \App\Repositories\Assets\AssetRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Assets\AssetLogRepository::class, \App\Repositories\Assets\AssetLogRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Assets\AssetAssetsHistoryRepository::class, \App\Repositories\Assets\AssetAssetsHistoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Assets\AssetAssetsRelationRepository::class, \App\Repositories\Assets\AssetAssetsRelationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Assets\AssetHandoverRepository::class, \App\Repositories\Assets\AssetHandoverRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Invest\AssetPackageRepository::class, \App\Repositories\Invest\AssetPackageRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Systems\StructureCompanyRepository::class, \App\Repositories\Systems\StructureCompanyRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Assets\AssetImportRepository::class, \App\Repositories\Assets\AssetImportRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Assets\UnitRepository::class, \App\Repositories\Assets\UnitRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Assets\AssetContractShowRepository::class, \App\Repositories\Assets\AssetContractShowRepositoryEloquent::class);
//        $this->app->bind(\App\Repositories\Invest\InvestmentRepository::class, \App\Repositories\Invest\InvestmentRepositoryEloquent::class);
//        $this->app->bind(\App\Repositories\Invest\InvestmentImportsRepository::class, \App\Repositories\Invest\InvestmentImportsRepositoryEloquent::class);
        //:end-bindings:
    }
}