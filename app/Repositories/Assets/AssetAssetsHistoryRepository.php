<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AssetAssetsHistoryRepository.
 *
 * @package namespace App\Repositories;
 */
interface AssetAssetsHistoryRepository extends RepositoryInterface
{
    //
}
