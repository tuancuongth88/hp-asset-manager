<?php

namespace App\Repositories\Assets;

use App\Models\Assets\AssetAssetsHistory;
use App\Validators\Assets\AssetAssetsHistoryValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class AssetAssetsHistoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AssetAssetsHistoryRepositoryEloquent extends BaseRepository implements AssetAssetsHistoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AssetAssetsHistory::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AssetAssetsHistoryValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
