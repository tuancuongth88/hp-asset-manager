<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AssetAssetsRelationRepository.
 *
 * @package namespace App\Repositories;
 */
interface AssetAssetsRelationRepository extends RepositoryInterface
{
    //
}
