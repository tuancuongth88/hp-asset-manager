<?php

namespace App\Repositories\Assets;

use App\Models\Assets\AssetAssetsRelation;
use App\Validators\Assets\AssetAssetsRelationValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class AssetAssetsRelationRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AssetAssetsRelationRepositoryEloquent extends BaseRepository implements AssetAssetsRelationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AssetAssetsRelation::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AssetAssetsRelationValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
