<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AssetContractShowRepository.
 *
 * @package namespace App\Repositories;
 */
interface AssetContractShowRepository extends RepositoryInterface
{
    //
}
