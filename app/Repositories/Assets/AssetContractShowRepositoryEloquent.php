<?php

namespace App\Repositories\Assets;

use App\Models\Assets\AssetContractShow;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Assets\AssetContractShowRepository;
use App\Validators\Assets\AssetContractShowValidator;

/**
 * Class AssetContractShowRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AssetContractShowRepositoryEloquent extends BaseRepository implements AssetContractShowRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AssetContractShow::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AssetContractShowValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
