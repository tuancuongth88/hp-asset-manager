<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AssetHandoverRepository.
 *
 * @package namespace App\Repositories;
 */
interface AssetHandoverRepository extends RepositoryInterface
{
    //
}
