<?php

namespace App\Repositories\Assets;

use App\Models\Assets\AssetHandover;
use App\Validators\Assets\AssetHandoverValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;


/**
 * Class AssetHandoverRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AssetHandoverRepositoryEloquent extends BaseRepository implements AssetHandoverRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AssetHandover::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AssetHandoverValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
