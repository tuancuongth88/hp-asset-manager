<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AssetImportRepository.
 *
 * @package namespace App\Repositories;
 */
interface AssetImportRepository extends RepositoryInterface
{
    //
}
