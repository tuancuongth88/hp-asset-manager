<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Assets\AssetImportRepository;
use App\Models\Assets\AssetImport;
use App\Validators\Assets\AssetImportValidator;

/**
 * Class AssetImportRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AssetImportRepositoryEloquent extends BaseRepository implements AssetImportRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AssetImport::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AssetImportValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
