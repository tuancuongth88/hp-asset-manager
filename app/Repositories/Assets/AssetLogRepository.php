<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AssetLogRepository.
 *
 * @package namespace App\Repositories;
 */
interface AssetLogRepository extends RepositoryInterface
{
    //
}
