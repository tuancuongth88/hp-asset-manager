<?php

namespace App\Repositories\Assets;

use App\Models\Assets\AssetLog;
use App\Validators\Assets\AssetLogValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class AssetLogRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AssetLogRepositoryEloquent extends BaseRepository implements AssetLogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AssetLog::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AssetLogValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
