<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AssetRepository.
 *
 * @package namespace App\Repositories\Assets;
 */
interface AssetRepository extends RepositoryInterface
{
    //
}
