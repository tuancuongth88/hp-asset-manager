<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Assets\assetRepository;
use App\Models\Assets\Asset;
use App\Validators\Assets\AssetValidator;

/**
 * Class AssetRepositoryEloquent.
 *
 * @package namespace App\Repositories\Assets;
 */
class AssetRepositoryEloquent extends BaseRepository implements AssetRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Asset::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AssetValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
