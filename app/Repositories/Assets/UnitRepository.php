<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UnitRepository.
 *
 * @package namespace App\Repositories;
 */
interface UnitRepository extends RepositoryInterface
{
    //
}
