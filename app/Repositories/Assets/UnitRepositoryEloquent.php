<?php

namespace App\Repositories\Assets;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Assets\UnitRepository;
use App\Models\Assets\Unit;
use App\Validators\Assets\UnitValidator;

/**
 * Class UnitRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UnitRepositoryEloquent extends BaseRepository implements UnitRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Unit::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return UnitValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
