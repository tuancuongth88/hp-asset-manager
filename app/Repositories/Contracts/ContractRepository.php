<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ContractRepository.
 *
 * @package namespace App\Repositories\Contracts;
 */
interface ContractRepository extends RepositoryInterface
{
    //
}
