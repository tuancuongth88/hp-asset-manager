<?php

namespace App\Repositories\Invest;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AssetPackageRepository.
 *
 * @package namespace App\Repositories;
 */
interface AssetPackageRepository extends RepositoryInterface
{
    //
}
