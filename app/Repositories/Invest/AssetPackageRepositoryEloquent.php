<?php

namespace App\Repositories\Invest;

use App\Models\Invest\AssetPackage;
use App\Validators\Invest\AssetPackageValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class AssetPackageRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AssetPackageRepositoryEloquent extends BaseRepository implements AssetPackageRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AssetPackage::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AssetPackageValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
