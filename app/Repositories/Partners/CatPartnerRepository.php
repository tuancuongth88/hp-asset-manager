<?php

namespace App\Repositories\Partners;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CatPartnerRepository.
 *
 * @package namespace App\Repositories\Partners;
 */
interface CatPartnerRepository extends RepositoryInterface
{
    //
}
