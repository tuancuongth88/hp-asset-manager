<?php

namespace App\Repositories\Partners;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Partners\cat_partnerRepository;
use App\Models\Partners\CatPartner;
use App\Validators\Partners\CatPartnerValidator;

/**
 * Class CatPartnerRepositoryEloquent.
 *
 * @package namespace App\Repositories\Partners;
 */
class CatPartnerRepositoryEloquent extends BaseRepository implements CatPartnerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CatPartner::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CatPartnerValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
