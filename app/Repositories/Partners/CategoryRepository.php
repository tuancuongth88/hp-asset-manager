<?php

namespace App\Repositories\Partners;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository.
 *
 * @package namespace App\Repositories\Partners;
 */
interface CategoryRepository extends RepositoryInterface
{
    //
}
