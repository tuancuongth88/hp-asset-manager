<?php

namespace App\Repositories\Partners;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface GroupRepository.
 *
 * @package namespace App\Repositories\Partners;
 */
interface GroupRepository extends RepositoryInterface
{
    //
}
