<?php

namespace App\Repositories\Partners;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PartnerRepository.
 *
 * @package namespace App\Repositories\Partners;
 */
interface PartnerRepository extends RepositoryInterface
{
    //
}
