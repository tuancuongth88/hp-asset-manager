<?php

namespace App\Repositories\Permissions;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoleRepository.
 *
 * @package namespace App\Repositories\Permissions;
 */
interface RoleRepository extends RepositoryInterface
{
    //
}
