<?php

namespace App\Repositories\Systems;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StructureCompanyRepository.
 *
 * @package namespace App\Repositories;
 */
interface StructureCompanyRepository extends RepositoryInterface
{
    //
}
