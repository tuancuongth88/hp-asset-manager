<?php

namespace App\Repositories\Systems;

use App\Models\Systems\StructureCompany;
use App\Validators\Systems\StructureCompanyValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class StructureCompanyRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class StructureCompanyRepositoryEloquent extends BaseRepository implements StructureCompanyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return StructureCompany::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return StructureCompanyValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
