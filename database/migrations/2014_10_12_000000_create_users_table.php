<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->string('username')->nullable();
            $table->string('email')->unique();
            $table->string('password', 60)->nullable();
            $table->string('fullname');
            $table->dateTime('birthday')->nullable();
            $table->string('avatar')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('position_id')->nullable();
            $table->integer('structure_id')->nullable();
            $table->integer('structure_type')->nullable();
            $table->string('provider_id')->nullable();
            $table->string('provider')->nullable();
            $table->string('address')->nullable();
            $table->string('identity')->nullable();
            $table->string('phone')->nullable();
            $table->string('website')->nullable();
            $table->integer('active')->default(0);
            $table->integer('gender')->default(0);
            $table->integer('last_login')->default(0);
            $table->string('active_code', 20)->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
