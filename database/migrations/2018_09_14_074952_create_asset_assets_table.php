<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_assets', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->integer('group_id')->nullable();
            $table->integer('partner_id')->nullable();
            $table->integer('contract_id')->nullable();
            $table->integer('type')->nullable();
            $table->string('material')->nullable();
            $table->string('color')->nullable();
            $table->string('producer')->nullable();
            $table->bigInteger('price')->nullable();
            $table->bigInteger('vat')->nullable();
            $table->bigInteger('price_vat')->nullable();
            $table->string('warranty', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->integer('count_handover')->nullable();
            $table->integer('owner')->nullable();
            $table->string('owner_name', 500)->nullable();
            $table->integer('structure_owner')->nullable();
            $table->integer('structure_owner_type')->nullable();
            $table->integer('company_owner')->nullable();
            $table->dateTime('date_use')->nullable();
            $table->json('handovers_params')->nullable();
            $table->integer('status')->nullable();
            $table->dateTime('serial_number')->nullable();
            $table->date('year_manufacture')->nullable();
            $table->date('date_import')->nullable();
            $table->date('sign_date')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_assets');
    }
}
