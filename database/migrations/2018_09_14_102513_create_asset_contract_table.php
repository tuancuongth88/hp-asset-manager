<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_contract', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name',255)->nullable();
            $table->string('contract_code', 255)->nullable();
            $table->string('partner_id', 255)->nullable();
            $table->bigInteger('total_money')->nullable();
            $table->string('file_id', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->string('type', 255)->nullable();
            $table->text('description')->nullable();
            $table->json('provide_category')->nullable();
            $table->integer('prioritize')->nullable();
            $table->integer('status_handover')->nullable();
            $table->date('sign_date')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_contract');
    }
}
