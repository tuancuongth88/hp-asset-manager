<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAssetLogsTable.
 */
class CreateAssetLogsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('asset_logs', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_id')->nullable();
            $table->integer('contract_id')->nullable();
            $table->integer('partner_id')->nullable();
            $table->json('params')->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('asset_logs');
	}
}
