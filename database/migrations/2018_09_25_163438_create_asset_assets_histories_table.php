<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAssetAssetsHistoriesTable.
 */
class CreateAssetAssetsHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_assets_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_id')->nullable();
            $table->integer('owner')->nullable();
            $table->string('owner_name', 500)->nullable();
            $table->integer('structure_owner')->nullable();
            $table->integer('structure_owner_type')->nullable();
            $table->integer('company_owner')->nullable();
            $table->date('start_time')->nullable();
            $table->date('end_time')->nullable();
            $table->integer('type')->default(0)->nullable();
            $table->integer('status')->nullable();
            $table->json('params')->nullable();
            $table->bigInteger('total_money')->nullable();
            $table->longText('description')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asset_assets_histories');
    }
}
