<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAssetHandoversTable.
 */
class CreateAssetHandoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_handovers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person')->nullable();
            $table->string('person_name')->nullable();
            $table->integer('person_structure_id')->nullable();
            $table->integer('person_structure_type')->nullable();
            $table->integer('person_company_id')->nullable();
            $table->longText('person_description')->nullable();
            $table->integer('receiver')->nullable();
            $table->string('receiver_name')->nullable();
            $table->integer('receiver_structure_id')->nullable();
            $table->integer('receiver_structure_type')->nullable();
            $table->integer('receiver_company_id')->nullable();
            $table->longText('receiver_description')->nullable();
            $table->integer('status')->nullable()
                ->comment('Trạng thái:0: Tạo mới bàn giao, 1: Nhận bàn giao, 2: Nhận bàn giao và lưu kho');
            $table->json('params')->nullable()->comment('json bao gồm: partner_id,contract_id,asset_id');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('handovers_datetime')->nullable()->comment('Ngày nhận bàn giao');
            $table->integer('number_handover')->nullable()->comment('Số lượng bàn giao');
            $table->integer('type')->nullable()
                ->comment('Loại bàn giao, 0: bàn giao tài sản mới, 1: Tài sản bảo hành bảo tri, 2: Trả về công ty  ');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asset_handovers');
    }
}
