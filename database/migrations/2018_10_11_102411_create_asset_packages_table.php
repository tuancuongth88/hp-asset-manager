<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAssetPackagesTable.
 */
class CreateAssetPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->comment('tên gói thầu');
            $table->bigInteger('price')->nullable()->comment('giá gói thầu');
            $table->string('capital')->nullable()->comment('Nguồn vốn');
            $table->date('start_time')->nullable()->comment('thời gian bắt đầu');
            $table->string('type_contract')->nullable()->comment('Loại hợp đồng');
            $table->integer('status')->nullable()->comment('trạng thái');
            $table->json('params')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asset_packages');
    }
}
