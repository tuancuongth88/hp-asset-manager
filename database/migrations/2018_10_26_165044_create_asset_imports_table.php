<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateAssetImportsTable.
 */
class CreateAssetImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unit_id')->nullable()->comment('Đơn vị(cái, chiếc, ....');
            $table->integer('group_id')->nullable()->comment('nhóm tài sản');
            $table->integer('contract_id')->nullable();
            $table->integer('partner_id')->nullable();
            $table->integer('company_id')->nullable()->comment('Công ty');
            $table->integer('structure_id')->nullable()->comment('cơ cấu công ty');
            $table->integer('structure_type')->nullable()->comment('loại cơ cấu');
            $table->string('name')->nullable()->comment('tên tài sản');
            $table->string('material')->nullable()->comment('Chất liệu');
            $table->string('color')->nullable()->comment('màu sắc');
            $table->integer('quantity')->nullable()->comment('Số lượng tài sản nhập');
            $table->string('producer')->nullable()->comment('Đơn vị sản xuất/cung cấp');
            $table->integer('status')->nullable()->comment('trạng thái tài sản');
            $table->text('description')->nullable()->comment('Ghi chú');
            $table->integer('type')->nullable()->comment('Phân loại tài sản');
            $table->bigInteger('price')->nullable()->comment('giá');
            $table->bigInteger('vat')->nullable()->comment('tên gói thầu');
            $table->integer('warranty')->nullable()->comment('Số tháng bảo hành');
            $table->date('year_manufactured')->nullable()->comment('Năm sản xuất');
            $table->date('date_import')->nullable()->comment('Ngày nhập kho');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asset_imports');
    }
}
