<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledDelegateHandoverToAssetHandover extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_handovers', function (Blueprint $table) {
            $table->string('person_positions')->after('person_company_id')->nullable();
            $table->string('receiver_positions')->after('receiver_company_id')->nullable();
            $table->string('delegate_positions')->after('receiver_description')->nullable();
            $table->integer('delegate_structure_id')->after('receiver_description')->nullable();
            $table->integer('delegate_company_id')->after('receiver_description')->nullable();
            $table->string('delegate_name')->after('receiver_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_handovers', function (Blueprint $table) {
            $table->dropColumn('person_positions,receiver_positions,delegate_positions,delegate_structure_id,delegate_company_id,delegate_name');
        });
    }
}
