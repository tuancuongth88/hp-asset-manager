<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetHandoverDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_handover_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('handover_id')->nullable();
            $table->integer('asset_id')->nullable();
            $table->string('name')->nullable();
            $table->integer('group_id')->nullable();
            $table->integer('type')->nullable();
            $table->integer('status')->nullable();
            $table->string('construct_code')->nullable();
            $table->integer('check_count_handover')->nullable();
            $table->bigInteger('price')->nullable();
            $table->json('params')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('unit_id')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asset_handover_detail');
    }
}
