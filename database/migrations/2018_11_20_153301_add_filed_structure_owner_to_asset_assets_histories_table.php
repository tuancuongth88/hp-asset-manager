<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledStructureOwnerToAssetAssetsHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_assets_histories', function (Blueprint $table) {
            $table->string('people_notice')->nullable();
            $table->text('note')->nullable();
            $table->bigInteger('price_repair')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_assets_histories', function (Blueprint $table) {
            $table->dropColumn('people_notice', 'note', 'price_repair');
        });
    }
}
