<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFieldQuantityInAssetHandoverDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_handover_detail', function (Blueprint $table) {
            $table->string('name',500)->nullable()->change();
            $table->decimal('quantity',10,2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_handover_detail', function (Blueprint $table) {
            //
        });
    }
}
