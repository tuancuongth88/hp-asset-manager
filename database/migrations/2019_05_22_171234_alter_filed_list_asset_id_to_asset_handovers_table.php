<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFiledListAssetIdToAssetHandoversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_handovers', function (Blueprint $table) {
            $table->json('list_asset_id')->nullable()->comment('Danh sach asset id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_handovers', function (Blueprint $table) {
            $table->dropColumn('list_asset_id');
        });
    }
}
