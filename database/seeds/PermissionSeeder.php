<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('permissions')->insert([
             [
                 'name'         => 'user.create',
                 'display_name' => 'Thêm user',
             ],
             [
                 'name'         => 'user.update',
                 'display_name' => 'Cập nhật user',
             ],
             [
                 'name'         => 'user.delete',
                 'display_name' => 'Xóa user',
             ],
             [
                 'name'         => 'user.view',
                 'display_name' => 'Xem user',
             ],
             [
                 'name'         => 'department.create',
                 'display_name' => 'Thêm phòng ban',
             ],
             [
                 'name'         => 'department.update',
                 'display_name' => 'Cập nhật phòng ban',
             ],
             [
                 'name'         => 'department.delete',
                 'display_name' => 'Xóa phòng ban',
             ],
             [
                 'name'         => 'department.view',
                 'display_name' => 'Xem phòng ban',
             ]

             , [
                 'name'         => 'branch.create',
                 'display_name' => 'Thêm chi nhánh',
             ],
             [
                 'name'         => 'branch.update',
                 'display_name' => 'Cập nhật chi nhánh',
             ],
             [
                 'name'         => 'branch.delete',
                 'display_name' => 'Xóa chi nhánh',
             ],
             [
                 'name'         => 'branch.view',
                 'display_name' => 'Xem chi nhánh',
             ]

             , [
                 'name'         => 'company.create',
                 'display_name' => 'Thêm công ty',
             ],
             [
                 'name'         => 'company.update',
                 'display_name' => 'Cập nhật công ty',
             ],
             [
                 'name'         => 'company.delete',
                 'display_name' => 'Xóa công ty',
             ],
             [
                 'name'         => 'company.view',
                 'display_name' => 'Xem công ty',
             ]

             , [
                 'name'         => 'position.create',
                 'display_name' => 'Thêm chức vụ',
             ],
             [
                 'name'         => 'position.update',
                 'display_name' => 'Cập nhật chức vụ',
             ],
             [
                 'name'         => 'position.delete',
                 'display_name' => 'Xóa chức vụ',
             ],
             [
                 'name'         => 'position.view',
                 'display_name' => 'Xem chức vụ',
             ],
             [
                'name'         => 'group.create',
                'display_name' => 'Thêm group',
             ],
             [
                'name'         => 'group.update',
                'display_name' => 'Cập nhật group',
             ],
             [
                'name'         => 'group.delete',
                'display_name' => 'Xóa group',
             ],
             [
                'name'         => 'group.view',
                'display_name' => 'Xem group',
             ],

             [
                 'name'         => 'role.create',
                 'display_name' => 'Thêm vai trò phân quyền',
             ],
             [
                 'name'         => 'role.update',
                 'display_name' => 'Cập nhật vai trò phân quyền',
             ],
             [
                 'name'         => 'role.delete',
                 'display_name' => 'Xóa vai trò phân quyền',
             ],
             [
                 'name'         => 'role.view',
                 'display_name' => 'Xóa vai trò phân quyền',
             ],

             [
                'name'         => 'assets.create',
                'display_name' => 'Thêm tài sản',
             ],
             [
                'name'         => 'assets.update',
                'display_name' => 'Cập nhật tài sản',
             ],
             [
                'name'         => 'assets.delete',
                'display_name' => 'Xóa tài sản',
             ],
             [
                'name'         => 'assets.view',
                'display_name' => 'Xem tài sản',
             ],

             [
                'name'         => 'contract.create',
                'display_name' => 'Thêm hợp đồng',
             ],
             [
                'name'         => 'contract.update',
                'display_name' => 'Cập nhật hợp đồng',
             ],
             [
                'name'         => 'contract.delete',
                'display_name' => 'Xóa hợp đồng',
             ],
             [
                'name'         => 'contract.view',
                'display_name' => 'Xem hợp đồng',
             ],

             [
                'name'         => 'category.create',
                'display_name' => 'Thêm hạng mục thầu',
             ],
             [
                'name'         => 'category.update',
                'display_name' => 'Cập nhật hạng mục thầu',
             ],
             [
                'name'         => 'category.delete',
                'display_name' => 'Xóa hạng mục thầu',
             ],
             [
                'name'         => 'category.view',
                'display_name' => 'Xem hạng mục thầu',
             ],

             [
                'name'         => 'partner.create',
                'display_name' => 'Thêm nhà thầu',
             ],
             [
                'name'         => 'partner.update',
                'display_name' => 'Cập nhật nhà thầu',
             ],
             [
                'name'         => 'partner.delete',
                'display_name' => 'Xóa nhà thầu',
             ],
             [
                'name'         => 'partner.view',
                'display_name' => 'Xem nhà thầu',
             ],
             [
                'name'         => 'tools.create',
                'display_name' => 'Thêm công cụ, dụng cụ',
             ],
             [
                'name'         => 'tools.update',
                'display_name' => 'Cập nhật công cụ, dụng cụ',
             ],
             [
                'name'         => 'tools.delete',
                'display_name' => 'Xóa công cụ, dụng cụ',
             ],
             [
                'name'         => 'tools.view',
                'display_name' => 'Xem công cụ, dụng cụ',
             ],
             [
                'name'         => 'handover.create',
                'display_name' => 'Thêm tài sản bàn giao',
             ],
             [
                'name'         => 'handover.update',
                'display_name' => 'Cập nhật tài sản bàn giao',
             ],
             [
                'name'         => 'handover.delete',
                'display_name' => 'Xóa tài sản bàn giao',
             ],
             [
                'name'         => 'handover.view',
                'display_name' => 'Xem tài sản bàn giao',
             ],
        ]);
    }
}
