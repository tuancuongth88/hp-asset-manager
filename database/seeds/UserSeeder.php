<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            'username'      => 'admin',
            'email'         => 'admin@nghetuvan.com',
            'fullname'      => 'Admin',
            'avatar'        => '/no-avatar.ico',
            // 'birthday'      => '01-01-2016',
            'password'      => \Hash::make(123456),
            'structure_id'     => 1,
            'company_id'    => 1,
            'position_id'   => 1,
            'active'        => 1,
            'created_at'    => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
