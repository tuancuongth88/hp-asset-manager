(function ($, mainObj) {
    // Whole-script strict mode syntax
    'use strict';
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('.tooltip').css("background:#fff", "font-size:100%", "color:#000");

        //Định dạng select2
        $('.m-select2').select2();

        // cho hiển thị bảng con trong table
        $('.sub-table').hide();
        $('.header').click(function () {
            $(this).toggleClass('expand').nextUntil('tr.header').slideToggle(200);
        });

        // Upload file attachment cho create contract

        // Định dạng giá cho các trường tính tiền
        $('.price').number(true, 0);
        $('.quantity').number(true, 0);
        $('.total_money').number(true, 0);

        // chọn con cho hạng mục thầu
        $('#category_id').on('change', function (e) {
            e.preventDefault();
            let id = $(this).val();
            mainObj.getChildCategory(id);
        });

        // Định dạng ngày tháng
        $('#sign_date').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            }
        });
        $('#birthday').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });


        // Lấy thông tin nhà thầu trong contract
        // $('#partner_id2').on('change', function (e) {
        //     console.log('FASDFADSFASD');
        //     let id = $(this).val();
        //     mainObj.resetForm();
        //     mainObj.getPartner(id);
        // });

        // Lấy thông tin quận huyện theo thành phố
        $('#city_id').change(function (e) {
            let id = $(this).val();
            if (id == "") {
                $('select[name="district_id"]').empty();
                $('select[name="district_id"]').append('<option value="">---Lựa chọn---</option>');
            } else {
                mainObj.getDistrictByCity(id);
            }
        });

        mainObj.getTotalMoney();
        mainObj.getPriceVat();
        mainObj.dateImport();
        $('#asset_import').repeater({
            initEmpty: false,
            show: function () {
                $(this).slideDown(300, function () {
                    $('#importsForm').validate();
                });
                mainObj.getTotalMoney();
                mainObj.getPriceVat();
                mainObj.dateImport();
            },

            hide: function (deleteElement) {
                if (confirm('Bạn có chắc chắn muốn xóa?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });

        $('#contact_clue').repeater({
            initEmpty: false,
            show: function () {
                $(this).slideDown();
                console.log($(this));
            },

            hide: function (deleteElement) {
                if (confirm('Bạn có chắc chắn muốn xóa?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });

        $('#price_vat').on('click', function (e) {
            mainObj.getPriceVat();
        });

        $('#total_money').on('click', function (e) {
            let id = $(this).val();
            mainObj.getTotalMoney();
        });

        // chọn phòng ban trực thuộc cho form search
        $('.drl_company_owner').on('change', function (e) {
            e.preventDefault();
            mainObj.initGetStructureByCompany();
        });

        // chọn phòng ban trực thuộc cho form create
        $('#company_id').on('change', function () {
            let id = $(this).val();
            mainObj.ajaxSetupFunc();
            $.ajax({
                url: '../../system/user/get-structure/' + id,
                cache: false,
                type: "get",
                processData: false,
                contentType: false,
                success: function (response) {
                    $('#structure_id').empty().append(response.html);
                },
                error: function (data) {

                }
            });
        });

        // chọn phòng ban trực thuộc cho form edit
        $('#company_asset_id').on('change', function () {
            let id = $(this).val();
            mainObj.ajaxSetupFunc();
            $.ajax({
                url: '../system/user/get-structure/' + id,
                cache: false,
                type: "get",
                processData: false,
                contentType: false,
                success: function (response) {
                    $('#structure_asset_id').empty().append(response.html);
                },
                error: function (data) {

                }
            });
        });
    });
})(jQuery, mainObj);


