/*
* Usage:
* ------------
*
*
* Public APIs:
* ------------
*
* Event triggers:
* --------------
*
* @author tungtran
* @since 2018-11-22
* */
// Whole-script strict mode syntax
'use strict';
let mainObj = {
    // get district flow city
    getDistrictByCity: function (id) {
        $.ajax({
            url: '/administrator/asset/district/' + id,
            type: 'GET',
            contentType: "application/json",
            success: function (result) {
                $('select[name="district_id"]').empty();
                $.each(result.data, function (key, value) {
                    $('select[name="district_id"]').append('<option value="' + key + '">' + value + '</option>');
                });
            }
        })
    },

    // format trường ngày tháng nhập kho
    dateImport: function () {
        $('.date_import').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            }
        });
    },

    totalMoney: function (price, vat, quantity) {
        return parseFloat((price + (price * vat)) * quantity);
    },

    priceVat: function (price, vat) {
        return parseFloat(price + (price * vat));
    },

    getTotalMoney: function () {
        $('.total_money').on('click', function (e) {
            let parent = $(this).closest('.class-repeater-item');
            let price = parent.find('.price').val();
            let vat = parent.find('.vat').val();
            let quantity = parent.find('.quantity').val();
            return parent.find('.total_money').val(mainObj.totalMoney(Number(price), Number(vat), Number(quantity)));
        });
    },

    getPriceVat: function () {
        $('.price_vat').on('click', function (e) {
            let parent = $(this).closest('.class-repeater-item');
            let price = parent.find('.price').val();
            let vat = parent.find('.vat').val();
            parent.find('.price_vat').val(mainObj.priceVat(Number(price), Number(vat)));
        });
    },

    resetForm: function () {
        $('#phone').val("");
        $('#email').val("");
        $('#address').val("");
        $('#tax_code').val("");
        $('#delegate').val("");
        $('#website').val("");
        $('#position').val("");
        $('#sign_date').val("");
        $('#contract_code').val("");
    },

    resetFormCatPartner: function () {
        $('#childCategory').empty();
    },

    getPartner: function (id) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: id,
            type: 'GET',
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                // resetForm();
                $('#address').val(data.data.address);
                $('#email').val(data.data.email);
                $('#phone').val(data.data.phone);
                $('#tax_code').val(data.data.tax_code);
                $('#delegate').val(data.data.delegate);
                $('#website').val(data.data.website);
                $('#position').val(data.data.position);
            }
        });
    },

    getChildCategory: function (id) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: id,
            type: 'GET',
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                mainObj.resetFormCatPartner();
                $('#childCategory').append(data.viewCategory);
            }
        });
    },

    initGetStructureByCompany: function () {
        let company_id = $('.drl_company_owner').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '../system/user/get-structure/' + company_id,
            cache: false,
            type: "get",
            processData: false,
            contentType: false,
            success: function (response) {
                $('.drl_structure_owner').empty().append(response.html);
            },
            error: function (data) {

            }
        });
        return false;
    },

    ajaxSetupFunc: function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
};