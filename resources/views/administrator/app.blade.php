<!DOCTYPE html>
<html>
@include('administrator.theme.header')
<body>
@include('administrator.theme.header-bar')
<div class="main-content">
@yield('content')
</div>
@include('administrator.theme.footer-bar')

@include('administrator.theme.script')

@yield('script')
</body>
</html>