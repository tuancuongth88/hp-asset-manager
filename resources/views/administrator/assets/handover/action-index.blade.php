{{--@if($value->status == 1)--}}
{{--    {{ Form::open(array('method'=>'POST',--}}
{{--    'route' => array('handover.active'),--}}
{{--    'style' => 'display: inline-block;')) }}--}}
{{--    <button class="btn-border-none"--}}
{{--            data-original-title="Đã nhận bàn giao"--}}
{{--            title="Đã nhận bàn giao"--}}
{{--            data-toggle="m-tooltip">--}}
{{--        <i class="fa fa-check-square"></i>--}}
{{--    </button>--}}
{{--    <input type="hidden" name="id" value="{{$value->id}}">--}}
{{--    <input type="hidden" name="filter_handover" value="{{$filter_handover}}">--}}
{{--    {{ Form::close() }}--}}
{{--@endif--}}

{{ Form::open(array('method'=>'GET','route' => array('handover.show', $value->id),'style' => 'display: inline-block;')) }}
<button class="btn-border-none" data-original-title="Xem bàn giao" title="Xem bàn giao" data-toggle="m-tooltip">
    <i class="fa fa-eye"></i>
</button>
{{ Form::close() }}

{{--{{ Form::open(array('method'=>'POST',--}}
{{--'route' => array('handover.active_store', $value->id),--}}
{{--'style' => 'display: inline-block;')) }}--}}
{{--<button class="btn-border-none"--}}
{{--data-original-title="Nhận bàn giao và lưu kho"--}}
{{--title="Nhận bàn giao và lưu kho"--}}
{{--data-toggle="m-tooltip"--}}
{{-->--}}
{{--<i class="fa fa-archive"></i>--}}
{{--</button>--}}
{{--{{ Form::close() }}--}}

@if($value->status == 1)
    {{ Form::open(array('method'=>'DELETE','route' => array('handover.destroy', $value->id),'style' => 'display: inline-block;')) }}
    <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="btn-border-none"
            data-original-title="Xóa bàn giao" data-toggle="m-tooltip" title="Xóa bàn giao">
        <i class="fa fa-trash"></i>
    </button>
@endif
{{ Form::close() }}

<a class="btn-border-none" data-original-title="In phiếu bàn giao" data-toggle="m-tooltip" title="In phiếu bàn giao"
   href="{{route('handover.detail',$value->id)}}" target="_blank">
    <i class="fa fa-print"></i>
</a>
