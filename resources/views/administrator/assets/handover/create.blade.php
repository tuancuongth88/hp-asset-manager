@extends('administrator.app')
@section('title','Bàn giao mới tài sản')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-6">
                    <h2 class="title_list">Thêm mới bàn giao tài sản</h2>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                    <a href="{{ route('handover.index') }}"
                       class="btn m-btn m-btn--icon btn-add-color-cat-right">
                    <span>
                        <i class="fa flaticon-list-3"></i>
                        <span>
                            Danh sách bàn giao tài sản
                        </span>
                    </span>
                    </a>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <h3 class="sub_title">Thông tin cơ bản</h3>
                        {{ Form::open(array(
                        'route' => 'handover.store',
                        'class' => 'm-form m-form--fit m-form--label-align-right',
                         'files'=>true)) }}
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <div class="m-portlet__body row">
                            <div class="col-lg-12">
                                <div class="form-group m-form__group">
                                    <label for="name">
                                        Hình thức bàn giao<span style="color: red;">(*)</span>
                                    </label>
                                    <select class="form-control m-input drl_type_handover m-select2"
                                            name="type_handover" style="width: 20%">
                                        @foreach (\App\Models\Assets\AssetHandover::$assetHandoverType as $key_hand_type => $value_hand_type)
                                            <option value="{{ $key_hand_type }}"
                                                    {{($type_handover == $key_hand_type) ? 'selected':'' }}
                                            >
                                                {{ $value_hand_type }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @include('administrator.assets.handover.type')

                        <div class="row" id="asset_handover">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <h3 class="sub_title" style="padding: 20px 0 20px 0;">
                                    Chi tiết bàn giao
                                </h3>
                            </div>
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <div class="divTable blueTable">
                                    <div class="divTableHeading">
                                        <div class="divTableRow">
                                            <div class="divTableHead">STT</div>
                                            <div class="divTableHead">Tên TS</div>
                                            <div class="divTableHead">Nhóm TS</div>
                                            <div class="divTableHead">Phân loại</div>
                                            <div class="divTableHead">Trạng thái</div>
                                            <div class="divTableHead">Đơn giá(VND)</div>
                                            <div class="divTableHead">TS hiện có</div>
                                            <div class="divTableHead">Số lượng BG</div>
                                            <div class="divTableHead">Thành tiền</div>
                                            <div class="divTableHead">Ghi chú</div>
                                        </div>
                                    </div>
                                    <div class="divTableBody handover_content_body"
                                         data-repeater-list="handover_content">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <p class="add_contract" data-toggle="modal" data-target="#edit-modal" style="background: #b7b7b7; border-radius: 4px; padding-bottom: 2px;">
                                    + Thêm nội dung bàn giao
                                </p>
                            </div>
                            @include('administrator.assets.handover.modal')
                        </div>
                        <section class="button">
                            <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                                <div class="m-form__actions">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-12"></div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <button type="submit" class="save_bid">
                                                    Lưu
                                                </button>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <a class="delete_bid" href="{{ route('handover.index') }}">
                                                    Trở về danh sách
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        {{ Form::close() }}
                    </div>
                    <!--end::Portlet-->
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
    @include('administrator.assets.handover.script')
@stop
