<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/handover.css') }}"/>
</head>
<body class="c4">
<p class="c27">
    <span class="c25 c38"></span>
</p>
<table class="c39">
    <tbody>
    <tr class="c1">
        <td class="c40" colspan="1" rowspan="1">
            <p class="c9">
                <span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 112.13px; height: 107.07px; text-align: center">
                    <img alt="" src="{{ URL::asset('images/image2.jpg') }}"
                         style=" height: 107.07px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); text-align: center;"
                         title="">
                </span>
                <span class="c2">  </span>
            </p>
            <p class="c9">
                <span class="c2">Số: </span>
                <span class="c8 c25">{{$data->id}}</span>
                <span class="c2">/BB-HPL</span>
            </p>
        </td>
        <td class="c36" colspan="1" rowspan="1">
            <p class="c7">
                <span class="c8 c25">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</span>
            </p>
            <p class="c7">
                <span class="c13">Độc lập - Tự do - Hạnh phúc</span>
                <span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 192.00px; height: 1.33px;">
                    <img alt="" src="{{ URL::asset('images/image1.png') }}"
                         style="width: 192.00px; height: 1.33px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
                         title="">
                </span>
            </p>
            <p class="c7 c34">
                <span class="c2">
                </span>
            </p>
            <p class="c7">
                <span class="c18 c37">Hà Nội, ngày .....tháng.....năm ......</span>
            </p>
        </td>
    </tr>
    </tbody>
</table>
<p class="c22">
    <span class="c19">BIÊN BẢN BÀN GIAO TÀI SẢN</span>
    <br/>
    <span class="c19" style="line-height: 10px;">
        ({{
            isset($data->type) ? \App\Models\Assets\AssetHandover::$assetHandoverType[$data->type]:''
        }})
    </span>
</p>
<p class="c12">
    <span class="c2">&nbsp;&nbsp;&nbsp;&nbsp;Hôm nay, ngày.....tháng.....năm ......, tại {{data_get($data,'personStructure.name') }}
        đã tiến hành bàn giao tài sản với {{data_get($data,'receiverStructure.name') }} như sau:
    </span>
</p>
<p class="c12">
    <span class="c8">1/ Bên giao</span>
</p>
<ul class="c31 lst-kix_list_1-0 start">
    <li class="c12 c20">
        <span class="c2">Ông/Bà: {{@$data->person_name}}</span>
    </li>
    <li class="c12 c20">
        <span class="c2">Chức vụ: {{@$data->person_positions}}</span>
        <span class="c2" style="float:right">Đơn vị: {{data_get($data,'personStructure.name')}}</span>
    </li>
</ul>
<p class="c12">
    <span class="c8">2/ Bên nhận</span>
</p>
<ul class="c31 lst-kix_list_1-0">
    <li class="c12 c20">
        <span class="c2">Ông/ Bà: {{@$data->receiver_name}}</span>
    </li>
    <li class="c12 c20">
        <span class="c18">Chức vụ: {{@$data->receiver_positions}}</span>
        <span class="c2" style="float:right">Đơn vị: {{data_get($data,'receiverStructure.name')}}</span>
    </li>
</ul>
@if(isset($data->delegate_name))
    <p class="c12">
        <span class="c8">3/ Bên đại diện</span>
    </p>
    <ul class="c31 lst-kix_list_1-0">
        <li class="c12 c20">
            <span class="c2">Ông/ Bà: {{@$data->delegate_name}}</span>
        </li>
        <li class="c12 c20">
            <span class="c18">Chức vụ: {{@$data->delegate_positions}}</span>
        </li>
        <li class="c12 c20">
            <span class="c2">Đơn vị: {{data_get($data,'delegateStructure.name')}}</span>
        </li>
    </ul>
@endif
<p class="c12">
    <span class="c8">NỘI DUNG BÀN GIAO</span>
</p>
<p class="c12">
    <span class="c2">&nbsp;&nbsp;&nbsp;&nbsp;{{@$data->person_description}}</span>
</p>
<p class="c7">
    <span class="c8">Bảng thống kê tài sản bàn giao</span></p>
<table class="c33">
    <tbody>
    <tr class="c24">
        <td class="c17" colspan="1" rowspan="1">
            <p class="c3"><span class="c8">STT</span></p>
        </td>
        <td class="c0" colspan="1" rowspan="1">
            <p class="c7"><span class="c8">Tên tài sản</span></p>
        </td>
        <td class="c0" colspan="1" rowspan="1">
            <p class="c7"><span class="c8">Nhóm tài sản</span></p>
        </td>
        <td class="c5" colspan="1" rowspan="1">
            <p class="c3"><span class="c8">Số lượng</span></p>
        </td>
        <td class="c5" colspan="1" rowspan="1">
            <p class="c3"><span class="c8">Đơn vị tính</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c8">Đơn giá</span></p>
        </td>
        <td class="c14" colspan="1" rowspan="1">
            <p class="c3"><span class="c8">Thành tiền</span></p>
        </td>
        <td class="c26" colspan="1" rowspan="1">
            <p class="c3"><span class="c8">Ghi chú</span></p>
        </td>
    </tr>
    @foreach($dataDetail as $key_d => $value_d)
        <?php
        $unit_lower = mb_strtolower(trim_all(data_get($value_d, 'unit.name')), 'UTF-8');
        $quantity = 0;
        if (in_array($unit_lower, UNIT_MEASUREMENT)) {
            $quantity = $value_d->quantity;
        } else {
            $quantity = number_format($value_d->quantity);
        }
        ?>
        <tr class="c35">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c3">
                    <span class="c10">{{$key_d +1}}</span>
                </p>
            </td>
            <td class="c0" colspan="1" rowspan="1">
                <p class="c11">
                <span class="c18">
                    {{$value_d->name}}
                </span>
                </p>
            </td>
            <td class="c0" colspan="1" rowspan="1">
                <p class="c11">
                <span class="c18">
                    {{data_get($value_d, 'group.group_code')}}
                </span>
                </p>
            </td>
            <td class="c5" colspan="1" rowspan="1">
                <p class="c3">
                <span class="c18 c23">
                    {{$quantity}}
                </span>
                </p>
            </td>
            <td class="c5" colspan="1" rowspan="1">
                <p class="c3">
                    <span class="c2">
                        {{data_get($value_d,'unit.name')}}
                    </span>
                </p>
            </td>
            <td class="c14" colspan="1" rowspan="1">
                <p class="c3">
                    <span class="c21">
                        {{number_format($value_d->price)}}
                    </span>
                </p>
            </td>
            <td class="c14" colspan="1" rowspan="1">
                <p class="c3">
                    <span class="c21">
                        {{number_format(($value_d->quantity*$value_d->price),0)}}
                    </span>
                </p>
            </td>
            <td class="c26" colspan="1" rowspan="1">
                <p class="c3"><span class="c21">{{$value_d->note}}</span></p>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<p class="c15"><span class="c2"></span></p>
<p class="c16">
    <span class="c2">&nbsp;&nbsp;&nbsp;&nbsp;Kể từ ngày ...................... số tài sản trên do bên nhận chịu trách nhiệm quản lý và sử dụng tại
        {{data_get($data,'receiverStructure.name') }}.</span>
</p>
<p class="c29">
    @if(isset($data->delegate_name))
        <span class="c2">Biên bản này lập thành 04 bản có giá trị như nhau. Bên giao giữ 01 bản, bên nhận giữ 01 bản, bên đại diện 01 bản,  TCKT giữ 01 bản./.</span>
    @else
        <span class="c2">Biên bản này lập thành 03 bản có giá trị như nhau. Bên giao giữ 01 bản, bên nhận giữ 01 bản, TCKT giữ 01 bản./.</span>
    @endif
</p>
<p class="c16">
    <span class="c6">    </span>
</p>
<table style="width: 100%; margin-bottom: 50px">
    <tr>
        <td style="text-align: center">
            <span class="c30" style="text-align: center">
                BÊN GIAO
            </span>
            <p class="font-italic">(ký và ghi rõ họ tên)</p>
        </td>
        <td style="text-align: center">
            <span class="c30">BÊN NHẬN</span>
            <p class="font-italic">(ký và ghi rõ họ tên)</p>
        </td>
        @if(isset($data->delegate_name))
            <td style="text-align: center">
                <span class="c30">BÊN ĐẠI DIỆN</span>
                <p class="font-italic">(ký và ghi rõ họ tên)</p>
            </td>
        @endif
        <td style="text-align: center">
            <span class="c30">B. HCNS</span>
            <p class="font-italic">(ký và ghi rõ họ tên)</p>
        </td>
    </tr>
</table>
<p class="c15"><span class="c6 c25"></span></p>
<p class="c15"><span class="c6 c25"></span></p>
<p class="c15"><span class="c6 c25"></span></p>
<p class="c16"><span class="c30"></span></p>
</body>
</html>
