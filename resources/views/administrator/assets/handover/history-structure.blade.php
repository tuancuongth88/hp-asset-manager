@extends('administrator.app')
@section('title','Lịch sử bàn giao')

@section('content')
    <div class="list-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <h2 class="title_list">
                        Lịch sử tài sản khu vực/phòng ban
                    </h2>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <section class="main-content">
                <div class="container">
                    @include('administrator.errors.info-search')
                    @include('administrator.errors.messages')
                    <div class="row gutter-2 gutter-lg-3 mb-4">
                        <div class="col-md-12 col-sm-12 mb-12 mb-md-0">
                            <h3>Khu vực/phòng ban: {{data_get($arrStructureCompany, $structure_id)}}</h3>
                        </div>
                    </div>

                    {{ Form::open(array('route' => array('handover.get-history-structure', $structure_id), 'method' => 'GET')  ) }}
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <div class="row gutter-2 gutter-lg-3 mb-4">
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Tên
                                </label>
                                <input name="name" class="form-control m-input" placeholder="Tên tài sản"
                                       value="{{request()->get('name')}}"
                                >
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Nhóm tài sản
                                </label>
                                <select class="form-control m-input drl_group m-select2"
                                        name="group_id">
                                    <option value="0">-- Tất cả --</option>
                                    <?php echo \App\Models\Partners\Group::genHtmlGroup(request()->get('group_id')) ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-6 mb-3 mb-md-0">
                            <label for="name">
                                Tìm kiếm
                            </label>
                            <input type="submit" class="btn form-control" id="search-button"
                                   value="Tìm kiếm" style="background: #f6811f;color: #fff;"
                                   data-action="{{route('assets.index')}}">
                        </div>
                    </div>
                    {{ Form::close() }}

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <table class="table m-table m-table--head-bg-warning table-hover table-mobile table_table">
                                <thead class="table_head">
                                <tr>
                                    <th style="width: 2%">
                                        #
                                    </th>
                                    <th style="width: 10%">
                                        Mã TS
                                    </th>
                                    <th style="width: 10%">
                                        Nhóm TS
                                    </th>
                                    <th style="width: 20%" class="text-left">
                                        Tên TS
                                    </th>
                                    <th>
                                        Lịch sử bàn giao
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $value)
                                    @if(!isset($value->asset->id))
                                        @continue
                                    @endif
                                    <tr>
                                        <td scope="row" class="text-left">
                                            {{ $key}}
                                        </td>
                                        <td class="text-center">
                                            {{$value->asset->id}}
                                        </td>
                                        <td class="text-center">
                                            {{ data_get($value->asset, 'group.group_code') }}
                                        </td>
                                        <td class="text-left">
                                            {{$value->asset->name}}
                                        </td>
                                        <td class="text-left">
                                            <?php $checkLop = 0; ?>
                                            @foreach($value->params as $key_p => $value_p)
                                                @if($checkLop == 0 && $checkLop = ($checkLop+1))
                                                    {{ data_get($arrStructureCompany, data_get($value_p, 'person_structure_id')) }}
                                                @endif
                                                @if($key_p > 0)
                                                    <span> <i class="fa fa-arrow-right" aria-hidden="true"></i> </span>
                                                    {{ data_get($arrStructureCompany, data_get($value_p, 'receiver_structure_id')) }}
                                                @endif
                                            @endforeach
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $data->appends(request()->all())->links() }}
                            Tổng số {{ $data->total() }} bản ghi
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@stop
