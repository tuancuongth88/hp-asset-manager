@extends('administrator.app')
@section('title','Danh sách bàn giao tài sản')

@section('content')
    <div class="list-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <h2 class="title_list">
                        <a href="{{ route('handover.index') }}">
                            Danh sách bàn giao tài sản
                        </a>
                    </h2>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <section class="main-content">
                <div class="container">
                    @include('administrator.errors.info-search')
                    @include('administrator.errors.messages')
                    @include('administrator.assets.handover.search-index')
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <table class="table m-table m-table--head-bg-warning table-hover table-mobile table_table table_handover">
                                <thead class="table_head">
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Số BBBG
                                    </th>
                                    <th>
                                        Bên bàn giao
                                    </th>
                                    <th>
                                        Bên nhận BG
                                    </th>
                                    <th>
                                        Đại diện BG
                                    </th>
                                    <th>
                                        Hình thức BG
                                    </th>
                                    <th style="width: 30%">
                                        Nội dung
                                    </th>
                                    <th>
                                        Số lượng
                                    </th>
                                    <th>
                                        Người tạo
                                    </th>
                                    <th>
                                        Ngày tạo
                                    </th>
                                    <th>
                                        Ngày ký/nhận BG
                                    </th>
                                    <th>
                                        Trạng thái
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $value)
                                    <tr>
                                        <th scope="row">
                                            {{ $key + 1 }}
                                        </th>
                                        <th>
                                            {{ $value->id }}
                                        </th>
                                        <td>
                                            {{$value->person_name}}
                                            -<br>
                                            {{ data_get($value, 'personStructure.name') }}
                                        </td>
                                        <td>
                                            {{$value->receiver_name}}
                                            -<br>
                                            {{ data_get($value, 'receiverStructure.name') }}
                                        </td>
                                        <td>
                                            @if(isset($value->delegate_name))
                                                {{$value->delegate_name}}
                                                -<br>
                                                {{ data_get($value, 'delegateStructure.name') }}
                                            @endif
                                        </td>
                                        <td>
                                            {{isset($assetHandoverType[$value->type]) ? $assetHandoverType[$value->type]:''  }}
                                        </td>
                                        <td>
                                            {{ $value->person_description }}
                                        </td>
                                        <td>
                                            {{ $value->number_handover }}
                                        </td>
                                        <td>
                                            {{ data_get($value, 'createdBy.fullname')  }}
                                        </td>
                                        <td>
                                            {{ @$value->created_at->format('d-m-Y')  }}
                                        </td>
                                        <td>
                                            @if(isset($value->handovers_datetime))
                                                {{  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$value->handovers_datetime)->format('d-m-Y') }}
                                            @endif
                                        </td>
                                        <td>
                                            {{@$handoverStatus[$value->status]}}
                                        </td>
                                        <td>
                                            @include('administrator.assets.handover.action-index',['key'=>$key,'value'=>$value])
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--end::Section-->
                    {{ $data->links() }}
                    Tổng số {{ $data->total() }} bản ghi
                </div>
            </section>
        </div>
    </div>
@stop
