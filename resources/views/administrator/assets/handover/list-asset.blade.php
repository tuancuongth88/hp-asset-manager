<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <table class="table table-responsive table-striped">
                <thead class="">
                <tr>
                    <th scope="col"></th>
                    <th scope="col">#</th>
                    <th scope="col">Tên</th>
                    <th scope="col">Nhóm tài sản</th>
                    <th scope="col">SL tài sản</th>
                    <th scope="col">Đơn vị tính</th>
                    <th scope="col">Phân loại</th>
                    <th scope="col">Giá trước thuế(VND)</th>
                    <th scope="col">Năm đưa vào SD</th>
                    <th scope="col">Trạng thái</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data->get() as $key => $value)
                    <tr class="class_list_asset_handover">
                        <?php $name_unit = @$value->unit->name ?>
                        <td class="text-left">{{ @$key + 1 }}</td>
                        <td scope="row">
                            <input type="checkbox" class="cbx_asset_handover"
                                   data-asset_id="{{($value->asset_count == 1) ? $value->id:''}}"
                                   data-name="{{@$value->name}}"
                                   data-group_id="{{@$value->group_id}}"
                                   data-group_code="{{@$value->group_code}}"
                                   data-type="{{@$value->type}}"
                                   data-type_name="{{isset($typeAsset[$value->type]) ? $typeAsset[$value->type]:''}}"
                                   data-status="{{@$value->status}}"
                                   data-status_name="{{ (isset($value->status) && $value->status) ? \App\Models\Assets\Asset::$assetListStatus[$value->status]:''}}"
                                   data-price="{{isset($value->price) ? $value->price:0}}"
                                   @if( in_array(str_slug($name_unit),UNIT_MEASUREMENT))
                                   data-current_quantity="{{@$value->quantity}}"
                                   @else
                                   data-current_quantity="{{@$value->asset_count}}"
                                   @endif
                                   data-unit_id="{{@$value->unit_id}}"
                                   data-unit_name="{{$name_unit}}">
                        </td>
                        <td class="text-left">{{ @$value->name }}</td>
                        <td class="text-left">{{ data_get($value, 'group.group_code') }}</td>
                        <td class="text-center">
                            @if( in_array(str_slug($name_unit),UNIT_MEASUREMENT))
                                {{@$value->quantity}}
                            @else
                                {{@$value->asset_count}}
                            @endif
                        </td>
                        <td class="text-left">{{ data_get($value, 'unit.name') }}</td>
                        <td class="text-center">{{ isset($typeAsset[$value->type]) ? $typeAsset[$value->type]:''}}</td>
                        <td class="text-right">{{ number_format($value->price) }}</td>
                        <td class="text-center">{{ @$value->year_use}}</td>
                        <td class="text-left">{{ (isset($value->status) && $value->status) ?
                                     \App\Models\Assets\Asset::$assetListStatus[$value->status]:'Tồn kho'
                                      }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
