<div class="modal  hide dialog1" tabindex="-1" role="dialog" id="edit-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tìm kiếm tài sản</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row gutter-2 gutter-lg-3 mb-4">
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <label for="name">
                                Tên tài sản
                            </label>
                            <input name="name" class="form-control m-input" placeholder="Tên tài sản"
                                   id="txt_search_name"
                                   value="{{isset($input['name'])?$input['name']:''}}"
                            >
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <label for="name">
                                Nhóm tài sản
                            </label>
                            <?php $selected = isset($input['group_id']) ? $input['group_id'] : '' ?>
                            <select class="form-control m-input drl_group m-select2" id="drl_search_group"
                                    name="group_id">
                                <option value="0">-- Tất cả --</option>
                                <?php echo \App\Models\Partners\Group::genHtmlGroup($selected) ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <label for="name">
                                Phân loại
                            </label>
                            <select class="form-control m-input drl_type m-select2" id="drl_search_type"
                                    name="type_id">
                                <option value="">-- Tất cả --</option>
                                @foreach (\App\Models\Assets\Asset::$assetClassification as $key_tp => $value_tp)
                                    <option value="{{ $key_tp }}"
                                            {{(isset($input['type_id']) && $input['type_id'] == $key_tp) ? 'selected':'' }}
                                    >
                                        {{ $value_tp }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group m-form__group">
                            <label for="name">
                                Trạng thái
                            </label><br/>
                            <select class="form-control m-input drl_status m-select2" id="drl_search_status"
                                    name="status">
                                <option value="">-- Tất cả --</option>
                                @foreach (\App\Models\Assets\Asset::$assetStatusHandover as $key_st => $value_st)
                                    <option value="{{ $key_st }}"
                                            {{(isset($input['status']) && $input['status'] == $key_st) ? 'selected':'' }}
                                    >
                                        {{ $value_st }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label>Tìm kiếm</label>
                        <input type="button" value="Tìm kiếm"
                               class="btn form-control action-search-handover btn-add-color-cat"
                        />
                    </div>
                </div>
                <section class="main-content" id="main-content-asset">
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" ID="add_list_asset_to_handover" data-dismiss="modal">
                    Thêm
                </button>
            </div>
        </div>
    </div>
</div>
