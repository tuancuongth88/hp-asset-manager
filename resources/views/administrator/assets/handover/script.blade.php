<script>

    $(document).ready(function () {
        $('.drl_person_company').on('change', function (e) {
            e.preventDefault();
            initHandoverGetStructureByCompany('person');
        });
        $('.drl_receiver_company').on('change', function (e) {
            e.preventDefault();
            initHandoverGetStructureByCompany('receiver');
        });
        $('.drl_delegate_company').on('change', function (e) {
            e.preventDefault();
            initHandoverGetStructureByCompany('delegate');
        });
        $('#handovers_datetime').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            }
        });


        $('#cbx_delegate_handover').on('change', function (e) {
            if ($(this).is(":checked")) {
                $('#display_delegate_handover').removeClass('d-none');
                $('#display_delegate_handover').find('.drl_delegate_company').attr('disabled', false);
                $('#display_delegate_handover').find('.drl_delegate_structure').attr('disabled', false);
                $('#display_delegate_handover').find('.txt_delegate_name').attr('disabled', false);
                $('#display_delegate_handover').find('.txt_delegate_positions').attr('disabled', false);
            } else {
                $('#display_delegate_handover').addClass('d-none');
                $('#display_delegate_handover').find('.drl_delegate_company').attr('disabled', true);
                $('#display_delegate_handover').find('.drl_delegate_structure').attr('disabled', true);
                $('#display_delegate_handover').find('.txt_delegate_name').attr('disabled', true);
                $('#display_delegate_handover').find('.txt_delegate_positions').attr('disabled', true);
            }
        });

        $('.action-search-handover').click(function () {
            initAssetHtml();
        });
        $('#add_list_asset_to_handover').click(function () {
            initPushAssetHtml();
        });

    });

    function initHandoverGetStructureByCompany(type) {
        var company_id = $('.drl_' + type + '_company').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '../../system/user/get-structure/' + company_id,
            cache: false,
            type: "get",
            processData: false,
            contentType: false,
            success: function (response) {
                $('.drl_' + type + '_structure').empty().append(response.html);
            },
            error: function (data) {

            }
        });
        return false;
    }

    function initAssetHtml() {
        var name = $('#txt_search_name').val();
        var group_id = $('#drl_search_group').val();
        var type = $('#drl_search_type').val();
        var status = $('#drl_search_status').val();
        var check_count_handover = $('#check_count_handover').val();
        var person_structure_owner = $('#drl_person_structure').val();

        if (!person_structure_owner || person_structure_owner === undefined || person_structure_owner === '') {
            alert('Bạn chưa chọn đơn vị bàn giao tài sản');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '../handover/get-asset-html',
            cache: false,
            type: "post",
            data: {
                name: name,
                group_id: group_id,
                type: type,
                status: status,
                check_count_handover: check_count_handover,
                structure_id: person_structure_owner,
            },
            dataType: 'json',
            success: function (response) {
                $('#main-content-asset').empty().append(response.data);
            },
            error: function (data) {
            }
        });
        return false;
    }

    function initPushAssetHtml() {
        $('.class_list_asset_handover').find('input:checked').each(function (i, e) {
            var asset_id = $(this).data('asset_id');
            var name = $(this).data('name');
            var group_id = $(this).data('group_id');
            var group_code = $(this).data('group_code');
            var type = $(this).data('type');
            var type_name = $(this).data('type_name');
            var status = $(this).data('status');
            var status_name = $(this).data('status_name');
            var price = $(this).data('price');
            var current_quantity = $(this).data('current_quantity');
            var unit_id = $(this).data('unit_id');
            var unit_name = $(this).data('unit_name');
            var quantity = 0;
            var total = 0;
            if (current_quantity === 1) {
                quantity = 1;
                total = price;
            }

            var html =
                '<div class="divTableRow class-repeater-item" data-repeater-item>\n' +
                '    <div class="divTableCell text-center rpt-index font-weight-bold">\n' +
                '    </div>\n' +
                '    <div class="divTableCell text-left">\n' + name +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="asset_id"\n' +
                '               value="' + asset_id + '"\n' +
                '        >\n' +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="unit_id"\n' +
                '               value="' + unit_id + '"\n' +
                '        >\n' +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="unit_name"\n' +
                '               value="' + unit_name + '"\n' +
                '        >\n' +
                '        <input class="form-control m-input" type="hidden" readonly\n' +
                '               name="name"\n' +
                '               value="' + name + '"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' + group_code +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="group_id"\n' +
                '               value="' + group_id + '"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' + type_name +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="type"\n' +
                '               value="' + type + '"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' + status_name +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="status"\n' +
                '               value="' + status + '"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' + price +
                '        <input class="form-control m-input" type="hidden" readonly\n' +
                '               name="price"\n' +
                '               value="' + price + '"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' + current_quantity +
                '        <input type="hidden" class="form-control" style="width: 50px"\n' +
                '               id="count_asset"\n' +
                '               value="' + current_quantity + '"\n' +
                '               readonly/>\n' + unit_name +
                '    </div>\n' +
                '    <div class="divTableCell">\n' +
                '        <input type="text" class="form-control m-input"\n' +
                '               id="quantity"\n' +
                '               value="' + quantity + '"\n' +
                '               name="quantity" style="width: 50px"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' + total +
                '    </div>\n' +
                '    <div class="divTableCell">\n' +
                '        <textarea type="text" class="form-control" name="note" ></textarea>\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' +
                '        <div data-repeater-delete="" class="">\n' +
                '            <span>\n' +
                '                <i class="la la-trash-o"></i>\n' +
                '            </span>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>';
            $('.handover_content_body').append(html);

            $(this).closest('.class_list_asset_handover').fadeOut(500, function () {
                $(this).remove();
            });

            $('.divTableRow.class-repeater-item').each(function (i, e) {
                $(this).find('.divTableCell.rpt-index').text(i + 1);
            })
        });

        $('#asset_handover').repeater({
            initEmpty: false,
            show: function () {
                let _this = $(this);
                $(this).slideDown(300, function () {
                    $('#importsForm').validate();
                });
                actionCountAsset();
            },

            hide: function (deleteElement) {
                if (confirm('Bạn có chắc chắn muốn xóa?')) {
                    $(this).slideUp(deleteElement);
                    setTimeout(function () {
                        $('.divTableRow.class-repeater-item').each(function (i, e) {
                            $(this).find('.divTableCell.rpt-index').text(i + 1);
                        })
                    }, 500);
                }
            }
        });
    }

</script>
