{{ Form::open(array('route' => 'handover.index', 'method' => 'GET')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-3 mb-3 mb-md-0">
        <label for="name">
            Số BBBG
        </label>
        <input class="form-control m-input" name="q_handover_id" value="{{request()->get('q_handover_id')}}"/>
    </div>
    <div class="col-md-2 col-sm-3 mb-3 mb-md-0">
        <label for="name">
            Lọc luân chuyển
        </label>
        <select class="form-control m-input" name="filter_handover">
            <option value="0">--Tất cả--</option>
            <option value="1" {{request()->get('filter_handover') == '1' ? 'selected' :'' }}>Bàn giao đi</option>
            <option value="2" {{request()->get('filter_handover') == '2' ? 'selected' :'' }}>Nhận bàn giao</option>
        </select>
    </div>

    {{--    <div class="col-lg-3">--}}
    {{--        <div class="form-group m-form__group">--}}
    {{--            <label for="name">--}}
    {{--                Công ty--}}
    {{--            </label>--}}
    {{--            <select class="form-control m-input drl_company_owner m-select2"--}}
    {{--                    name="company_owner">--}}
    {{--                <option value="0">-- Tất cả --</option>--}}
    {{--                @foreach ($listCompany as $key_cp => $value_cp)--}}
    {{--                    <option value="{{ $key_cp }}"--}}
    {{--                            {{(isset($input['company_owner']) && $input['company_owner'] == $key_cp) ? 'selected':'' }}--}}
    {{--                    >--}}
    {{--                        {{ $value_cp }}--}}
    {{--                    </option>--}}
    {{--                @endforeach--}}
    {{--            </select>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Đơn vị trực thuộc công ty
            </label>
            <select class="form-control m-input drl_structure_owner m-select2"
                    name="structure_owner">
                @if($list_structure)
                    <option value="">-- Chọn đơn vị trực thuộc --</option>
                    @foreach($list_structure as $key_str => $val_str)
                        <option value="{{ $val_str['id'] }}"
                                {{ (request()->get('structure_owner') == $val_str['id'])  ? 'selected':'' }}
                        >
                            {{ $val_str['name']. ' | ' . \App\Models\Systems\StructureCompany::$listType[$val_str['type']]  }}
                        </option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group m-form__group">
            <label for="name">
                Trạng thái
            </label>
            {{ Form::select('q_status', [0 => '--Tất cả--'] + $handoverStatus, request()->get('q_status'),
             ['class' => 'form-control m-input m-select2', 'id' => 'category_id']) }}
        </div>
    </div>

</div>
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-3 mb-3 mb-md-0">
        <input type="submit" value="Tìm kiếm" class="btn  form-control" style="background: #f6811f;color: #fff;"/>
    </div>
    <div class="col-md-2 col-sm-3 mb-3 mb-md-0">
        <a href="{{ route('handover.create') }}" class="btn btn-add-color m-btn m-btn--icon btn-add-color">
            <span>
                Thêm mới bàn giao
            </span>
        </a>
    </div>
    <div class="col-md-2 col-sm-3 mb-3 mb-md-0">
        <button class="btn btn-info btn-history-structure" type="button">
            Lịch sử tài sản PB/KV
        </button>
    </div>
</div>
</form>

<script>
    $('.drl_company_owner').on('change', function (e) {
        e.preventDefault();
        initGetStructureByCompany();
    });

    function initGetStructureByCompany() {
        var company_id = $('.drl_company_owner').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '../../system/user/get-structure/' + company_id,
            cache: false,
            type: "get",
            processData: false,
            contentType: false,
            success: function (response) {
                $('.drl_structure_owner').empty().append(response.html);
            },
            error: function (data) {

            }
        });
        return false;
    }

    $('.btn-history-structure').click(function (e) {
        var structure_owner = $('.drl_structure_owner').val();
        if (structure_owner === '' || !structure_owner) {
            alert('Bạn chưa chọn đơn vị/phòng ban');
        } else {
            var url = '{{route('handover.get-history-structure',['structure_id'=>'_structure_id'])}}';
            url = url.replace('_structure_id', structure_owner);
            $('<a href="' + url + '" target="blank"></a>')[0].click();
        }
    });

</script>
