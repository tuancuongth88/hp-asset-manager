@extends('administrator.app')
@section('title','Chi tiết bàn giao')

@section('content')
    <div class="list-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-6">
                    <h2 class="title_list">Chi tiết bàn giao tài sản</h2>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                    <a href="{{ route('handover.index') }}"
                       class="btn m-btn m-btn--icon btn-add-color-cat-right">
					<span>
						<i class="fa flaticon-list-3"></i>
						<span>
							Danh sách bàn giao tài sản
						</span>
					</span>
                    </a>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <h3 class="sub_title">Thông tin cơ bản</h3>
                        @if($data->status == 1)
                            {{ Form::open(array(
                           'route' => 'handover.active',
                           'class' => 'm-form m-form--fit m-form--label-align-right frm-handover-active',
                           'method'=>'POST',
                            'files'=>true)) }}
                            <input type="hidden" name="id" value="{{@$data->id}}">
                        @endif
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h5>Bên giao</h5>
                                    </div>
                                    <div class="col-lg-12">
                                        Ông/bà: {{@$data->person_name}}
                                    </div>
                                    <div class="col-lg-12">
                                        Chức vụ: {{@$data->person_positions}}
                                    </div>
                                    <div class="col-lg-12">
                                        Đơn vị: {{@data_get($data,'personStructure.name')}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h5>Bên nhận</h5>
                                    </div>
                                    <div class="col-lg-12">
                                        Ông/bà: {{@$data->receiver_name}}
                                    </div>
                                    <div class="col-lg-12">
                                        Chức vụ: {{@$data->receiver_positions}}
                                    </div>
                                    <div class="col-lg-12">
                                        Đơn vị: {{@data_get($data,'receiverStructure.name')}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h5>Bên đại diện</h5>
                                    </div>
                                    <div class="col-lg-12">
                                        Ông/bà: {{@$data->delegate_name}}
                                    </div>
                                    <div class="col-lg-12">
                                        Chức vụ: {{@$data->delegate_positions}}
                                    </div>
                                    <div class="col-lg-12">
                                        Đơn vị: {{@data_get($data,'delegateStructure.name')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="asset_handover">
                            <div class="col-lg-8 col-md-8 col-xs-8">
                                <h3 class="sub_title">
                                    Nội dung bàn giao
                                </h3>
                                {{@$data->person_description}}
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-4">
                                <label for="name" style="padding-top: 30px">
                                    Ngày ký/nhận bàn giao
                                </label>
                                @if($data->status == 1)
                                    <input type="text" class="form-control m-input" id="handovers_datetime"
                                           placeholder="Ngày ký/nhận bàn giao" name="handovers_datetime"
                                           value="{{ @$data->handovers_datetime }}">
                                @else
                                    {{@$data->handovers_datetime}}
                                @endif
                            </div>
                        </div>
                        <div class="row" id="asset_handover">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <h3 class="sub_title" style="padding: 20px 0 20px 0;">
                                    Tài sản bàn giao
                                </h3>
                            </div>
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <table class="table table-hover table-responsive table-striped">
                                    <thead class="table_head">
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên tài sản</th>
                                        <th>Số lượng</th>
                                        <th>Đơn vị tính</th>
                                        <th>Đơn giá</th>
                                        <th>Thành tiền</th>
                                        <th>Ghi chú</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataDetail as $key_d => $value_d)
                                        <?php
                                        $unit_lower = mb_strtolower(trim_all(data_get($value_d, 'unit.name')), 'UTF-8');
                                        $quantity = 0;
                                        if (in_array($unit_lower, UNIT_MEASUREMENT)) {
                                            $quantity = $value_d->quantity;
                                        } else {
                                            $quantity = number_format($value_d->quantity);
                                        }
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                {{$key_d +1}}
                                            </td>
                                            <td class="text-left">
                                                {{$value_d->name}}
                                            </td>
                                            <td class="text-center">
                                                {{$quantity}}
                                            </td>
                                            <td class="text-left">
                                                {{data_get($value_d,'unit.name')}}
                                            </td>
                                            <td class="text-right">
                                                {{number_format($value_d->price)}}
                                            </td>
                                            <td class="text-right">
                                                {{number_format(($quantity * $value_d->price))}}
                                            </td>
                                            <td class="text-left">
                                                @if($data->status == 1)
                                                    <textarea class="form-control description"
                                                              name="detail[{{$key_d}}][note]">{{$value_d->note}}</textarea>
                                                    <input type="hidden" name="detail[{{$key_d}}][id]"
                                                           value="{{$value_d->id}}">
                                                @else
                                                    {{$value_d->note}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @if($data->status == 1)
                            {{ Form::close() }}
                        @endif

                        <section class="button">
                            <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                                <div class="m-form__actions">
                                    <div class="container">
                                        <div class="row">
                                            <div class="m-form__actions">
                                                {{--{{ Form::open(array('method'=>'POST',--}}
                                                {{--route('handover.active_store',[$data->id]),--}}
                                                {{--'style' => 'display: inline-block;')) }}--}}
                                                {{--<button class="btn btn-primary m-btn m-btn--icon m-btn--icon-only"--}}
                                                {{--data-original-title="Nhận bàn giao và lưu kho"--}}
                                                {{--title="Nhận bàn giao và lưu kho"--}}
                                                {{--data-toggle="m-tooltip"--}}
                                                {{-->--}}
                                                {{--<i class="fa fa-archive"></i>--}}
                                                {{--</button>--}}
                                                {{--<input type="hidden" name="id" value="{{$data->id}}">--}}
                                                {{--<input type="hidden" name="filter_handover" value="{{$data->type}}">--}}
                                                {{--{{ Form::close() }}--}}
                                                @if($data->status == 1)
                                                    <button class="btn btn-primary m-btn m-btn--icon m-btn--icon-only btn-confirm-handover"
                                                            data-original-title="Đã nhận bàn giao"
                                                            title="Đã nhận bàn giao"
                                                            data-toggle="m-tooltip"
                                                            type="button"
                                                    >
                                                        <i class="fa fa-check-square"></i>
                                                    </button>

                                                    {{ Form::open(array('method'=>'DELETE','route' => array('handover.destroy', $data->id),'style' => 'display: inline-block;')) }}
                                                    <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                                            class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
                                                            data-original-title="Xóa bàn giao"
                                                            data-toggle="m-tooltip"
                                                            title="Xóa bàn giao"
                                                    >
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    {{ Form::close() }}

                                                @endif
                                                {{--{{ Form::open(array('method'=>'POST',--}}
                                                {{--'route' => array('handover.cancel'),--}}
                                                {{--'style' => 'display: inline-block;')) }}--}}
                                                {{--<button onclick="return confirm('Bạn có chắc chắn muốn hủy bàn giao này không');"--}}
                                                {{--class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"--}}
                                                {{--data-original-title="Hủy và từ chối nhận bàn giao tài sản"--}}
                                                {{--data-toggle="m-tooltip"--}}
                                                {{--title="Hủy và từ chối nhận bàn giao tài sản"--}}
                                                {{-->--}}
                                                {{--<i class="fa fa-ban"></i>--}}
                                                {{--</button>--}}
                                                {{--{{ Form::close() }}--}}
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--end::Portlet-->
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
    @include('administrator.assets.handover.script')
    <script>
        $('.btn-confirm-handover').click(function (e) {
            $('.frm-handover-active').submit();
        });
    </script>
@stop
