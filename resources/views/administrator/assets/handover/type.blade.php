<div class="row">
    <div class="col-lg-12">
        <div class="form-group m-form__group">
            Bên bàn giao tài sản
        </div>
    </div>

    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Công ty<span style="color: red;">(*)</span>
            </label>
            <select class="form-control m-input drl_person_company m-select2"
                    name="person_company_id">
                <option value="0">-- Chọn công ty --</option>
                @foreach ($listCompany as $key_l => $value_l)
                    <option value="{{ $key_l }}"
                            {{(old("person_company_id") == $key_l) ? 'selected':'' }} >
                        {{ $value_l }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Đơn vị trực thuộc công ty<span style="color: red;">(*)</span>
            </label>
            <select class="form-control m-input drl_person_structure m-select2" id="drl_person_structure"
                    name="person_structure_id">
                @if(old("person_company_id"))
                    <?php echo \App\Models\Systems\StructureCompany::genHtmlStructure(old("person_company_id"), old("person_structure_id")) ?>
                @endif
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Người bàn giao<span style="color: red;">(*)</span>
            </label>
            <input class="form-control m-input txt_person_name" placeholder="Người bàn giao" name="person_name"
                   value="{{old('person_name')}}">
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Chức vụ<span style="color: red;">(*)</span>
            </label>
            <input class="form-control m-input txt_person_positions" placeholder="Chức vụ" name="person_positions"
                   value="{{old('person_positions')}}">
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group m-form__group">
            Bên nhận tài sản
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-xs-12">
        <div class="form-group m-form__group">
            <label for="name">
                Công ty<span style="color: red;">(*)</span>
            </label>
            <select class="form-control m-input drl_receiver_company m-select2"
                    name="receiver_company_id">
                <option value="0">-- Chọn công ty --</option>
                @foreach ($listCompany as $key_com => $value_com)
                    <option value="{{ $key_com }}"
                            {{(old("receiver_company_id") == $key_com) ? 'selected':'' }} >
                        {{ $value_com }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-xs-12">
        <div class="form-group m-form__group">
            <label for="name">
                Đơn vị trực thuộc công ty<span style="color: red;">(*)</span>
            </label>
            <select class="form-control m-input drl_receiver_structure m-select2"
                    name="receiver_structure_id">
                @if(old("receiver_company_id"))
                    <?php echo \App\Models\Systems\StructureCompany::genHtmlStructure(old("receiver_company_id"), old("receiver_structure_id")) ?>
                @endif
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Người nhận bàn giao<span style="color: red;">(*)</span>
            </label>
            <input class="form-control m-input txt_receiver_name"
                   placeholder="Người nhận bàn giao"
                   name="receiver_name"
                   value="{{old('receiver_name')}}">
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Chức vụ<span style="color: red;">(*)</span>
            </label>
            <input class="form-control m-input txt_receiver_positions" placeholder="Chức vụ"
                   name="receiver_positions"
                   value="{{old('receiver_positions')}}">
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="cbx_delegate_handover"
                   {{(old('is_delegate_handover') == 'on' ? 'checked':'' )}}
                   name="is_delegate_handover">
            <label class="form-check-label" for="inlineCheckbox1">Bên đại diện</label>
        </div>
    </div>
</div>
<div class="row {{(old('is_delegate_handover') == 'on' ? '':'d-none' )}} " id="display_delegate_handover">
    <div class="col-lg-3 col-md-3 col-xs-12">
        <div class="form-group m-form__group">
            <label for="name">
                Công ty<span style="color: red;">(*)</span>
            </label>
            <select class="form-control m-input drl_delegate_company m-select2"
                    name="delegate_company_id" {{(old('is_delegate_handover') == 'on' ? '':'disabled="disabled"' )}}>
                <option value="0">-- Chọn công ty --</option>
                @foreach ($listCompany as $key_com => $value_com)
                    <option value="{{ $key_com }}"
                            {{(old("delegate_company_id") == $key_com) ? 'selected':'' }} >
                        {{ $value_com }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-xs-12">
        <div class="form-group m-form__group">
            <label for="name">
                Đơn vị trực thuộc công ty<span style="color: red;">(*)</span>
            </label>
            <select class="form-control m-input drl_delegate_structure m-select2"
                    name="delegate_structure_id" {{(old('is_delegate_handover') == 'on' ? '':'disabled="disabled"' )}}>
                @if(old("delegate_company_id"))
                    <?php echo \App\Models\Systems\StructureCompany::genHtmlStructure(old("delegate_company_id"), old("delegate_structure_id")) ?>
                @endif
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Người đại diện<span style="color: red;">(*)</span>
            </label>
            <input class="form-control m-input txt_delegate_name"
                   placeholder="Người đại diện"
                   name="delegate_name"
                   {{(old('is_delegate_handover') == 'on' ? '':'disabled="disabled"' )}}
                   value="{{old('delegate_name')}}">
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Chức vụ<span style="color: red;">(*)</span>
            </label>
            <input class="form-control m-input txt_delegate_positions" placeholder="Chức vụ"
                   name="delegate_positions"
                   disabled="disabled"
                   value="{{old('delegate_positions')}}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-9 col-md-9 col-xs-12">
        <div class="form-group m-form__group">
            <label for="name">
                Nội dung bàn giao<span style="color: red;">(*)</span>
            </label>
            <textarea type="text" class="form-control m-input" id="drl_handover_description"
                      name="person_description">{{old('person_description')}}</textarea>
        </div>
    </div>
{{--    <div class="col-lg-3 col-md-3 col-xs-12">--}}
{{--        <div class="form-group m-form__group">--}}
{{--            <label for="name">--}}
{{--                Ngày nhận bàn giao--}}
{{--            </label>--}}
{{--            <input type="text" class="form-control m-input" id="handovers_datetime"--}}
{{--                   placeholder="Ngày nhận bàn giao" name="handovers_datetime"--}}
{{--                   value="{{ old('handovers_datetime') }}">--}}
{{--        </div>--}}
{{--    </div>--}}
</div>
