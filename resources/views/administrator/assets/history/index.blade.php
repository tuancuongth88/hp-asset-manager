@extends('administrator.app')
@section('title','Lịch sử bàn giao')

@section('content')
    <div class="list-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <h2 class="title_list">
                        Lịch sử tài sản
                    </h2>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <section class="main-content">
                <div class="container">
                    @include('administrator.errors.info-search')
                    @include('administrator.errors.messages')
                    @include('administrator.assets.history.search-index')
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <table class="table m-table m-table--head-bg-warning table-hover table-mobile table_table">
                                <thead class="table_head">
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Từ
                                    </th>
                                    <th>
                                        Đến
                                    </th>
                                    <th>
                                        Số lượng BG
                                    </th>
                                    <th>
                                        Đơn vị
                                    </th>
                                    <th>
                                        Ngày tạo BG
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data->params as $key => $value)
                                    <tr>
                                        <th scope="row" class="text-center">
                                            {{ $key}}
                                        </th>
                                        <td class="text-center">
                                            {{data_get($value, 'person_name')}}
                                            -<br>
                                            {{ data_get($arrStructureCompany, data_get($value, 'person_structure_id')) }}
                                        </td>
                                        <td class="text-center">
                                            {{data_get($value, 'receiver_name')}}
                                            -<br>
                                            {{ data_get($arrStructureCompany, data_get($value, 'receiver_structure_id')) }}
                                        </td>
                                        <td class="text-center">
                                            {{data_get($value, 'quantity')}}
                                        </td>
                                        <td class="text-center">
                                            {{data_get($arrUnit, data_get($value, 'unit_id'))}}
                                        </td>
                                        <td class="text-center">
                                            {{data_get($value,'person_start_time')}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@stop
