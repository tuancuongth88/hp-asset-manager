@extends('administrator.app')
@section('title','Nhập dữ liệu tài sản')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet col-xl-8 offset-xl-2">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Nhập dữ liệu tài sản
                                {{--<small>--}}
                                {{--<a href="{{ route('customer_stores.download')}}">Mẫu import File</a>--}}
                                {{--</small>--}}
                            </h3>
                        </div>
                    </div>
                </div>
            @include('administrator.errors.errors-validate')
            @include('administrator.errors.messages')
            <!--begin::Form-->
                {{ Form::open(array(
                'route' => 'imports.post-import',
                 'class' => 'm-form m-form--fit m-form--label-align-right',
                  'enctype' => 'multipart/form-data'))
                  }}
                <div class="m-portlet__body">
                    <div class="m-form__heading">
                        <h3 class="m-form__heading-title">

                        </h3>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label for="company">
                                Công ty
                            </label>
                            <select class="form-control m-input m-select2" id="company" name="company_id">
                                <option value="">-- Chọn công ty --</option>
                                @foreach ($listCompany as $element)
                                    <option value="{{ $element['id'] }}">
                                        {{ $element['name'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label for="branch_id">
                                Đơn vị trực thuộc
                            </label>
                            <select class="form-control m-input m-select2" id="structure_id" name="structure_id">
                                <option value="">-- Chọn đơn vị trực thuộc --</option>
                                @foreach ($listStructer as $element)
                                    <option value="{{ $element['id'] }}">
                                        {{ $element['name'] }}
                                        | {{\App\Models\Systems\StructureCompany::$listType[$element['type']]}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="receiver_handover">
                                    Chọn file
                                </label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="validatedCustomFile" required
                                           name="import_file">
                                    <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section class="button">
                        <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                            <div class="m-form__actions">
                                <div class="container">
                                    <div class="row">
                                        <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                                            <button class="save_bid">
                                                Lưu
                                            </button>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <a class="delete_bid" href="{{ route('assets.index') }}">
                                                Trở về danh sách
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                {{ Form::close() }}
                <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        $(document).ready(function () {
            $('#company').on('change', function (e) {
                e.preventDefault();
                initGetStructureByCompany(this);
            });
        });

        function initGetStructureByCompany($i) {
            var company_id = $($i).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '../../system/user/get-structure/' + company_id,
                cache: false,
                type: "get",
                processData: false,
                contentType: false,
                success: function (response) {
                    $('#structure_id').empty().append(response.html);
                },
                error: function (data) {

                }
            });
            return false;
        }
    </script>
@stop