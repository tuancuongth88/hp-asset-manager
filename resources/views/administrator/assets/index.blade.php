@extends('administrator.app')
@section('title','Danh sách tài sản và công cụ')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">
                    <a href="{{ route('assets.index') }}">Danh sách tài sản và công cụ</a>
                </h2>
            </div>
            @include('administrator.errors.messages')
            @include('administrator.assets.search-asset')
            <section class="main-content">
                <div class="wrap-table">
                    <table class="table table-hover table-responsive table-striped">
                        <thead class="table_head">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Tên</th>
                            <th scope="col">Mã</th>
                            <th scope="col">Mã nhóm</th>
                            <th scope="col">Số hiệu HĐ</th>
                            <th scope="col">Chất liệu</th>
                            <th scope="col">Màu sắc</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">Đơn vị tính</th>
                            <th scope="col">Giá trị(VND)</th>
                            <th scope="col">Đơn vị SD</th>
                            <th scope="col">Người nhập kho</th>
                            <th scope="col">Ngày nhập kho</th>
                            <th scope="col">Phân loại</th>
                            <th scope="col">Bảo hành(tháng)</th>
                            <th scope="col">Năm sử dụng</th>
                            <th scope="col">Tình trạng</th>
                            <th scope="col">Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr class="header">
                                <td scope="row">{{@$key +1}}</td>
                                <td class="text-left">{{ $value->name }}</td>
                                <td class="text-left">{{(isset($value->group->group_code)? $value->group->group_code.'-':'').$value->id }}</td>
                                <td class="text-left">{{ data_get($value, 'group.group_code') }}</td>
                                <td class="text-left">{{ @$value->contract_code }}</td>
                                <td class="text-left">{{ $value->material }}</td>
                                <td class="text-left">{{ $value->color }}</td>
                                <?php
                                $unit_lower = mb_strtolower(trim_all(data_get($value, 'unit.name')), 'UTF-8');
                                ?>
                                @if (in_array($unit_lower, UNIT_MEASUREMENT))
                                    <td class="text-right">{{ $value->quantity }}</td>
                                @else
                                    <td class="text-right">{{ number_format($value->quantity) }}</td>
                                @endif
                                <td class="text-left">{{ data_get($value, 'unit.name') }}</td>
                                <td class="text-right">{{ number_format($value->price) }}</td>
                                <td class="text-center">{{ data_get($value, 'ownerStructure.name') }}</td>
                                <td class="text-left">{{ $value->receiver_handover }}</td>
                                <td class="text-left">
                                    {{ isset($value->date_import)?
                                    Carbon\Carbon::createFromFormat('Y-m-d',$value->date_import)->format('d-m-Y'):''  }}
                                </td>
                                <td class="text-center">{{ isset($value->type) ?
                                    \App\Models\Assets\Asset::$assetClassification[$value->type] : 'Chưa phân loai' }}</td>
                                <td class="text-center">{{ $value->warranty }}</td>
                                <td class="text-center">{{ isset($value->year_use) ? $value->year_use : 'Chưa có' }}</td>
                                <td class="text-center">
                                    {{(isset($value->status) && $value->status) ?
                                    \App\Models\Assets\Asset::$assetListStatus[$value->status]: '' }}
                                </td>
                                <td>
                                    @if(isset($value->assetLog->id))
                                        <a href="{{ route('assets.history', $value->id) }}" class="">
                                            <i class="fa fa-history" aria-hidden="true"></i>
                                        </a>
                                    @endif
                                    <a href="{{ route('assets.edit', $value->id) }}" class="">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                    {{ Form::open(array('method'=>'DELETE', 'route' => array('assets.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                    <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                            class="btn-delete">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="wrap-title">
                    {{ $data->appends($input)->links() }}
                    Tổng số {{ $data->total() }} bản ghi
                </div>
            </section>
        </div>
    </div>
@stop
