@extends('administrator.app')
@section('title','Thêm mới tài sản sửa chữa bảo hành')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="row">
                <div class="wrap-title">
                    <h2 class="title_list">
                        Thêm tài sửa chữa bảo hành
                    </h2>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12" style="margin-bottom: 20px;">
                    <a href="{{ route('repair.index') }}" class="btn m-btn m-btn--icon btn-add-color-cat">
                        Danh sách sửa chữa và bảo hành
                    </a>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    @include('administrator.errors.errors-validate')
                    @include('administrator.errors.messages')
                    <!--begin::Portlet-->
                        <div class="m-portlet m-portlet--tab">
                            <h3 class="sub_title">Thông tin cơ bản</h3>
                            {{ Form::open(array(
                            'route' => 'repair.store',
                            'class' => 'm-form m-form--fit m-form--label-align-right',
                             'files'=>true)) }}
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-xs-12">
                                    <div class="form-group m-form__group">
                                        <label for="name">
                                            Công ty<span class="required">(*)</span>
                                        </label>
                                        <select class="form-control m-input drl_person_company m-select2"
                                                name="company_owner">
                                            <option value="0">-- Chọn công ty --</option>
                                            @foreach ($listCompany as $key_l => $value_l)
                                                <option value="{{ $key_l }}"
                                                        {{(old("company_owner") == $key_l) ? 'selected':'' }} >
                                                    {{ $value_l }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-12">
                                    <div class="form-group m-form__group">
                                        <label for="name">
                                            Đơn vị trực thuộc công ty<span style="color: red;">(*)</span>
                                        </label>
                                        <select class="form-control m-input drl_person_structure m-select2" id="drl_person_structure"
                                                name="structure_owner">
                                            @if(old("company_owner"))
                                                <?php echo \App\Models\Systems\StructureCompany::genHtmlStructure(old("company_owner"), old("structure_owner")) ?>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-12">
                                    <div class="form-group m-form__group">
                                        <label for="name">
                                            Người báo bảo hành<span class="required">(*)</span>
                                        </label>
                                        <input class="form-control m-input txt_people_notice" placeholder="Người thông báo"
                                               name="people_notice"
                                               value="{{old('people_notice')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body row" id="asset_repair">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <h3 class="sub_title" style="padding: 20px 0 20px 0;">
                                        Chi tiết sửa chữa
                                    </h3>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="divTable blueTable">
                                        <div class="divTableHeading">
                                            <div class="divTableRow">
                                                <div class="divTableHead">Tên TS</div>
                                                <div class="divTableHead">Nhóm TS</div>
                                                <div class="divTableHead">TS hiện có</div>
                                                <div class="divTableHead">SL sửa chữa</div>
                                                <div class="divTableHead">Ngày bắt đầu</div>
                                                <div class="divTableHead">Ngày kết thúc</div>
                                                <div class="divTableHead">Đơn giá SC</div>
                                                <div class="divTableHead">Loại sửa chữa</div>
                                                <div class="divTableHead">Ghi chú</div>
                                            </div>
                                        </div>
                                        <div class="divTableBody handover_content_body"
                                             data-repeater-list="repair_content">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <p class="add_contract" data-toggle="modal" data-target="#edit-modal">
                                        + Thêm nội dung sửa chữa
                                    </p>
                                </div>
                                @include('administrator.assets.handover.modal')
                            </div>
                            <section class="button">
                                <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                                    <div class="m-form__actions">
                                        <div class="container">
                                            <div class="row">
                                                <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                                                    <button type="submit" class="save_bid">
                                                        Lưu
                                                    </button>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-xs-12">
                                                    <a class="delete_bid" href="{{ route('repair.index') }}">
                                                        Trở về danh sách
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            {{ Form::close() }}
                        </div>
                        <!--end::Portlet-->
                    </div>
                    <!--end::Form-->
                </div>
        </div>
    </div>
    @include('administrator.assets.repair.script')
@stop