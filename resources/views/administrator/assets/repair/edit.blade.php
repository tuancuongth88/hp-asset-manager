@extends('administrator.app')
@section('title','Thêm mới tài sản sửa chữa bảo hành')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <h2 class="title_list">Sửa tài sửa chữa bảo hành</h2>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12" style="margin-bottom: 20px;">
                    <a href="{{ route('repair.index') }}" class="btn m-btn m-btn--icon btn-add-color-cat">
                    <span>
                        <i class="fa flaticon-list-3"></i>
                        <span>
                            Danh sách sửa chữa và bảo hành
                        </span>
                    </span>
                    </a>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                @include('administrator.errors.errors-validate')
                @include('administrator.errors.messages')
                <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <h3 class="sub_title">Thông tin cơ bản</h3>
                        {{ Form::open(array(
                        'route' => array('repair.update', $data->id),
                        'class' => 'm-form m-form--fit m-form--label-align-right',
                        'files'=>true, 'enctype' => 'multipart/form-data')) }}
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <div class="m-portlet__body row">
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label for="name">
                                        Công ty
                                    </label>
                                    <select class="form-control m-input drl_owner_company m-select2"
                                            name="company_id">
                                        <option value="0">-- Chọn công ty --</option>
                                        @foreach ($listCompany as $key_l => $value_l)
                                            <option value="{{ $key_l }}">
                                                {{ $value_l }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label for="name">
                                        Chi nhánh
                                    </label>
                                    <select class="form-control m-input drl_owner_branch"
                                            name="branch_id">
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label for="name">
                                        Phòng ban
                                    </label>
                                    <select class="form-control m-input drl_owner_department"
                                            name="department_id">
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label for="name">
                                        Người báo bảo hành<span style="color: red;">(*)</span>
                                    </label>
                                    <select class="form-control m-input drl_owner_user"
                                            name="owner">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body row">
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label for="name">
                                        Phân loại TS<span style="color: red;">(*)</span>
                                    </label>
                                    <select class="form-control m-input drl_roles m-select2" id="asset_type"
                                            name="type">
                                        <option value="">-- Chọn phân loại TS --</option>
                                        @foreach ( \App\Models\Assets\Asset::$assetClassification as $key_l => $value_l)
                                            <option value="{{ $key_l }}">
                                                {{ $value_l }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label for="name">
                                        Nhóm tài sản<span style="color: red;">(*)</span>
                                    </label>
                                    <select class="form-control m-input drl_roles m-select2" id="asset_group_id"
                                            name="group_id">
                                        <option value="">-- Chọn nhóm tài sản --</option>
                                        @foreach ($listAssetGroup as $key_l => $value_l)
                                            <option value="{{ $key_l }}">
                                                {{ $value_l }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label for="name">
                                        Tên tài sản
                                    </label>
                                    <input type="text" class="form-control m-input" id="asset_name"
                                           placeholder="Tên tài sản" name="name" value="{{ old('short_name') }}">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label for="name" class="" style="display: block !important;">
                                        Kiểm tra tài sản
                                    </label>
                                    <button class="btn btn-add-color-cat" id="btn_count_asset">
                                        Kiểm tra
                                    </button>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label>
                                        Tổng tài sản theo điều kiện lọc: <span id="count_asset"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label>
                                        Số lượng tài sản cần sửa chữa
                                    </label>
                                    <input type="text" class="form-control m-input" id="asset_quantity"
                                           placeholder="Số lượng" name="number_repair" value="{{ old('quantity') }}">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group m-form__group">
                                    <label>
                                        Loại sửa chữa
                                    </label>
                                    <select class="form-control m-input drl_roles m-select2" id="history_type"
                                            name="history_type">
                                        <option value="">-- Chọn nhóm tài sản --</option>
                                        @foreach (\App\Models\Assets\AssetAssetsHistory::$assetHistoryType as $key => $value)
                                            <option value="{{ $key }}">
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <section class="button">
                            <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                                <div class="m-form__actions">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-12"></div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <button type="submit" class="save_bid">
                                                    Lưu
                                                </button>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <a class="delete_bid" href="{{ route('repair.index') }}">
                                                    Trở về danh sách
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        {{ Form::close() }}
                    </div>
                    <!--end::Portlet-->
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var type_owner = 'owner';
            $('.drl_owner_company').on('change', function (e) {
                e.preventDefault();
                initGetBranchByCompany(type_owner);
            });
            $('.drl_owner_branch').on('change', function (e) {
                e.preventDefault();
                initGetDepartment(type_owner);
            });
            $('.drl_owner_department').on('change', function (e) {
                e.preventDefault();
                initGetUser(type_owner);
            });

            $('#btn_count_asset').on('click', function (e) {
                e.preventDefault();
                countAsset();
            });


        });

        function initGetBranchByCompany(type) {
            var company_id = $('.drl_' + type + '_company').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '../handover/branch-by-company/' + company_id,
                cache: false,
                type: "post",
                processData: false,
                contentType: false,
                success: function (response) {
                    $('.drl_' + type + '_branch').empty().append(response.data);
                    return false;
                },
                error: function (data) {

                }
            });
            return false;
        }

        function initGetDepartment(type) {
            var branch_id = $('.drl_' + type + '_branch').val();
            var company_id = $('.drl_' + type + '_company').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '../handover/get-department',
                cache: false,
                type: "post",
                data: {branch_id: branch_id, company_id: company_id},
                dataType: 'json',
                success: function (response) {
                    $('.drl_' + type + '_department').empty().append(response.data);
                    return false;
                },
                error: function (data) {

                }
            });
            return false;
        }

        function initGetUser(type) {
            var department_id = $('.drl_' + type + '_department').val();
            var branch_id = $('.drl_' + type + '_branch').val();
            var company_id = $('.drl_' + type + '_company').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '../handover/get-user',
                cache: false,
                type: "post",
                data: {branch_id: branch_id, company_id: company_id, department_id: department_id},
                dataType: 'json',
                success: function (response) {
                    $('.drl_' + type + '_user').empty().append(response.data);
                    return false;
                },
                error: function (data) {

                }
            });
            return false;
        }

        function countAsset() {
            var asset_type = $('#asset_type').val();
            var asset_group_id = $('#asset_group_id').val();
            var asset_name = $('#asset_name').val();
            var owner = $('.drl_owner_user').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: './count-asset',
                cache: false,
                type: "post",
                data: {
                    type: asset_type,
                    group_id: asset_group_id,
                    name: asset_name,
                    owner: owner,
                },
                dataType: 'json',
                success: function (response) {
                    $('#count_asset').empty().text(response.data);
                    return false;
                },
            });
            return false;
        }
    </script>
@stop