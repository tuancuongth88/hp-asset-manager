@extends('administrator.app')
@section('title','Danh sách sửa chữa và bảo hành')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="container">
    <div class="list-content">
        <div class="wrap-title">
            <h2 class="title_list">
                <a href="{{ route('repair.index') }}">Danh sách sửa chữa và bảo hành</a>
            </h2>
        </div>
        <div class="wrap-title">
            <a href="{{ route('repair.create') }}" class="btn btn-primary m-btn m-btn--icon btn-add-color">
                <span>Thêm mới</span>
            </a>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                @include('administrator.errors.errors-validate')
                @include('administrator.errors.messages')
            </div>
        </div>
        <section class="main-content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <table class="table table-hover table-responsive table-striped">
                        <thead class="table_head">
                            <tr>
                                <th scope="col">STT</th>
                                <th scope="col">Tên TS</th>
                                <th scope="col">Mã nhóm TS</th>
                                <th scope="col">Đơn vị</th>
                                <th scope="col">Giá SC</th>
                                <th scope="col">Người báo SC/BH</th>
                                <th scope="col">Ngày tạo</th>
                                <th scope="col">Ghi chú</th>
                                <th scope="col">Trạng thái</th>
                                <th scope="col">Tùy chọn</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr class="header expand item_history_repair">
                                <td>{{ $key + 1 }}</td>
                                <td>{{ data_get($value, 'asset.name') }}</td>
                                <td>{{ data_get($value, 'asset.group.group_code') }}</td>
                                <td>
                                    {{ data_get($value, 'asset.ownerStructure.name') }}
                                </td>
                                <td>{{ @number_format($value->price_repair) }}</td>
                                <td>{{ @$value->people_notice }}</td>
                                <td>{{ @$value->created_at->format('d-m-Y') }}</td>
                                <td>{{ @$value->note }}</td>
                                <td>
                                    <select class="form-control m-input drl_history_status"
                                            name="status">
                                        @foreach (\App\Models\Assets\AssetAssetsHistory::$assetHistoryStatus as $key_s=>$value_s)
                                            <option value="{{ $key_s }}" {{($value->status == $key_s) ? 'selected':'' }} >
                                                {{ $value_s }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" value="{{$value->id}}" class="txt_history_id">
                                </td>
                                <td class="align-self-center">
                                    {{ Form::open(array('method'=>'DELETE', 'route' => array('repair.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                    <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                            class="btn-border-none ">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{ $data->links() }}
            Tổng số {{ $data->total() }} bản ghi
        </section>
    </div>
    <script>
        $(document).ready(function () {
            $('.drl_history_status').on('change', function (e) {
                e.preventDefault();
                initUpdateStatus(this);
            });

        });

        function initUpdateStatus(that) {
            var status = $(that).val();
            var id = $(that).closest('.item_history_repair').find('.txt_history_id').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: './repair/update-status',
                cache: false,
                type: "post",
                data: {id: id, status: status},
                dataType: 'json',
                success: function (response) {
                    location.reload(true);
                },
                error: function (data) {

                }
            });
            return false;
        }

    </script>

@stop