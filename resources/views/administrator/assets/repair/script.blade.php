<script>

    $(document).ready(function () {
        $('.drl_person_company').on('change', function (e) {
            e.preventDefault();
            initHandoverGetStructureByCompany('person');
        });

        $('.action-search-handover').click(function () {
            initAssetHtml();
        });
        $('#add_list_asset_to_handover').click(function () {
            initPushAssetHtml();
        });

    });

    function initHandoverGetStructureByCompany(type) {
        var company_id = $('.drl_' + type + '_company').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '../../system/user/get-structure/' + company_id,
            cache: false,
            type: "get",
            processData: false,
            contentType: false,
            success: function (response) {
                $('.drl_' + type + '_structure').empty().append(response.html);
            },
            error: function (data) {

            }
        });
        return false;
    }

    function initAssetHtml() {
        var name = $('#txt_search_name').val();
        var group_id = $('#drl_search_group').val();
        var type = $('#drl_search_type').val();
        var status = $('#drl_search_status').val();
        var check_count_handover = $('#check_count_handover').val();
        var person_structure_owner = $('#drl_person_structure').val();

        if (!person_structure_owner || person_structure_owner === undefined || person_structure_owner === '') {
            alert('Bạn chưa chọn đơn vị bàn giao tài sản');
            return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '../handover/get-asset-html',
            cache: false,
            type: "post",
            data: {
                name: name,
                group_id: group_id,
                type: type,
                status: status,
                check_count_handover: check_count_handover,
                structure_id: person_structure_owner,
            },
            dataType: 'json',
            success: function (response) {
                $('#main-content-asset').empty().append(response.data);
            },
            error: function (data) {
            }
        });
        return false;
    }

    function initPushAssetHtml() {
        $('.class_list_asset_handover').find('input:checked').each(function (e) {
            var asset_id = $(this).data('asset_id');
            var name = $(this).data('name');
            var group_id = $(this).data('group_id');
            var group_code = $(this).data('group_code');
            var type = $(this).data('type');
            var type_name = $(this).data('type_name');
            var status = $(this).data('status');
            var status_name = $(this).data('status_name');
            var price = $(this).data('price');
            var current_quantity = $(this).data('current_quantity');
            var unit_id = $(this).data('unit_id');
            var quantity = 0;
            var total = 0;
            if (current_quantity === 1) {
                quantity = 1;
                total = price;
            }

            var html =
                '<div class="divTableRow class-repeater-item" data-repeater-item>\n' +
                '    <div class="divTableCell">\n' + name +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="asset_id"\n' +
                '               value="' + asset_id + '"\n' +
                '        >\n' +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="unit_id"\n' +
                '               value="' + unit_id + '"\n' +
                '        >\n' +
                '        <input class="form-control m-input" type="hidden" readonly\n' +
                '               name="name"\n' +
                '               value="' + name + '"\n' +
                '        >\n' +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="type"\n' +
                '               value="' + type + '"\n' +
                '        >\n' +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="status"\n' +
                '               value="' + status + '"\n' +
                '        >\n' +
                '        <input class="form-control m-input" type="hidden" readonly\n' +
                '               name="price"\n' +
                '               value="' + price + '"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' + group_code +
                '        <input class="form-control m-input" type="hidden"\n' +
                '               name="group_id"\n' +
                '               value="' + group_id + '"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' + current_quantity +
                '    </div>\n' +
                '    <div class="divTableCell">\n' +
                '        <input type="text" class="form-control m-input"\n' +
                '               id="quantity"\n' +
                '               value="' + quantity + '"\n' +
                '               name="quantity" style="width: 50px"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' +
                '        <input type="text" class="form-control m-input start_time"\n' +
                '               id="start_time"\n' +
                '               name="start_time" style="width: 100px"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' +
                '        <input type="text" class="form-control m-input end_time"\n' +
                '               id="end_time"\n' +
                '               name="end_time" style="width: 100px"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' +
                '        <input type="text" class="form-control m-input"\n' +
                '               id="price_repair"\n' +
                '               name="price_repair" style="width: 100px"\n' +
                '        >\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' +
                '       <select class="form-control m-input drl_roles" id="history_type" name="history_type" title="">\n' +
                '           <option value="">-- Chọn loại --</option>\n' +
                '           <option value="1">Bảo hành, bảo trì</option>\n' +
                '           <option value="2">Sửa chữa</option>\n' +
                '       </select>' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' +
                '        <textarea type="text" class="form-control" name="note" ></textarea>\n' +
                '    </div>\n' +
                '    <div class="divTableCell">\n' +
                '        <div data-repeater-delete="" class="">\n' +
                '            <span>\n' +
                '                <i class="la la-trash-o"></i>\n' +
                '            </span>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>';
            $('.handover_content_body').append(html);

            $(this).closest('.class_list_asset_handover').fadeOut(500, function () {
                $(this).remove();
            });
        });

        $('#asset_repair').repeater({
            initEmpty: false,
            show: function () {
                let _this = $(this);
                $(this).slideDown(300, function () {
                    $('#importsForm').validate();
                });
            },

            hide: function (deleteElement) {
                if (confirm('Bạn có chắc chắn muốn xóa?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });

        $('.start_time').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            }
        });
        $('.end_time').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            }
        });
    }

</script>