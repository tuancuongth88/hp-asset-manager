{{ Form::open(array('route' => 'assets.index', 'method' => 'GET')  ) }}
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Tên
            </label>
            <input name="name" class="form-control m-input" placeholder="Tên tài sản"
                   value="{{isset($input['name'])?$input['name']:''}}"
            >
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Nhóm tài sản
            </label>
            <?php $selected = isset($input['group_id']) ? $input['group_id'] : '' ?>
            <select class="form-control m-input drl_group m-select2"
                    name="group_id">
                <option value="0">-- Tất cả --</option>
                <?php echo \App\Models\Partners\Group::genHtmlGroup($selected) ?>
            </select>

        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Phân loại
            </label>
            <select class="form-control m-input drl_type m-select2"
                    name="type_id">
                <option value="">-- Tất cả --</option>
                @foreach (\App\Models\Assets\Asset::$assetClassification as $key_tp => $value_tp)
                    <option value="{{ $key_tp }}"
                            {{(isset($input['type_id']) && $input['type_id'] == $key_tp) ? 'selected':'' }}
                    >
                        {{ $value_tp }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Trạng thái
            </label>
            <select class="form-control m-input drl_status m-select2"
                    name="status">
                <option value="">-- Tất cả --</option>
                @foreach (\App\Models\Assets\Asset::$assetListStatus as $key_st => $value_st)
                    <option value="{{ $key_st }}"
                            {{(isset($input['status']) && $input['status'] == $key_st) ? 'selected':'' }}
                    >
                        {{ $value_st }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Trạng thái ban đầu
            </label>
            <select class="form-control m-input drl_status m-select2"
                    name="original">
                <option value="">-- Tất cả --</option>
                @foreach (\App\Models\Assets\Asset::$assetOriginalClassification as $key_st => $value_st)
                    <option value="{{ $key_st }}"
                            {{(isset($input['original']) && $input['original'] == $key_st) ? 'selected':'' }}
                    >
                        {{ $value_st }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Nhà thầu
            </label>
            <input name="name_partner" class="form-control m-input" placeholder="Tên nhà thầu">
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Công ty
            </label>
            <select class="form-control m-input drl_company_owner m-select2"
                    name="company_owner">
                <option value="0">-- Tất cả --</option>
                @foreach ($listCompany as $key_cp => $value_cp)
                    <option value="{{ $key_cp }}"
                            {{(isset($input['company_owner']) && $input['company_owner'] == $key_cp) ? 'selected':'' }}
                    >
                        {{ $value_cp }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Đơn vị trực thuộc công ty
            </label>
            <select class="form-control m-input drl_structure_owner m-select2"
                    name="structure_owner">
                @if($list_structure)
                    <option value="">-- Chọn đơn vị trực thuộc --</option>
                    @foreach($list_structure as $key_str => $val_str)
                        <option value="{{ $val_str['id'] }}"
                                {{(isset($input['structure_owner']) && $input['structure_owner'] == $val_str['id']) ? 'selected':'' }}
                        >
                            {{ $val_str['name']. ' | ' . \App\Models\Systems\StructureCompany::$listType[$val_str['type']]  }}
                        </option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 mb-3 mb-md-0">
        <input type="submit" class="form-control" id="search-button"
               value="Tìm kiếm" style="background: #f6811f;color: #fff;" data-action="{{route('assets.index')}}">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 mb-3 mb-md-0">
        <div class="dropdown">
            <button class="dropdown-toggle_button btn-add-color btn m-btn m-btn--icon" type="button"
                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                Tùy chọn thêm mới
            </button>
            <div class="dropdown-menu drowdown-addnew" aria-labelledby="dropdownMenuButton">
                <a href="{{ route('units.index') }}" class="dropdown-item">
                    Danh sách đơn vị tính
                </a>
                <a href="{{ route('group.index') }}" class="dropdown-item">
                    Danh sách nhóm tài sản
                </a>
                <a href="{{ route('imports.create') }}" class="dropdown-item">
                    Thêm mới tài sản
                </a>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}