@extends('administrator.app')
@section('title','Sửa thông tin đơn vị')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">Sửa đơn vị tính</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                    @include('administrator.errors.errors-validate')
                </div>
            </div>
            {{ Form::open(array('route' => array('units.update', $data->id),
            'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
            <section class="main-content-add">
                <div class="row">
                    <div class="wrap-title">
                        <h3 class="sub_title">Thông tin cơ bản</h3>
                    </div>
                    <div class="col-lg-9 col-md-9 col-xs-12">
                        <div class="form-group">
                            <label for="name">
                                Tên đơn vị tính
                                <span class="required">(*)</span></label>
                            <input type="name" class="form-control m-input" id="name"
                                   placeholder="Tên đơn vị" name="name" value="{{ $data->name }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12"></div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <button class="save_bid">
                            Lưu
                        </button>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <a class="delete_bid" href="{{ route('group.index') }}">
                            Trở về danh sách
                        </a>
                    </div>
                </div>
            </section>
            {{ Form::close() }}
        </div>
    </div>
@stop
