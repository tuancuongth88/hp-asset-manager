@extends('administrator.app')
@section('title','Danh sách đơn vị tính')

@section('content')
    <div class="list-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    @include('administrator.errors.messages')
                    <h2 class="title_list">Danh sách đơn vị tính</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-xs-12">
                    <a href="{{ route('units.create') }}" class="btn m-btn m-btn--icon btn-add-color">
                        <span>
                            <i class="fa flaticon-plus"></i>
                            <span>
                                Thêm mới đơn vị tính
                            </span>
                        </span>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-xs-12">
                    <a href="{{ route('assets.index') }}" class="btn m-btn m-btn--icon btn-add-color-cat" style="color:#fff">
                        <span>
                            <span>
                                Trở về danh sách tài sản
                            </span>
                        </span>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <a href="{{ route('investment.index') }}" class="btn m-btn m-btn--icon btn-add-color-cat" style="color:#fff">
                        <span>
                            <span>
                                Trở về danh sách đầu tư
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <section class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <table class="table table-hover table-responsive table-striped">
                            <thead class="table_head">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tên</th>
                                <th scope="col">Tùy chọn</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $value)
                                <tr class="header expand">
                                    <td scope="row">
                                        {{ $key + 1 }}
                                    </td>
                                    <td>
                                        {{ $value->name }}
                                    </td>
                                    <td>
                                        <a href="{{ route('units.edit', $value->id) }}" class="">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                        {{ Form::open(array('method'=>'DELETE', 'route' => array('units.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                        <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                                class="btn-delete">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
{{--                    {{ $data->links() }}--}}
{{--                    Tổng số {{ $data->total() }} bản ghi--}}
                </div>
            </div>
        </section>
    </div>
@stop