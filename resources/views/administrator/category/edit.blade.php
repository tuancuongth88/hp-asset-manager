@extends('administrator.app')
@section('title','Sửa thông tin hạng mục thầu')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">Sửa hạng mục thầu</h2>
            </div>
            {{ Form::open(array('route' => array('category.update', $data->id),
            'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
            <section class="main-content-add">
                <div class="wrap-title">
                    <h3 class="sub_title">Thông tin cơ bản</h3>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                        @include('administrator.errors.errors-validate')
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-xs-12">
                        <div class="form-group">
                            <label for="name">
                                Tên hạng mục thầu
                                <span class="required">(*)</span></label>
                            <input type="name" class="form-control m-input" id="name" placeholder="Tên hạng mục thầu"
                                   name="name" value="{{ $data->name }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleSelect1">
                                Hạng mục thầu cha
                            </label>
                            <select class="form-control m-input" id="parent" name="parent">
                                <option value="0">Không có cha</option>
                                @foreach ($listCategory as $key => $element)
                                    <option value="{{ $element['id'] }}" {{($data->parent == $element->id) ? 'selected':''}}>
                                        {{ $element['name'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <div class="form-group">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                        <button class="save_bid">
                            Lưu
                        </button>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <a class="delete_bid" href="{{ route('category.index') }}">
                            Trở về danh sách
                        </a>
                    </div>
                </div>
            </section>
            {{ Form::close() }}
    </div>
    </div>
@stop
