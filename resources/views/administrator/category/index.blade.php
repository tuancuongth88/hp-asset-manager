@extends('administrator.app')
@section('title','Danh sách hạng mục thầu')

@section('content')
<div class="container">
    <div class="list-content">
        <div class="wrap-title">
            <h2 class="title_list"><a href="{{ route('category.index') }}">Danh sách hạng mục thầu</a></h2>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                @include('administrator.errors.errors-validate')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-xs-12">
                <a href="{{ route('category.create') }}" class="btn m-btn m-btn--icon btn-add-color">
                    Thêm mới hạng mục
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12">
                <a href="{{ route('partner.index') }}" class="btn m-btn m-btn--icon btn-add-color-cat">
                    Trở về danh sách nhà thầu
                </a>
            </div>
        </div>
        <section class="main-content">
            <div class="wrap-table">
                <table class="table table-hover table-responsive table-striped">
                    <thead class="table_head">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key => $value)
                        <tr class="header expand">
                            <td scope="row">
                                {{ $key + 1 }}
                            </td>
                            <td>
                                {{ $value->name }}
                            </td>
                            <td>
                                <a href="{{ route('category.edit', $value->id) }}" class="">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                {{ Form::open(array('method'=>'DELETE', 'route' => array('category.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                        class="btn-delete">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="wrap-title">
                {{ $data->links() }}
                Tổng số {{ $data->total() }} bản ghi
            </div>
        </section>
    </div>
</div>
@stop

