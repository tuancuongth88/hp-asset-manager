@extends('administrator.app')
@section('title','Danh sách nhà thầu')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">Thêm mới hợp đồng</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                    @include('administrator.errors.errors-validate')
                </div>
            </div>
        {{ Form::open(array('route' => 'contract.store', 'method' => 'POST',
        'class' => 'm-form m-form--fit m-form--label-align-right',
        'enctype' => 'multipart/form-data', 'id' => 'create_contract', 'files'=>true)) }}
        <section class="main-content-add">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-xs-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-xs-12">
                            <div class="form-group">
                                <label>Tên nhà thầu: <span class="required">(*)</span></label>
                                {{ Form::select('partner_id', ['' => 'Tên nhà thầu cung cấp'] + $listPartner,
                                old('partner_id'), ['class' => 'form-control m-input m-select2', 'id' => 'partner_id2']) }}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Mã số thuế: </label>
                                <input type="text" class="form-control" id="tax_code"
                                       placeholder="Mã số thuế" name="tax_code" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-xs-12">
                            <div class="form-group">
                                <label>Địa chỉ: </label>
                                <input type="text" class="form-control" id="address"
                                       placeholder="Địa chỉ" name="address" readonly/>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Người đại diện: </label>
                                <input type="text" class="form-control" id="delegate"
                                       placeholder="Người đại diện" name="delegate" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Số điện thoại: </label>
                                <input type="text" class="form-control" id="phone"
                                       placeholder="Điện thoại" name="phone" readonly/>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Email: </label>
                                <input type="text" class="form-control" id="email"
                                       placeholder="Email" name="email" readonly/>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Website: </label>
                                <input type="text" class="form-control" id="website"
                                       placeholder="Website" name="website" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-xs-12">
                            <div class="form-group">
                                <label>Tên hợp đồng: </label>
                                <input type="text" class="form-control" id="name"
                                       placeholder="Tên hợp đồng" name="name"/>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Số hiệu hợp đồng: <span class="required">(*)</span></label>
                                <input type="text" class="form-control" id="contract_code"
                                       placeholder="Số hiệu hợp đồng" name="contract_code"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-8 col-xs-12">
                            <div class="form-group">
                                <label>Ngày ký hợp đồng: <span class="required">(*)</span></label>
                                <input type="text" class="form-control" id="sign_date"
                                       placeholder="Ngày ký hợp đồng" name="sign_date"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12" style="padding-top: 25px;">
                    <div class="m-dropzone dropzone m-dropzone--primary dropzone_file box_upload" id="attachment">
                        <div class="m-dropzone__msg style-file needsclick">
                        <span>
                            Upload File scan hợp đồng
                        </span>
                        <br/>
                        <span class="m-dropzone__msg-desc" style="font-size: 10px; text-transform: capitalize;">
                            (Tối đa 1 File kích thước 5MB/file)
                        </span>
                        </div>
                    </div>
                    <div class="fallback">
                        <input type="hidden" name="id_file" id="id_file">
                    </div>
                </div>
            </div>
        </section>
        <section class="contract">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="cross-title">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-xs-12">
                                <h3 class="sub_title title_contract">Danh mục hợp đồng</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="asset_import">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div data-repeater-list="director">
                        <div class="class-repeater-item" data-repeater-item>
                            <div class="row" style="padding-bottom: 20px; align-items: center !important;">
                                <div class="col-lg-11 col-md-11 col-xs-11 asset-background" style="flex: 0 0 96%; max-width: 96%; border: 1px solid #c7c7c7; border-radius: 4px; padding: 20px;">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="name_asset">
                                                Tên tài sản:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control name_asset" id="name_asset"
                                                       placeholder="Tên tài sản" name="name_asset"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="type">
                                                Phân loại:
                                            </label>
                                            <div class="form-group">
                                                {{ Form::select('type', ['' => '-- Phân loại --'] + \App\Models\Assets\Asset::$assetClassification, old('type'), [
                                                    'class' => 'form-control m-input',
                                                    'id' => 'type'])
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="group_id">
                                                Nhóm tài sản:
                                            </label>
                                            <div class="form-group">
                                                {{ Form::select('group_id', ['' => '-- Nhóm tài sản --'] + $listGroup, old('group_id'), [
                                                    'class' => 'form-control m-input',
                                                    'id'    => 'group_id',])
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="date_import">
                                                Ngày nhập kho:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control m-input date_import" id="date_import"
                                                       placeholder="Ngày nhập kho" name="date_import"
                                                       value="{{ old('date_import') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="material">
                                                Chất liệu:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control material"
                                                       id="material" placeholder="Chất liệu"
                                                       name="material"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="color">
                                                Màu sắc:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control color" id="color"
                                                       placeholder="Màu sắc" name="color"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="unit_id">
                                                Đơn vị tính:
                                            </label>
                                            <div class="form-group">
                                                {{ Form::select('unit_id', ['' => '-- Đơn vị tính --'] + $listUnit,old('unit_id'), [
                                                    'class' => 'form-control m-input',
                                                     'id' => 'unit_id'])
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="unit_id">
                                                Đơn vị sản xuất/cung cấp:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control producer" id="producer"
                                                       placeholder="Đơn vị sản xuất/cung cấp" name="producer"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-bottom:4px">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="quantity">
                                                Số lượng:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control quantity" id="quantity"
                                                       placeholder="Số lượng" name="quantity"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="price">
                                                Đơn giá:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control price" id="price"
                                                       placeholder="Đơn giá" name="price"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="vat">
                                                Thuế VAT:
                                            </label>
                                            <div class="form-group">
                                                {{ Form::select('vat', ['' => '-- Thuế VAT --'] + \App\Models\Assets\AssetImport::$listVat, null,[
                                                    'class' => 'form-control m-input vat'])
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="total_money">
                                                Thành tiền:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control total_money" id="total_money"
                                                       placeholder="Thành tiền" name="total_money"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-bottom:4px">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="year_use">
                                                Năm sử dụng:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control year_use" id="year_use"
                                                       placeholder="Năm sử dụng" name="year_use"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="warranty">
                                                Bảo hành:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control warranty" id="warranty"
                                                       placeholder="Thời gian bảo hành" name="warranty"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="status">
                                                Trạng thái tài sản:
                                            </label>
                                            <div class="form-group">
                                                {{ Form::select('status', ['' => '-- Trạng thái tài sản --'] + \App\Models\Assets\AssetImport::$listStatus, null,[
                                                    'class' => 'form-control m-input'])
                                                 }}
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="description">
                                                    Ghi chú:
                                                </label>
                                                <input type="text" class="form-control description" id="description"
                                                       placeholder="Ghi chú" name="description"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-md-9 col-xs-12">
                                            <label for="receiver_handover">
                                                Người nhập tài sản:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control m-input" id="receiver_handover"
                                                       placeholder="Người nhập tài sản" name="receiver_handover"
                                                       value="{{ old('receiver_handover') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-xs-1" style="flex: 0 0 4%; max-width: 4%;">
                                    <div data-repeater-delete="" class="">
                                    <span>
                                        <i class="la la-trash-o" style="font-size: 20px;"></i>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12" data-repeater-create="" style="background: #b7b7b7; border-radius: 4px;">
                    <p class="add_contract" data-toggle="modal" data-target="#myModal">
                        + ADD Thêm hợp đồng vào nhà cung cấp này
                    </p>
                </div>
            </div>
        </section>
        <section class="button">
            <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                <div class="m-form__actions">
                    <div class="row">
                        <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                            <button class="save_bid">
                                Lưu
                            </button>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12">
                            <a class="delete_bid" href="{{ route('contract.index') }}">
                                Trở về danh sách
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{ Form::close() }}
    </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            $('#partner_id2').on('change', function (e) {
                let id = $(this).val();
                resetForm();
                getPartner(id);
            });
            function resetForm() {
                $('#phone').val("");
                $('#email').val("");
                $('#address').val("");
                $('#tax_code').val("");
                $('#delegate').val("");
                $('#website').val("");
                $('#position').val("");
                $('#sign_date').val("");
                $('#contract_code').val("");
            }
            function getPartner(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: id,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json;charset=utf-8",
                    success: function (data) {
                        // resetForm();
                        $('#address').val(data.data.address);
                        $('#email').val(data.data.email);
                        $('#phone').val(data.data.phone);
                        $('#tax_code').val(data.data.tax_code);
                        $('#delegate').val(data.data.delegate);
                        $('#website').val(data.data.website);
                        $('#position').val(data.data.position);
                    }
                });
            }
            var listAttachment = [];
            $("#attachment").dropzone({
                addRemoveLinks: true,
                url: '{{ route('contract.upload-attachment') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                maxFiles: 1,
                paramName: "file",
                dictRemoveFile: "Xóa",
                dictRemoveFileConfirmation: "Bạn có chắc muốn xóa tệp này",
                acceptedFiles: "application/pdf",
                dictInvalidFileType: "Định dạng file không phải dạng cho phép",
                dictFileTooBig: "Dung lượng file quá lớn vượt giới hạn cho phép",
                maxFilesize: 10,

                removedfile: function (file) {
                    var fileRemove = findObjectByKey(listAttachment, 'name', file.name);

                    $.ajax({
                        type: 'DELETE',
                        url: '{{ route('contract.delete-attachment') }}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: fileRemove.id,
                            url: fileRemove.file_url,
                        },
                        sucess: function (data) {

                        }
                    });
                    var _ref;
                    listAttachment = $.grep(listAttachment, function (e) {
                        return e.id != fileRemove.id;
                    });
                    var nameArray = listAttachment.map(function (el) {
                        return el.id;
                    });
                    $('#id_file').val(nameArray);
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                },
                init: function () {
                    this.on("success", function (file, response) {
                        listAttachment.push(response.data);
                        var nameArray = listAttachment.map(function (el) {
                            return el.id;
                        });
                        $('#id_file').val(nameArray);
                        alert(response.message);
                    });
                    // Handle errors
                    this.on('error', function (file, response) {
                        alert('File tải lên không phù hợp');
                    });
                }
            });
        });
    </script>
@stop
