@extends('administrator.app')
@section('title','Danh sách nhà thầu')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">Sửa hợp đồng</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                    @include('administrator.errors.errors-validate')
                </div>
            </div>
            {{ Form::open(array('route' => array('contract.update', $data->id), 'method' => 'PUT',
            'class' => 'm-form m-form--fit m-form--label-align-right')) }}
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <section class="main-content-add">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-xs-12">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-xs-12">
                                <div class="form-group">
                                    <label>Tên nhà thầu: <span class="required">(*)</span></label>
                                    {{ Form::select('partner_id', ['' => 'Tên nhà thầu cung cấp'] + @$listPartner,
                                    @$data->partner_id, ['class' => 'form-control m-input m-select2', 'id' => 'partner_id']) }}
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Mã số thuế: <span class="required">(*)</span></label>
                                    <input type="text" class="form-control" id="tax_code" placeholder="Mã số thuế"
                                           name="tax_code" value="{{ @$data->partner->tax_code }}" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-xs-12">
                                <div class="form-group">
                                    <label>Địa chỉ: </label>
                                    <input type="text" class="form-control" id="address" placeholder="Địa chỉ"
                                           name="address" value="{{ @$data->partner->address }}" readonly/>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Người đại diện: </label>
                                    <input type="text" class="form-control" id="delegate" placeholder="Người đại diện"
                                           name="delegate" value="{{ @$data->partner->delegate }}" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Số điện thoại: </label>
                                    <input type="text" class="form-control" id="phone" placeholder="Điện thoại"
                                           name="phone" value="{{ @$data->partner->phone }}" readonly/>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Email: </label>
                                    <input type="text" class="form-control" id="email" placeholder="Email" name="email"
                                           value="{{ @$data->partner->email }}" readonly/>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Website: </label>
                                    <input type="text" class="form-control" id="website" placeholder="Website"
                                           name="website" value="{{ @$data->partner->website }}" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-xs-12">
                                <div class="form-group">
                                    <label>Tên hợp đồng:</label>
                                    <input type="text" class="form-control" id="name" placeholder="Tên hợp đồng"
                                           name="name" value="{{ $data->name }}"/>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Số hiệu hợp đồng: <span class="required">(*)</span></label>
                                    <input type="text" class="form-control" id="contract_code"
                                           placeholder="Số hiệu hợp đồng" name="contract_code"
                                           value="{{ @$data->contract_code }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-8 col-xs-12">
                                <div class="form-group">
                                    <label>Ngày ký hợp đồng: <span class="required">(*)</span></label>
                                    <input type="text" class="form-control" id="sign_date"
                                           placeholder="Ngày ký hợp đồng" name="sign_date"
                                           value="{{ $data->sign_date }}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12" style="padding-top: 25px;">
                        <div class="m-dropzone dropzone m-dropzone--primary dropzone_file box_upload" id="attachment">
                            <div class="m-dropzone__msg style-file needsclick">
                            <span>
                                Upload File scan hợp đồng
                            </span>
                            </div>
                        </div>
                        <div class="fallback">
                            <input type="hidden" name="id_file" id="id_file" value="{{ @$data->file_id }}">
                        </div>
                        <div>
                            @if(@$data->file_id !== NULL)
                                <h3 class="sub_title title_contract">File đã được tải lên</h3>
                                <a href="{{ route('contract_stores.download', $data->file_id) }}" class="">
                                    <i class="fa fa-download" aria-hidden="true"></i>
                                </a>
                                <span class="text_contract">{{ @$data->document->name }}</span>
                            @else
                                <h3 class="sub_title title_contract">Chưa có file được tải lên</h3>
                            @endif
                        </div>
                    </div>
                </div>
            </section>
            <section class="contract">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="cross-title">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-xs-12">
                                    <h3 class="sub_title title_contract">Danh mục hợp đồng</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="asset_import">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div data-repeater-list="director">
                            <div class="class-repeater-item" data-repeater-item>
                                <div class="row" style="padding-bottom: 20px; align-items: center !important;">
                                    <div class="col-lg-11 col-md-11 col-xs-11" style="flex: 0 0 96%; max-width: 96%;">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control name_asset" id="name_asset"
                                                           placeholder="Tên tài sản" name="name_asset" value="{{ $data->name }}"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::select('type', ['' => '-- Phân loại --'] +
                                                    \App\Models\Assets\Asset::$assetClassification,
                                                    $data->type, [
                                                        'class' => 'form-control m-input m-select2',
                                                        'id' => 'type'])
                                                    }}
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::select('group_id', ['' => '-- Nhóm tài sản --'] + $listGroup, $data->group_id, [
                                                        'class' => 'form-control m-input m-select2',
                                                        'id'    => 'group_id',])
                                                    }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control material"
                                                           id="material" placeholder="Chất liệu"
                                                           name="material" value="{{ $data->material }}"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control color" id="color"
                                                           placeholder="Màu sắc" name="color" value="{{ $data->color }}"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::select('unit_id', ['' => '-- Tên đơn vị --'] + $listUnit, $data->unit_id, [
                                                        'class' => 'form-control m-input m-select2',
                                                         'id' => 'unit_id'])
                                                    }}
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control producer" id="producer"
                                                           placeholder="Đơn vị sản xuất/cung cấp" name="producer" value="{{ $data->producer }}"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-bottom:4px">
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control quantity" id="quantity"
                                                           placeholder="Số lượng" name="quantity" value="{{ $data->quantity }}"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <input type="text" class="form-control price" id="price"
                                                       placeholder="Giá bán" name="price" value="{{ $data->price }}"/>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::select('vat', ['' => '-- Thuế VAT --'] + \App\Models\Assets\AssetImport::$listVat, $data->vat,[
                                                        'class' => 'form-control vat m-input m-select2'])
                                                    }}
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control price_vat" id="price_vat"
                                                           placeholder="Thành tiền" name="price_vat" value="{{ $data->price_vat }}"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-bottom:4px">
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control year_use" id="year_use"
                                                           placeholder="Thời gian đưa vào sử dụng" name="year_use" value="{{ $data->year_use }}"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control warranty" id="warranty"
                                                           placeholder="Thời gian bảo hành" name="warranty" value="{{ $data->warranty }}"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::select('status', ['' => '-- Trạng thái tài sản --'] +
                                                    \App\Models\Assets\AssetImport::$listStatus,
                                                    $data->status,[
                                                        'class' => 'form-control m-input m-select2'])
                                                     }}
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control description" id="description"
                                                           placeholder="Ghi chú" name="description"  value="{{ $data->description }}"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-xs-1" style="flex: 0 0 4%; max-width: 4%;">
                                        <div data-repeater-delete="" class="">
                                        <span>
                                            <i class="la la-trash-o" style="font-size: 20px;"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="button">
                <div class="row">
                    <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                        <button class="save_bid">
                            Lưu
                        </button>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <a class="delete_bid" href="{{ route('contract.index') }}">
                            Trở về danh sách
                        </a>
                    </div>
                </div>
            </section>
            {{ Form::close() }}
        </div>
    </div>
@stop
