@extends('administrator.app')
@section('title','Danh sách hợp đồng đã ký kết')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">
                    <a href="{{ route('contract.index') }}">Danh sách hợp đồng</a>
                </h2>
            </div>
            @include('administrator.contracts.search')
            <section class="main-content">
                <div class="wrap-table">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                            @include('administrator.errors.messages')
                        </div>
                    </div>
                    <table class="table table-hover table-responsive table-striped">
                        <thead class="table_head">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">STT</th>
                            <th scope="col">Số hiệu hợp đồng</th>
                            <th scope="col">Nhà thầu</th>
                            <th scope="col">Ngày ký</th>
                            <th scope="col">Giá trị</th>
                            <th scope="col">File scan</th>
                            <th scope="col">Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr class="header expand">
                                <td scope="row"><span class="sign"></span></td>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $value->contract_code }}</td>
                                <td>{{ data_get($value, 'partner.name') }}</td>
                                <td>{{ date('d-m-Y', strtotime($value->sign_date)) }}</td>
                                {{--<td>{{number_format($value->where(\App\Models\Contracts\Contract::PARENT, $value->id)->pluck('total_money')->sum())}} VND</td>--}}
                                <td>{{number_format($value->pluck('total_money')->sum())}} VND</td>

                                <td>
                                    @if(@$value->file_id !== NULL)
                                        <a href="{{ route('contract_stores.download', $value->file_id) }}" class="">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                        </a>
                                        <span class="text_contract">{{ @$value->document->name }}</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('contract.show', $value->id) }}" class=""
                                       data-toggle="tooltip" title="Xem chi tiết hợp đồng">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    {{ Form::open(array(
                                    'method'=>'DELETE',
                                    'route' => array('contract.destroy',$value->id),
                                     'style' => 'display: inline-block;'))
                                     }}
                                    <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                            class="btn-delete" data-toggle="tooltip" title="Xóa hợp đồng">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            <tr class="sub-table">
                                <td colspan="12">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="wrap-title">
                    {{ $data->links() }}
                    Tổng số {{ $data->total() }} bản ghi
                </div>
            </section>
        </div>
    </div>
@stop
