{{ Form::open(array('route' => 'contract.index', 'method' => 'GET', 'id' =>'search_form_contract')  ) }}
<section class="search-content">
    <div class="row">
        <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
            <input type="text" name="contract_code" id="contract_code" class="form-control"
                   placeholder="Số hiệu hợp đồng"/>
        </div>
        <div class="col-lg-2 col-md-2 col-xs-12">
            <div class="form-group">
                {{ Form::select('category_id', ['' => 'Hạng mục'] + $listCategory, @$input['category'], ['class' => 'form-control m-input m-select2', 'id' => 'category_id']) }}
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 mb-3 mb-md-0">
            <div class="form-group">
                {{ Form::select('city_id', [0 => 'Chọn tỉnh/thành'] + $listCity, null, ['class' => 'form-control m-input m-select2', 'id' => 'city_id']) }}
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 mb-3 mb-md-0">
            <div class="form-group">
                {{ Form::select('district_id', ['' => 'Quận huyện'],  @$input['district_id'], ['class' => 'form-control m-input m-select2', 'id' => 'district_id']) }}
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-xs-12">
            <div class="form-group">
                <input type="submit" class="form-control" id="search-button" value="Tìm kiếm"
                       style="background: #f6811f;color: #fff;"/>
            </div>
        </div>
        <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
            <a href="{{ route('contract.create') }}"
               class="btn btn-add-color btn m-btn m-btn--icon" role="button"
               aria-pressed="true">Thêm mới hợp đồng</a>
            {{--<a href="{{ route('contract.index') }}" class="form-control" id="search-button" value="Hủy tìm kiếm"--}}
            {{--style="background: #f63d1f;color: #fff; text-align: center;">Hủy tìm</a>--}}
            {{--<div class="dropdown">--}}
                {{--<button class="dropdown-toggle_button btn-add-color btn m-btn m-btn--icon" type="button"--}}
                        {{--id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                    {{--Tùy chọn thêm mới--}}
                {{--</button>--}}
                {{--<div class="dropdown-menu drowdown-addnew" aria-labelledby="dropdownMenuButton">--}}
                    {{--<a href="{{ route('contract.create') }}" class="dropdown-item">--}}
                        {{--Thêm mới hợp đồng--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</section>
{{ Form::close() }}