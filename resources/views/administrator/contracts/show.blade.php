@extends('administrator.app')
@section('title','Chi tiết bàn giao')
@section('content')
    <div class="list-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-6">
                    <h2 class="title_list">Chi tiết hợp đồng</h2>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                    <a href="{{ route('contract.index') }}"
                       class="btn m-btn m-btn--icon btn-add-color-cat-right">
					<span>
						<i class="fa flaticon-list-3"></i>
						<span>
							Danh sách hợp đồng
						</span>
					</span>
                    </a>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    @include('administrator.errors.errors-validate')
                    <!--begin::Portlet-->
                        <div class="m-portlet m-portlet--tab">
                            <h3 class="sub_title">Thông tin hợp đồng</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="hd-txt">
                                        <span class="titleContractShow">Tên hợp đồng:</span>
                                        <span>{{$data->name}}</span>
                                    </div>
                                    <div class="hd-txt">
                                        <span class="titleContractShow">Mã hợp đồng:</span>
                                        <span>{{$data->contract_code}}</span>
                                    </div>
                                    <div class="hd-txt">
                                        <span class="titleContractShow">Mã số thuế:</span>
                                        <span>{{$data->partner->tax_code}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="hd-txt">
                                        <span class="titleContractShow">Người đại diện:</span>
                                        <span>{{$data->partner->delegate}}</span>
                                    </div>
                                    <div class="hd-txt">
                                        <span class="titleContractShow">Email:</span>
                                        <span>{{$data->partner->email}}</span>
                                    </div>
                                    <div class="hd-txt">
                                        <span class="titleContractShow">Địa chỉ:</span>
                                        <span>{{$data->partner->address}}</span>
                                    </div>
                                    <div class="hd-txt">
                                        <span class="titleContractShow">Website:</span>
                                        <span>{{$data->partner->website}}</span>
                                    </div>
                                    <div class="hd-txt">
                                        <span class="titleContractShow">Số điện thoại:</span>
                                        <span>{{$data->partner->phone}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="wrap-title">
                                <h3 class="sub_title">
                                    Danh sách tài sản
                                </h3>
                            </div>
                            <div class="wrap-table">
                                <table class="table table-hover table-responsive table-striped">
                                    <thead class="table_head">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Tên</th>
                                        <th scope="col">Mã</th>
                                        <th scope="col">Mã nhóm</th>
                                        <th scope="col">Số hiệu HĐ</th>
                                        <th scope="col">Chất liệu</th>
                                        <th scope="col">Màu sắc</th>
                                        <th scope="col">Số lượng</th>
                                        <th scope="col">Đơn vị tính</th>
                                        <th scope="col">Giá trị(VND)</th>
                                        <th scope="col">Đơn vị SD</th>
                                        <th scope="col">Người nhập kho</th>
                                        <th scope="col">Ngày nhập kho</th>
                                        <th scope="col">Phân loại</th>
                                        <th scope="col">Bảo hành(tháng)</th>
                                        <th scope="col">Năm sử dụng</th>
                                        <th scope="col">Tình trạng</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataContractShow as $key => $value)
                                        <tr class="header">
                                            <td scope="row"></td>
                                            <td class="text-left">{{ $value->name }}</td>
                                            <td class="text-left">{{ (isset($value->group->group_code)? $value->group->group_code.'-':'').$value->id }}</td>
                                            <td class="text-left">{{ data_get($value, 'group.group_code') }}</td>
                                            <td class="text-left">{{ $value->contract_code }}</td>
                                            <td class="text-left">{{ $value->material }}</td>
                                            <td class="text-left">{{ $value->color }}</td>
                                            <td class="text-left">{{ $value->quantity }}</td>
                                            <td class="text-center">{{ data_get($value, 'unit.name') }}</td>
                                            <td class="text-right">{{ number_format($value->price) }}</td>
                                            <td class="text-center">{{ data_get($value, 'ownerStructure.name') }}</td>
                                            <td class="text-left">{{ $value->receiver_handover }}</td>
                                            <td class="text-left">
                                                {{ isset($value->date_import)?
                                                Carbon\Carbon::createFromFormat('Y-m-d',$value->date_import)->format('d-m-Y'):''  }}
                                            </td>
                                            <td class="text-center">{{ isset($value->type) ?
                                            \App\Models\Assets\Asset::$assetClassification[$value->type] : 'Chưa phân loai' }}</td>
                                            <td class="text-center">{{ $value->warranty }}</td>
                                            <td class="text-center">{{ isset($value->year_use) ? $value->year_use : 'Chưa có' }}</td>
                                            <td class="text-center">
                                                {{(isset($value->status) && $value->status) ?
                                                \App\Models\Assets\Asset::$assetListStatus[$value->status]: 'Lưu kho' }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@stop