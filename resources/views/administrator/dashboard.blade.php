@extends('administrator.app')
@section('title','HỆ THỐNG QUẢN LÝ TÀI SẢN - HẢI PHÁT LAND')

@section('content')
<div class="main-box">
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="box_5 hvr-push">
                        <a href="{{ route('package.index') }}"><img src="{{ URL::asset('images/icon_xaylap.png') }}" class="img-responsive icon-box"></a>
                        <a href="{{ route('package.index') }}"><p class="title-box" data-toggle="tooltip" data-placement="bottom" title="Gói thầu">Gói thầu</p></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="box_1 hvr-push">
                        <a href="{{ route('partner.index') }}"><img src="{{ URL::asset('images/icon_nhathau.png') }}" class="img-responsive icon-box"></a>
                        <a href="{{ route('partner.index') }}"><p class="title-box" data-toggle="tooltip" data-placement="bottom" title="Quản lý nhà thầu">Quản lý nhà thầu</p></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="box_2 hvr-push">
                        <a href="{{ route('contract.index') }}"><img src="{{ URL::asset('images/icon_hopdong.png') }}" class="img-responsive icon-box"></a>
                        <a href="{{ route('contract.index') }}"><p class="title-box" data-toggle="tooltip" data-placement="bottom" title="Quản lý hợp đồng">Quản lý hợp đồng</p></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="box_3 hvr-push">
                        <a href="{{ route('assets.index') }}"><img src="{{ URL::asset('images/icon_taisan.png') }}" class="img-responsive icon-box"></a>
                        <a href="{{ route('assets.index') }}"><p class="title-box" data-toggle="tooltip" data-placement="bottom" title="Tài sản - công cụ - dụng cụ">Tài sản - công cụ - dụng cụ</p></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="box_4 hvr-push">
                        <a href="{{ route('investment.index') }}"><img src="{{ URL::asset('images/profits.png') }}" class="img-responsive icon-box"></a>
                        <a href="{{ route('investment.index') }}"><p class="title-box" data-toggle="tooltip" data-placement="bottom" title="Quản lý hạng mục đầu tư">Quản lý hạng mục đầu tư</p></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="box_6 hvr-push">
                        <a href="{{ route('repair.index') }}"><img src="{{ URL::asset('images/icon_baohanh.png') }}" class="img-responsive icon-box"></a>
                        <a href="{{ route('repair.index') }}"><p class="title-box" data-toggle="tooltip" data-placement="bottom" title="Sửa chữa - Bảo hành - Bảo trì">Sửa chữa - Bảo hành - Bảo trì</p></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="box_7 hvr-push">
                        <a href="{{ route('handover.index') }}"><img src="{{ URL::asset('images/hand-shake.png') }}" class="img-responsive icon-box"></a>
                        <a href="{{ route('handover.index') }}"><p class="title-box" data-toggle="tooltip" data-placement="bottom" title="Bàn giao - Thanh lý">Bàn giao tài sản</p></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="box_8 hvr-push">
                        <a href="{{ route('reports.index') }}"><img src="{{ URL::asset('images/icon_baocao.png') }}" class="img-responsive icon-box"></a>
                        <a href="{{ route('reports.index') }}"><p class="title-box" data-toggle="tooltip" data-placement="bottom" title="Thống kê - Báo cáo">Thống kê - Báo cáo</p></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
    <script>
        $(document).ready(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
        });
    </script>
@stop