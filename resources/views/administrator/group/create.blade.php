@extends('administrator.app')
@section('title','Thêm mới hạng mục thầu')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">Thêm mới nhóm tài sản</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                    @include('administrator.errors.errors-validate')
                </div>
            </div>
            {{ Form::open(array('route' => 'group.store', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
            <section class="main-content-add">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <h3 class="sub_title">Thông tin cơ bản</h3>
                    </div>
                    <div class="col-lg-9 col-md-9 col-xs-12">
                        <div class="form-group">
                            <label for="name">
                                Tên
                                <span class="required">(*)</span></label>
                            <input type="text" class="form-control m-input" id="name" placeholder="Tên nhóm tài sản"
                                   name="name" value="{{ old('name') }}">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <div class="form-group">
                            <label for="group_code">
                                Mã nhóm tài sản
                            </label>
                            <input type="text" class="form-control m-input" id="group_code"
                                   placeholder="Mã nhóm tài sản"
                                   name="group_code" value="{{ old('group_code') }}">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="group_code">
                                Nhóm tài sản cha
                            </label>
                            <select class="form-control m-input m-select2" id="drl_group_parent"
                                    name="parent_id">
                                <option value="0">-- Không có cha --</option>
                                <?php echo \App\Models\Partners\Group::genHtmlGroup('') ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                        <button class="save_bid">
                            Lưu
                        </button>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <a class="delete_bid" href="{{ route('group.index') }}">
                            Trở về danh sách
                        </a>
                    </div>
                </div>
            </section>
            {{ Form::close() }}
        </div>
    </div>
@stop
