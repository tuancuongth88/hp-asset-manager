@extends('administrator.app')
@section('title','Danh sách nhóm tài sản')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">
                    <a href="{{ route('group.index') }}">Danh sách nhóm tài sản</a>
                </h2>
            </div>
            @include('administrator.errors.messages')
            {{ Form::open(array('route' => 'group.index', 'method' => 'GET',
            'class'=>'m-form m-form--fit m-form--label-align-right')  ) }}
            <div class="row">
                <div class="col-lg-2 col-md-2 col-xs-12">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Tên nhóm tài sản"/>
                </div>
                <div class="col-lg-2 col-md-2 col-xs-12">
                    <input type="submit" class="form-control" id="search-button"
                           value="Tìm kiếm" style="background: #f6811f;color: #fff;">
                </div>
                <div class="col-lg-2 col-md-2 col-xs-12">
                    <a href="{{ route('group.create') }}" class="btn m-btn m-btn--icon btn-add-color">
                            <span>
                                <i class="fa flaticon-plus"></i>
                                <span>
                                    Thêm mới nhóm
                                </span>
                            </span>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-xs-12">
                    <a href="{{ route('asset.index') }}" class="btn m-btn m-btn--icon btn-add-color-cat"
                       style="color:#fff;">
                        Trở về danh sách tài sản
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <a href="{{ route('investment.index') }}" class="btn m-btn m-btn--icon btn-add-color-cat"
                       style="color:#fff;">
                        Trở về danh sách đầu tư
                    </a>
                </div>
            </div>
            {{ Form::close() }}
            <section class="main-content">
                <div class="wrap-table">
                    <table class="table table-hover table-responsive table-striped">
                        <thead class="table_head">
                        <tr>
                            <th scope="col" width="2%">#</th>
                            <th scope="col" width="20%">Tên</th>
                            <th scope="col">Mã nhóm</th>
                            <th scope="col">Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr class="header expand">
                                <td scope="row">
                                    {{ $key + 1 }}
                                </td>
                                <td class="text-left">
                                    {{ $value['name'] }}
                                </td>
                                <td>
                                    {{ $value['group_code'] }}
                                </td>
                                <td>
                                    <a href="{{ route('group.edit', $value['id']) }}" class="">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                    {{ Form::open(array('method'=>'DELETE', 'route' => array('group.destroy', $value['id']), 'style' => 'display: inline-block;')) }}
                                    <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                            class="btn-delete">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="wrap-title">
                    {{ $data->links() }}
                    Tổng số {{ $data->total() }} bản ghi
                </div>
            </section>
        </div>
    </div>
@stop