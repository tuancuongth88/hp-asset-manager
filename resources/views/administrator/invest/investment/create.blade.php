@extends('administrator.app')
@section('title','Danh sách tài sản')

@section('content')
    {{ Form::open(array('route' => 'imports.store', 'method' => 'POST',
        'class' => 'm-form m-form--fit m-form--label-align-right', 'id' => 'importsForm', 'files'=>true)) }}
    <div class="container">
        <section class="main-content-add">
            <div class="wrap-title">
                <h3 class="sub_title">Thông tin cơ bản</h3>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                    @include('administrator.errors.errors-validate')
                </div>
            </div>
            <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <label for="company_id">
                            Công ty: <span class="required">(*)</span>
                        </label>
                        <select class="form-control m-input m-select2" id="company_id" name="company_id">
                            <option value="">-- Chọn công ty --</option>
                            @foreach ($listCompany as $element)
                                <option value="{{ $element['id'] }}">
                                    {{ $element['name'] }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <label for="structure_id">
                            Trực thuộc: <span class="required">(*)</span>
                        </label>
                        <select class="form-control m-input m-select2" id="structure_id" name="structure_id">
                            <option value="">-- Chọn cấu trúc phòng ban --</option>
                            @foreach ($listStructer as $element)
                                <option value="{{ $element['id'] }}">
                                    {{ $element['name'] }}
                                    | {{\App\Models\Systems\StructureCompany::$listType[$element['type']]}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            <div class="row" id="asset_import">
                    <div class="wrap-title">
                        <h3 class="sub_title" style="padding: 20px 0 20px 0;">Danh mục tài sản</h3>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div data-repeater-list="director">
                            <div class="class-repeater-item" data-repeater-item>
                                <div class="row" style="padding-bottom: 20px; align-items: center !important;">
                                    <div class="col-lg-11 col-md-11 col-xs-11" style="flex: 0 0 90%; max-width: 90%;">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="name_asset">
                                                    Tên tài sản: <span class="required">(*)</span>
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control name_asset" id="name_asset"
                                                           placeholder="Tên tài sản" name="name_asset"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="contract_id">
                                                    Số hiệu hợp đồng:
                                                </label>
                                                <div class="form-group">
                                                    {{ Form::select('contract_id', ['' => '-- Số hiệu hợp đồng --'] + $listContract, old('contract_id'), [
                                                        'class' => 'form-control m-input',
                                                        'id' => 'contract_id'])
                                                    }}
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="type">
                                                    Phân loại: <span class="required">(*)</span>
                                                </label>
                                                <div class="form-group">
                                                    {{ Form::select('type', ['' => '-- Phân loại --'] + \App\Models\Assets\Asset::$assetClassification, old('type'), [
                                                        'class' => 'form-control m-input',
                                                        'id' => 'type'])
                                                    }}
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="group_id">
                                                    Nhóm tài sản: <span class="required">(*)</span>
                                                </label>
                                                <div class="form-group">
                                                    {{ Form::select('group_id', ['' => '-- Nhóm tài sản --'] + $listGroup, old('group_id'), [
                                                        'class' => 'form-control m-input',
                                                        'id'    => 'group_id',])
                                                    }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="material">
                                                    Chất liệu:
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control material"
                                                           id="material" placeholder="Chất liệu"
                                                           name="material"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="color">
                                                    Màu sắc:
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control color" id="color"
                                                           placeholder="Màu sắc" name="color"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="unit_id">
                                                    Đơn vị tính:
                                                </label>
                                                <div class="form-group">
                                                    {{ Form::select('unit_id', ['' => '-- Đơn vị tính --'] + $listUnit,old('unit_id'), [
                                                        'class' => 'form-control m-input',
                                                         'id' => 'unit_id'])
                                                    }}
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="unit_id">
                                                    Đơn vị sản xuất/cung cấp:
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control producer" id="producer"
                                                           placeholder="Đơn vị sản xuất/cung cấp" name="producer"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-bottom:4px">
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="quantity">
                                                    Số lượng:
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control quantity" id="quantity"
                                                           placeholder="Số lượng" name="quantity"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="price">
                                                    Đơn giá:
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control price" id="price"
                                                           placeholder="Đơn giá" name="price"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="vat">
                                                    Thuế VAT:
                                                </label>
                                                <div class="form-group">
                                                    {{ Form::select('vat', ['' => '-- Thuế VAT --'] + \App\Models\Assets\AssetImport::$listVat, null,[
                                                        'class' => 'form-control m-input vat'])
                                                    }}
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="total_money">
                                                    Thành tiền:
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control total_money" id="total_money"
                                                           placeholder="Thành tiền" name="total_money"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-bottom:4px">
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="year_use">
                                                    Năm sử dụng:
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control year_use" id="year_use"
                                                           placeholder="Năm sử dụng" name="year_use"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="warranty">
                                                    Bảo hành:
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control warranty" id="warranty"
                                                           placeholder="Thời gian bảo hành" name="warranty"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="status">
                                                    Trạng thái tài sản: <span class="required">(*)</span>
                                                </label>
                                                <div class="form-group">
                                                    {{ Form::select('status', ['' => '-- Trạng thái tài sản --'] +
                                                    \App\Models\Assets\AssetImport::$listStatus, null,[
                                                        'class' => 'form-control m-input'])
                                                     }}
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="original">
                                                        Hiện trạng tài sản: <span class="required">(*)</span>
                                                    </label>
                                                    <div class="form-group">
                                                        {{ Form::select('original', ['' => '-- Hiện trạng tài sản --'] +
                                                        \App\Models\Assets\AssetImport::$assetOriginalClassification, null,[
                                                            'class' => 'form-control m-input'])
                                                         }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <label for="receiver_handover">
                                                    Người nhập tài sản:
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control m-input" id="receiver_handover"
                                                           placeholder="Người nhập tài sản" name="receiver_handover"
                                                           value="{{ old('receiver_handover') }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label for="date_import">
                                                    Ngày nhập kho:
                                                </label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control m-input date_import" id="date_import"
                                                           placeholder="Ngày nhập kho" name="date_import"
                                                           value="{{ old('date_import') }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <div class="form-group">
                                                    <label for="description">
                                                        Ghi chú:
                                                    </label>
                                                    <input type="text" class="form-control description" id="description"
                                                           placeholder="Ghi chú" name="description"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-xs-1" style="flex: 0 0 10%; max-width: 10%;">
                                        <div data-repeater-delete="" class="">
                                            <span>
                                                <i class="la la-trash-o" style="font-size: 20px;"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12" data-repeater-create="" style="background: #b7b7b7; border-radius: 4px;">
                        <p class="add_contract" data-toggle="modal" data-target="#myModal">
                            + ADD Thêm tài sản vào hệ thống
                        </p>
                    </div>
                </div>
        </section>
        <section class="button">
            <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                <div class="m-form__actions">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-xs-12"></div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <button class="save_bid">
                                    Lưu
                                </button>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <a class="delete_bid" href="{{ route('assets.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    {{ Form::close() }}
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
