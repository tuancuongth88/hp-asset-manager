@extends('administrator.app')
@section('title','Danh sách tài sản')

@section('content')
    {{ Form::open(array(
    'route' => array('investment.update', $data->id), 'method' => 'PUT',
    'class' => 'm-form m-form--fit m-form--label-align-right',
    'files'=>true, 'enctype' => 'multipart/form-data')) }}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="container">
        <section class="main-content-add">
            <div class="wrap-title">
                <h3 class="sub_title">Thông tin cơ bản</h3>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                    @include('administrator.errors.errors-validate')
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <label for="company">
                        Công ty: <span>{{ data_get($data, 'company.name') }}</span>
                    </label>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <label for="structure_asset_id">
                        Trực thuộc:
                        <span>{{ data_get($data, 'ownerStructure.name') }} | {{ \App\Models\Systems\StructureCompany::$listType[data_get($data, 'ownerStructure.type')] }}</span>
                    </label>
                </div>
            </div>
            <div class="row" id="asset_import">
                <div class="wrap-title">
                    <h3 class="sub_title" style="padding: 20px 0 20px 0;">Danh mục tài sản</h3>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div data-repeater-list="director">
                        <div class="class-repeater-item" data-repeater-item>
                            <div class="row" style="padding-bottom: 20px; align-items: center !important;">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="name_asset">
                                                Tên tài sản: <span class="required">(*)</span>
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control name_asset" id="name_asset"
                                                       placeholder="Tên tài sản" name="name_asset"
                                                       value="{{ $data->name }}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="contract_id">
                                                Số hiệu hợp đồng:
                                            </label>
                                            <div class="form-group">
                                                {{ Form::select('contract_id', ['' => '-- Số hiệu hợp đồng --'] + $listContract, $data->contract_id, [
                                                    'class' => 'form-control m-input',
                                                    'id' => 'contract_id'])
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="type">
                                                Phân loại: <span class="required">(*)</span>
                                            </label>
                                            <div class="form-group">
                                                {{ Form::select('type', ['' => '-- Phân loại --'] + \App\Models\Assets\Asset::$assetClassification, $data->type, [
                                                    'class' => 'form-control m-input',
                                                    'id' => 'type'])
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="group_id">
                                                Nhóm tài sản: <span class="required">(*)</span>
                                            </label>
                                            <div class="form-group">
                                                <select class="form-control m-input drl_group m-select2"
                                                        name="group_id">
                                                    <option value="0">-- Tất cả --</option>
                                                    <?php echo \App\Models\Partners\Group::genHtmlGroup($data->group_id) ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="material">
                                                Chất liệu:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control material"
                                                       id="material" placeholder="Chất liệu"
                                                       name="material" value="{{ $data->material }}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="color">
                                                Màu sắc:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control color" id="color"
                                                       placeholder="Màu sắc" name="color" value="{{ $data->color }}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="unit_id">
                                                Tên đơn vị:
                                            </label>
                                            <div class="form-group">
                                                {{ Form::select('unit_id', ['' => '-- Tên đơn vị --'] + $listUnit,
                                                $data->unit_id, [
                                                    'class' => 'form-control m-input',
                                                     'id' => 'unit_id'])
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="unit_id">
                                                Đơn vị sản xuất/cung cấp:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control producer" id="producer"
                                                       placeholder="Đơn vị sản xuất/cung cấp" name="producer"
                                                       value="{{ $data->producer }}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-bottom:4px">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="price">
                                                Đơn giá:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control price" id="price"
                                                       placeholder="Đơn giá" name="price" value="{{ $data->price }}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="vat">
                                                Thuế VAT:
                                            </label>
                                            <div class="form-group">
                                                {{ Form::select('vat', ['' => '-- Thuế VAT --'] +
                                                \App\Models\Assets\AssetImport::$listVat, $data->vat,[
                                                    'class' => 'form-control vat'])
                                                }}
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="price">
                                                Thành tiền:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control price_vat" id="price_vat"
                                                       placeholder="Thành tiền" name="price_vat"
                                                       value="{{ $data->price_vat }}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="description">
                                                Ghi chú:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control description" id="description"
                                                       placeholder="Ghi chú" name="description"
                                                       value="{{ $data->description }}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-bottom:4px">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="year_use">
                                                Năm sử dụng:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control year_use" id="year_use"
                                                       placeholder="Năm sử dụng" name="year_use"
                                                       value="{{ $data->year_use }}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="warranty">
                                                Bảo hành:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control warranty" id="warranty"
                                                       placeholder="Thời gian bảo hành" name="warranty"
                                                       value="{{ $data->warranty }}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="status">
                                                Trạng thái tài sản: <span class="required">(*)</span>
                                            </label>
                                            <div class="form-group">
                                                {{ Form::select('status', ['' => '-- Trạng thái tài sản --'] +
                                                 \App\Models\Assets\AssetImport::$listStatus, $data->status,[
                                                    'class' => 'form-control m-input'])
                                                 }}
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <div class="form-group">
                                                <label for="original">
                                                    Trạng thái ban đầu tài sản: <span class="required">(*)</span>
                                                </label>
                                                <div class="form-group">
                                                    {{ Form::select('original', ['' => '-- Hiện trạng tài sản --'] +
                                                     \App\Models\Assets\AssetImport::$assetOriginalClassification,
                                                     $data->original,[
                                                        'class' => 'form-control m-input'])
                                                     }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9 col-md-9 col-xs-12">
                                            <label for="receiver_handover">
                                                Người nhập tài sản:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control m-input" id="receiver_handover"
                                                       placeholder="Người nhập tài sản" name="receiver_handover"
                                                       value="{{ $data->receiver_handover }}"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <label for="date_import">
                                                Ngày nhập kho:
                                            </label>
                                            <div class="form-group">
                                                <input type="text" class="form-control m-input date_import"
                                                       id="date_import"
                                                       placeholder="Ngày nhập kho" name="date_import"
                                                       value="{{ $data->date_import }}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="button">
            <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                <div class="m-form__actions">
                    <div class="container">
                        <div class="row">
                            <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                                <button class="save_bid">
                                    Lưu
                                </button>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <a class="delete_bid" href="{{ route('assets.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    {{ Form::close() }}
@stop
