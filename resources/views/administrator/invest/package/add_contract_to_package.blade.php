@extends('administrator.app')
@section('title','Danh sách nhà thầu')

@section('content')
    <div class="container">
        <div class="list-content">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-xs-12">
                <h3 class="title_list">
                    Danh sách hợp đồng
                </h3>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-12" style="padding-top:20px;">
                <a href="{{ route('package.index') }}" class="btn btn-add-color-cat">Danh sách gói thầu</a>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12">
                @include('administrator.errors.info-search')
                @include('administrator.errors.errors-validate')
                @include('administrator.errors.messages')
            </div>
        </div>
        <section class="main-content-add">
            <div class="wrap-table">
                <table class="table table-hover table-responsive table-striped">
                        <thead>
                        <tr>
                            <th scope="col">STT</th>
                            <th scope="col">Tên hợp đồng</th>
                            <th scope="col">Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            @if(!in_array($value->id,$package_params))
                                <tr class="header">
                                    <td style="padding: 3px;">{{ $key + 1 }}</td>
                                    <td style="padding: 3px;">{{ $value['name'] }}</td>
                                    <td style="padding: 3px;">
                                        {{ Form::open(array(
                                        'method'=>'POST',
                                        'route' => array('package.add-contract'),
                                         'style' => 'display: inline-block;')) }}
                                        <input type="hidden" name="contract_id" value="{{ $value->id }}">
                                        <input type="hidden" name="package_id" value="{{ $id }}">
                                        <button class="btn btn-add-color">
                                            Add
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
            </div>
            <div class="wrap-title">
                {{ $data->links() }}
                Tổng số {{ $data->total() }} bản ghi
            </div>
        </section>
    </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop