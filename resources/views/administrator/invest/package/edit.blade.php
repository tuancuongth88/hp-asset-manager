@extends('administrator.app')
@section('title','Sửa gói thầu')

@section('content')
    <div class="container">
        <div class="list-content">
        <div class="wrap-title">
            <h2 class="title_list">Sửa gói thầu</h2>
        </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                    @include('administrator.errors.errors-validate')
                </div>
            </div>
        {{ Form::open(array('route' => array('package.update', $data->id),
         'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data', 'method'=>'PUT')) }}
        <section class="main-content-add">
            <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Tên gói thầu: <span class="required">(*)</span></label>
                            <input type="text" class="form-control" id="name" placeholder="Tên gói thầu"
                                   name="name" value="{{$data->name}}"/>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Giá gói thầu(VND): <span class="required">(*)</span></label>
                            <input type="text" class="form-control" id="price" placeholder="Giá gói thầu"
                                   name="price" value="{{$data->price}}"/>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Loại gói thầu: <span class="required">(*)</span></label>
                            <input type="text" class="form-control" id="type" placeholder="Loại gói thầu"
                                   name="type_contract" value="{{$data->type_contract}}"/>
                        </div>
                    </div>
                </div>
            <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Nguồn vốn: <span class="required">(*)</span></label>
                            <input type="text" class="form-control" id="capital" placeholder="Nguồn vốn"
                                   name="capital" value="{{$data->capital}}"/>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Thời gian thầu: <span class="required">(*)</span></label>
                            <input type="text" class="form-control" id="start_time"
                                   placeholder="Thời gian thầu" name="start_time"
                                   value="{{$data->start_time}}"/>
                        </div>
                    </div>
                </div>
        </section>
        <section class="button">
            <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                <div class="m-form__actions">
                    <div class="row">
                            <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                                <button class="save_bid">
                                    Lưu
                                </button>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <a class="delete_bid" href="{{ route('package.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                </div>
            </div>
        </section>
        {{ Form::close() }}
    </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop