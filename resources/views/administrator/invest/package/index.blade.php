@extends('administrator.app')
@section('title','Danh sách gói thầu')

@section('content')
    <div class="container">
        <div class="list-content">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-xs-12">
                <h2 class="title_list"><a href="{{ route('package.index') }}">Danh sách gói thầu</a></h2>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12">
            @include('administrator.errors.messages')
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12" style="margin-top: 20px;">
                <a href="{{ route('package.create') }}"
                   class="btn btn-add-color">Thêm mới</a>
            </div>
        </div>
        <section class="main-content">
            <div class="wrap-table">
                <table class="table table-hover table-responsive table-striped">
                        <thead class="table_head">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">STT</th>
                            <th scope="col">Tên</th>
                            <th scope="col">Giá</th>
                            <th scope="col">Nguồn vốn</th>
                            <th scope="col">Thời gian bắt đầu</th>
                            <th scope="col">Loại hợp đồng</th>
                            <th scope="col">Trạng thái</th>
                            <th scope="col">Ngày tạo</th>
                            <th scope="col">Người tạo</th>
                            <th scope="col">Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr class="header">
                                <td scope="row"></td>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $value->name }}</td>
                                <td>{{ number_format($value->price) }}</td>
                                <td>{{ $value->capital }}</td>
                                <td>{{
                                isset($value->start_time) ?
                                \Carbon\Carbon::createFromFormat('Y-m-d',$value->start_time)->format('d-m-Y') : ''
                                 }}</td>
                                <td>{{ $value->type }}</td>
                                <td>{{ $value->status }}</td>
                                <td>{{ $value->created_at->format('d-m-Y') }}</td>
                                <td>{{ data_get($value, 'createdBy.fullname') }}</td>
                                <td>
                                    <a href="{{ route('package.edit', $value->id) }}" class=""
                                       data-toggle="tooltip" title="Sửa thông tin gói thầu">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{ route('package.contract', $value->id) }}"
                                       class="" data-toggle="tooltip" title="Thêm hợp đồng vào gói thầu">
                                        <i class="fa fa-list" aria-hidden="true"></i>
                                    </a>
                                    {{ Form::open(
                                    array('method'=>'DELETE',
                                     'route' => array('package.destroy', $value->id),
                                      'style' => 'display: inline-block;'))
                                      }}
                                    <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                            class="btn-border-none btn-delete-color" data-toggle="tooltip"
                                            title="Xóa thông tin gói thầu">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
            <div class="wrap-title">
                {{ $data->links() }}
                Tổng số {{ $data->total() }} bản ghi
            </div>
        </section>
    </div>
    </div>
@stop