@extends('administrator.app')
@section('title','Danh sách hợp đồng trong gói')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">
                    Danh sách hợp đồng trong gói
                </h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                    @include('administrator.errors.errors-validate')
                    @include('administrator.errors.messages')
                </div>
            </div>
            <!-- END: Subheader -->
            <section class="main-content-add">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-xs-12">
                        <a href="{{ route('package.index') }}" class="btn btn-add-color-cat">Danh sách gói thầu</a>
                    </div>
                    <div class="col-lg-2 col-md-2 col-xs-12">
                        <a href="{{ route('package.add-contract').'/'.$id }}" class="btn btn-add-color">
                            Thêm hợp đồng vào gói thầu
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-2 col-xs-12">
                        @if(\Auth::user()->can('package.add-lesson-to-subject'))
                        <a href="" id="add_new_lessson" class="btn btn-add-color">Thêm hợp đồng</a>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <h3 class="title-h3">
                            Gói thầu: <span>{{@$data_packages->name}}</span>
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <h3 class="title-h3">Danh sách hợp đồng</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <table class="table table-hover table-responsive table-striped">
                            <thead class="table_head">
                            <tr>
                                <th scope="col">STT</th>
                                <th scope="col">Tên</th>
                                <th scope="col">Trạng thái</th>
                                <th scope="col">Ngày tạo</th>
                                <th scope="col">Tùy chọn</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $value)
                                <tr class="header">
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ @$value->name }}</td>
                                    <td>{{ @$value->status }}</td>
                                    <td>{{ @$value->created_at }}</td>
                                    <td>
                                        {{ Form::open(array(
                                        'method'=>'POST',
                                         'route' => array('package.delete-contract'),
                                          'style' => 'display: inline-block;')) }}
                                        <button class="btn-delete"
                                                onclick="return confirm('Bạn có chắc chắn muốn xóa nhà thầu khỏi gói thầu');"
                                                data-toggle="tooltip" title="Xóa thông tin gói thầu">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                        <input type="hidden" name="contract_id" value="{{ $value->id }}">
                                        <input type="hidden" name="package_id" value="{{ $id }}">
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                            {{ $data->links() }}
                            Tổng số {{ $data->total() }} bản ghi
                    </div>
                    <!--end::Form-->
                </div>
            </section>
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
