@extends('administrator.app')
@section('title','Danh sách nhà thầu')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">Thêm mới nhà thầu</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                    @include('administrator.errors.errors-validate')
                </div>
            </div>
            {{ Form::open(array('route' => 'partner.store', 'method' => 'POST' ,
            'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
            <section class="main-content-add">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <h3 class="sub_title">Thông tin cơ bản</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-xs-12">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-xs-12">
                                <div class="form-group">
                                    <label>Tên công ty: <span class="required">(*)</span></label>
                                    <input type="text" class="form-control" id="name"
                                           placeholder="Tên công ty" name="name" value="{{ old('name') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Hotline:</label>
                                    <input type="text" class="form-control" id="hot_line"
                                           placeholder="Hotline" name="hot_line" value="{{ old('hot_line') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-xs-12">
                                <div class="form-group">
                                    <label>Địa chỉ:</label>
                                    <input type="text" class="form-control" id="address"
                                           placeholder="Địa chỉ" name="address" value="{{ old('address') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Chức vụ:</label>
                                    <input type="text" class="form-control" id="position"
                                           placeholder="Chức vụ" name="position" value="{{ old('position') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Số điện thoại:</label>
                                    <input type="text" class="form-control" id="phone"
                                           placeholder="Điện thoại" name="phone" value="{{ old('phone') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="text" class="form-control" id="email"
                                           placeholder="Email" name="email" value="{{ old('email') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Website: </label>
                                    <input type="text" class="form-control" id="website"
                                           placeholder="Website" name="website" value="{{ old('website') }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Mã số thuế:</label>
                                    <input type="text" class="form-control" id="tax_code"
                                           placeholder="Mã số thuế" name="tax_code" value="{{ old('tax_code') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Người đại diện:</label>
                                    <input type="text" class="form-control" id="delegate" placeholder="Người đại diện" name="delegate" value="{{ old('delegate') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Tỉnh thành phố:</label>
                                    {{ Form::select('city_id', ['' => '---Lựa chọn---'] + $listCity,
                                    old('city_id'), ['class' => 'form-control m-input m-select2', 'id' => 'city_id']) }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Quận huyện:</label>
                                    <select name="district_id" id="district_id" class="form-control m-input m-select2">
                                        <option selected value="">
                                            Lựa chọn
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12" style="padding-top: 25px;">
                        <div class="m-dropzone dropzone m-dropzone--primary dropzone_file box_upload" id="attachment">
                            <div class="m-dropzone__msg style-file needsclick">
                            <span>
                                Upload ảnh logo
                            </span>
                            <br/>
                            <span class="m-dropzone__msg-desc" style="font-size: 10px; text-transform: capitalize;">
                                (Tối đa 1 File kích thước 5MB/file)
                            </span>
                            </div>
                        </div>
                        <div class="fallback">
                            <input type="hidden" name="logo_image" id="logo_image">
                        </div>
                    </div>
                </div>
            </section>
            <section class="contact">
                <div id="contact_clue" class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <h3 class="sub_title">Đầu mối</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <label>Đầu mối liên hệ: <span class="required">(*)</span></label>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <label>Chức vụ: <span class="required">(*)</span></label>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12">
                            <label>Email: <span class="required">(*)</span></label>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12">
                            <label>Điện thoại: <span class="required">(*)</span></label>
                        </div>
                    </div>
                    <div data-repeater-list="contact_direct">
                        <div class="row" data-repeater-item>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="clue-contact"
                                           placeholder="Đầu mối liên hệ" name="clue_contact" value="{{ old('clue_contact') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="clue-contact"
                                           placeholder="Chức vụ" name="position_contact" value="{{ old('position_contact') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email-contact"
                                           placeholder="Email" name="email_contact" value="{{ old('email_contact') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="phone-contact"
                                           placeholder="Điện thoại" name="phone_contact" value="{{ old('phone_contact') }}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12"  style="background: #b7b7b7; border-radius: 4px;">
                            <div data-repeater-create="">
                                <p class="add_contact">+ Thêm đầu mối liên hệ</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="bid_provide">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="border_provide">
                            <h3 class="sub_title left_bid">Hạng mục thầu cung cấp</h3>
                            <div class="right_bid">
                                {!!Form::select('category_id', ['' => 'Vui lòng chọn hạng mục'] + $listCategory,
                                old('category_id'), ['class' => 'form-control m-input m-select2', 'id' => 'category_id'])!!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-bottom: 15px;" id="childCategory">
                </div>
            </section>
            <section class="button">
                <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                                <button class="save_bid">
                                    Lưu
                                </button>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <a class="delete_bid" href="{{ route('partner.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {{ Form::close() }}
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            var listAttachment = [];
            $("#attachment").dropzone({
                addRemoveLinks: true,
                url: '{{ route('partner.upload-attachment') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                maxFiles: 1,
                paramName: "file",
                dictRemoveFile: "Xóa",
                dictRemoveFileConfirmation: "Bạn có chắc muốn xóa tệp này",
                acceptedFiles: "image/jpeg,image/png,image/gif",
                dictInvalidFileType: "Định dạng file không phải dạng cho phép",
                dictFileTooBig: "Dung lượng file quá lớn vượt giới hạn cho phép",

                removedfile: function (file) {
                    var fileRemove = findObjectByKey(listAttachment, 'name', file.name);

                    $.ajax({
                        type: 'DELETE',
                        url: '{{ route('partner.delete-attachment') }}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: fileRemove.id,
                            url: fileRemove.file_url,
                        },
                        sucess: function (data) {

                        }
                    });
                    var _ref;
                    listAttachment = $.grep(listAttachment, function (e) {
                        return e.id != fileRemove.id;
                    });
                    var nameArray = listAttachment.map(function (el) {
                        return el.id;
                    });
                    $('#logo_image').val(nameArray);
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                },
                init: function () {
                    this.on("success", function (file, response) {
                        listAttachment.push(response.data);
                        var nameArray = listAttachment.map(function (el) {
                            return el.id;
                        });
                        $('#logo_image').val(nameArray);
                        alert(response.message);
                    });
                    // Handle errors
                    this.on('error', function (file, response) {
                        alert('File tải lên không phù hợp');
                    });
                }
            });
        });
    </script>
@stop