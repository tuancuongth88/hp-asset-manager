@extends('administrator.app')
@section('title','Sửa thông tin nhà thầu')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">Sửa thông tin nhà thầu</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top:20px;">
                    @include('administrator.errors.errors-validate')
                </div>
            </div>
        {{ Form::open(array('route' => array('partner.update', $data->id), 'method' => 'PUT',
        'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <section class="main-content-add">
            <div class="row">
                <div class="wrap-title">
                    <h3 class="sub_title">Thông tin cơ bản</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-xs-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-xs-12">
                            <div class="form-group">
                                <label>Tên công ty: <span class="required">(*)</span></label>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="Tên công ty" value="{{ $data->name }}" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Hotline:</label>
                                <input type="text" class="form-control" id="hot_line"
                                       placeholder="Hotline" name="hot_line" value="{{ $data->hot_line }}" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-xs-12">
                            <div class="form-group">
                                <label>Địa chỉ:</label>
                                <input type="text" class="form-control" id="address"
                                       placeholder="Địa chỉ" name="address" value="{{ $data->address }}" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Chức vụ:</label>
                                <input type="text" class="form-control" id="position" name="position"
                                       placeholder="Chức vụ" name="position" value="{{ $data->position }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Số điện thoại:</label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       placeholder="Điện thoại" value="{{$data->phone}}" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Email:</label>
                                <input type="text" class="form-control" id="email" name="email"
                                       placeholder="Email" value="{{$data->email}}" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Website: </label>
                                <input type="text" class="form-control" id="website" name="website"
                                       placeholder="Website" value="{{$data->website}}" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Mã số thuế:</label>
                                <input type="text" class="form-control" id="tax_code" name="tax_code"
                                       placeholder="Mã số thuế" value="{{$data->tax_code}}" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Người đại diện:</label>
                                <input type="text" class="form-control" id="delegate" name="delegate"
                                       placeholder="Người đại diện" value="{{$data->delegate}}" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Tỉnh thành phố:</label>
                                {{ Form::select('city_id', ['' => '---Lựa chọn---'] + $listCity, $data->city_id,
                                ['class' => 'form-control m-input', 'id' => 'city_id']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Quận huyện:</label>
                                {{ Form::select('district_id', $listDistrict, $data->district_id,
                                ['class' => 'form-control m-input', 'id' => 'district_id']) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12" style="padding-top: 25px;">
                    <div class="m-dropzone dropzone m-dropzone--primary dropzone_file box_upload" id="attachment">
                        <div class="m-dropzone__msg style-file needsclick">
                        <span>
                            Upload File scan hợp đồng
                        </span>
                        </div>
                    </div>
                    <div class="fallback">
                        <input type="hidden" name="logo_image" id="logo_image" value="{{ @$data->logo }}">
                    </div>
                    <div>
                        @if(@$data->logo !== NULL)
                            <span class="m-form__help">
                                <img src="{{ $data->document->url }}" style="visibility:hidden"
                                     onload="this.style.visibility = 'visible'" width="200px" height="200px">
                            </span>
                        @else
                            <h3 class="sub_title title_contract">Chưa có file được tải lên</h3>
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <section class="contact">
            <div id="contact_clue" class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <h3 class="sub_title">Đầu mối</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <label>Đầu mối liên hệ: <span class="required">(*)</span></label>
                    </div>
                    <div class="col-lg-2 col-md-2 col-xs-12">
                        <label>Chức vụ: <span class="required">(*)</span></label>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <label>Email: <span class="required">(*)</span></label>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <label>Điện thoại: <span class="required">(*)</span></label>
                    </div>
                </div>
                <div data-repeater-list="contact_direct">
                    @for($i = 1; $i <= @$data->contact_info[$i] ; $i++)
                    <div class="row" data-repeater-item>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="clue-contact"
                                       placeholder="Đầu mối liên hệ" name="clue_contact"
                                       value="{{ @$data->contact_info[$i]['clue_contact'] }}" />
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="clue-contact"
                                       placeholder="Chức vụ" name="position_contact"
                                       value="{{ @$data->contact_info[$i]['position_contact'] }}" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email-contact"
                                       placeholder="Email" name="email_contact"
                                       value="{{ @$data->contact_info[$i]['email_contact'] }}"/>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="phone-contact"
                                       placeholder="Điện thoại" name="phone_contact"
                                       value="{{ @$data->contact_info[$i]['phone_contact'] }}"/>
                            </div>
                        </div>
                    </div>
                    @endfor
                        <div class="row"  data-repeater-item>
                            <div class="col-lg-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="clue-contact"
                                           placeholder="Đầu mối liên hệ" name="clue_contact" />
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="clue-contact"
                                           placeholder="Chức vụ" name="position_contact" />
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email-contact"
                                           placeholder="Email" name="email_contact" />
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="phone-contact"
                                           placeholder="Điện thoại" name="phone_contact" />
                                </div>
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div data-repeater-create=""  style="background: #b7b7b7; border-radius: 4px; padding: 1px;">
                            <p class="add_contact">+ Thêm đầu mối liên hệ</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="bid_provide">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="border_provide">
                            <h3 class="sub_title left_bid">Hạng mục thầu cung cấp</h3>
                            <div class="right_bid">
                                {!!Form::select('category_id', ['' => 'Vui lòng chọn hạng mục'] + $listCategory,
                                $data->category, ['class' => 'form-control m-input m-select2', 'id' => 'category_id'])!!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-bottom: 15px;" id="childCategory">
                </div>
            </div>
        </section>
        <section class="contract">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <h3 class="sub_title title_contract">Hợp đồng đã cung cấp</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <table class="table table-hover table-mobile">
                            <thead>
                            <tr>
                                <th scope="col">STT</th>
                                <th scope="col">Số hiệu hợp đồng</th>
                                <th scope="col">Tên hợp đồng</th>
                                <th scope="col">Ngày ký</th>
                                <th scope="col">Giá trị hợp đồng</th>
                                <th scope="col">File scan hợp đồng</th>
                                <th scope="col">Tùy chọn</th>
                            </tr>
                            </thead>
                            <tbody class="new_contract">
                            @foreach ($listContract as $key => $value)
                                <tr>
                                    <td scope="row">{{ $key + 1 }}</td>
                                    <td>{{ @$value->contract_code }}</td>
                                    <td>{{ @$value->name }}</td>
                                    <td>{{ date('d-m-Y', strtotime(@$value->sign_date)) }}</td>
                                    <td>{{ @$value->price_vat }} VND</td>
                                    <td>
                                        @if(@$value->file_id !== NULL)
                                            {{ Form::open(array('method'=>'POST', 'route' => array('contract_stores.download', $value->file_id), 'style' => 'display: inline-block;')) }}
                                            {{--<button class="btn-delete">--}}
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            {{--</button>--}}
                                            <span class="text_contract">{{ @$value->document->name }}</span>
                                            {{ Form::close() }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ Form::open(array('method'=>'DELETE',
                                        'route' => array('contract.destroy', $value['id']),
                                        'style' => 'display: inline-block;')) }}
                                        <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                                class="btn-delete">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12" style="background: #b7b7b7; border-radius: 4px;">
                        <p class="add_contract">
                            <a href="{{ route('contract.partner-contract', $data->id) }}" style="color: #000">
                                + ADD Thêm hợp đồng vào nhà cung cấp này</a>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="button">
            <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                <div class="m-form__actions">
                    <div class="container">
                        <div class="row">
                            <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                                <button class="save_bid">
                                    Lưu
                                </button>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <a class="delete_bid" href="{{ route('partner.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{ Form::close() }}
    </div>
    </div>
    <script>
        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            var listAttachment = [];
            $("#attachment").dropzone({
                addRemoveLinks: true,
                url: '{{ route('partner.upload-attachment') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                paramName: "file",
                dictRemoveFile: "Xóa",
                dictRemoveFileConfirmation: "Bạn có chắc muốn xóa tệp này",
                acceptedFiles: "image/jpeg,image/png,image/gif",
                dictInvalidFileType: "Định dạng file không phải dạng cho phép",
                dictFileTooBig: "Dung lượng file quá lớn vượt giới hạn cho phép",
                maxFilesize: 10,

                removedfile: function (file) {
                    var fileRemove = findObjectByKey(listAttachment, 'name', file.name);

                    $.ajax({
                        type: 'DELETE',
                        url: '{{ route('partner.delete-attachment') }}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: fileRemove.id,
                            url: fileRemove.file_url,
                        },
                        sucess: function (data) {

                        }
                    });
                    var _ref;
                    listAttachment = $.grep(listAttachment, function (e) {
                        return e.id != fileRemove.id;
                    });
                    var nameArray = listAttachment.map(function (el) {
                        return el.id;
                    });
                    $('#logo_image').val(nameArray);
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                },
                init: function () {
                    this.on("success", function (file, response) {
                        listAttachment.push(response.data);
                        var nameArray = listAttachment.map(function (el) {
                            return el.id;
                        });
                        $('#logo_image').val(nameArray);
                        alert(response.message);
                    });
                    // Handle errors
                    this.on('error', function (file, response) {
                        alert('File tải lên không phù hợp');
                    });
                }
            });
        });
    </script>
@stop