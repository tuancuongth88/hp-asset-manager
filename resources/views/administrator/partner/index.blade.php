@extends('administrator.app')
@section('title','Danh sách nhà thầu')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">
                    <a href="{{ route('partner.index') }}">Danh sách nhà thầu</a>
                </h2>
            </div>
            @include('administrator.partner.search')
            <section class="main-content">
                <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            @include('administrator.errors.messages')
                            <table class="table table-hover table-responsive table-striped">
                                <thead class="table_head">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">STT</th>
                                    <th scope="col">Nhà thầu</th>
                                    <th scope="col">Hạng mục</th>
                                    <th scope="col">Địa chỉ</th>
                                    <th scope="col">Điện thoại</th>
                                    <th scope="col">MST</th>
                                    <th scope="col">Ủy quyền</th>
                                    <th scope="col">Hotline</th>
                                    <th scope="col">Tùy chọn</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $value)
                                    <tr class="header expand">
                                        <td scope="row"><span class="sign"></span></td>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ @$value->category[0]->name }}</td>
                                        <td>{{ $value->address }}</td>
                                        <td>{{ $value->phone }}</td>
                                        <td>{{ $value->tax_code }}</td>
                                        <td>{{ $value->delegate }}</td>
                                        <td>{{ $value->hot_line }}</td>
                                        <td>
                                            <a href="{{ route('partner.edit', $value->id) }}" class="" data-toggle="tooltip"
                                               title="Sửa thông tin nhà thầu">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </a>
                                            <a href="{{ route('contract.partner-contract', $value->id) }}" class=""
                                               data-toggle="tooltip" title="Thêm hợp đồng">
                                                <i class="fa fa-book" aria-hidden="true"></i>
                                            </a>
                                            <a href="{{ route('partner.provide-category', $value->id) }}" class=""
                                               data-toggle="tooltip" title="Thêm hạng mục thầu">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </a>
                                            {{ Form::open(array('method'=>'DELETE', 'route' => array('partner.destroy',
                                            $value->id), 'style' => 'display: inline-block;')) }}
                                            <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                                    class="btn-delete" data-toggle="tooltip" title="Xóa nhà thầu">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    <tr class="sub-table">
                                        <td colspan="12">
                                            <table class="child-table table-hover table-mobile">
                                                <thead>
                                                <tr>
                                                    <th scope="col">STT</th>
                                                    <th scope="col">Số hiệu hợp đồng</th>
                                                    <th scope="col">Tên hợp đồng</th>
                                                    <th scope="col">Ngày ký</th>
                                                    <th scope="col">Giá trị hợp đồng</th>
                                                    <th scope="col">File scan hợp đồng</th>
                                                </tr>
                                                </thead>
                                                <tbody class="new_contract">
                                                @foreach ($listContract as $key => $value)
                                                    <tr>
                                                        <td scope="row">{{ $key + 1 }}</td>
                                                        <td>{{ @$value->contract_code }}</td>
                                                        <td>{{ @$value->name }}</td>
                                                        <td>{{ date('d-m-Y', strtotime(@$value->sign_date)) }}</td>
                                                        <td>{{ @$value->total_money }} VND</td>
                                                        <td>
                                                            @if(@$value->file_id !== NULL)
                                                                {{ Form::open(array('method'=>'POST', 'route' => array('contract_stores.download', $value->file_id), 'style' => 'display: inline-block;')) }}
                                                                {{--<button class="btn-delete">--}}
                                                                    <i class="fa fa-download" aria-hidden="true"></i>
                                                                {{--</button>--}}
                                                                <span class="text_contract">{{ @$value->document->name }}</span>
                                                                {{ Form::close() }}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="wrap-title">
                            {{ $data->links() }}
                            Tổng số {{ $data->total() }} bản ghi
                        </div>
                    </div>
            </section>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            function getDistrictByCity(id) {

                $.ajax({
                    url: '/administrator/asset/district/' + id,
                    type: 'GET',
                    contentType: "application/json",
                    success: function (result) {
                        $('select[name="district_id"]').empty();
                        $.each(result.data, function (key, value) {
                            $('select[name="district_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                })
            }

            $('#city_id').change(function (e) {
                var id = $(this).val();
                if (id == "") {
                    $('select[name="district_id"]').empty();
                    $('select[name="district_id"]').append('<option value="">---Lựa chọn---</option>');
                } else {
                    getDistrictByCity(id);
                }
            });
            $("#city_id").select2();
            $("#district_id").select2();
            $("#category_id").select2();

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });
        });
    </script>
@stop