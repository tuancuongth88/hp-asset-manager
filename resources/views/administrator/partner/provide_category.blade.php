@extends('administrator.app')
@section('title','Hạng mục nhà thầu')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12" style="margin-bottom: 20px">
                        <a href="{{ route('partner.index') }}" class="btn m-btn m-btn--icon btn-add-color-group">
                            Trở về trang chính
                        </a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12" style="margin-bottom: 20px">
                        @include('administrator.errors.errors-validate')
                        <!--begin::Portlet-->
                            <h2 class="title_list">Hạng mục nhà thầu</h2>
                        <!--end::Portlet-->
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        {{ Form::open(array('route' => array('partner.update-provide-category',$partner->id), 'class' => 'm-form m-form--fit m-form--label-align-right', 'files'=>true)) }}
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <div class="m-portlet__body row">
                                <div style="font-weight: bold;">
                                    Thông tin nhà thầu
                                </div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group m-form__group">
                                        <label for="name" style="font-weight: bold;">
                                            Nhà thầu:
                                        </label>
                                        <span>{{@$partner->name}}</span>
                                        <input type="hidden" value="{{@$partner->id}}">
                                    </div>
                                </div>
                                <div style="font-weight: bold;">
                                    Hạng mục nhà thầu cung cấp
                                </div>
                                @foreach($categories as $key=>$value)
                                    <div class="col-lg-12">
                                        <div class="form-group m-form__group">
                                            <label for="name">
                                                {{$value->name}}
                                            </label>
                                            <div class="row">
                                                @foreach($value->categoryChild as $key_ch =>$val_child)
                                                    <div class="col-md-3">
                                                        <input type="checkbox" value="{{$val_child->id}}"
                                                               name="provide_category[]"
                                                                {{(in_array($val_child->id,$list_category)) ? 'checked':'' }}
                                                        >
                                                        {{$val_child->name}}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <section class="button">
                                <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                                    <div class="m-form__actions">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-xs-12"></div>
                                                <div class="col-lg-3 col-md-3 col-xs-12">
                                                    <button  type="submit"  class="save_bid">
                                                        Lưu
                                                    </button>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-xs-12">
                                                    <a class="delete_bid" href="{{ route('partner.index') }}">
                                                        Trở về danh sách
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        {{ Form::close() }}
                    </div>
                </div>
        </div>
    </div>
@stop