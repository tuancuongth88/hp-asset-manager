{{ Form::open(array('route' => 'partner.index', 'method' => 'GET', 'id' =>'search_form_partner')  ) }}
<section class="search-content">
    <div class="row">
            <div class="col-md-2 col-sm-2 mb-3 mb-md-0">
                <input type="text" name="name" id="name" class="form-control"
                       placeholder="Tên nhà thầu"/>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-12">
                <div class="form-group">
                    {{ Form::select('category_id', ['' => 'Hạng mục'] + $listCategory, @$input['category'], ['class' => 'form-control m-input m-select2', 'id' => 'category_id']) }}
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 mb-3 mb-md-0">
                <div class="form-group">
                    {{ Form::select('city_id', [0 => 'Chọn tỉnh/thành'] + $listCity, null, ['class' => 'form-control m-input m-select2', 'id' => 'city_id']) }}
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 mb-3 mb-md-0">
                <div class="form-group">
                    {{ Form::select('district_id', ['' => 'Quận huyện'],  @$input['district_id'], ['class' => 'form-control m-input m-select2', 'id' => 'district_id']) }}
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-xs-12">
                <div class="form-group">
                    <input type="submit" class="form-control" id="search-button" value="Tìm kiếm"
                           style="background: #f6811f;color: #fff;"/>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
                {{--<a href="{{ route('partner.index') }}" class="form-control" id="search-button" value="Hủy tìm kiếm"--}}
                   {{--style="background: #f63d1f;color: #fff; text-align: center;">Hủy tìm</a>--}}
                <div class="dropdown">
                    <button class="dropdown-toggle_button btn-add-color btn m-btn m-btn--icon" type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Tùy chọn thêm mới
                    </button>
                    <div class="dropdown-menu drowdown-addnew" aria-labelledby="dropdownMenuButton">
                        <a href="{{ route('partner.create') }}" class="dropdown-item">
                            Thêm mới nhà thầu
                        </a>
                        <a href="{{ route('category.create') }}" class="dropdown-item">
                            Thêm hạng mục thầu
                        </a>
                    </div>
                </div>
            </div>
        </div>
</section>
{{ Form::close() }}