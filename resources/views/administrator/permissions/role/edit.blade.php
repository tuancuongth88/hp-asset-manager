@extends('administrator.app')
@section('title','Vai trò')

@section('content')
    <style>
        .sub_title_list {
            display: table-cell;
            vertical-align: middle;
            font-size: 20px;
            font-weight: 600;
        }
        .save_role {
            background-color: #f6811f;
            border: none;
            color: white;
            padding: 10px 25px;
        }
        .delete_role {
            background-color: #ff0000;
            padding: 10px 25px;
            border: none;
            color: white;
        }
    </style>
    <div class="list-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <h2 class="title_list">Vai trò</h2>
                </div>
            </div>
        </div>
        <section class="main-content-add">
            <div class="container" style="background:#fff; padding: 15px;">
                {{ Form::open(array('route' => 'role.store', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed')) }}
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12" style="margin-bottom: 20px">
                        <a href="{{ route('role.index') }}" class="btn btn-success">Danh sách</a>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <h3 class="sub_title_list">Thêm mới</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <label>Vai trò:</label>
                        <div class="input-group m-input-group m-input-group--square">
                            <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-slack"></i>
                                    </span>
                            </div>
                            <input type="text" class="form-control m-input" placeholder="Nhập vai trò" name="name" value="{{ $data->name }}">
                        </div>
                        <span class="m-form__help">
                                Hệ thống định danh theo vai trò<br>
                                Tên này là duy nhất, không được trùng trong hệ thống<br>
                                Ví dụ: admin, quanly, bientap...
                            </span>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <label class="">Tên vai trò:</label>
                        <div class="input-group m-input-group m-input-group--square">
                            <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-eye"></i>
                                    </span>
                            </div>
                            <input type="text" class="form-control m-input" placeholder="Nhập tên vai trò" name="display_name" value="{{ $data->display_name }}">
                        </div>
                        <span class="m-form__help">
                                Tên hiển thị<br>
                                Ví dụ: Quản trị viên, Biên tập viên, Thành viên...
                            </span>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <label>Mô tả:</label>
                        <div class="input-group m-input-group m-input-group--square">
                            <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="la la-info"></i>
                                    </span>
                            </div>
                            <input type="text" class="form-control m-input" placeholder="" name="description" value="{{ $data->description }}">
                        </div>
                        <span class="m-form__help">
                                Mô tả về quyền
                            </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-xs-12"></div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary save_role">
                                    <i class="fa fa-save"></i>
                                    Lưu
                                </button>
                                <a class="btn btn-secondary delete_role" href="{{ route('role.index') }}">
                                    <i class="la la-arrow-circle-o-left"></i>
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </section>
    </div>
@stop