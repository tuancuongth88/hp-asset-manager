@extends('administrator.app')
@section('title','Vai trò')

@section('content')
<div class="list-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <h2 class="title_list">Vai trò</h2>
            </div>
        </div>
    </div>
    <section class="main-content-add">
        <div class="container" style="background:#fff; padding: 15px;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    {{ Form::open(array('enctype' => 'multipart/form-data', 'method'=>'POST', 'route' => 'role.import', 'id' => 'import-form')) }}
                        <a href="{{ route('role.create') }}" class="btn btn-primary m-btn m-btn--icon">
                            <span>
                                <i class="fa flaticon-plus"></i>
                                <span>
                                    Thêm mới
                                </span>
                            </span>
                        </a>
                        <a href="{{ route('role.config-user') }}" class="btn btn-success m-btn m-btn--icon">
                            <span>
                                <i class="fa flaticon-user-ok"></i>
                                <span>
                                    User
                                </span>
                            </span>
                        </a>
                        <a href="{{ route('role.config-role') }}" class="btn btn-info m-btn m-btn--icon">
                            <span>
                                <i class="fa flaticon-puzzle"></i>
                                <span>
                                    Quyền
                                </span>
                            </span>
                        </a>
                        {{--<div class="custom-file col-md-3">--}}
                            {{--<input type="file" class="custom-file-input" id="customFile" name="import_file">--}}
                            {{--<label class="custom-file-label" for="customFile">--}}
                                {{--Chọn file import--}}
                            {{--</label>--}}
                        {{--</div>--}}
                        {{--<button type="submit" class="btn btn-success" id="import_file"><i class="fa fa-file-excel-o"></i> Import</button>--}}
                    {{ Form::close() }}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <a href="{{ url('/role.xlsx') }}" style="font-weight: bold; font-size: 17px;color: #000;">Mẫu import File</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    @include('administrator.errors.info-search')
                    <div class="m-section__content">
                        <table class="table m-table m-table--head-bg-warning">
                            <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Tên
                                    </th>
                                    <th>
                                        Tùy chọn
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $value)
                                <tr>
                                    <th scope="row" style="text-align: center;">
                                        {{ $key + 1 }}
                                    </th>
                                    <td style="text-align: center;">
                                        {{ $value['display_name'] }}
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="{{ route('role.edit', $value->id) }}" class="">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {{ Form::open(array('method'=>'DELETE', 'route' => array('role.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                        <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="btn-delete">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop