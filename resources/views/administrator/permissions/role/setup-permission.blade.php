@extends('administrator.app')
@section('title','Vai trò')

@section('content')
<div class="list-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <h2 class="title_list">Vai trò</h2>
            </div>
        </div>
    </div>
    {{ Form::open(array('route' => array('role.put-permission'),'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
    <section class="main-content-add">
        <div class="container" style="background:#fff; padding: 15px;">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    @include('administrator.errors.errors-validate')
                    <h3 class="sub_title_list">Thêm mới</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="form-group m-form__group">
                        {{ Form::select('role_id', [0 => 'Chọn vai trò'] + $listRole, null, ['class' => 'form-control m-select2', 'id' => 'role_id']) }}
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-xs-12">
                    <div class="form-group m-form__group">
                        <label class="m-checkbox m-checkbox--state-success">
                            <input type="checkbox" onclick="toggle(this);" id="select-all"/>
                            Chọn tất cả
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-xs-12">
                    <button class="btn save_role">
                        Lưu
                    </button>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <a href="{{ route('role.index') }}" class="btn delete_role">
                        Trở về danh sách
                    </a>
                </div>
                <div class="col-md-12 m--margin-top-30">
                    <div class="m_datatable" id="ajax_data"></div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                </div>
            </div>
        </div>
    </section>
    {{ Form::close() }}
</div>
<script>
    $(document).ready(function () {
        $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: '/administrator/system/permission-by-role',
                    },
                },
                pageSize: 10,
                saveState: false,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // column sorting
            sortable: false,

            pagination: false,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            // search: {
            //     input: $('#generalSearch'),
            // },
            // columns definition
            columns: [
            {
                field: "id",
                title: "#",
                width: 50,
                sortable: false,
                textAlign: 'center',
                // selector: {class: 'm-checkbox--solid m-checkbox--brand', test: 'ok'},
                template: function(row) {
                    if (row.checked) {
                        return '<input type="checkbox" class="checkbox-permission" checked name="permission[' + row.id + ']">';
                    }
                    return '<input type="checkbox" class="checkbox-permission" name="permission[' + row.id + ']">';
                },
            }
            ,{
                field: 'display_name',
                title: 'Quyền',
            },{
                field: 'description',
                title: 'Mô tả',
            }],
        });
        $('body').on('change', '#role_id', function() {
            $('#select-all').prop('checked', false);
            var id = $(this).val();
            $( '#list-permission').empty();
            $.ajax({
                url: '/administrator/system/permission-by-role/' + id,
                type : "get",
                success: function (result){
                    $('#list-permission').append(result);
                }
            });
            $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'role_id');
        });
    });

    function toggle(source) {
        var checkboxes = document.querySelectorAll('input[type="checkbox"]');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i] != source)
                checkboxes[i].checked = source.checked;
        }
    }
</script>
@stop