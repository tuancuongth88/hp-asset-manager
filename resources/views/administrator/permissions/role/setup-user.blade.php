@extends('administrator.app')
@section('title','Vai trò')

@section('content')
<div class="list-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <h2 class="title_list">Vai trò</h2>
            </div>
        </div>
    </div>
    @include('administrator.errors.errors-validate')
    <section class="main-content-add">
        <div class="container" style="background:#fff; padding: 15px;">
            {{ Form::open(array('route' => array('role.put-role'),'method' => 'PUT',  'class' => 'm-form m-form--fit m-form--label-align-right')) }}
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-6">
                    <label class="col-form-label">
                        Tài khoản
                    </label>
                    <div class="">
                        <select class="form-control m-select2" name="user_id" id="user_id">
                            {{-- <select class="form-control m-bootstrap-select m_selectpicker" name="user_id" id="user_id" data-live-search="true"> --}}
                            <option>Chọn tài khoản</option>
                            @foreach ($listUser as $user)
                                <option value="{{ $user->id }}" data-role-id="{{ ($user->roles()->first()) ? $user->roles()->first()->id : '' }}">
                                    {{ $user->email }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                    <label class="col-form-label">
                        Vai trò
                    </label>
                    <div class="">
                        <select class="form-control m-bootstrap-select m_selectpicker" name="role_id" id="role_id">
                            <option>Chọn vai trò</option>
                            @foreach ($listRole as $role)
                                <option value="{{ $role->id }}">
                                    {{ $role->display_name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12"></div>
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="m-portlet__foot m-portlet__foot--fit" style="padding: 20px;">
                        <div class="m-form__actions">
                            <button class="btn btn-primary save_role">
                                Lưu
                            </button>
                            <a class="btn delete_role" href="{{ route('role.index') }}">
                                Trở về danh sách
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function (argument) {
        $('.m-select2').select2({
            placeholder: "Select a value",
        });
    });
</script>
@stop
