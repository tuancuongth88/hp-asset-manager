<html style="font-family: 'Times New Roman', sans-serif">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<h1 style="text-align: center">BÁO CÁO THỐNG KÊ TÀI SẢN</h1>
<p>&nbsp;</p>
<table style="font-family: 'Times New Roman', sans-serif">
    <?php
    use App\Models\Assets\Asset;
    use Illuminate\Support\Facades\Log;
    $statusDefault = Asset::$lstStatusExportDefault;
    $queryGet = clone $data;
    $query_use = clone $data;
    $query_total_all = clone $data;
    $query_repair = clone $data;
    $query_warranty = clone $data;
    $query_breakdown = clone $data;
    $query_store = clone $data;
    $query_never_used = clone $data;
    //    $total_all = $query_total_all->get()->sum('asset_count');
    $total_use = $query_use->where('status', \App\Models\Assets\Asset::STATUS_USE)->get()->sum('asset_count');
    $total_repair = $query_repair->where('status', \App\Models\Assets\Asset::STATUS_REPAIR)->get()->sum('asset_count');
    $total_warranty = $query_warranty->where('status', \App\Models\Assets\Asset::STATUS_WARRANTY)->get()->sum('asset_count');
    $total_breakdown = $query_breakdown->where('status', \App\Models\Assets\Asset::STATUS_BREAKDOWN)->get()->sum('asset_count');
    $total_store = $query_store->where('status', \App\Models\Assets\Asset::STATUS_STORE)->get()->sum('asset_count');
    $total_never_used = $query_never_used->where('status', \App\Models\Assets\Asset::STATUS_NEVER_BEEN_USED)->get()->sum('asset_count');
    ?>
    <tbody>
    <tr>
        <td>
            <p><span style="font-weight: 400;">Ngày tạo: {{\Carbon\Carbon::now()->format('d-m-Y')}}</span></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">Người tạo: {{@\Illuminate\Support\Facades\Auth::user()->fullname}}</span>
            </p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">Đang sử dụng:
                    {{$total_use}}
                </span>
            </p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">Sửa chữa:
                    {{$total_repair}}
                </span></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">Bảo hành:
                    {{$total_warranty}}
                </span></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">Hỏng hóc:
                    {{$total_breakdown}}
                </span></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">Tồn kho:
                    {{$total_store}}
                </span></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">Chưa sử dụng:
                    {{$total_never_used}}
                </span></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th scope="col">STT</th>
        <th scope="col">Tên</th>
        <th scope="col">Mã nhóm tài sản</th>
        <th scope="col">Chất liệu</th>
        <th scope="col">Màu sắc</th>
        <th scope="col">Số lượng</th>
        <th scope="col">Đơn vị tính</th>
        <th scope="col">Đơn giá(VND)</th>
        <th scope="col">Thành tiền(VND)</th>
        <th scope="col">Đơn vị sản xuất/cung cấp</th>
        <th scope="col">Năm đưa vào SD</th>
        <th scope="col">Tình trạng SD</th>
        <th scope="col">Ghi chú</th>
    </tr>
    </thead>
    <tbody>
    @foreach($listNameStructure as $key_s => $value_s)
        <tr class="header expand">
            <td scope="row" style="font-weight: bold">
                <span style="font-weight: 400;">{{ $value_s }}</span>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php
        $i = 0;

        $data_clone_status_default = clone $data;
        $checkStatusUse = false;
        if (!empty($status) && is_array($status)) {
            if (sizeof($status) == 1 && in_array(Asset::STATUS_USE, $status)) {
                $checkStatusUse = true;
            }
        } else {
            $checkStatusUse = true;
        }

        if (!$checkStatusUse) {
            $data_all = $data_clone_status_default->where('structure_owner', $key_s)->get();
        } else {
            $data_all = $data_clone_status_default->where('structure_owner', $key_s)
                ->where('status', Asset::STATUS_USE)
                ->get();
        }
        ?>
        @if($data_all->count() > 0)
            @foreach($data_all as $key => $value)
                <?php
                $i = $i + 1;
                $unit_lower = mb_strtolower(trim_all(data_get($value, 'unit.name')), 'UTF-8');
                $quantity = 0;
                if (in_array($unit_lower, UNIT_MEASUREMENT)) {
                    $quantity = $value->sub_quantity;
                } else {
                    $quantity = number_format($value->sub_quantity);
                }
                ?>
                <tr class="header expand">
                    <td scope="row">{{ $i }}</td>
                    <td>{{ @$value->name }}</td>
                    <td>{{ @$value->group->group_code }}</td>
                    <td>{{ @$value->material }}</td>
                    <td>{{ @$value->color }}</td>
                    <td>{{ @$quantity }} </td>
                    <td>{{ @$value->unit->name }}</td>
                    <td class="text-right">{{ $value->price }}</td>
                    <td class="text-right">{{ $quantity*$value->price }}</td>
                    <td>{{ @$value->producer }}</td>
                    <td>{{ @$value->year_use }}</td>
                    <td>
                        {{
                            (isset($value->status) && $value->status) ?
                            \App\Models\Assets\Asset::$assetListStatus[$value->status]:''
                        }}
                    </td>
                    <td>
                    </td>
                </tr>
            @endforeach
        @endif
    @endforeach
    </tbody>
</table>
</html>
