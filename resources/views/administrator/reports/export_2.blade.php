<?php
use App\Models\Assets\Asset;
$statusDefault = Asset::$lstStatusExportDefault;
?>
<html style="font-family: 'Times New Roman', sans-serif">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<h3>Công ty CP ĐT và KD BĐS Hải Phát</h3>
<table style="font-family: 'Times New Roman', sans-serif">
    <tbody>
    <tr>
        <td>
            <p><span>Tầng 2 CT4, Tổ hợp TMDV&CH The Pride, KĐT mới AN Hưng, Hà Đông, Hà Nội</span></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span>Mã số thuế: 0108567242</span>
            </p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span>Thời gian: {{\Carbon\Carbon::now()->format('d-m-Y')}}</span>
            </p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span>Phòng ban:
                    {{ @$listNameStructure[$structure_owner] }}
                </span>
            </p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span>Đơn vị kiểm kê: HCNS vs TCKT</span>
            </p>
        </td>
        <td>&nbsp;</td>
    </tr>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th scope="col">STT</th>
        <th scope="col">Tên</th>
        <th scope="col">Mã nhóm tài sản</th>
        <th scope="col">Chất liệu</th>
        <th scope="col">Màu sắc</th>
        <th scope="col">Số lượng</th>
        <th scope="col">Đơn vị tính</th>
        <th scope="col">Đơn giá(VND)</th>
        <th scope="col">Thành tiền(VND)</th>
        <th scope="col">Đơn vị sản xuất/cung cấp</th>
        <th scope="col">Năm đưa vào SD</th>
        <th scope="col">Tình trạng SD</th>
        <th scope="col">Ghi chú</th>
    </tr>
    </thead>
    <tbody>
    @foreach($listNameStructure as $key_s => $value_s)
        <tr class="header expand">
            <td scope="row">{{ $value_s }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php
        $i = 0;
        $data_clone_status_default = clone $data;
        $checkStatusUse = false;
        if (!empty($status) && is_array($status)) {
            if (sizeof($status) == 1 && in_array(Asset::STATUS_USE, $status)) {
                $checkStatusUse = true;
            }
        } else {
            $checkStatusUse = true;
        }

        if (!$checkStatusUse) {
            $data_all = $data_clone_status_default->where('structure_owner', $key_s)->get();
        } else {
            $data_all = $data_clone_status_default->where('structure_owner', $key_s)->whereIn('status', $statusDefault)->get();
        }
        ?>
        @foreach($data_all as $key => $value)
            {{$i= $i+1}}
            <?php
            $unit_lower = mb_strtolower(trim_all(data_get($value, 'unit.name')), 'UTF-8');
            $quantity = 0;
            if (in_array($unit_lower, UNIT_MEASUREMENT)) {
                $quantity = $value->sub_quantity;
            } else {
                $quantity = number_format($value->sub_quantity);
            }
            ?>
            <tr class="header expand">
                <td scope="row">{{ $i }}</td>
                <td>{{ @$value->name }}</td>
                <td>{{ @$value->group->group_code }}</td>
                <td>{{ @$value->material }}</td>
                <td>{{ @$value->color }}</td>
                <td>{{ @$quantity }} </td>
                <td>{{ @$value->unit->name }}</td>
                <td class="text-right">{{ $value->price }}</td>
                <td class="text-right">{{ $quantity*$value->price }}</td>
                <td>{{ @$value->producer }}</td>
                <td>{{ @$value->year_use }}</td>
                <td>
                    {{
                        (isset($value->status) && $value->status) ?\App\Models\Assets\Asset::$assetListStatus[$value->status]:''
                    }}
                </td>
                <td>
                    @if(!$checkStatusUse)
                        {{ @$value->description }}
                    @else
                        <?php
                        $clone_status_gift = clone $data;
                        $clone_status_liquidation = clone $data;
                        $clone_status_breakdown = clone $data;
                        $clone_status_lost = clone $data;

                        $data_status_gift = $clone_status_gift->where('structure_owner', $key_s)
                            ->where('status', Asset::STATUS_GIFT)
                            ->where('group_id', @$value->group->id)
                            ->get()->sum('sub_quantity');
                        $data_status_liquidation = $clone_status_liquidation->where('structure_owner', $key_s)
                            ->where('status', Asset::STATUS_LIQUIDATION)
                            ->where('group_id', @$value->group->id)
                            ->get()->sum('sub_quantity');
                        $data_status_breakdown = $clone_status_breakdown->where('structure_owner', $key_s)
                            ->where('status', Asset::STATUS_BREAKDOWN)
                            ->where('group_id', @$value->group->id)
                            ->get()->sum('sub_quantity');
                        $data_status_lost = $clone_status_lost->where('structure_owner', $key_s)
                            ->where('status', Asset::STATUS_LOST)
                            ->where('group_id', @$value->group->id)
                            ->get()->sum('sub_quantity');

                        $list_st_more = [];

                        if ($data_status_gift > 0) {
                            $list_st_more[] = (string)$data_status_gift . ' ' . Asset::$assetListStatus[Asset::STATUS_GIFT];
                        }
                        if ($data_status_liquidation > 0) {
                            $list_st_more[] = (string)$data_status_liquidation . ' ' . Asset::$assetListStatus[Asset::STATUS_LIQUIDATION];
                        }
                        if ($data_status_breakdown > 0) {
                            $list_st_more[] = (string)$data_status_breakdown . ' ' . Asset::$assetListStatus[Asset::STATUS_BREAKDOWN];
                        }
                        if ($data_status_lost > 0) {
                            $list_st_more[] = (string)$data_status_lost . ' ' . Asset::$assetListStatus[Asset::STATUS_LOST];
                        }

                        ?>
                        {{implode(',',$list_st_more)}}
                    @endif
                </td>
            </tr>
            <?php unset($data_all[$key]);?>
        @endforeach
    @endforeach
    </tbody>
</table>
</html>
