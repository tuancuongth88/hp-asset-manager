@extends('administrator.app')
@section('title','Thống kê tài sản')

@section('content')
    <div class="list-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    @include('administrator.errors.messages')
                    <h2 class="title_list"><a href="{{ route('reports.index') }}">Thống kê tài sản</a></h2>
                </div>
            </div>
            @include('administrator.reports.search-index')
        </div>
        <section class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <table class="table table-responsive table-striped">
                            <thead class="">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tên</th>
                                <th scope="col">Mã nhóm TS</th>
                                <th scope="col">Phân loại</th>
                                <th scope="col">Số lượng</th>
                                <th scope="col">Đơn vị tính</th>
                                <th scope="col">Đơn vị SD</th>
                                <th scope="col">Người nhận bàn giao</th>
                                <th scope="col">Đơn giá(VND)</th>
                                <th scope="col">Thành tiền(VND)</th>
                                <th scope="col">Bảo hành(tháng)</th>
                                <th scope="col">Năm đưa vào SD</th>
                                <th scope="col">Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $queryGet = clone $data;
                            $queryPaginate = clone $data;
                            $queryCount = clone $data;
                            ?>
                            @foreach($queryPaginate->paginate(20) as $key => $value)
                                <?php
                                $unit_lower = mb_strtolower(trim_all(data_get($value, 'unit.name')), 'UTF-8');
                                $quantity = 0;
                                if (in_array($unit_lower, UNIT_MEASUREMENT)) {
                                    $quantity = $value->sub_quantity;
                                } else {
                                    $quantity = number_format($value->sub_quantity);
                                }
                                ?>
                                <tr>
                                    <td class="text-left">{{ @$key +1 }}</td>
                                    <td class="text-left">{{ @$value->name }}</td>
                                    <td class="text-left">{{ data_get($value, 'group.group_code') }}</td>
                                    <td class="text-center">{{ isset($typeAsset[$value->type]) ? $typeAsset[$value->type]:''}}</td>
                                    <td class="text-right">{{ @$quantity }} </td>
                                    <td class="text-left">{{ @data_get($value, 'unit.name') }}</td>
                                    <td class="text-left">{{ data_get($value, 'ownerStructure.name') }}</td>
                                    <td class="text-left">{{ @$value->receiver_handover }}</td>
                                    <td class="text-right">{{ number_format($value->price) }}</td>
                                    <td class="text-right">{{ number_format($quantity*$value->price) }}</td>
                                    <td class="text-center">{{ @$value->warranty }}</td>
                                    <td class="text-center">{{ @$value->year_use}}</td>
                                    <td class="text-left">{{ (isset($value->status) && $value->status) ?
                                     \App\Models\Assets\Asset::$assetListStatus[$value->status]:''
                                      }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{
                    $queryPaginate->paginate(20)->appends($input)->links()
                }}
                Tổng số {{ $queryPaginate->paginate()->total() }} bản ghi
                <br>
                <br>
                <br>
                Tổng số tài sản tìm thấy theo điều kiện lọc: {{ $queryGet->get()->sum('asset_count') }}
            </div>
        </section>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="pagination">
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.drl_company_owner').on('change', function (e) {
                e.preventDefault();
                initGetStructureByCompany();
            });


            $('.action-search-report').click(function () {
                var action = $(this).data('action');
                $('#search_form_report').attr('action', action);
                $('#search_form_report').attr('target', '');
                $('#search_form_report').submit();
            });

            $('.action-export-report').click(function () {
                var action = $(this).data('action');
                $('#search_form_report').attr('action', action);
                $('#search_form_report').attr('target', '_blank');
                $('#search_form_report').submit();
            });

        });

        function initGetStructureByCompany() {
            var company_id = $('.drl_company_owner').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '../system/user/get-structure/' + company_id,
                cache: false,
                type: "get",
                processData: false,
                contentType: false,
                success: function (response) {
                    $('.drl_structure_owner').empty().append(response.html);
                },
                error: function (data) {

                }
            });
            return false;
        }

    </script>
    <script>
        $(document).ready(function () {
            $('#list-asset-status').multiselect({
                nonSelectedText: '-- Tất cả --',
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%',
                allSelectedText: 'Chọn tất cả',
                filterPlaceholder: 'Tìm kiếm',
                nSelectedText: 'lựa chọn',
                templates: {
                    filterClearBtn: '<span class="input-group-btn">' +
                        '<button class="btn btn-default multiselect-clear-filter" type="button">' +
                        '<i class="fa fa-times-circle"></i>' +
                        '</i>' +
                        '</button>' +
                        '</span>'
                }
            });
        });
    </script>
@stop
