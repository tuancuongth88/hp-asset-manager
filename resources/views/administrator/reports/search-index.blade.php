{{ Form::open(array('route' => 'reports.index', 'method' => 'GET','id'=>'search_form_report')  ) }}
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="row gutter-2 gutter-lg-3">
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Tên tài sản
            </label>
            <input name="name" class="form-control m-input" placeholder="Tên tài sản"
                   value="{{isset($input['name'])?$input['name']:''}}"
            >
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Nhóm tài sản
            </label>
            <?php $selected = isset($input['group_id']) ? $input['group_id'] : '' ?>
            <select class="form-control m-input drl_group m-select2"
                    name="group_id">
                <option value="0">-- Tất cả --</option>
                <?php echo \App\Models\Partners\Group::genHtmlGroup($selected) ?>
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Phân loại TS
            </label>
            <select class="form-control m-input drl_type m-select2"
                    name="type_id">
                <option value="">-- Tất cả --</option>
                @foreach (\App\Models\Assets\Asset::$assetClassification as $key_tp => $value_tp)
                    <option value="{{ $key_tp }}"
                            {{(isset($input['type_id']) && $input['type_id'] == $key_tp) ? 'selected':'' }}
                    >
                        {{ $value_tp }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Trạng thái
            </label>
            <select id="list-asset-status" name="status[]" multiple class="form-control">
                @foreach (\App\Models\Assets\Asset::$assetListStatus as $key_st => $value_st)
                    <option value="{{ $key_st }}"
                            {{(isset($input['status']) && in_array($key_st,$input['status'])) ? 'selected':'' }}
                    >
                        {{ $value_st }}
                    </option>
                @endforeach
            </select>

        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Nhà thầu
            </label>
            <input name="name_partner" class="form-control m-input" placeholder="Tên nhà thầu">
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Công ty
            </label>
            <select class="form-control m-input drl_company_owner m-select2"
                    name="company_owner">
                <option value="0">-- Tất cả --</option>
                @foreach ($listCompany as $key_cp => $value_cp)
                    <option value="{{ $key_cp }}"
                            {{(isset($input['company_owner']) && $input['company_owner'] == $key_cp) ? 'selected':'' }}
                    >
                        {{ $value_cp }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group m-form__group">
            <label for="name">
                Đơn vị trực thuộc công ty
            </label>
            <select class="form-control m-input drl_structure_owner m-select2"
                    name="structure_owner">
                @if($list_structure)
                    <option value="">-- Chọn đơn vị trực thuộc --</option>
                    @foreach($list_structure as $key_str => $val_str)
                        <option value="{{ $val_str['id'] }}"
                                {{(isset($input['structure_owner']) && $input['structure_owner'] == $val_str['id']) ? 'selected':'' }}
                        >
                            {{ $val_str['name']. ' | ' . \App\Models\Systems\StructureCompany::$listType[$val_str['type']]  }}
                        </option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
</div>
<div class="row gutter-2 gutter-lg-3">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <h3 class="group_asset">Nhóm tài sản theo</h3>
    </div>
</div>
<div class="row gutter-2 gutter-lg-3">
    <div class="col-lg-2">
        <div class="form-group m-form__group">
            <label for="name">
                Mã nhóm tài sản
            </label>
            <input type="checkbox" name="groupby_group" {{isset($input['groupby_group']) ? 'checked' : ''}}/>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group m-form__group">
            <label for="name">
                Đơn vị trực thuộc
            </label>
            <input type="checkbox"
                   name="groupby_structure_owner" {{isset($input['groupby_structure_owner']) ? 'checked' : ''}}/>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group m-form__group">
            <label for="name">
                Năm sử dụng
            </label>
            <input type="checkbox" name="groupby_year_use" {{isset($input['groupby_year_use']) ? 'checked' : ''}}/>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group m-form__group">
            <label for="name">
                Tên tài sản
            </label>
            <input type="checkbox" name="groupby_name" {{isset($input['groupby_name']) ? 'checked' : ''}}/>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group m-form__group">
            <label for="name">
                Giá
            </label>
            <input type="checkbox" name="groupby_price" {{isset($input['groupby_price']) ? 'checked' : ''}}/>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group m-form__group">
            <label for="name">
                Trạng thái tài sản
            </label>
            <input type="checkbox" name="groupby_status" {{isset($input['groupby_status']) ? 'checked' : ''}}/>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        <input type="button" value="Tìm kiếm" class="btn form-control action-search-report btn-add-color-cat"
               data-action="{{route('reports.index')}}"
        />
    </div>
    <div class="col-md-2">
        <input type="button" value="Xuất báo cáo" class="btn form-control action-export-report btn-add-color"
               data-action="{{route('reports.export')}}"
        />
    </div>
</div>
{{ Form::close() }}
