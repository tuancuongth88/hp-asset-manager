@extends('administrator.app')
@section('title','Sơ đồ công ty')

@section('content')
    <div class="list-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <h2 class="title_list">Cơ cấu công ty</h2>
                </div>
            </div>
            @include('administrator.errors.info-search')
            @include('administrator.errors.messages')
        </div>
        <section class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="well">
                            <p class="lead">
                                <a href="#newModal" class="btn btn-default pull-right" data-toggle="modal">
                                <span class="glyphicon glyphicon-plus-sign">
                                </span> Thêm cấp
                                </a>&nbsp;
                                <a href="{{ route('company.index') }}">
                                    Trở về danh sách
                                </a>
                            </p>
                            <div class="dd" id="nestable">
                                <?php echo $structure ?>
                            </div>
                            <p id="success-indicator" style="display:none; margin-right: 10px;">
                            <span class="glyphicon glyphicon-ok">
                            </span> Xắp xếp vị trí thành công
                            </p>
                        </div>
                    </div>
                </div>

                <!-- Create new item Modal -->
                <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Thêm mới</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                            </div>
                            {{ Form::open(array('route' => array('structure.store',$company),'class'=>'form-horizontal','role'=>'form'))}}
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="title" class="col-lg-12 control-label">Tên cấp</label>
                                    <div class="col-lg-10">
                                        <input name="name" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="url" class="col-lg-2 control-label">Loại</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="type">
                                            @foreach(\App\Models\Systems\StructureCompany::$listType as $key=>$val)
                                                <option value="{{$key}}">
                                                    {{$val}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                <button type="submit" class="btn btn-primary">Lưu</button>
                            </div>
                            {{ Form::close()}}
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!-- Delete item Modal -->
                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            {{ Form::open(array('url'=>route('structure.delete',$company))) }}
                            <div class="modal-header">
                                <h4 class="modal-title">Cảnh báo</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Bạn có chắc chắn muốn xóa?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                <input type="hidden" name="delete_id" id="postvalue" value=""/>
                                <input type="submit" class="btn btn-danger" value="Đồng ý"/>
                            </div>
                            {{ Form::close() }}
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->


                <div class="modal" tabindex="-1" role="dialog" id="edit-modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Sửa thông tin</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            {{ Form::open(array('route' => array('structure.edit-post',$company),'class'=>'form-horizontal','role'=>'form'))}}
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                <button type="submit" class="btn btn-primary">Lưu</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        $(function () {
            $('.dd#nestable').nestable({
                dropCallback: function (details) {
                    var order = new Array();
                    $("li[data-id='" + details.destId + "']").find('ol:first').children().each(function (index, elem) {
                        order[index] = $(elem).attr('data-id');
                    });

                    if (order.length === 0) {
                        var rootOrder = new Array();
                        $("#nestable > ol > li").each(function (index, elem) {
                            rootOrder[index] = $(elem).attr('data-id');
                        });
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.post('{{route('structure.post-index',$company)}}',
                        {
                            source: details.sourceId,
                            destination: details.destId,
                            order: JSON.stringify(order),
                            rootOrder: JSON.stringify(rootOrder)
                        },
                        function (data) {
                            // console.log('data '+data);
                        })
                        .done(function () {
                            $("#success-indicator").fadeIn(100).delay(1000).fadeOut();
                        })
                        .fail(function () {
                        })
                        .always(function () {
                        });
                }
            });

            $('.delete_toggle').each(function (index, elem) {
                $(elem).click(function (e) {
                    e.preventDefault();
                    $('#postvalue').attr('value', $(elem).attr('rel'));
                    $('#deleteModal').modal('toggle');
                });
            });
        });

        function callModalEdit(index) {
            var that = index;
            var id = $(that).data('id');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: './structure/edit/' + id,
                cache: false,
                type: "get",
                processData: false,
                contentType: false,
                success: function (response) {
                    $('#edit-modal .modal-body').empty().append(response.html);
                    $('#edit-modal').modal('show');
                },
                error: function (data) {

                }
            });
        }
    </script>
@stop