@extends('administrator.app')
@section('title','Sửa thông tin công ty')

@section('content')
    <div class="container">
        <div class="list-content">
            <div class="wrap-title">
                <h2 class="title_list">Thêm mới thông tin công ty</h2>
            </div>
            {{ Form::open(array('route' => array('company.update', 'id' => $data->id),
            'method' => 'PUT', 'enctype' => 'multipart/form-data')) }}
            <section class="main-content-add">
                <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <h3 class="sub_title">Thông tin cơ bản</h3>
                        </div>
                        <div class="col-lg-9 col-md-9 col-xs-12">
                            <div class="form-group">
                                <label>Tên công ty: <span class="required">(*)</span></label>
                                <input type="name" class="form-control m-input" id="name" placeholder="Tên công ty"
                                       name="name" value="{{ $data->name }}">
                            </div>
                            <div class="form-group">
                                <label>Mã số thuế:</label>
                                <input type="text" class="form-control m-input" id="tax" placeholder="Mã số thuế" name="tax"
                                       value="{{ $data->tax }}">
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại:</label>
                                <input type="text" class="form-control m-input" id="phone" placeholder="Số điện thoại"
                                       name="phone" value="{{ $data->phone }}">
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ:</label>
                                <input type="text" class="form-control m-input" id="address" placeholder="Địa chỉ"
                                       name="address" value="{{ $data->address }}">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12" style="padding-top: 25px;">
                            <div class="m-dropzone dropzone m-dropzone--primary dropzone_file box_upload" id="attachment">
                                <div class="m-dropzone__msg style-file needsclick">
                                <span>
                                    Upload ảnh logo
                                </span>
                                    <br/>
                                    <span class="m-dropzone__msg-desc" style="font-size: 10px; text-transform: capitalize;">
                                    (Tối đa 1 File kích thước 5MB/file)
                                </span>
                                </div>
                            </div>
                            <div class="fallback">
                                <input type="hidden" name="file_logo" id="file_logo">
                                <img src="{{ $data->logo }}" style="width: 200px; height: 150px;"/>
                            </div>
                        </div>
                    </div>
            </section>
            <section class="button">
                <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                                <button type="submit" class="save_bid">
                                    Lưu
                                </button>
                            </div>
                            <div class="col-lg-3 col-md-3 col-xs-12">
                                <a class="delete_bid" href="{{ route('company.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {{ Form::close() }}
        </div>
        <script>
            Dropzone.autoDiscover = false;
            $(document).ready(function () {
                var listAttachment = [];
                $("#attachment").dropzone({
                    addRemoveLinks: true,
                    url: '{{ route('company.upload-attachment') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    maxFiles: 1,
                    paramName: "file",
                    dictRemoveFile: "Xóa",
                    dictRemoveFileConfirmation: "Bạn có chắc muốn xóa tệp này",
                    acceptedFiles: "image/jpeg,image/png,image/gif",
                    dictInvalidFileType: "Định dạng file không phải dạng cho phép",
                    dictFileTooBig: "Dung lượng file quá lớn vượt giới hạn cho phép",

                    removedfile: function (file) {
                        var fileRemove = findObjectByKey(listAttachment, 'name', file.name);

                        $.ajax({
                            type: 'DELETE',
                            url: '{{ route('company.delete-attachment') }}',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                id: fileRemove.id,
                                url: fileRemove.file_url,
                            },
                            sucess: function (data) {

                            }
                        });
                        var _ref;
                        listAttachment = $.grep(listAttachment, function (e) {
                            return e.id != fileRemove.id;
                        });
                        var nameArray = listAttachment.map(function (el) {
                            return el.id;
                        });
                        $('#logo_image').val(nameArray);
                        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                    },
                    init: function () {
                        this.on("success", function (file, response) {
                            listAttachment.push(response.data);
                            var nameArray = listAttachment.map(function (el) {
                                return el.id;
                            });
                            $('#logo_image').val(nameArray);
                            alert(response.message);
                        });
                        // Handle errors
                        this.on('error', function (file, response) {
                            alert('File tải lên không phù hợp');
                        });
                    }
                });
            });
        </script>
    </div>
@stop