@extends('administrator.app')
@section('title','Thêm mới vị trí')

@section('content')
<div class="container">
    <div class="list-content">
        <div class="wrap-title">
            <h2 class="title_list">Thêm mới vị trí</h2>
        </div>
        @include('administrator.errors.errors-validate')
        {{ Form::open(array('route' => 'position.store', 'class' => '')) }}
            <section class="main-content-add">
                <div class="row">
                    <div class="wrap-title">
                        <h3 class="sub_title">Thông tin cơ bản</h3>
                    </div>
                    <div class="col-lg-9 col-md-9 col-xs-12">
                        <div class="form-group">
                            <label for="name">
                                Tên: <span class="required">(*)</span>
                            </label>
                            <input type="name" class="form-control m-input" id="name" placeholder="Tên vị trí" name="name" value="{{ old('name') }}">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <div class="form-group">

                        </div>
                    </div>
                </div>
            </section>
            <section class="button">
                <div class="m-portlet__foot m-portlet__foot--fit" style="text-align: right;">
                    <div class="m-form__actions">
                        <div class="container">
                            <div class="row">
                                <div class="offset-md-6 col-lg-3 col-md-3 col-xs-12">
                                    <button type="submit" class="save_bid">
                                        Lưu
                                    </button>
                                </div>
                                <div class="col-lg-3 col-md-3 col-xs-12">
                                    <a class="delete_bid" href="{{ route('position.index') }}">
                                        Trở về danh sách
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        {{ Form::close() }}
    </div>
</div>
@stop