@extends('administrator.app')
@section('title','Danh sách các vị trí')

@section('content')
    <div class="container">
        <div class="list-content">
        <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12" style="margin-bottom: 20px">
                    <h2 class="title_list">Danh sách các vị trí</h2>
                </div>
                <div class="cocol-lg-12 col-md-12 col-xs-12">
                    <a href="{{ route('position.create') }}" class="btn m-btn m-btn--icon btn-add-color">
                        <span>
                            <i class="fa flaticon-plus"></i>
                            <span>
                                Thêm mới
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        <section class="main-content">
            <div class="wrap-table">
                @include('administrator.errors.messages')
                <table class="table table-hover table-responsive table-striped">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Tên</th>
                        <th scope="col">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key => $value)
                        <tr class="header expand">
                            <td scope="row">{{ $key + 1 }}</td>
                            <td>
                                {{ $value->name }}
                            </td>
                            <td>
                                <a href="{{ route('position.edit', $value->id) }}" class="">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                {{ Form::open(array('method'=>'DELETE', 'route' => array('position.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="btn-delete">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="wrap-title">
                {{ $data->links() }}
                Tổng số {{ $data->total() }} bản ghi
            </div>
        </section>
    </div>
    </div>
@stop