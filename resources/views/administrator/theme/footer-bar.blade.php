<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="left-footer">
                    <p>2018 &copy; Phát triển bởi <b>Hải Phát Tech</b></p>
                </div>
                <div class="right-footer">
                    <p>Phiên bản hệ thống: 1.0</p>
                </div>
            </div>
        </div>
    </div>
</footer>