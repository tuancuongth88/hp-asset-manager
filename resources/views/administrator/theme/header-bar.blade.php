<header>
    <nav class="navbar navbar-expand-lg navbar-light navbar-main-head">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="{{ URL::asset('images/logo.png') }}"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li><a href="#"><p class="header-title">Hệ thống quản lý tài sản HAIPHATLAND<p></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-left" style="padding-top: 10px;">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="m-nav__link-icon">
                        <i class="flaticon-share"></i></span><b class="caret"></b></a>
                        <ul class="dropdown-menu ml-auto navbar-header-custom-top">
                            {{--<li class="m-nav__item"><a href="{{ route('user.index') }}" class="m-nav__link">Danh sách user</a></li>--}}
                            <li class="m-nav__item"><a href="{{ route('company.index') }}" class="m-nav__link">Công ty</a></li>
                            <li class="m-nav__item"><a href="{{ route('position.index') }}" class="m-nav__link">Chức vụ</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img
                                    src="{{ URL::asset('images/no-avatar.ico') }}" class="img-responsive avatar"><b
                                    class="caret"></b></a>
                        <ul class="dropdown-menu ml-auto navbar-header-custom">
                            <div class="m-dropdown__header m--align-center"
                                 style="background: url({{ URL::asset('images/user_profile_bg.jpg') }});background-size: cover;">
                                <div class="m-card-user">
                                    <div class="m-card-user__pic">
                                        <img src="{{ URL::asset('images/no-avatar.ico') }}"
                                             class="m--img-rounded m--marginless" alt=""/>
                                    </div>
                                    <div class="m-card-user__details">
                                        <span class="m-card-user__name m--font-weight-500" style="color:#fff">
                                            {{ (\Auth::user()) ? Auth::user()->fullname : '' }}
                                        </span>
                                        <a href="{{ route('user.edit', \Auth::user()->id) }}"
                                           class="m-card-user__email m--font-weight-300 m-link" style="color:#fff">
                                            {{ (\Auth::user()) ? Auth::user()->email : '' }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav m-nav--skin-light">
                                        <li class="m-nav__section m--hide">
                                            <span class="m-nav__section-text">
                                                Section
                                            </span>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                <span class="m-nav__link-title">
                                                    <span class="m-nav__link-wrap">
                                                        <span class="m-nav__link-text">
                                                            Thông tin cá nhân
                                                        </span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                <span class="m-nav__link-text">
                                                    Thay đổi mật khẩu
                                                </span>
                                            </a>
                                        </li>
                                        <li class="m-nav__separator m-nav__separator--fit"></li>
                                        <li class="m-nav__item">
                                            <a href="{{ route('logout') }}"
                                               class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                                Đăng xuất
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
</header>