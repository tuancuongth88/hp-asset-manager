<head>
    <title>Hệ thống quản lý tài sản HAIPHATLAND</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/font/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/vendors/base/vendors.bundle.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/select2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/hover.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/basic.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/dropzone.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/danhsach.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/themmoi.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/vendors/nestable/nestable.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap-multiselect.css') }}"/>

    {{--Thêm mới một số css + js--}}

    <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/vendors/base/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/demo/default/base/scripts.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/vendors/nestable/jquery.nestable.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-multiselect.js') }}"></script>
</head>