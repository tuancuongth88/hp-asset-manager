<!--begin::Base Scripts -->
<script type="text/javascript" src="{{ URL::asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/jquery-localize/jquery.localize.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/select2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/demo/default/custom/components/forms/widgets/form-repeater.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/repeater/jquery.repeater.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.number.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/jquery-validate/vi.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/main.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/custom.js') }}"></script>
<!--end::Base Scripts -->