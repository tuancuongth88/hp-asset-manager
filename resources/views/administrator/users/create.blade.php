@extends('administrator.app')
@section('title','Danh sách user')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Danh sách tài khoản
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            @include('administrator.errors.messages')
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 20px">
                    <a href="{{ route('user.index') }}" class="btn btn-success">Danh sách tài khoản</a>
                </div>
                <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                    <h3 class="m-portlet__head-text">
                                        Thêm mới tài khoản
                                    </h3>
                                </div>
                            </div>
                        </div>
                        {{ Form::open(array('route' => 'user.store', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype' => 'multipart/form-data')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Tên tài khoản <span style="color: red;">(*)</span>:
                                    </label>
                                    <input type="text" class="form-control m-input" placeholder="Tên tài khoản"
                                           name="fullname" value="{{ old('fullname') }}">
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Email <span style="color: red;">(*)</span>:
                                    </label>
                                    <input type="email" class="form-control m-input" placeholder="Email" name="email"
                                           value="{{ old('email') }}">
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        CMT <span style="color: red;">(*)</span>:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-list-alt"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="" name="identity"
                                               value="{{ old('identity') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Mật khẩu:
                                    </label>
                                    <input type="password" class="form-control m-input" placeholder="" name="password">
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Xác nhận mật khẩu:
                                    </label>
                                    <input type="password" class="form-control m-input" placeholder=""
                                           name="password_confirmation">
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Điện thoại <span style="color: red;">(*)</span>:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-phone"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="" name="phone"
                                               value="{{ old('phone') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label for="name">
                                        Người quản lý
                                    </label>
                                    <select class="form-control m-input" id="parent_id" name="parent_id">
                                        <option value="0"> -- Lựa chọn --</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label for="name">
                                        Giới tính
                                    </label>
                                    <select class="form-control m-input" id="gender" name="gender">
                                        @foreach (\App\Models\Users\User::$listGender as $key => $value)
                                            <option value="{{ $key }}">
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Ngày sinh <span class="required">(*)</span>:
                                    </label>
                                    <input type="text" class="form-control" id="birthday" placeholder="Chọn ngày"
                                           name="birthday"/>
                                </div>
                                <div class="col-lg-4">
                                    <label for="company">
                                        Công ty
                                    </label>
                                    <select class="form-control m-input" id="company" name="company_id">
                                        @foreach ($data['companies'] as $element)
                                            <option value="{{ $element['id'] }}">
                                                {{ $element['name'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label for="branch_id">
                                        Trực thuộc
                                    </label>
                                    <select class="form-control m-input" id="structure_id" name="structure_id">
                                        @foreach ($data['structure'] as $element)
                                            <option value="{{ $element['id'] }}">
                                                {{ $element['name'] }}
                                                | {{\App\Models\Systems\StructureCompany::$listType[$element['type']]}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label for="position">
                                        Chức vụ
                                    </label>
                                    <select class="form-control m-input" id="position" name="position_id">
                                        @foreach ($data['positions'] as $element)
                                            <option value="{{ $element['id'] }}">
                                                {{ $element['name'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label for="exampleInputEmail1">
                                        Ảnh
                                    </label>
                                    <div></div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile" name="avatar">
                                        <label class="custom-file-label" for="customFile">
                                            Chọn ảnh
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('user.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                        {{-- </form> --}}
                        {{ Form::close() }}
                    </div>
                    <!--end::Portlet-->
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
    @include('administrator.users.script')
@stop