@extends('administrator.app')
@section('title','Danh sách user')

@section('content')
    <section class="main-content">
        <div class="container">
            <div class="row">
            {{ Form::open(array('enctype' => 'multipart/form-data', 'method'=>'POST',
                        'route' => 'user.import', 'id' => 'import-form')) }}
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="custom-file m--padding-top-5">
                        <input type="file" class="custom-file-input" id="customFile" name="import_file">
                        <label class="custom-file-label" for="customFile">
                            Chọn file import
                        </label>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-xs-12">
                    <a href="{{ route('user.create') }}" class="btn btn-primary m-btn m-btn--icon btn-add-color">
                        <span>
                            <i class="fa flaticon-plus"></i>
                            <span>
                                Thêm mới tài khoản
                            </span>
                        </span>
                    </a>
                </div>
                <div class="col-lg-2 col-md-2 col-xs-12">
                    <button type="submit" class="btn btn-add-color-cat" id="import_file">
                        <i class="fa fa-file-excel-o"></i> Import
                    </button>
                </div>
            {{ Form::close() }}<br/>
                <a href="{{ url('/user.xlsx') }}" style="font-weight: bold; font-size: 17px; color: #000;">Mẫu File Import
                </a>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    @if(session('listUserFail'))
                        <div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                            @foreach (session('listUserFail') as $element)
                                Email {{ $element }} đã có trong cơ sở dữ liệu<br>
                            @endforeach
                        </div>
                    @endif
                    @include('administrator.errors.info-search')
                    <div class="m-section">
                        @include('administrator.errors.messages')
                        <div class="m-section__content">
                            <div class="">
                                <div class="">
                                    <div class="">
                                        <div class="">
                                            <h3 class="">
                                                Người dùng
                                                <small>
                                                    danh sách user
                                                </small>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <!--begin: Search Form -->
                                    <div class="m-form">
                                        <div class="row align-items-center">
                                            <div class="col-lg-8 col-md-8">
                                                <div class="form-group m-form__group row align-items-center">
                                                    <div class="col-md-3">
                                                        <div class="m-input-icon m-input-icon--left">
                                                            <input type="text" class="form-control m-input"
                                                                   placeholder="Tên ..." id="email" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        {{ Form::select('position_id', [0 => 'Chọn chức vụ'] + $listPosition, null,
                                                            ['class' => 'form-control m-select2', 'id' => 'position_id']) }}
                                                    </div>
                                                    <div class="col-md-3">
                                                        {{--{{ Form::select('department_id', [0 => 'Chọn phòng ban'] + $listDepartment, null,--}}
                                                            {{--['class' => 'form-control m-select2', 'id' => 'department_id']) }}--}}
                                                    </div>
                                                    <div class="col-md-3">
                                                        {{--{{ Form::select('branch', [0 => 'Chọn chi nhánh'] + $listBranch, null,--}}
                                                            {{--['class' => 'form-control m-select2', 'id' => 'branch_id']) }}--}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4">
                                                <a href="{{ route('user.create') }}"
                                                   class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                                    <span>
                                                        <i class="la la-plus-circle"></i>
                                                        <span>
                                                            Thêm mới
                                                        </span>
                                                    </span>
                                                </a>
                                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Search Form -->
                                    <!--begin: Datatable -->
                                    <div class="m_datatable" id="ajax_data"></div>
                                    <!--end: Datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.m_datatable').mDatatable({
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '/administrator/system/user/list',
                            method: 'GET',
                        },
                    },

                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },
                // column sorting
                sortable: false,

                pagination: true,

                toolbar: {
                    // toolbar items
                    items: {
                        // pagination
                        pagination: {
                            // page size select
                            pageSizeSelect: [10, 20, 30, 50, 100],
                        },
                    },
                },

                search: {
                    input: $('#email'),
                },

                rows: {
                    // auto hide columns, if rows overflow
                    autoHide: true,
                },

                // columns definition
                columns: [
                    {
                        field: 'avatar',
                        title: 'Ảnh',
                        template: function (row) {
                            return '<img width="25" src="' + row.avatar + '">';
                        },
                    },
                    {
                        field: 'fullname',
                        title: 'Tên',
                    }, {
                        field: 'email',
                        title: 'Email',
                    }, {
                        field: 'phone',
                        title: 'Điện thoại',
                    }, {
                        field: 'position',
                        title: 'Chức vụ',
                        template: function (row) {
                            if (row.position) {
                                return row.position.name;
                            }
                            return '';
                        },
                    }, {
                        field: 'birthday',
                        title: 'Ngày sinh',
                    }, {
                        field: 'department',
                        title: 'Phòng ban',
                        template: function (row) {
                            if (row.department) {
                                return row.department.name;
                            }
                            return '';
                        },
                    },  {
                        field: 'branch',
                        title: 'Chi nhánh',
                        template: function (row) {
                            if (row.branch) {
                                return row.branch.name;
                            }
                            return '';
                        },
                    },  {
                        field: 'company',
                        title: 'Công ty',
                        template: function (row) {
                            if (row.company) {
                                return row.company.name;
                            }
                            return '';
                        },
                    }, {
                        field: 'address',
                        title: 'Địa chỉ',
                    }, {
                        field: 'Tùy chọn',
                        title: 'Tùy chọn',
                        sortable: false,
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                            return '<a href="/administrator/system/user/' + row.id + '/edit" class="" title="Sửa">\
                                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>\
                            </a>\
                            <button data-id="' + row.id + '" class="btn-delete" title="Xóa">\
                                  <i class="la la-trash"></i>\
                            </button>\
                            ';
                        },
                    }],
            });
            $('body').on('click', '.delete-user', function () {
                var id = $(this).attr('data-id');
                swal({
                    title: 'Bạn có chắc?',
                    text: "muốn xóa người dùng này!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Có!'
                }).then(function (result) {
                    if (result.value) {
                        if (id > 0) {
                            mApp.block($('#ajax_data'));
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            $.ajax({
                                url: '/administrator/system/user/' + id,
                                type: 'POST',
                                contentType: 'application/json',
                                success: function (response) {
                                    mApp.unblock($('#ajax_data'));
                                    swal({
                                        "title": "Thông báo",
                                        "text": response.message,
                                        "type": "success",
                                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                    });
                                    if (response.status) {
                                        $('.m_datatable').mDatatable('reload');
                                    }
                                },
                                error: function (data) {
                                    console.log(data);
                                }
                            });
                        }
                    }
                });

            });
            $('#position_id').on('change', function () {
                $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'position_id');
            });
            $('#department_id').on('change', function () {
                $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'department_id');
            });
            $('#branch').on('change', function () {
                $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'branch');
            });
        });
    </script>
@stop