@extends('administrator.app')
@section('title','Danh sách user')

@section('content')
    <div class="list-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    @include('administrator.errors.errors-validate')
                    <h2 class="title_list">Thông tin cá nhân</h2>
                </div>
            </div>
        </div>
        <section class="main-content-add" style="padding-bottom: 60px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-xs-12">
                        <div class="m-portlet__body">
                            <div class="m-card-profile">
                                <div class="m-card-profile__title m--hide">
                                    Your Profile
                                </div>
                                <div class="m-card-profile__pic">
                                    <div class="m-card-profile__pic-wrapper">
                                        <img src="{{ $user->avatar }}" alt=""/>
                                    </div>
                                </div>
                                <div class="m-card-profile__details">
                                <span class="m-card-profile__name">
                                    {{ $user->fullname }}
                                </span>
                                    <a href="" class="m-card-profile__email m-link">
                                        {{ $user->phone }}
                                    </a>
                                </div>
                            </div>
                            <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                                <li class="m-nav__separator m-nav__separator--fit"></li>
                                <li class="m-nav__section m--hide">
                                <span class="m-nav__section-text">
                                    Section
                                </span>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{ route('user.edit', \Auth::user()->id) }}" class="m-nav__link">
                                        <i class="m-nav__link-icon flaticon-profile-1"></i>
                                        <span class="m-nav__link-title">
                                        <span class="m-nav__link-wrap">
                                            <span class="m-nav__link-text">
                                                Thông tin cá nhân
                                            </span>
                                        </span>
                                    </span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{ route('user.get-change-password') }}" class="m-nav__link">
                                        <i class="m-nav__link-icon flaticon-share"></i>
                                        <span class="m-nav__link-text">
                                        Thay đổi mật khẩu
                                    </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-xs-12">
                        <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-tools">
                                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                        role="tablist">
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link active tabs_info_user" data-toggle="tab"
                                               href="#m_user_profile_tab_1" role="tab">
                                                <i class="flaticon-share m--hide"></i>
                                                Cập nhật thông tin tài khoản
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @include('administrator.errors.errors-validate')
                            <div class="tab-content">
                                <div class="tab-pane active" id="m_user_profile_tab_1">
                                    {{ Form::open(array(
                                    'route' => array('user.update', $user->id),
                                     'method' => 'PUT',
                                     'class' => 'm-form m-form--fit m-form--label-align-right',
                                      'enctype' => 'multipart/form-data'))
                                       }}
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row">
                                            <div class="col-10 ml-auto">
                                                <h3 class="m-form__section">
                                                    1. Thông tin cơ bản
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Họ tên
                                            </label>
                                            <div class="col-7">
                                                <input class="form-control m-input" type="text" name="fullname"
                                                       value="{{ $user->fullname }}">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Email
                                            </label>
                                            <div class="col-7">
                                                <input class="form-control m-input" type="email"
                                                       value="{{ $user->email }}"
                                                       disabled>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Ngày sinh
                                            </label>
                                            <div class="col-7">
                                                <input type="text" class="form-control" id="birthday"
                                                       placeholder="Chọn ngày" name="birthday"
                                                       value="{{ $user->birthday }}">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                CMT
                                            </label>
                                            <div class="col-7">
                                                <input class="form-control m-input" type="text"
                                                       value="{{ $user->identity }}" name="identity">
                                                <span class="m-form__help">
                                                Số chứng minh nhân dân hoặc hộ chiếu
                                            </span>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Điện thoại
                                            </label>
                                            <div class="col-7">
                                                <input name="phone" class="form-control m-input" type="text"
                                                       value="{{ $user->phone }}">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Giới tính
                                            </label>
                                            <div class="col-7">
                                                <select class="form-control m-input" id="gender" name="gender">
                                                    <option value="1" {{ ($user['gender'] == 1) ? 'selected' : ''}}>
                                                        Nam
                                                    </option>
                                                    <option value="2" {{ ($user['gender'] == 2) ? 'selected' : ''}}>
                                                        Nữ
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-10 ml-auto">
                                                <h3 class="m-form__section">
                                                    2. Công tác
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Công ty
                                            </label>
                                            <div class="col-7">
                                                <select class="form-control m-input" id="company" name="company_id">
                                                    @foreach ($data['companies'] as $element)
                                                        <option value="{{ $element['id'] }}" {{ ($user['company_id'] == $element['id']) ? 'selected' : ''}}>
                                                            {{ $element['name'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Trực thuộc
                                            </label>
                                            <div class="col-7">
                                                <select class="form-control m-input" id="structure_id"
                                                        name="structure_id">
                                                    @foreach ($data['structure'] as $element)
                                                        <option value="{{ $element['id'] }}" {{ ($user['structure_id'] == $element['id']) ? 'selected' : ''}} >
                                                            {{ $element['name'] }}
                                                            | {{\App\Models\Systems\StructureCompany::$listType[$element['type']]}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Người quản lý
                                            </label>
                                            <div class="col-7">
                                                {{ Form::select('parent_id', [0 => '--- Lựa chọn---'] + $listUserByDepartment->pluck('email', 'id')->toArray(0), $user->parent_id, ['class' => 'form-control m-input m-select2', 'id' => 'parent_id']) }}

                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label">
                                                Chức vụ
                                            </label>
                                            <div class="col-7">
                                                <select class="form-control m-input" id="position" name="position_id">
                                                    @foreach ($data['positions'] as $element)
                                                        <option value="{{ $element['id'] }}" {{ ($user['position_id'] == $element['id']) ? 'selected' : ''}}>
                                                            {{ $element['name'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                        <div class="m-form__group form-group row">
                                            <label class="col-2 col-form-label">
                                                Kích hoạt
                                            </label>
                                            <div class="col-7">
                                            <span class="m-switch m-switch--icon m-switch--success">
                                                <label>
                                                    <input type="checkbox"
                                                           {{ ($user['active'] == 1) ? 'checked' : ''}} name="active"
                                                           value="1">
                                                    <span></span>
                                                </label>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-portlet__foot m-portlet__foot--fit">
                                        <div class="m-form__actions">
                                            <div class="row">
                                                <div class="col-2"></div>
                                                <div class="col-7">
                                                    <button type="submit"
                                                            class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                        Lưu
                                                    </button>
                                                    &nbsp;&nbsp;
                                                    <button type="reset"
                                                            class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                        Hủy
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                                <div class="tab-pane " id="m_user_profile_tab_2"></div>
                                <div class="tab-pane " id="m_user_profile_tab_3"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-xs-12"></div>
                </div>
            </div>
        </section>
    </div>
    @include('administrator.users.script')
@stop