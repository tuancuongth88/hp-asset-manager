<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('administrator/login', ['uses' => 'Auth\AuthsController@getLogin'])->name('login');
Route::post('administrator/login', ['uses' => 'Auth\AuthsController@postLogin'])->name('login.post');
Route::post('administrator/register', ['uses' => 'Auth\AuthsController@postRegister'])->name('register');
Route::get('administrator/logout', ['uses' => 'Auth\AuthsController@getLogout'])->name('logout');
Route::get('user/activation/{token}', 'Auth\AuthsController@getActiveUser')->name('user.activate');
Route::post('user/forgot', 'Auth\AuthsController@postForgot')->name('user.forgot');
Route::get('/reset-password/{token}', 'Auth\AuthsController@getResetPassword')->name('user.reset');
Route::post('/reset-password', 'Auth\AuthsController@postResetPassword')->name('user.post-reset');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(['prefix' => 'administrator', 'middleware' => 'auth'], function () {
    Route::group(['prefix' => 'system'], function () {
        Route::resource('position', Systems\PositionsController::class);
        Route::get('user/user-department/{id}', 'Users\UsersController@getUserByDepartment')->name('user.list-user-by-department');
        Route::post('user/import', 'Users\UsersController@postImport')->name('user.import');
        Route::post('role/import', 'Permissions\RolesController@postImport')->name('role.import');
        Route::post('user/change-password', 'Users\UsersController@postChangePassword')->name('user.post-change-password');
        Route::get('/user/list', 'Users\UsersController@getList');
        Route::get('/user/change-password', 'Users\UsersController@getChangePassword')->name('user.get-change-password');
        Route::get('/user/be-managed', 'Users\UsersController@getBeManaged');
        Route::get('/user/search', 'Users\UsersController@getSearch');
        Route::post('/user/{id}', 'Users\UsersController@destroy');
        Route::get('/user/read-notification/{id}', 'Users\UsersController@readNotification')->name('user.read.notification');
        Route::get('/user/get-structure/{company}', 'Users\UsersController@getStructureByCompany')->name('user.get-structure');
        Route::resource('user', Users\UsersController::class);

        Route::post('company/{company}/structure/post-index', 'Systems\StructureCompaniesController@postIndex')->name('structure.post-index');
        Route::post('company/{company}/structure/delete', 'Systems\StructureCompaniesController@postDelete')->name('structure.delete');
        Route::get('company/{company}/structure/edit/{id}', 'Systems\StructureCompaniesController@getEdit');
        Route::post('company/{company}/structure/edit', 'Systems\StructureCompaniesController@postEdit')->name('structure.edit-post');
        Route::get('company/{company}/structure', 'Systems\StructureCompaniesController@index')->name('structure.index');
        Route::post('company/{company}/structure', 'Systems\StructureCompaniesController@store')->name('structure.store');
        Route::post('/company/attachment', 'Systems\CompaniesController@postUploadAttachment')->name('company.upload-attachment');
        Route::delete('/company/attachment', 'Systems\CompaniesController@postDeleteAttachment')->name('company.delete-attachment');
        Route::resource('company', Systems\CompaniesController::class);
        Route::resource('role', Permissions\RolesController::class);
        Route::resource('permission', Permissions\PermissionsController::class);
        Route::get('/config-role', 'Permissions\RolesController@getConfigRole')->name('role.config-role');
        Route::put('/put-permission', 'Permissions\RolesController@putPermissionIntoRole')->name('role.put-permission');
        Route::put('put-role', 'Permissions\RolesController@putRoleIntoUser')->name('role.put-role');
        Route::get('config-user', 'Permissions\RolesController@getConfigUser')->name('role.config-user');
        Route::get('permission-by-role', 'Permissions\RolesController@getPermissionByRole');
    });
    Route::group(['prefix' => 'asset'], function () {
        Route::get('district/{id}', 'Partners\PartnersController@getDistrictOnCity')->name('city.district.list');
        Route::get('dashboard', 'Assets\DashboardController@index')->name('asset.index');
        Route::post('/contract/attachment', 'Contracts\ContractsController@postUploadAttachment')->name('contract.upload-attachment');
        Route::delete('/contract/attachment', 'Contracts\ContractsController@postDeleteAttachment')->name('contract.delete-attachment');
        Route::post('/partner/attachment', 'Partners\PartnersController@postUploadAttachment')->name('partner.upload-attachment');
        Route::delete('/partner/attachment', 'Partners\PartnersController@postDeleteAttachment')->name('partner.delete-attachment');
        Route::get('/partner/download-file/{id}', 'Partners\PartnersController@downloadFile')->name('partner_stores.download');
        Route::post('/assets/attachment', 'Assets\AssetsController@postUploadAttachment')->name('asset.upload-attachment');
        Route::delete('/assets/attachment', 'Assets\AssetsController@postDeleteAttachment')->name('asset.delete-attachment');
        Route::get('/partner/search-partner', 'Partners\PartnersController@searchPartner')->name('partner.search-partner');
        Route::get('/contract/search-contract', 'Contracts\ContractsController@searchContract')->name('contract.search-contract');
        Route::get('/contract/partner-contract/{id}', 'Contracts\ContractsController@getPartnerContract')->name('contract.partner-contract');
        Route::get('/contract/download-file/{id}', 'Contracts\ContractsController@downloadFile')->name('contract_stores.download');
        Route::get('/contract/show/{id}', 'Contracts\ContractsController@show')->name('contract.show');
        Route::get('partner/{id}/provide-category/', 'Partners\PartnersController@provideCategory')->name('partner.provide-category');
        Route::post('partner/{id}/provide-category/', 'Partners\PartnersController@updateProvideCategory')->name('partner.update-provide-category');
        Route::resource('partner', Partners\PartnersController::class);
        Route::resource('contract', Contracts\ContractsController::class);
        Route::resource('assets', Assets\AssetsController::class);
        Route::resource('category', Partners\CategoriesController::class);
        Route::get('group/import-excel', 'Partners\GroupsController@getImportForm')->name('group.get-import');
        Route::post('group/import-excel', 'Partners\GroupsController@postImport')->name('group.post-import');
        Route::resource('group', Partners\GroupsController::class);

        Route::post('/handover/branch-by-company/{id}', 'Assets\AssetHandoversController@getBranchByCompany');
        Route::post('/handover/get-department', 'Assets\AssetHandoversController@getDepartment');
        Route::post('/handover/get-user', 'Assets\AssetHandoversController@getUser');
        Route::post('/handover/active', 'Assets\AssetHandoversController@active')->name('handover.active');
        Route::post('/handover/active-store', 'Assets\AssetHandoversController@activeStore')->name('handover.active_store');
        Route::get('/handover/show/{id}', 'Assets\AssetHandoversController@show')->name('handover.show');
        Route::get('/handover/detail/{id}', 'Assets\AssetHandoversController@detail')->name('handover.detail');
        Route::post('/handover/cancel', 'Assets\AssetHandoversController@cancel')->name('handover.cancel');
        Route::post('/handover/get-asset-html', 'Assets\AssetHandoversController@getAssetHtml')->name('handover.get-asset-html');
        Route::get('/handover/history-structure/{structure_id}', 'Assets\AssetHandoversController@getHistoryStructure')->name('handover.get-history-structure');

        Route::resource('handover', Assets\AssetHandoversController::class);

        Route::get('/history/{id}', 'Assets\AssetHandoverHistoryController@index')->name('assets.history');
        Route::post('/repair/count-asset', 'Assets\AssetAssetsHistoriesController@countAsset');
        Route::post('/repair/update-status', 'Assets\AssetAssetsHistoriesController@updateStatus');
        Route::resource('repair', Assets\AssetAssetsHistoriesController::class);
        Route::get('imports/import-excel', 'Assets\AssetImportsController@getImportExcelForm')->name('imports.get-import');
        Route::post('imports/import-excel', 'Assets\AssetImportsController@postImportExcel')->name('imports.post-import');
        Route::resource('imports', Assets\AssetImportsController::class);
        Route::resource('units', Assets\UnitsController::class);

        Route::get('/reports/export', 'Reports\AssetsReportsController@export')->name('reports.export');
        Route::resource('reports', Reports\AssetsReportsController::class);

    });
    Route::group(['prefix' => 'invest'], function () {
        Route::get('/package/list-contract/{id}', 'Invest\AssetPackagesController@getContractByPackage')->name('package.contract');
        Route::get('/package/add-contract/{id}', 'Invest\AssetPackagesController@addContractByPackage')->name('package.add-contract');
        Route::post('/package/add-contract', 'Invest\AssetPackagesController@addContract')->name('package.add-contract');
        Route::post('/package/delete-contract', 'Invest\AssetPackagesController@deleteContract')->name('package.delete-contract');
        Route::resource('package', Invest\AssetPackagesController::class);
        Route::resource('investment', Invest\InvestmentsController::class);
        Route::resource('InvestImports', Invest\InvestmentImportsController::class);
    });

});
